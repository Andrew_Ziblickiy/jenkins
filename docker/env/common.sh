#!/usr/bin/env bash

applicationEnv=""
importEnvVariables=0

while [ -n "$1" ]
do
	case "$1" in
	#   You can pass env with -e options
	-e) applicationEnv="$2"
		shift;;
    #   You can import env file for modifying this variables using this flag
	--import) importEnvVariables=1;;
	*) echo "$1 is not an option";;
	esac
shift
done

if [ ! -z "$applicationEnv" ]
then
    export APPLICATION_ENVIRONMENT="$applicationEnv"
fi

if [ -z "$APPLICATION_ENVIRONMENT" ]
then
    echo "You must set APPLICATION_ENVIRONMENT env variable"
    exit 1
fi

#   Jenkins Proxy
export MAGE_HTTP_INTERNET_PROXY="http://proxy.dcn.versatel.net:3128"
export MAGE_HTTPS_INTERNET_PROXY="http://proxy.dcn.versatel.net:3128"
export MAGE_HTTP_PROXY="http://t2nl-b2bproxy.dmz.lan:8080"
export MAGE_HTTPS_PROXY="http://t2nl-b2bproxy.dmz.lan:8080"

#   Docker compose
export MAGE_DOCKER_COMPOSE_VERSION="3.4"

#   Docker client bundle
export MAGE_DOCKER_CLIENT_BUNDLE="/var/jenkins_home/client-bundles/dta-dmz-client"

#   Domain names
export MAGE_DIRECTRETAIL_DOMAIN="directretailshop.${APPLICATION_ENVIRONMENT}.tele2.nl"
export MAGE_CALLCENTER_DOMAIN="callcentershop.${APPLICATION_ENVIRONMENT}.tele2.nl"
export MAGE_ADMIN_DOMAIN="admin.${APPLICATION_ENVIRONMENT}.tele2.nl"
export MAGE_ONLINERETAIL_DOMAIN="${APPLICATION_ENVIRONMENT}.tele2.nl"
export MAGE_CDN_DOMAIN="m2cdn.${APPLICATION_ENVIRONMENT}.tele2.nl"
export MAGE_RESELLER_DOMAIN="resellershop.${APPLICATION_ENVIRONMENT}.tele2.nl"

#   Docker stack
export MAGE_DOCKER_STACK_NAME="m2tele2_${APPLICATION_ENVIRONMENT}"
export MAGE_DOCKER_UCP_GROUP_LABEL="/Shared/Development"
export MAGE_DOCKER_UCP_OWNER_LABEL="nlsvc-jenkins-docker"

#   Docker Registry
export MAGE_DOCKER_REGISTRY_HOST="dtrdd.nl.corp.tele2.com:9443"
export MAGE_DOCKER_REGISTRY_PATH="development"
export MAGE_DOCKER_REGISTRY_IMAGE_PREFIX="mag-"

#   Docker service host names
export MAGE_DOCKER_VARNISH_HOST="${MAGE_DOCKER_STACK_NAME}_varnish"
export MAGE_DOCKER_NGINX_HOST="${MAGE_DOCKER_STACK_NAME}_nginx"

#   Docker Images
export MAGE_DOCKER_IMAGE_TAG=""
export MAGE_DOCKER_IMAGE_REDIS="redis:3.2-alpine"
export MAGE_DOCKER_IMAGE_PHP_BASE="m2tele2-base:latest"
export MAGE_DOCKER_IMAGE_PHP="${MAGE_DOCKER_REGISTRY_HOST}/${MAGE_DOCKER_REGISTRY_PATH}/${MAGE_DOCKER_REGISTRY_IMAGE_PREFIX}php${MAGE_DOCKER_IMAGE_TAG}"
export MAGE_DOCKER_IMAGE_TUNER="${MAGE_DOCKER_REGISTRY_HOST}/${MAGE_DOCKER_REGISTRY_PATH}/${MAGE_DOCKER_REGISTRY_IMAGE_PREFIX}tuner${MAGE_DOCKER_IMAGE_TAG}"
export MAGE_DOCKER_IMAGE_NGINX="${MAGE_DOCKER_REGISTRY_HOST}/${MAGE_DOCKER_REGISTRY_PATH}/${MAGE_DOCKER_REGISTRY_IMAGE_PREFIX}nginx${MAGE_DOCKER_IMAGE_TAG}"
export MAGE_DOCKER_IMAGE_VARNISH="${MAGE_DOCKER_REGISTRY_HOST}/${MAGE_DOCKER_REGISTRY_PATH}/${MAGE_DOCKER_REGISTRY_IMAGE_PREFIX}varnish${MAGE_DOCKER_IMAGE_TAG}"

#   Docker Network
export MAGE_DOCKER_NETWORK_NAME="magento_two_${APPLICATION_ENVIRONMENT}"

#   Docker secrets
export MAGE_DOCKER_DB_SECRET="magento_password_${APPLICATION_ENVIRONMENT}"
export MAGE_DOCKER_SECRET_PATH="/run/secrets"

#   Docker stack deploy replicas
export MAGE_DOCKER_NGINX_DEPLOY_REPLICAS=2
export MAGE_DOCKER_VARNISH_DEPLOY_REPLICAS=2
export MAGE_DOCKER_PHP_DEPLOY_REPLICAS=2
export MAGE_DOCKER_ADMIN_DEPLOY_REPLICAS=1
export MAGE_DOCKER_TUNER_DEPLOY_REPLICAS=1

#   Docker stack limitation config
export MAGE_DOCKER_PHP_MEMORY_LIMIT="2000M"
export MAGE_DOCKER_PHP_RESERVATION_MEMORY_LIMIT="500M"
export MAGE_DOCKER_PHP_CPU_LIMIT="2.5"
export MAGE_DOCKER_PHP_RESERVATION_CPU_LIMIT="1"

export MAGE_DOCKER_ADMIN_MEMORY_LIMIT="2000M"
export MAGE_DOCKER_ADMIN_RESERVATION_MEMORY_LIMIT="1000M"
export MAGE_DOCKER_ADMIN_CPU_LIMIT="3"
export MAGE_DOCKER_ADMIN_RESERVATION_CPU_LIMIT="1"

export MAGE_DOCKER_TUNER_MEMORY_LIMIT="500M"
export MAGE_DOCKER_TUNER_RESERVATION_MEMORY_LIMIT="300M"
export MAGE_DOCKER_TUNER_CPU_LIMIT="3"
export MAGE_DOCKER_TUNER_RESERVATION_CPU_LIMIT="0.5"

export MAGE_DOCKER_NGINX_MEMORY_LIMIT="200M"
export MAGE_DOCKER_NGINX_RESERVATION_MEMORY_LIMIT="100M"
export MAGE_DOCKER_NGINX_CPU_LIMIT="2"
export MAGE_DOCKER_NGINX_RESERVATION_CPU_LIMIT="1"

export MAGE_DOCKER_VARNISH_MEMORY_LIMIT="200M"
export MAGE_DOCKER_VARNISH_RESERVATION_MEMORY_LIMIT="50M"
export MAGE_DOCKER_VARNISH_CPU_LIMIT="2"
export MAGE_DOCKER_VARNISH_RESERVATION_CPU_LIMIT="1"

export MAGE_DOCKER_REDIS_MEMORY_LIMIT="100M"
export MAGE_DOCKER_REDIS_RESERVATION_MEMORY_LIMIT="50M"
export MAGE_DOCKER_REDIS_CPU_LIMIT="2"
export MAGE_DOCKER_REDIS_RESERVATION_CPU_LIMIT="1"

if [ $importEnvVariables -eq 1 ]
then
    echo "Trying to include environment [${APPLICATION_ENVIRONMENT}.sh] file if them exists"

    SCRIPT=`realpath $0`
    SCRIPTPATH=`dirname $SCRIPT`

    if [ -f "${SCRIPTPATH}/${APPLICATION_ENVIRONMENT}.sh" ]
    then
        echo "Including ${APPLICATION_ENVIRONMENT}.sh"
        source "${SCRIPTPATH}/${APPLICATION_ENVIRONMENT}.sh"
    fi
fi