#!/usr/bin/env bash

#   Docker Images
export MAGE_DOCKER_IMAGE_MYSQL_BASE="percona"
export MAGE_DOCKER_IMAGE_MYSQL="${MAGE_DOCKER_REGISTRY_HOST}/${MAGE_DOCKER_REGISTRY_PATH}/${MAGE_DOCKER_REGISTRY_IMAGE_PREFIX}mysql:generic"

#   Docker placement Node
export MAGE_DOCKER_MYSQL_NODE_PLACEMENT="nlup-dockerdde08"