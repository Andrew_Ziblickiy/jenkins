return [
    'ENV' : 'prf',

    'MAGE_APPDYNAMICS_APPLICATION_NAME' : 'nl-magento-prf',

    'MAGE_DB_HOST' : 'magentodb.prf.nl.corp.tele2.com',

    //  Domains
    'MAGE_DIRECTRETAIL_DOMAIN' : 'directretailshop.prf.dde.nl.corp.tele2.com',
    'MAGE_ONLINERETAIL_DOMAIN' : 'espresso-prf.tele2.nl',
    'MAGE_CALLCENTERSHOP_DOMAIN' : 'callcentershop.prf.dde.nl.corp.tele2.com',
    'MAGE_RESELLER_DOMAIN' : 'resellershop.prf.dde.nl.corp.tele2.com',
    'MAGE_CDN_DOMAIN' : 'm2cdn-prf.tele2.nl',
    'MAGE_ADMIN_DOMAIN' : 'admin.prf.dde.nl.corp.tele2.com',
    'MAGE_INTERNAL_API_DOMAIN' : 'm2api-prf.tele2.nl',

    //  Docker images
    'DOCKER_IMAGE_APPLICATION_TAG' : 'prf',
    'DOCKER_IMAGE_TUNER_TAG' : 'prf',
    'DOCKER_IMAGE_MAGENTO_TAG' : 'prf',
    'DOCKER_IMAGE_NGINX_TAG' : 'prf',
    'DOCKER_IMAGE_VARNISH_TAG' : 'prf',

    'MAGE_ONLINERETAIL_LABEL_HTTPS' : '-\\ com.docker.ucp.mesh.http.6=external_route=sni://espresso-prf.tele2.nl,internal_port=443',
    'MAGE_ONLINERETAIL_LABEL_HTTP' : '-\\ com.docker.ucp.mesh.http.6=external_route=http://espresso-prf.tele2.nl,internal_port=80',

    'MAGE_DOCKER_PHP_DEPLOY_REPLICAS' : '2',
    'MAGE_DOCKER_VARNISH_DEPLOY_REPLICAS' : '2',
    'MAGE_DOCKER_NGINX_DEPLOY_REPLICAS' : '2',

    'MAGE_STACK_NAME' : 'm2tele2_prf'
]
