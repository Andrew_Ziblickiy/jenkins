return [
    'ENV' : 'neon',

    'MAGE_DB_HOST' : 'mysql',
    'MAGE_DB_DATABASE_MEDIA' : 'magento_media_neon',
    'MAGE_DB_DATABASE' : 'magento_neon',

    'REPOSITORY_BRANCH_APPLICATION' : 'MAG-6476-v2',
    'MAGE_DOCKER_MYSQL_NETWORK' : 'm2tele2_mysql_generic_mysql:',
    'MAGE_DOCKER_MYSQL_NETWORK_PARAMS' : 'external:\\ true',
    'MAGE_DOCKER_MYSQL_NETWORK_SERVICE' : '- m2tele2_mysql_generic_mysql',

    //  Domains
    'MAGE_DIRECTRETAIL_DOMAIN' : 'directretailshop.neon.tele2.nl',
    'MAGE_ONLINERETAIL_DOMAIN' : 'neon.tele2.nl',
    'MAGE_CALLCENTERSHOP_DOMAIN' : 'callcentershop.neon.tele2.nl',
    'MAGE_RESELLER_DOMAIN' : 'resellershop.neon.tele2.nl',
    'MAGE_CDN_DOMAIN' : 'm2cdn.neon.tele2.nl',
    'MAGE_ADMIN_DOMAIN' : 'admin.neon.tele2.nl',
    'MAGE_INTERNAL_API_DOMAIN' : 'm2api.neon.tele2.nl',

    'MAGE_ONLINERETAIL_LABEL_HTTPS' : '-\\ com.docker.ucp.mesh.http.6=external_route=sni://neon.tele2.nl,internal_port=443',
    'MAGE_ONLINERETAIL_LABEL_HTTP' : '-\\ com.docker.ucp.mesh.http.6=external_route=http://neon.tele2.nl,internal_port=80',

    //  Docker images
    'DOCKER_IMAGE_APPLICATION_TAG' : 'neon',
    'DOCKER_IMAGE_TUNER_TAG' : 'neon',
    'DOCKER_IMAGE_NGINX_TAG' : 'neon',
    'DOCKER_IMAGE_VARNISH_TAG' : 'neon',
    'DOCKER_IMAGE_ADMIN_TAG' : 'neon'
]
