return [
    //  Dump File
    'DUMP_FILE' : 'env.sh',

    //  Docker compose
    'DOCKER_COMPOSE_VERSION' : '3.4',

    //  Docker
    'DOCKER_UCPGROUP' : '/Shared/Development',
    'DOCKER_REGISTRY_HOST' : 'dtrdd.nl.corp.tele2.com:9443',
    'DOCKER_REGISTRY_PATH' : 'development',

    //  Docker Images
    'DOCKER_IMAGE_VARNISH_BASE' : 'million12/varnish',
    'DOCKER_IMAGE_NGINX_BASE' : 'nginx:latest',
    'DOCKER_IMAGE_REDIS' : 'redis:3.2-alpine',
    'DOCKER_IMAGE_PHP' : 'php:7.1-fpm',
    'DOCKER_IMAGE_APPLICATION' : '',        //  will be filled in runtime
    'DOCKER_IMAGE_TUNER' : '',              //  will be filled in runtime
    'DOCKER_IMAGE_NGINX' : '',              //  will be filled in runtime
    'DOCKER_IMAGE_ADMIN' : '',              //  will be filled in runtime
    'DOCKER_IMAGE_VARNISH' : '',            //  will be filled in runtime
    'DOCKER_IMAGE_MAGENTO' : '',            //  will be filled in runtime
    'DOCKER_IMAGE_CONTENT_BUILDER' : 'm2tele2-helper:latest',
    'DOCKER_IMAGE_CONTAINER_APPLICATION' : 'm2tele2-container-app:latest',
    'DOCKER_IMAGE_AUTOTESTS' : 'm2tele2-autotests:latest',

    'DOCKER_IMAGE_APPLICATION_NAME' : 'shop',
    'DOCKER_IMAGE_APPLICATION_TAG' : 'latest',
    'DOCKER_IMAGE_ADMIN_NAME' : 'shop',
    'DOCKER_IMAGE_ADMIN_TAG' : 'latest',
    'DOCKER_IMAGE_TUNER_NAME' : 'shop-tuner',
    'DOCKER_IMAGE_TUNER_TAG' : 'latest',
    'DOCKER_IMAGE_MAGENTO_NAME' : 'magento',
    'DOCKER_IMAGE_MAGENTO_TAG' : 'latest',
    'DOCKER_IMAGE_NGINX_NAME' : 'nginx',
    'DOCKER_IMAGE_NGINX_TAG' : 'latest',
    'DOCKER_IMAGE_VARNISH_NAME' : 'varnish',
    'DOCKER_IMAGE_VARNISH_TAG' : 'latest',
    'DOCKER_IMAGE_PREFIX' : 'mag-',
    'DOCKER_DTR_CREDENTIALS' : 'nlsvc-jenkins-docker',

    //  Docker Deploy Default Configuration
    //  Domains
    'MAGE_DIRECTRETAIL_DOMAIN' : 'directretailshop.undef.tele2.nl',
    'MAGE_ONLINERETAIL_DOMAIN' : 'undef.tele2.nl',
    'MAGE_CALLCENTERSHOP_DOMAIN' : 'callcentershop.undef.tele2.nl',
    'MAGE_RESELLER_DOMAIN' : 'resellershop.undef.tele2.nl',
    'MAGE_CDN_DOMAIN' : 'm2cdn.undef.tele2.nl',
    'MAGE_ADMIN_DOMAIN' : 'admin.undef.tele2.nl',
    'MAGE_INTERNAL_API_DOMAIN' : 'm2api.undef.tele2.nl',

    'MAGE_ONLINERETAIL_LABEL_HTTPS' : '',
    'MAGE_ONLINERETAIL_LABEL_HTTP' : '',

    //  Nginx
    'MAGE_DOCKER_NGINX_DEPLOY_REPLICAS' : '1',
    'MAGE_DOCKER_NGINX_RESTART_DELAY' : '5s',
    'MAGE_DOCKER_NGINX_RESTART_ATTEMPTS' : '20',
    'MAGE_DOCKER_NGINX_RESTART_WINDOW' : '60s',

    'MAGE_DOCKER_NGINX_UPDATE_PARALLELISM' : '1',
    'MAGE_DOCKER_NGINX_UPDATE_DELAY' : '10s',
    'MAGE_DOCKER_NGINX_UPDATE_FAILURE_ACTION' : 'pause',
    'MAGE_DOCKER_NGINX_UPDATE_MONITOR' : '5s',
    'MAGE_DOCKER_NGINX_UPDATE_FAILURE_RATIO' : '0',
    'MAGE_DOCKER_NGINX_UPDATE_ORDER' : 'start-first',

    //  PHP
    'MAGE_DOCKER_PHP_DEPLOY_REPLICAS' : '1',
    'MAGE_DOCKER_PHP_RESTART_DELAY' : '5s',
    'MAGE_DOCKER_PHP_RESTART_ATTEMPTS' : '5',
    'MAGE_DOCKER_PHP_RESTART_WINDOW' : '60s',

    'MAGE_DOCKER_PHP_UPDATE_PARALLELISM' : '1',
    'MAGE_DOCKER_PHP_UPDATE_DELAY' : '5s',
    'MAGE_DOCKER_PHP_UPDATE_FAILURE_ACTION' : 'pause',
    'MAGE_DOCKER_PHP_UPDATE_MONITOR' : '5s',
    'MAGE_DOCKER_PHP_UPDATE_FAILURE_RATIO' : '0',
    'MAGE_DOCKER_PHP_UPDATE_ORDER' : 'start-first',

    //  Admin
    'MAGE_DOCKER_ADMIN_DEPLOY_REPLICAS' : '1',
    'MAGE_DOCKER_ADMIN_RESTART_DELAY' : '5s',
    'MAGE_DOCKER_ADMIN_RESTART_ATTEMPTS' : '10',
    'MAGE_DOCKER_ADMIN_RESTART_WINDOW' : '60s',

    'MAGE_DOCKER_ADMIN_UPDATE_PARALLELISM' : '1',
    'MAGE_DOCKER_ADMIN_UPDATE_DELAY' : '10s',
    'MAGE_DOCKER_ADMIN_UPDATE_FAILURE_ACTION' : 'pause',
    'MAGE_DOCKER_ADMIN_UPDATE_MONITOR' : '5s',
    'MAGE_DOCKER_ADMIN_UPDATE_FAILURE_RATIO' : '0',
    'MAGE_DOCKER_ADMIN_UPDATE_ORDER' : 'start-first',

    //  Tuner
    'MAGE_DOCKER_TUNER_DEPLOY_REPLICAS' : '1',
    'MAGE_DOCKER_TUNER_RESTART_DELAY' : '15s',
    'MAGE_DOCKER_TUNER_RESTART_ATTEMPTS' : '10',
    'MAGE_DOCKER_TUNER_RESTART_WINDOW' : '60s',

    'MAGE_DOCKER_TUNER_UPDATE_PARALLELISM' : '1',
    'MAGE_DOCKER_TUNER_UPDATE_DELAY' : '30s',
    'MAGE_DOCKER_TUNER_UPDATE_FAILURE_ACTION' : 'pause',
    'MAGE_DOCKER_TUNER_UPDATE_MONITOR' : '20s',
    'MAGE_DOCKER_TUNER_UPDATE_FAILURE_RATIO' : '0',
    'MAGE_DOCKER_TUNER_UPDATE_ORDER' : 'start-first',

    //  Varnish
    'MAGE_DOCKER_VARNISH_DEPLOY_REPLICAS' : '1',
    'MAGE_DOCKER_VARNISH_RESTART_DELAY' : '5s',
    'MAGE_DOCKER_VARNISH_RESTART_ATTEMPTS' : '20',
    'MAGE_DOCKER_VARNISH_RESTART_WINDOW' : '100s',

    'MAGE_DOCKER_VARNISH_UPDATE_PARALLELISM' : '1',
    'MAGE_DOCKER_VARNISH_UPDATE_DELAY' : '10s',
    'MAGE_DOCKER_VARNISH_UPDATE_FAILURE_ACTION' : 'pause',
    'MAGE_DOCKER_VARNISH_UPDATE_MONITOR' : '20s',
    'MAGE_DOCKER_VARNISH_UPDATE_FAILURE_RATIO' : '0',
    'MAGE_DOCKER_VARNISH_UPDATE_ORDER' : 'start-first',

    //  Magento Database
    'MAGE_DB_DATABASE_MEDIA' : 'magento_media',
    'MAGE_DB_DATABASE' : 'magento',
    'MAGE_DB_USERNAME' : 'magento',
    'MAGE_APP_MODE' : 'production',

    //  Appdynamics
    'MAGE_APPDYNAMICS_NODE_NAME' : '',
    'MAGE_APPDYNAMICS_ACCOUNT_NAME' : '',
    'MAGE_APPDYNAMICS_ACCOUNT_ACCESS_KEY' : '',
    'MAGE_APPDYNAMICS_APPLICATION_NAME' : '',

    'MAGE_APPDYNAMICS_CONTROLLER_HOST' : 'appdcontrol.tele2.com',
    'MAGE_APPDYNAMICS_CONTROLLER_PORT' : '443',
    'MAGE_APPDYNAMICS_TIER_NAME' : 'magento-php',

    //  Proxies
    'HTTP_INTERNET_PROXY' : 'http://proxy.dcn.versatel.net:3128',
    'HTTPS_INTERNET_PROXY' : 'http://proxy.dcn.versatel.net:3128',
    'HTTP_PROXY' : 'http://t2nl-b2bproxy.dmz.lan:8080',
    'HTTPS_PROXY' : 'http://t2nl-b2bproxy.dmz.lan:8080',

    //  Docker Compose Yml Variables
    'MAGE_DOCKER_NETWORK' : 'magento_two',
    'MAGE_DOCKER_HRM_NETWORK' : 'ucp-hrm',
    'MAGE_DOCKER_MYSQL_NETWORK' : '',
    'MAGE_DOCKER_MYSQL_NETWORK_PARAMS' : '',
    'MAGE_DOCKER_MYSQL_NETWORK_SERVICE' : '',
    'MAGE_TELE2_CERT_KEY_NAME' : 'STAR.tele2.nl.key',
    'MAGE_TELE2_CERT_NAME' : 'STAR.tele2.nl.crt',
    'MAGE_SECRET_MOUNT_POINT' : '/run/secrets',

    'SOURCE_FOLDER_TELE2_ENV' : '/env/tele2/app',

    'STATIC_GENERATION_OPTIONS' : '"--configure --install-magento --create-configs --static-deploy  --production --production-static"',

    'GIT_MAGENTO_COMMIT_ID' : '',
    'GIT_APPLICATION_COMMIT_ID' : '',
    'GIT_DOCKER_COMMIT_ID' : '',
    'GIT_FOUNDATION_COMMIT_ID' : ''
]
