return [
    //  Domains
    'MAGE_DIRECTRETAIL_DOMAIN' : 'directretailshop.tele2.local',
    'MAGE_ONLINERETAIL_DOMAIN' : 'shop.tele2.local',
    'MAGE_CALLCENTERSHOP_DOMAIN' : 'callcentershop.tele2.local',
    'MAGE_RESELLER_DOMAIN' : 'resellershop.tele2.local',
    'MAGE_CDN_DOMAIN' : 'shop.tele2.local',
    'MAGE_ADMIN_DOMAIN' : 'admin.tele2.local',
    'MAGE_INTERNAL_API_DOMAIN' : 'm2api.tele2.local'
]
