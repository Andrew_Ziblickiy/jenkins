return [

    'SWARM' : 'dde',

    //  Docker client bundle
    'DOCKER_CLIENT_BUNDLE_UCP_HOST' : 'tcp://dockerdde.nl.corp.tele2.com:8443',
    'DOCKER_CLIENT_BUNDLE_CERT_PATH' : '/var/jenkins_home/client-bundles/dta-dmz-client',

    //  Docker DTR
    'DOCKER_DTR_CREDENTIALS' : 'nlsvc-jenkins-docker',
    'DOCKER_REGISTRY_HOST' : 'dtrdd.nl.corp.tele2.com:9443',

    'MAGE_APPDYNAMICS_ACCOUNT_NAME' : 'test',
    'MAGE_APPDYNAMICS_ACCOUNT_ACCESS_KEY' : '87a38355-03ab-499b-b78f-8d1f389c0876'
]
