return [

    'SWARM' : 'pde',

    //  Docker client bundle
    'DOCKER_CLIENT_BUNDLE_UCP_HOST' : 'tcp://dockerpde.nl.corp.tele2.com:8443',
    'DOCKER_CLIENT_BUNDLE_CERT_PATH' : '/var/jenkins_home/client-bundles/prd-dmz-client',

    //  Docker DTR
    'DOCKER_DTR_CREDENTIALS' : 'nlsvc-jenkins-docker',
    'DOCKER_DTR_REGISTRY_HOST' : 'dtrpd.nl.corp.tele2.com:9443',
    'DOCKER_REGISTRY_HOST' : 'dtrpd.nl.corp.tele2.com:9443',

    //  Appdynamics
    'MAGE_APPDYNAMICS_ACCOUNT_NAME' : 'customer1',
    'MAGE_APPDYNAMICS_ACCOUNT_ACCESS_KEY' : 'c459528b-3308-44a1-91e9-b8843b6fe8ef',
]
