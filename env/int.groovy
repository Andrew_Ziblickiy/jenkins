return [
    'ENV' : 'int',

    'MAGE_APPDYNAMICS_APPLICATION_NAME' : 'nl-magento-int',

    'MAGE_DB_HOST' : 'magentodb.int.nl.corp.tele2.com',

    //  Domains
    'MAGE_DIRECTRETAIL_DOMAIN' : 'directretailshop.int.dde.nl.corp.tele2.com',
    'MAGE_ONLINERETAIL_DOMAIN' : 'espresso-sit.tele2.nl',
    'MAGE_CALLCENTERSHOP_DOMAIN' : 'callcentershop.int.dde.nl.corp.tele2.com',
    'MAGE_RESELLER_DOMAIN' : 'resellershop.int.dde.nl.corp.tele2.com',
    'MAGE_CDN_DOMAIN' : 'm2cdn-int.tele2.nl',
    'MAGE_ADMIN_DOMAIN' : 'admin.int.dde.nl.corp.tele2.com',
    'MAGE_INTERNAL_API_DOMAIN' : 'm2api-int.tele2.nl',

    //  Docker images
    'DOCKER_IMAGE_APPLICATION_TAG' : 'int',
    'DOCKER_IMAGE_TUNER_TAG' : 'int',
    'DOCKER_IMAGE_MAGENTO_TAG' : 'int',
    'DOCKER_IMAGE_NGINX_TAG' : 'int',
    'DOCKER_IMAGE_VARNISH_TAG' : 'int',

    'MAGE_DOCKER_PHP_DEPLOY_REPLICAS' : '2',
    'MAGE_DOCKER_VARNISH_DEPLOY_REPLICAS' : '2',
    'MAGE_DOCKER_NGINX_DEPLOY_REPLICAS' : '2',

    'MAGE_STACK_NAME' : 'm2tele2_int'
]
