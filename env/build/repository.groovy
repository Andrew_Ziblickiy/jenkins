return [
    'REPOSITORY_MAGENTO' : "https://bitbucket.nl.corp.tele2.com/scm/mtel/tel2-m2-ee.git",
    'REPOSITORY_APPLICATION' : "https://bitbucket.nl.corp.tele2.com/scm/mtel/tel2-m2-app.git",
    'REPOSITORY_DOCKER' : "https://bitbucket.nl.corp.tele2.com/scm/mtel/tel2-docker-env.git",
    'REPOSITORY_FOUNDATION' : "https://bitbucket.nl.corp.tele2.com/scm/mtel/tel2-m2-foundation.git",
    'REPOSITORY_CONFIG' : "https://bitbucket.nl.corp.tele2.com/scm/mtel/tel2-m2-config.git",
    'REPOSITORY_MEDIA' : "https://bitbucket.nl.corp.tele2.com/scm/mtel/tel2-m2-media.git",
    'REPOSITORY_STASH_API' : 'https://bitbucket.nl.corp.tele2.com/scm/mtel/jenkins-stash-integration.git',
    'REPOSITORY_AUTOTESTS' : 'https://bitbucket.nl.corp.tele2.com/scm/mtel/tel2-autotests.git',

    'REPOSITORY_BRANCH_MAGENTO' : 'develop',
    'REPOSITORY_BRANCH_APPLICATION' : 'master',
    'REPOSITORY_BRANCH_DOCKER' : 'master',
    'REPOSITORY_BRANCH_CONFIG' : 'master',
    'REPOSITORY_BRANCH_FOUNDATION' : 'v.1.3',
    'REPOSITORY_BRANCH_STASH_API' : 'master',
    'REPOSITORY_BRANCH_AUTOTESTS' : 'develop',

    'REPOSITORY_CREDENTIALS' : "nlsvc-jenkins-docker"
]
