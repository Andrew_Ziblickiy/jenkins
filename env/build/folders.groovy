return [
    'SOURCE_MAGENTO' : 'magento',
    'SOURCE_APPLICATION' : 'application',
    'SOURCE_DOCKER' : 'docker',
    'SOURCE_CONFIG' : 'config',
    'SOURCE_FOUNDATION' : 'foundation',
    'SOURCE_MEDIA' : 'media',
    'SOURCE_STASH_API' : 'stash_api',
    'SOURCE_AUTOTESTS' : 'autotests',

    'GENERATED' : 'generated',
    'GENERATED_STATIC' : 'generated/static',
    'GENERATED_VAR' : 'generated/var',
    'GENERATED_MEDIA' : 'generated/media',
    'GENERATED_NODE_MODULES' : 'generated/node_modules',

    'BUILD_SOURCE' : 'build/src',
    'LOGS' : 'logs'
]
