return [
    'ENV' : 'uat',

    'MAGE_APPDYNAMICS_APPLICATION_NAME' : 'nl-magento-uat',

    'MAGE_DB_HOST' : 'magentodb.uat.nl.corp.tele2.com',

    //  Domains
    'MAGE_DIRECTRETAIL_DOMAIN' : 'directretailshop.uat.dde.nl.corp.tele2.com',
    'MAGE_ONLINERETAIL_DOMAIN' : 'espresso-uat.tele2.nl',
    'MAGE_CALLCENTERSHOP_DOMAIN' : 'callcentershop.uat.dde.nl.corp.tele2.com',
    'MAGE_RESELLER_DOMAIN' : 'resellershop.uat.dde.nl.corp.tele2.com',
    'MAGE_CDN_DOMAIN' : 'm2cdn-uat.tele2.nl',
    'MAGE_ADMIN_DOMAIN' : 'admin.uat.dde.nl.corp.tele2.com',
    'MAGE_INTERNAL_API_DOMAIN' : 'm2api-uat.tele2.nl',

    //  Docker images
    'DOCKER_IMAGE_APPLICATION_TAG' : 'uat',
    'DOCKER_IMAGE_TUNER_TAG' : 'uat',
    'DOCKER_IMAGE_MAGENTO_TAG' : 'uat',
    'DOCKER_IMAGE_NGINX_TAG' : 'uat',
    'DOCKER_IMAGE_VARNISH_TAG' : 'uat',

    'MAGE_DOCKER_PHP_DEPLOY_REPLICAS' : '2',
    'MAGE_DOCKER_VARNISH_DEPLOY_REPLICAS' : '2',
    'MAGE_DOCKER_NGINX_DEPLOY_REPLICAS' : '2',

    'MAGE_STACK_NAME' : 'm2tele2_uat'
]
