return [
    'ENV' : 'dev',

    'MAGE_APPDYNAMICS_APPLICATION_NAME' : 'nl-magento-dev',

    'MAGE_DB_HOST' : 'magentodb.dev.nl.corp.tele2.com',

    //  Domains
    'MAGE_DIRECTRETAIL_DOMAIN' : 'directretailshop.dev.dde.nl.corp.tele2.com',
    'MAGE_ONLINERETAIL_DOMAIN' : 'espresso-dev.tele2.nl',
    'MAGE_CALLCENTERSHOP_DOMAIN' : 'callcentershop.dev.dde.nl.corp.tele2.com',
    'MAGE_RESELLER_DOMAIN' : 'resellershop.dev.dde.nl.corp.tele2.com',
    'MAGE_CDN_DOMAIN' : 'm2cdn-dev.tele2.nl',
    'MAGE_ADMIN_DOMAIN' : 'admin.dev.dde.nl.corp.tele2.com',
    'MAGE_INTERNAL_API_DOMAIN' : 'm2api-dev.tele2.nl',

    'MAGE_STACK_NAME' : 'm2tele2_dev'
]
