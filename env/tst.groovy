return [
    'ENV' : 'tst',

    'MAGE_DB_HOST' : 'mysql',
    'MAGE_DB_DATABASE_MEDIA' : 'magento_media_tst',
    'MAGE_DB_DATABASE' : 'magento_tst',

    'REPOSITORY_BRANCH_APPLICATION' : '18S15',
    'MAGE_DOCKER_MYSQL_NETWORK' : 'm2tele2_mysql_generic_mysql:',
    'MAGE_DOCKER_MYSQL_NETWORK_PARAMS' : 'external:\\ true',
    'MAGE_DOCKER_MYSQL_NETWORK_SERVICE' : '- m2tele2_mysql_generic_mysql',

    //  Domains
    'MAGE_DIRECTRETAIL_DOMAIN' : 'directretailshop.tst.tele2.nl',
    'MAGE_ONLINERETAIL_DOMAIN' : 'tst.tele2.nl',
    'MAGE_CALLCENTERSHOP_DOMAIN' : 'callcentershop.tst.tele2.nl',
    'MAGE_RESELLER_DOMAIN' : 'resellershop.tst.tele2.nl',
    'MAGE_CDN_DOMAIN' : 'm2cdn-tst.tele2.nl',
    'MAGE_ADMIN_DOMAIN' : 'admin.tst.tele2.nl',
    'MAGE_INTERNAL_API_DOMAIN' : 'm2api-tst.tele2.nl',

    'MAGE_ONLINERETAIL_LABEL_HTTPS' : '-\\ com.docker.ucp.mesh.http.6=external_route=sni://tst.tele2.nl,internal_port=443',
    'MAGE_ONLINERETAIL_LABEL_HTTP' : '-\\ com.docker.ucp.mesh.http.6=external_route=http://tst.tele2.nl,internal_port=80',

    //  Docker images
    'DOCKER_IMAGE_APPLICATION_TAG' : 'tst',
    'DOCKER_IMAGE_TUNER_TAG' : 'tst',
    'DOCKER_IMAGE_NGINX_TAG' : 'tst',
    'DOCKER_IMAGE_VARNISH_TAG' : 'tst',
    'DOCKER_IMAGE_ADMIN_TAG' : 'tst'
]
