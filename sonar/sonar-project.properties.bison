sonar.projectKey=m2tele2_bison
sonar.projectName=m2tele2-bison
sonar.language=php
sonar.sourceEncoding=UTF-8
sonar.projectVersion=1.0
sonar.sources=${dir.src}/app/code
sonar.exclusions=${dir.src}/app/code/*/*/Test/**/*.php,${dir.src}/app/code/*/*/registration.php

sonar.scm.disabled=true

sonar.php.tests.reportPath=output/phpunit/log/phpunit.junit.xml
sonar.php.coverage.reportPath=output/phpunit/coverage/phpunit.clover.xml
sonar.php.coverage.overallReportPath=output/phpunit/coverage/phpunit.clover.xml
