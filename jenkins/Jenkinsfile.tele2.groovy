class Repository {
	final static MAGENTO = "http://t2nl-devtooling.itservices.lan:7990/scm/mtel/tel2-m2-ee.git"
	final static APPLICATION = "http://t2nl-devtooling.itservices.lan:7990/scm/mtel/tel2-m2-app.git"
	final static DOCKER = "http://t2nl-devtooling.itservices.lan:7990/scm/mtel/tel2-docker-env.git"
	final static API = "http://t2nl-devtooling.itservices.lan:7990/scm/mtel/tel2-test-runner.git"
	final static FOUNDATION = "http://t2nl-devtooling.itservices.lan:7990/scm/mtel/tel2-m2-foundation.git"
	final static CONFIG = "http://t2nl-devtooling.itservices.lan:7990/scm/mtel/tel2-m2-config.git"
	final static MEDIA = "http://t2nl-devtooling.itservices.lan:7990/scm/mtel/tel2-m2-media.git"

	static BRANCH_MAGENTO = 'develop'
	static BRANCH_DOCKER = 'master-reseller'
	static BRANCH_API = 'master'
	static BRANCH_CONFIG = 'master'
	static BRANCH_FOUNDATION = 'v1.1'

	final static CREDENTIALS = "nlsvc-jenkins-docker"

	static void pull(rep, branch, folder) {
		Context.script.checkout changelog: false, poll: false, scm: [$class: 'GitSCM', branches: [[name: branch]], browser: [$class: 'Stash', repoUrl: ''], doGenerateSubmoduleConfigurations: false, extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: folder]], submoduleCfg: [], userRemoteConfigs: [[credentialsId: CREDENTIALS, url: rep]]]
	}

	static void pullWithClean(script, rep, branch, folder) {
		script.checkout changelog: false, poll: false, scm: [$class: 'GitSCM', branches: [[name: branch]], browser: [$class: 'Stash', repoUrl: ''], doGenerateSubmoduleConfigurations: false, extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: folder], [$class: 'CleanBeforeCheckout']], submoduleCfg: [], userRemoteConfigs: [[credentialsId: CREDENTIALS, url: rep]]]
	}
}

class SourceFolder {
	final static MAGENTO = "magento"
    final static APPLICATION = "application"
    final static DOCKER = "docker"
    final static API = "api"
    final static CONFIG = "config"
    final static FOUNDATION = "foundation"
    final static GENERATED = "generated"
    final static GENERATED_STATIC = "generated/static"
    final static GENERATED_VAR = "generated/var"
    final static GENERATED_MEDIA = "generated/media"
    final static CONTENT_BUILDER = "docker/docker/content-builder"
    final static SRC = "build/src"
    final static LOGS = "logs"
}

class Jenkins {
	static SERVICE_ACCOUNT = "nlsvc-jenkins-docker"
}

class Context {
	static script;
}

class Docker {

	static REGISTRY_HOST = "dtrdd.nl.corp.tele2.com:9443"
	static REGISTRY_PATH = "development"
	static SWARM_HOST = "dde.nl.corp.tele2.com"
	static STACK_NAME = "m2tele2"
	static CONFIG_FOLDER = "tele2_dev"
	static CLIENT_BUNDLE_ENV_VARS = ""
	static DOCKER_CERT_PATH = "/var/jenkins_home/client-bundles/prd-dmz-client"
	static LOCAL_ENVS = ""

	private static conf = [:]
	private static deployEnvs = [:]

	class Ucp {
	    static HOST = "tcp://dockerpde.nl.corp.tele2.com:8443"
	    static OWNER = "nlsvc-jenkins-docker"
	    static GROUP = "/Shared/Development"
	}

	class Image {

	    private static images = [:]

	    static PREFIX = "mag-"

	    static BASE_NAME = "m2tele2-base"
	    static BASE_TAG = "latest"

	    static APP_NAME = "shop"
	    static APP_TAG = "latest"

	    static NGINX_NAME = "nginx"
	    static NGINX_TAG = "latest"

	    static VARNISH_NAME = "varnish"
        static VARNISH_TAG = "latest"

        static CONTENT_BUILDER_NAME = "m2tele2-static-generator"
        static CONTENT_BUILDER_TAG = "1.0"

		static boolean exists(name) {
			def status = Context.script.sh returnStatus: true, script: "docker inspect ${name}"
			return status == 0
		}

		static buildAndPublishReport(script, String command, String reportTitle) {
		    def num = new Random().nextInt()
		    def file = "${SourceFolder.LOGS}/docker-build-${num}.log"

		    script.sh "touch ${file}"
		    def status = script.sh returnStatus: true, script: "2>&1 1>>${file} ${Docker.LOCAL_ENVS} ${command} 2>&1 1>>${file}"

		    script.publishHTML(
                [
                    allowMissing: true,
                    alwaysLinkToLastBuild: false,
                    keepAll: true,
                    reportDir: "${SourceFolder.LOGS}",
                    reportFiles: "docker-build-${num}.log",
                    reportName: "${reportTitle}",
                    reportTitles: ''
                ]
            )

            //  Remove log file
            script.sh "rm -f ${file}"

            if (status != 0) {
                error "Error when building image\n${command}"
            }
		}

		static add(String name, String alias) {
		    images[alias] = name;
		}

		static get(String alias, String env = null) {
		    def img = images[alias]

			if (env) {
				def name = "${alias}-${env}"
				if (images[name]) {
					img = images[name]
				}
			}

			return img
		}

		static has(String alias) {
			return images.containsKey(alias);
		}

		static tag(script, String alias, String name) {
			def img = get(alias)
			//	set status non zero because of image could not be there
			def status = 1

			if (img) {
				status = script.sh returnStatus: true, script: "docker image tag ${img} ${name}"
			}

			return status
		}

		static pushAll(script, String env = null) {
		    def file = "${SourceFolder.LOGS}/docker-push-${env}.log"
		    def status = 0
		    def imageName = ""
		    script.sh "touch ${file} && truncate ${file} -s 0"

			Docker.login(script, env)

		    for (image in Docker.Image.images.values()) {
		        script.echo "Pushing image: ${image}"
		        script.sh "echo \"Pushing image: ${image}\n\" >> ${file}"
                status = script.sh returnStatus: true, script: "2>&1 1>>${file} ${Docker.LOCAL_ENVS} docker image push ${image} 2>&1 1>>${file}"

                if (status != 0) {
                    script.sh "echo \"Exited with code: ${status}\n\" >> ${file}"
                    script.publishHTML(
                        [
                            allowMissing: true,
                            alwaysLinkToLastBuild: false,
                            keepAll: true,
                            reportDir: "${SourceFolder.LOGS}",
                            reportFiles: "docker-push.log",
                            reportName: "Push Images Log",
                            reportTitles: ''
                        ]
                    )
					Docker.logout(script, env)
                    error "Could not push image ${image}"
                }
		    }

			Docker.logout(script, env)

		    script.publishHTML(
                [
                    allowMissing: true,
                    alwaysLinkToLastBuild: false,
                    keepAll: true,
                    reportDir: "${SourceFolder.LOGS}",
                    reportFiles: "docker-push.log",
                    reportName: "Push Images ${env} Log",
                    reportTitles: ''
                ]
            )
		}
	}

	static addEnvConfig(String env, String registryHost, String swarmHost, String ucpHost, String certPath, phpReplicas = 1) {
		conf[env] = ["REGISTRY_HOST": registryHost, 'SWARM_HOST' : swarmHost, 'UCP_HOST' : ucpHost, 'CERT_PATH' : certPath, 'PHP_DEPLOY_REPLICAS' : phpReplicas]
	}

	static getConfig(String env) {
		if (conf.containsKey(env)) {
			return conf[env]
		}
		return null
	}

	static deploy(script, String stackName, String env) {
	    def ymlFolder = "${SourceFolder.DOCKER}/docker/${Docker.CONFIG_FOLDER}"
		def clientBundle = Docker.CLIENT_BUNDLE_ENV_VARS;
		def ucpOwner = Ucp.OWNER
		def ucpGroup = Ucp.GROUP
		def swarmHost = SWARM_HOST
		def parsedYaml = "${ymlFolder}/${stackName}-${env}-docker-compose.yml"
		def phpReplicas = 1
		def mageMode = "default"

		if (conf.containsKey(env)) {
			def config = Docker.getConfig(env)
			clientBundle = "DOCKER_HOST=${config.UCP_HOST} DOCKER_TLS_VERIFY=1 DOCKER_CERT_PATH=${config.CERT_PATH}"
			swarmHost = config.SWARM_HOST;
			phpReplicas = config.PHP_DEPLOY_REPLICAS;
		}

		//	should be rewrited
		switch(env) {
			case "prf":
				mageMode = "production"
			break
			case "dev":
				mageMode = "production"
			break
			default:

			break
		}

		def cfg = Appdynamics.getConfig(env)
		def serverName = Application.getApplicationDomain(env)
		def buildArgs = ""

		script.echo "Appdynamics Server Name: ${serverName}"

		if (cfg) {
			buildArgs = "--build-arg ACCOUNT_NAME=${cfg.ACCOUNT_NAME}"
			buildArgs = "${buildArgs} --build-arg ACCOUNT_ACCESS_KEY=${cfg.ACCOUNT_ACCESS_KEY}"
			buildArgs = "${buildArgs} --build-arg CONTROLLER_HOST=${cfg.CONTROLLER_HOST}"
			buildArgs = "${buildArgs} --build-arg CONTROLLER_PORT=${cfg.CONTROLLER_PORT}"
			buildArgs = "${buildArgs} --build-arg APPLICATION_NAME=${cfg.APPLICATION_NAME}"
			buildArgs = "${buildArgs} --build-arg SERVER_NAME=${serverName}"
			buildArgs = "${buildArgs} --build-arg TIER_NAME=${cfg.TIER_NAME}"
		}

        def yaml = script.readFile "${ymlFolder}/docker-compose.yml"
        yaml = yaml.replaceAll("#NGINX_IMAGE_NAME#", Image.get('nginx', env))
        yaml = yaml.replaceAll("#PHP_IMAGE_NAME#", Image.get('app', env))
        yaml = yaml.replaceAll("#VARNISH_IMAGE_NAME#", Image.get('varnish'))
        yaml = yaml.replaceAll("#TUNER_IMAGE_NAME#", Image.get('tuner'))
        yaml = yaml.replaceAll("#ENV#", env)
		//	Appdynamics
		yaml = yaml.replaceAll("#APPDYNAMICS_TIER_NAME#", cfg.TIER_NAME)
		yaml = yaml.replaceAll("#APPDYNAMICS_NODE_NAME#", serverName)
		yaml = yaml.replaceAll("#APPDYNAMICS_ACCOUNT_NAME#", cfg.ACCOUNT_NAME)
		yaml = yaml.replaceAll("#APPDYNAMICS_ACCOUNT_ACCESS_KEY#", cfg.ACCOUNT_ACCESS_KEY)
		//	Appdynamics END
		yaml = yaml.replaceAll("#DB_HOST#", "magentodb.${env}.nl.corp.tele2.com")
        yaml = yaml.replaceAll("#UCPOWNER#", ucpOwner)
        yaml = yaml.replaceAll("#UCPGROUP#", ucpGroup)
		yaml = yaml.replaceAll("#PHP_DEPLOY_REPLICAS#", "${phpReplicas}")
		yaml = yaml.replaceAll("#MAGE_MODE#", "${mageMode}")

        yaml = yaml.replaceAll("#HOST_1#", "http://directretailshop.${env}.${swarmHost}")
        yaml = yaml.replaceAll("#HOST_2#", "http://shop.${env}.${swarmHost}")
        yaml = yaml.replaceAll("#HOST_3#", "http://espresso-${proxyEnv}.tele2.nl")
        yaml = yaml.replaceAll("#HOST_4#", "http://admin.${env}.${swarmHost}")
        yaml = yaml.replaceAll("#HOST_5#", "http://m2cdn.${env}.${swarmHost}")

        yaml = yaml.replaceAll("#HOST_HTTPS_1#", "sni://directretailshop.${env}.${swarmHost}")
        yaml = yaml.replaceAll("#HOST_HTTPS_2#", "sni://shop.${env}.${swarmHost}")
        yaml = yaml.replaceAll("#HOST_HTTPS_3#", "sni://espresso-${proxyEnv}.tele2.nl")
        yaml = yaml.replaceAll("#HOST_HTTPS_4#", "sni://admin.${env}.${swarmHost}")
        yaml = yaml.replaceAll("#HOST_HTTPS_5#", "sni://m2cdn.${env}.${swarmHost}")

        yaml = yaml.replaceAll("#HOST#", "http://shop.${env}.${swarmHost}")
		yaml = yaml.replaceAll("#APPLICATION_DOMAIN#", "shop.${env}.${swarmHost}")
        yaml = yaml.replaceAll("#DBPREFIX#", "magento")
        script.writeFile file: parsedYaml , text: yaml

        def cmd = "${clientBundle} docker stack deploy -c ${parsedYaml} ${stackName}_${env}"
        //script.echo "${cmd}"
        script.sh "${cmd}"
	}

	static addDeployEnv(String env) {
		deployEnvs[env] = true
	}

	static canDeploy(String env) {
		def exists = deployEnvs.containsKey(env)

		return exists
	}

	static login(script, String env = null) {
		script.echo "Trying to login"
		def registryHost = getRegistryHost(env)

		script.withCredentials([script.usernamePassword(credentialsId: "${Jenkins.SERVICE_ACCOUNT}", usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
	        script.sh "docker login -u ${script.USERNAME} -p ${script.PASSWORD} ${registryHost}"
	    }
	}

	static logout(script, String env = null) {
		def registryHost = Docker.getRegistryHost(env)

		script.sh "docker logout ${registryHost}"
	}

	static getRegistryHost(String env = null) {
		if (env) {
			def envConfig = conf[env]
			if (envConfig) {
				return envConfig.REGISTRY_HOST
			}
		}
		return Docker.REGISTRY_HOST
	}
}

class Phing {

    static VERSION = "2.15.2"
	static CODE_COVERAGE_LEVEL = 30

    public static init()
    {
        Context.script.sh "cp -Rf -t ./ ${SourceFolder.CONFIG}/phing/*"

        Phing.execute("init-env -Denv=jenkins -Dsrc=\$(realpath ${SourceFolder.SRC}) -Ddir.jenkins.m2app=${SourceFolder.APPLICATION} -Ddir.jenkins.m2ee=${SourceFolder.MAGENTO} -Ddir.jenkins.m2docker=${SourceFolder.DOCKER} -Ddir.jenkins.m2tr=${SourceFolder.API} -Ddir.jenkins.m2foundation=${SourceFolder.FOUNDATION}")
        Phing.execute("make-phpunit-config")
    }

    public static execute(String command)
    {
        Context.script.sh "php phing-${Phing.VERSION}.phar ${command}"
    }
}

class Application {

	static DOWNLOAD_MEDIA_PATH = "/deploy_helpers/media/download"
	static STATUS_PATH = "/status.php"

	static getApplicationDomain(String env) {
		def swarmHost = Docker.SWARM_HOST
		def conf = Docker.getConfig(env)

		if (conf) {
			swarmHost = conf.SWARM_HOST;
		}
		def host = "shop.${env}.${swarmHost}"

		return host
	}

	static getApplicationHost(String env) {
		def domain = getApplicationDomain(env)
		def host = "http://${domain}"

		return host
	}

	static getMediaDownloadLink(String env) {
		def host = getApplicationHost(env)

		return "${host}${DOWNLOAD_MEDIA_PATH}"
	}

	static isApplicationUp(script, String env) {
		def host = getApplicationHost(env)
		def checkUrl = "${host}${STATUS_PATH}"
		def status

		try {
			status = script.sh returnStdout: true, script: "curl --connect-timeout 30  ${checkUrl}"
		} catch (e) {
			return false
		}

		if (status == 'OK') {
			return true
		} else {
			return false
		}
	}
}

class Appdynamics {

	private static config = [:]

	static configureEnv(String env, Map cfg) {
		config[env] = cfg
	}

	static getConfig(String env) {
		def cfg = config[env]
		return cfg
	}
}

class Sonar
{
	static BIN = "/var/jenkins_home/tools/hudson.plugins.sonar.SonarRunnerInstallation/SonarQube_Scanner/bin/sonar-scanner"
	static HOST = "http://sonar.intranet.itservices.lan:9000/"

	public static scan(script, String propertiesFile, String projectRoot)
	{
		script.withSonarQubeEnv('Tele2 Sonar') {
            def status = script.sh returnStatus: true, script: "${BIN} -Dsonar.projectBaseDir=${projectRoot} -Dproject.settings=${propertiesFile} -Dsonar.host.url=${HOST}"
        }
	}
}

Docker.CLIENT_BUNDLE_ENV_VARS = "DOCKER_HOST=${Docker.Ucp.HOST} DOCKER_TLS_VERIFY=1 DOCKER_CERT_PATH=${Docker.DOCKER_CERT_PATH}"
Docker.addEnvConfig(
	'prd',
	'dtrpd.nl.corp.tele2.com:9443',
	'pde.nl.corp.tele2.com',
	'tcp://dockerpde.nl.corp.tele2.com:8443',
	'/var/jenkins_home/client-bundles/prd-dmz-client',
	2
)

Docker.addEnvConfig(
	'dev',
	'dtrdd.nl.corp.tele2.com:9443',
	'dde.nl.corp.tele2.com',
	'tcp://dockerdde.nl.corp.tele2.com:8443',
	'/var/jenkins_home/client-bundles/dta-dmz-client'
)

Docker.addEnvConfig(
	'prf',
	'dtrdd.nl.corp.tele2.com:9443',
	'dde.nl.corp.tele2.com',
	'tcp://dockerdde.nl.corp.tele2.com:8443',
	'/var/jenkins_home/client-bundles/dta-dmz-client',
	2
)

Docker.addEnvConfig(
	'int',
	'dtrdd.nl.corp.tele2.com:9443',
	'dde.nl.corp.tele2.com',
	'tcp://dockerdde.nl.corp.tele2.com:8443',
	'/var/jenkins_home/client-bundles/dta-dmz-client',
	2
)

Docker.addEnvConfig(
	'uat',
	'dtrdd.nl.corp.tele2.com:9443',
	'dde.nl.corp.tele2.com',
	'tcp://dockerdde.nl.corp.tele2.com:8443',
	'/var/jenkins_home/client-bundles/dta-dmz-client',
	2
)

Appdynamics.configureEnv(
	"dev",
	[
		"CONTROLLER_HOST" : "appdcontrol.tele2.com",
		"CONTROLLER_PORT" : 443,
		"APPLICATION_NAME" : "Magento2",
		"TIER_NAME" : "Magento-DEV",
		"ACCOUNT_NAME" : "test",
		"ACCOUNT_ACCESS_KEY" : "91f57f69-8566-4e65-839a-7202a734470c"
	]
)

Appdynamics.configureEnv(
	"prf",
	[
		"CONTROLLER_HOST" : "appdcontrol.tele2.com",
		"CONTROLLER_PORT" : 443,
		"APPLICATION_NAME" : "Magento2",
		"TIER_NAME" : "Magento-PRF",
		"ACCOUNT_NAME" : "test",
		"ACCOUNT_ACCESS_KEY" : "91f57f69-8566-4e65-839a-7202a734470c"
	]
)

Appdynamics.configureEnv(
	"int",
	[
		"CONTROLLER_HOST" : "appdcontrol.tele2.com",
		"CONTROLLER_PORT" : 443,
		"APPLICATION_NAME" : "Magento2",
		"TIER_NAME" : "Magento-INT",
		"ACCOUNT_NAME" : "test",
		"ACCOUNT_ACCESS_KEY" : "91f57f69-8566-4e65-839a-7202a734470c"
	]
)

Appdynamics.configureEnv(
	"uat",
	[
		"CONTROLLER_HOST" : "appdcontrol.tele2.com",
		"CONTROLLER_PORT" : 443,
		"APPLICATION_NAME" : "Magento2",
		"TIER_NAME" : "Dev",
		"ACCOUNT_NAME" : "test",
		"ACCOUNT_ACCESS_KEY" : "91f57f69-8566-4e65-839a-7202a734470c"
	]
)

Appdynamics.configureEnv(
	"prd",
	[
		"CONTROLLER_HOST" : "appdcontrol.tele2.com",
		"CONTROLLER_PORT" : 443,
		"APPLICATION_NAME" : "Magento2",
		"TIER_NAME" : "Dev",
		"ACCOUNT_NAME" : "test",
		"ACCOUNT_ACCESS_KEY" : "91f57f69-8566-4e65-839a-7202a734470c"
	]
)

def buildDockerMagentoImage() {

	def magentoDockerFile = "${SourceFolder.DOCKER}/docker/base/MagentoDockerfile"
	def magentoDockerFileExists = fileExists "${magentoDockerFile}"

	if (magentoDockerFileExists) {
        commitFile = "${SourceFolder.LOGS}/base-image-commit.cache"
	    def commit = ''
	    def exists = fileExists "${commitFile}"

		//  check if commit file exists
        if (exists) {
            commit = readFile "${commitFile}"
        }

        isImageExists = sh returnStatus: true, script: "1>/dev/null 2>&1 ${Docker.LOCAL_ENVS} docker inspect ${Docker.Image.BASE_NAME}:${Docker.Image.BASE_TAG}"
        currentCommit = sh returnStdout: true, script: "cd ${SourceFolder.MAGENTO} && git rev-parse --short HEAD"

        if (currentCommit != commit || isImageExists != 0) {

			echo "Build magento image"

            //  Copy Docker file to magento folder
            sh "cp -f ${magentoDockerFile} ${SourceFolder.MAGENTO}"

            logfile = "${SourceFolder.LOGS}/base-image-build.log"
            sh "touch ${logfile} && truncate ${logfile} -s 0"

            //  we need to build magento image
            status = sh returnStatus: true, script: "2>&1 1>>${logfile} ${Docker.LOCAL_ENVS} docker build --build-arg https_proxy=http://proxy.dcn.versatel.net:3128 --build-arg http_proxy=http://proxy.dcn.versatel.net:3128 -f ${SourceFolder.MAGENTO}/MagentoDockerfile -t ${Docker.Image.BASE_NAME}:${Docker.Image.BASE_TAG} ${SourceFolder.MAGENTO} 2>&1 1>>${logfile}"

            publishHTML(
                [
                    allowMissing: true,
                    alwaysLinkToLastBuild: false,
                    keepAll: true,
                    reportDir: "${SourceFolder.LOGS}",
                    reportFiles: 'base-image-build.log',
                    reportName: 'Base Image Build Log',
                    reportTitles: ''
                ]
            )

            if (status != 0) {
                error "Errors occurred while building Base Image, see Base Image Build Log for details"
            }

            //  save latest commit id
            writeFile file: commitFile, text: currentCommit

            if (currentCommit != commit) {
                publishHTML(
                    [
                        allowMissing: true,
                        alwaysLinkToLastBuild: true,
                        keepAll: true,
                        reportDir: "${SourceFolder.LOGS}",
                        reportFiles: 'base-image-commit.cache',
                        reportName: 'Base Image Commit',
                        reportTitles: ''
                    ]
                )
            }
        } else {
            echo "It seems like magento image has the latest code"
        }
	}
}

def buildApplicationImage() {

	//  Copy necessary staff
	sh "mkdir -p ${SourceFolder.APPLICATION}/docker/php-fpm ${SourceFolder.APPLICATION}/container-api"
	sh "cp -Rf ${SourceFolder.DOCKER}/docker/${Docker.CONFIG_FOLDER}/php-fpm/* ${SourceFolder.APPLICATION}/docker/php-fpm"
	sh "cp -Rf ${SourceFolder.API}/src/* ${SourceFolder.APPLICATION}/container-api"

	//  Prepare Dockerfile
	def applicationDockerFile = "${SourceFolder.APPLICATION}/docker/php-fpm/Dockerfile"
	def dockerfile = readFile applicationDockerFile
    dockerfile = dockerfile.replaceAll("#MagentoImage#", "${Docker.Image.BASE_NAME}:${Docker.Image.BASE_TAG}")
    writeFile file: applicationDockerFile , text: dockerfile

	//  Copy foundation to the application dir
	sh "rm -Rf ${SourceFolder.APPLICATION}/app/code/Alpha"
    sh "cp -Rf ${SourceFolder.FOUNDATION}/app/code/* ${SourceFolder.APPLICATION}/app/code"

    //  Copy generated content
    sh "mkdir -p ${SourceFolder.APPLICATION}/generated && rm -Rf ${SourceFolder.APPLICATION}/generated/*"
    sh "cp -Rf -t ${SourceFolder.APPLICATION}/generated/ ${SourceFolder.GENERATED_VAR} ${SourceFolder.GENERATED_STATIC} ${SourceFolder.GENERATED_MEDIA}"

    //  Create magento configuration files
    sh "cp -f ${SourceFolder.APPLICATION}/app/etc/env.php.configurable-dist ${SourceFolder.APPLICATION}/app/etc/env.php"
    sh "cp -f ${SourceFolder.APPLICATION}/app/etc/config.php.dist ${SourceFolder.APPLICATION}/app/etc/config.php"

	//  Create deploy info
	def foundationCommit = sh returnStdout: true, script: "cd ${SourceFolder.FOUNDATION} && git rev-parse --short HEAD"
	def magentoCommit = sh returnStdout: true, script: "cd ${SourceFolder.MAGENTO} && git rev-parse --short HEAD"
    info = "Application=${env.BRANCH_NAME};Magento=${Repository.BRANCH_MAGENTO}:${magentoCommit};Foundation=${Repository.BRANCH_FOUNDATION}:${foundationCommit}"
    writeFile file: "${SourceFolder.APPLICATION}/DeployInfo.conf", text: info

    imageName = "${Docker.REGISTRY_HOST}/${Docker.REGISTRY_PATH}/${Docker.Image.PREFIX}${Docker.Image.APP_NAME}:${Docker.Image.APP_TAG}"

	def buildArgs = ""

	if (Docker.canDeploy('dev') || Docker.canDeploy('prf') || Docker.canDeploy('int') || Docker.canDeploy('uat')) {
		def cfg = Appdynamics.getConfig('dev')
		def serverName = "shop.wp.dde.nl.corp.tele2.com"

		echo "Appdynamics Server Name: ${serverName}"

		if (cfg) {
			buildArgs = "--build-arg ACCOUNT_NAME=__ACCOUNT_NAME__"
			buildArgs = "${buildArgs} --build-arg ACCOUNT_ACCESS_KEY=__ACCOUNT_ACCESS_KEY__"
			buildArgs = "${buildArgs} --build-arg CONTROLLER_HOST=${cfg.CONTROLLER_HOST}"
			buildArgs = "${buildArgs} --build-arg CONTROLLER_PORT=${cfg.CONTROLLER_PORT}"
			buildArgs = "${buildArgs} --build-arg APPLICATION_NAME=${cfg.APPLICATION_NAME}"
			buildArgs = "${buildArgs} --build-arg SERVER_NAME=__SERVER_NAME__"
			buildArgs = "${buildArgs} --build-arg TIER_NAME=__TIER_NAME__"
		}

	} else if (Docker.canDeploy('prd')) {

	}

	echo "Build arguments: ${buildArgs}"

    //  Build image
    Docker.Image.buildAndPublishReport(
        this,
        "docker build ${buildArgs} -f ${SourceFolder.APPLICATION}/docker/php-fpm/Dockerfile -t ${imageName} ${SourceFolder.APPLICATION}",
        "App Image Build Log"
    )

    //  Add to image list
    Docker.Image.add(imageName, 'app')
}

def buildContentBuilderImage(imageName) {
	logfile = "${SourceFolder.LOGS}/static-content-image-build.log"
	sh "touch ${logfile} && truncate ${logfile} -s 0"
	status = sh returnStatus: true, script: "2>&1 1>>${logfile} ${Docker.LOCAL_ENVS} sh ${SourceFolder.CONTENT_BUILDER}/build.sh -bo \"--build-arg https_proxy=http://proxy.dcn.versatel.net:3128 --build-arg http_proxy=http://proxy.dcn.versatel.net:3128\" -i ${imageName} 2>&1 1>>${logfile}"

	publishHTML(
        [
            allowMissing: true,
            alwaysLinkToLastBuild: false,
            keepAll: true,
            reportDir: "${SourceFolder.LOGS}",
            reportFiles: 'static-content-image-build.log',
            reportName: 'ST Image Build',
            reportTitles: ''
        ]
    )

	return status
}

def generateStaticContent(magentoSrc, foundationSrc, appSrc, staticDst, mediaDst, varDst, imageName) {
	sh "mkdir -p ${mediaDst} ${staticDst} ${varDst}"
    sh "rm -Rf ${mediaDst}/* ${staticDst}/* ${varDst}/*"

    logfile = "${SourceFolder.LOGS}/static-content.log"

    sh "touch ${logfile} && truncate ${logfile} -s 0"

	def flags = ""

	if (Docker.canDeploy('prf')) {
		//flags = "--production --production-static"
		flags = "--production"
	}

	status = sh returnStatus: true, script: "2>&1 1>>${logfile} ${Docker.LOCAL_ENVS} sh ${SourceFolder.CONTENT_BUILDER}/exec.sh -bo \"--rm -e https_proxy=http://proxy.dcn.versatel.net:3128 -e http_proxy=http://proxy.dcn.versatel.net:3128\" --args \"--configure --install-magento --create-configs --compile-di --static-deploy ${flags}\" --magento-src ${magentoSrc} --foundation-src ${foundationSrc} --app-src ${appSrc} --pub-static ${staticDst} --media ${mediaDst} --var ${varDst} -i ${imageName} 2>&1 1>>${logfile}"

	publishHTML(
        [
            allowMissing: true,
            alwaysLinkToLastBuild: false,
            keepAll: true,
            reportDir: "${SourceFolder.LOGS}",
            reportFiles: 'static-content.log',
            reportName: 'ST Gen Log',
            reportTitles: ''
        ]
    )

	//	fix resource config add attribute
	sh "cp -f ${SourceFolder.CONFIG}/resource_config.json ${varDst}/resource_config.json"

	return status
}

def downloadMedia(script, String env, String outputFolder) {
	if (Application.isApplicationUp(script, env)) {
		def url = Application.getMediaDownloadLink(env)
		script.sh "wget -O media.tar ${url} && tar -xf media.tar -C ${outputFolder}"
		script.sh "rm -f media.tar"
	}
}

def buildApplicationImageWithMedia(String env) {
	//  Prepare Dockerfile
	def root = "${SourceFolder.DOCKER}/docker/${Docker.CONFIG_FOLDER}/php-fpm-images"
	def parsedDockerFile = "${root}/Dockerfile.parsed"

	def dockerfile = readFile "${root}/Dockerfile"
    dockerfile = dockerfile.replaceAll("#ApplicationImage#", Docker.Image.get('app'))
    writeFile file: parsedDockerFile , text: dockerfile

	def mediaRoot = "${root}/context"

	Repository.pullWithClean(this, Repository.MEDIA, "dev", mediaRoot)

	try {
		//	copy media to generated media
		sh "cp -Rf ${mediaRoot}/media/* ${SourceFolder.GENERATED_MEDIA}"
	} catch (e) {
		echo "Media copying error: ${e}"
	}


	//imageName = "${Docker.REGISTRY_HOST}/${Docker.REGISTRY_PATH}/${Docker.Image.PREFIX}${Docker.Image.APP_NAME}:${Docker.Image.APP_TAG}-${env}"
	imageName = "${Docker.REGISTRY_HOST}/${Docker.REGISTRY_PATH}/${Docker.Image.PREFIX}${Docker.Image.APP_NAME}:${Docker.Image.APP_TAG}"

    //  Build image
    Docker.Image.buildAndPublishReport(
        this,
        "docker build -f ${parsedDockerFile} -t ${imageName} ${root}",
        "App With Images ${env} Image Build Log"
    )

    //  Add to image list
    //Docker.Image.add(imageName, "app-${env}")
	Docker.Image.add(imageName, "app")

	sh "rm -Rf ${root}/media"
}

properties ([
	buildDiscarder(
		logRotator(
			artifactDaysToKeepStr: '',
			artifactNumToKeepStr: '',
			daysToKeepStr: '3',
			numToKeepStr: '5'
		)
	),
	disableConcurrentBuilds(),
	parameters ([
        string (
            defaultValue: '17S16',
            description: 'Application Branch',
            name: 'GIT_BRANCH_APP'
        ),
        string (
            defaultValue: 'v1.1',
            description: 'Foundation Branch',
            name: 'GIT_BRANCH_FOUNDATION'
        )
    ])
])

node {

    stage("Init") {
        sh "mkdir -p ${SourceFolder.LOGS} ${SourceFolder.SRC}"

		Docker.Image.APP_TAG = "wp"
		Docker.Image.NGINX_TAG = "wp"
		Docker.Image.VARNISH_TAG = "wp"

		Context.script = this
		env.BRANCH_NAME = params.GIT_BRANCH_APP
	    Repository.pull(Repository.MAGENTO, Repository.BRANCH_MAGENTO, SourceFolder.MAGENTO)
	    Repository.pullWithClean(this, Repository.APPLICATION, params.GIT_BRANCH_APP, SourceFolder.APPLICATION)
	    Repository.pull(Repository.DOCKER, Repository.BRANCH_DOCKER, SourceFolder.DOCKER)
	    Repository.pull(Repository.API, Repository.BRANCH_API, SourceFolder.API)
	    Repository.pullWithClean(this, Repository.FOUNDATION, params.GIT_BRANCH_FOUNDATION, SourceFolder.FOUNDATION)
	    Repository.pull(Repository.CONFIG, Repository.BRANCH_CONFIG, SourceFolder.CONFIG)
    }

    stage('Build') {
        Context.script = this
		Phing.init()
        Phing.execute("merge")

        //  Copy foundation to the application dir
        sh "cp -Rf ${SourceFolder.FOUNDATION}/app/code/* ${SourceFolder.SRC}/app/code"

		Phing.execute("prepare-output-folders")

        parallel(
            //  Run phpcs
            "phpcs": { Phing.execute("phpcs") },

            //  Run phpmd
            //"phpmd": { Phing.execute("phpmd") },

            //  Run phpcpd
            "phpcpd": { Phing.execute("phpcpd") },

            //  Run phpunit
            "phpunit": {
				Phing.execute("phpunit")
				publishHTML(
			        [
			            allowMissing: true,
			            alwaysLinkToLastBuild: false,
			            keepAll: true,
			            reportDir: "${SourceFolder.SRC}/output/phpunit/html",
			            reportFiles: 'index.html',
			            reportName: 'PHPUnit',
			            reportTitles: ''
			        ]
			    )
			},
        )

        publishHTML(
            [
                allowMissing: true,
                alwaysLinkToLastBuild: false,
                keepAll: true,
                reportDir: 'output/phpunit/html',
                reportFiles: 'index.html',
                reportName: 'PHPUnit Code Coverage',
                reportTitles: ''
            ]
        )
    }

    stage('Generate Static Content') {
		status = buildContentBuilderImage("${Docker.Image.CONTENT_BUILDER_NAME}:${Docker.Image.CONTENT_BUILDER_TAG}")

		if (status != 0) {
			error "Cant build static content helper image"
		}

        status = generateStaticContent(
            "${workspace}/${SourceFolder.MAGENTO}",
			"${workspace}/${SourceFolder.FOUNDATION}",
            "${workspace}/${SourceFolder.APPLICATION}",
            "${workspace}/${SourceFolder.GENERATED_STATIC}",
            "${workspace}/${SourceFolder.GENERATED_MEDIA}",
            "${workspace}/${SourceFolder.GENERATED_VAR}",
            "${Docker.Image.CONTENT_BUILDER_NAME}:${Docker.Image.CONTENT_BUILDER_TAG}"
        )

        if (status != 0) {
            error "Pipeline error during building static content"
        }
    }

    stage("Build Base Image") {
        Context.script = this
        buildDockerMagentoImage()
    }

    stage("Build App Images") {
        Context.script = this
        buildApplicationImage()

		try {
			buildApplicationImageWithMedia('wp')
		} catch (e) {}
    }

    stage('Build Varnish & Nginx & Tuner') {
        Context.script = this
		dockerFolder = "${SourceFolder.DOCKER}/docker/${Docker.CONFIG_FOLDER}"

		//  We expect that we have nginx and varnish always and we need build images
		parallel(
			//  Build nginx
			"nginx" : {

			    //  we need copy static to nginx dockerfile context
			    sh "mkdir -p ${dockerFolder}/nginx/generated && rm -Rf ${dockerFolder}/nginx/generated/*"
			    sh "cp -Rf -t ${dockerFolder}/nginx/generated/ ${SourceFolder.GENERATED_MEDIA} ${SourceFolder.GENERATED_STATIC}"

                nginxImageName = "${Docker.REGISTRY_HOST}/${Docker.REGISTRY_PATH}/${Docker.Image.PREFIX}${Docker.Image.NGINX_NAME}:${Docker.Image.NGINX_TAG}"

			    Docker.Image.buildAndPublishReport(
                    this,
                    "docker build -f ${dockerFolder}/nginx/Dockerfile -t ${nginxImageName} ${dockerFolder}/nginx",
                    "Nginx Image Build Log"
                )

                Docker.Image.add(nginxImageName, 'nginx')

				sh "rm -Rf ${dockerFolder}/nginx/generated"
			},

			//  Build varnish
			"varnish" : {
                varnishImageName = "${Docker.REGISTRY_HOST}/${Docker.REGISTRY_PATH}/${Docker.Image.PREFIX}${Docker.Image.VARNISH_NAME}:${Docker.Image.VARNISH_TAG}"

			    Docker.Image.buildAndPublishReport(
                    this,
                    "docker build --build-arg https_proxy=http://proxy.dcn.versatel.net:3128 --build-arg http_proxy=http://proxy.dcn.versatel.net:3128 -f ${dockerFolder}/varnish/Dockerfile -t ${varnishImageName} ${dockerFolder}/varnish",
                    "Varnish Image Build Log"
                )

                Docker.Image.add(varnishImageName, 'varnish')
			}
		)

		exists = fileExists "${dockerFolder}/selenium/Dockerfile"
        if (exists) {
            sh "${Docker.LOCAL_ENVS} docker build -f ${dockerFolder}/selenium/Dockerfile -t ${selenium_image} ${dockerFolder}/selenium"
        }
    }

    stage("Push & Deploy") {
		Docker.Image.pushAll(this, "dev")

		sh "DOCKER_HOST=tcp://dockerdde.nl.corp.tele2.com:8443 DOCKER_TLS_VERIFY=1 DOCKER_CERT_PATH=/var/jenkins_home/client-bundles/dta-dmz-client docker stack deploy -c ${SourceFolder.CONFIG}/docker/docker-compose.yml m2tele2_wp"
    }

	stage('Clean') {
		nginx_image = Docker.Image.get('nginx')
		varnish_image = Docker.Image.get('varnish')
		application_image = Docker.Image.get('app')
		sh "docker image rm -f ${nginx_image} ${varnish_image} ${application_image}"
		sh "rm -Rf ${SourceFolder.GENERATED}/* ${SourceFolder.SRC}/*"
	}
}
