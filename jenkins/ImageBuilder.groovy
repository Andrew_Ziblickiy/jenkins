def buildMagentoImage(SourceFolder, ImageNameProvider, DockerConfiguration)
{
	def magentoDockerFile = "${SourceFolder.DOCKER}/docker/base/MagentoDockerfile"
	def magentoDockerFileExists = fileExists "${magentoDockerFile}"

	if (magentoDockerFileExists) {
        commitFile = "${SourceFolder.LOGS}/base-image-commit.cache"
	    def commit = ''
	    def exists = fileExists "${commitFile}"

		//  check if commit file exists
        if (exists) {
            commit = readFile "${commitFile}"
        }

        isImageExists = sh returnStatus: true, script: "1>/dev/null 2>&1 docker inspect ${ImageNameProvider.MAGENTO_NAME}:${ImageNameProvider.MAGENTO_TAG}"
        currentCommit = sh returnStdout: true, script: "cd ${SourceFolder.MAGENTO} && git rev-parse --short HEAD"

        if (currentCommit != commit || isImageExists != 0) {

			echo "Build magento image"

            //  Copy Docker file to magento folder
            sh "cp -f ${magentoDockerFile} ${SourceFolder.MAGENTO}"

            logfile = "${SourceFolder.LOGS}/base-image-build.log"
            sh "touch ${logfile} && truncate ${logfile} -s 0"

            //  we need to build magento image
            status = sh returnStatus: true, script: "2>&1 1>>${logfile} docker build --build-arg https_proxy=${DockerConfiguration.HTTPS_INTERNET_PROXY} --build-arg http_proxy=${DockerConfiguration.HTTP_INTERNET_PROXY} -f ${SourceFolder.MAGENTO}/MagentoDockerfile -t ${ImageNameProvider.MAGENTO_NAME}:${ImageNameProvider.MAGENTO_TAG} ${SourceFolder.MAGENTO} 2>&1 1>>${logfile}"

            publishHTML(
                [
                    allowMissing: true,
                    alwaysLinkToLastBuild: false,
                    keepAll: true,
                    reportDir: "${SourceFolder.LOGS}",
                    reportFiles: 'base-image-build.log',
                    reportName: 'Base Image Build Log',
                    reportTitles: ''
                ]
            )

            if (status != 0) {
                error "Errors occurred while building Base Image, see Base Image Build Log for details"
            }

            //  save latest commit id
            writeFile file: commitFile, text: currentCommit

            if (currentCommit != commit) {
                publishHTML(
                    [
                        allowMissing: true,
                        alwaysLinkToLastBuild: true,
                        keepAll: true,
                        reportDir: "${SourceFolder.LOGS}",
                        reportFiles: 'base-image-commit.cache',
                        reportName: 'Base Image Commit',
                        reportTitles: ''
                    ]
                )
            }
        } else {
            echo "It seems like magento image has the latest code"
        }
	}
}

def buildApplicationImage(SourceFolder, ImageNameProvider, RepositoryConfiguration, DockerConfiguration, AppdynamicsConfigProvider, String branchName, String env = "dev", String jobUrl = "")
{
	//  Copy necessary staff
	sh "mkdir -p ${SourceFolder.APPLICATION}/docker/php-fpm ${SourceFolder.APPLICATION}/container-api"
	sh "cp -Rf ${SourceFolder.DOCKER}/docker/${DockerConfiguration.CONFIG_FOLDER}/php-fpm/* ${SourceFolder.APPLICATION}/docker/php-fpm"
	sh "cp -Rf ${SourceFolder.API}/src/* ${SourceFolder.APPLICATION}/container-api"

	//  Prepare Dockerfile
	def applicationDockerFile = "${SourceFolder.APPLICATION}/docker/php-fpm/Dockerfile"
	def dockerfile = readFile applicationDockerFile
    dockerfile = dockerfile.replaceAll("#MagentoImage#", "${ImageNameProvider.MAGENTO_NAME}:${ImageNameProvider.MAGENTO_TAG}")
    writeFile file: applicationDockerFile , text: dockerfile

	//  Copy foundation to the application dir
	sh "rm -Rf ${SourceFolder.APPLICATION}/app/code/Alpha"
    sh "cp -Rf ${SourceFolder.FOUNDATION}/app/code/* ${SourceFolder.APPLICATION}/app/code"
	//sh "cp prd.03.01.18.dump.sql ${SourceFolder.GENERATED_MEDIA}"
	//sh "cp prd.media.03.01.18.dump.sql ${SourceFolder.GENERATED_MEDIA}"

    //  Copy generated content
    sh "mkdir -p ${SourceFolder.APPLICATION}/generated && rm -Rf ${SourceFolder.APPLICATION}/generated/*"
    //sh "cp -Rf -t ${SourceFolder.APPLICATION}/generated/ ${SourceFolder.GENERATED_VAR} ${SourceFolder.GENERATED_STATIC} ${SourceFolder.GENERATED_MEDIA}"
	sh "cp -Rf -t ${SourceFolder.APPLICATION}/generated/ ${SourceFolder.GENERATED_VAR} ${SourceFolder.GENERATED_STATIC}/deployed_version.txt ${SourceFolder.GENERATED_MEDIA}"


    //  Create magento configuration files
    sh "cp -f ${SourceFolder.APPLICATION}/app/etc/env.php.configurable-dist ${SourceFolder.APPLICATION}/app/etc/env.php"
    //sh "cp -f ${SourceFolder.APPLICATION}/app/etc/config.php ${SourceFolder.APPLICATION}/app/etc/config.php"

	//  Create deploy info
	def foundationCommit = sh returnStdout: true, script: "cd ${SourceFolder.FOUNDATION} && git rev-parse --short HEAD"
	def magentoCommit = sh returnStdout: true, script: "cd ${SourceFolder.MAGENTO} && git rev-parse --short HEAD"

	def commitID = sh returnStdout: true, script: "cd ${SourceFolder.APPLICATION} && git rev-parse --short HEAD"
	def buildDateTime = sh returnStdout: true, script: "date"
    info = "Application=${branchName}:${commitID};Magento=${RepositoryConfiguration.BRANCH_MAGENTO}:${magentoCommit};Foundation=${RepositoryConfiguration.BRANCH_FOUNDATION}:${foundationCommit};BUILD_DATE=${buildDateTime};JOB_URL=${jobUrl}"
    writeFile file: "${SourceFolder.APPLICATION}/DeployInfo.conf", text: info

    echo "Build Image with info: ${info}"

    imageName = "${DockerConfiguration.REGISTRY_HOST}/${DockerConfiguration.REGISTRY_PATH}/${ImageNameProvider.PREFIX}${ImageNameProvider.APP_NAME}:${ImageNameProvider.APP_TAG}"

	//	Resolve and set build info for Appdynamics
	def buildArgs = ""
	def cfg = AppdynamicsConfigProvider.getConfig(env)

	if (null != cfg) {
		buildArgs = "--build-arg ACCOUNT_NAME=__ACCOUNT_NAME__"
		buildArgs = "${buildArgs} --build-arg ACCOUNT_ACCESS_KEY=__ACCOUNT_ACCESS_KEY__"
		buildArgs = "${buildArgs} --build-arg CONTROLLER_HOST=${cfg.CONTROLLER_HOST}"
		buildArgs = "${buildArgs} --build-arg CONTROLLER_PORT=${cfg.CONTROLLER_PORT}"
		buildArgs = "${buildArgs} --build-arg APPLICATION_NAME=__APPLICATION_NAME__"
		buildArgs = "${buildArgs} --build-arg SERVER_NAME=__SERVER_NAME__"
		buildArgs = "${buildArgs} --build-arg TIER_NAME=__TIER_NAME__"
	}

	echo "Build arguments: ${buildArgs}"

    //  Build image
    buildAndPublishReport(
        "docker build ${buildArgs} -f ${SourceFolder.APPLICATION}/docker/php-fpm/Dockerfile -t ${imageName} ${SourceFolder.APPLICATION}",
        "App Image Build Log",
		SourceFolder
    )

    //  Add to image list
    ImageNameProvider.add(imageName, 'app')
}

def buildApplicationImageWithMedia(SourceFolder, ImageNameProvider, RepositoryConfiguration, DockerConfiguration, Git) {
	//  Prepare Dockerfile
	def root = "${SourceFolder.DOCKER}/docker/${DockerConfiguration.CONFIG_FOLDER}/php-fpm-images"
	def parsedDockerFile = "${root}/Dockerfile.parsed"

	def dockerfile = readFile "${root}/Dockerfile"
    dockerfile = dockerfile.replaceAll("#ApplicationImage#", ImageNameProvider.get('app'))
    writeFile file: parsedDockerFile , text: dockerfile

	def mediaRoot = "${root}/context"

	Git.pullWithClean(RepositoryConfiguration.MEDIA, "dev", mediaRoot, RepositoryConfiguration.CREDENTIALS)

	try {
		//	copy media to generated media
		sh "cp -Rf ${mediaRoot}/media/* ${SourceFolder.GENERATED_MEDIA}"
	} catch (e) {
		echo "Media copying error: ${e}"
	}

	imageName = "${DockerConfiguration.REGISTRY_HOST}/${DockerConfiguration.REGISTRY_PATH}/${ImageNameProvider.PREFIX}${ImageNameProvider.APP_NAME}:${ImageNameProvider.APP_TAG}"

    //  Build image
    buildAndPublishReport(
        "docker build -f ${parsedDockerFile} -t ${imageName} ${root}",
        "App With Images Build Log",
		SourceFolder
    )

    //  Add to image list
	ImageNameProvider.add(imageName, "app")

	sh "rm -Rf ${root}/media"
}

def buildContentBuilderImage(SourceFolder, String imageName)
{
	logfile = "${SourceFolder.LOGS}/static-content-image-build.log"
	sh "touch ${logfile} && truncate ${logfile} -s 0"
	status = sh returnStatus: true, script: "2>&1 1>>${logfile} sh ${SourceFolder.CONTENT_BUILDER}/build.sh -bo \"--build-arg https_proxy=http://proxy.dcn.versatel.net:3128 --build-arg http_proxy=http://proxy.dcn.versatel.net:3128\" -i ${imageName} 2>&1 1>>${logfile}"

	publishHTML(
        [
            allowMissing: true,
            alwaysLinkToLastBuild: false,
            keepAll: true,
            reportDir: "${SourceFolder.LOGS}",
            reportFiles: 'static-content-image-build.log',
            reportName: 'ST Image Build',
            reportTitles: ''
        ]
    )

	return status
}

def buildAndPublishReport(String command, String reportTitle, SourceFolder) {
    def num = new Random().nextInt()
    def file = "${SourceFolder.LOGS}/docker-build-${num}.log"

    sh "touch ${file}"
    def status = sh returnStatus: true, script: "2>&1 1>>${file} ${command} 2>&1 1>>${file}"

    publishHTML(
        [
            allowMissing: true,
            alwaysLinkToLastBuild: false,
            keepAll: true,
            reportDir: "${SourceFolder.LOGS}",
            reportFiles: "docker-build-${num}.log",
            reportName: "${reportTitle}",
            reportTitles: ''
        ]
    )

    //  Remove log file
    sh "rm -f ${file}"

    if (status != 0) {
        error "Error when building image\n${command}"
    }
}

def buildTunerImage(String dockerFolder, ImageNameProvider, DockerConfiguration, SourceFolder) {
	dockerFile = "${dockerFolder}/tuner/Dockerfile"
	exists = fileExists dockerFile

	if (exists) {
		def df = readFile dockerFile
		df = df.replaceAll("#ApplicationImage#", ImageNameProvider.get('app'))
		writeFile file: "${dockerFile}_parsed" , text: df

		tunerImageName = "${DockerConfiguration.REGISTRY_HOST}/${DockerConfiguration.REGISTRY_PATH}/${ImageNameProvider.PREFIX}${ImageNameProvider.APP_NAME}-tuner:${ImageNameProvider.APP_TAG}"

		buildAndPublishReport(
			"docker build -f ${dockerFile}_parsed -t ${tunerImageName} ${dockerFolder}/tuner",
			"Tuner Image Build Log",
			SourceFolder
		)

		ImageNameProvider.add(tunerImageName, 'tuner')
	}
}

def buildNginxImage(String dockerFolder, ImageNameProvider, DockerConfiguration, SourceFolder) {
	//  we need copy static to nginx dockerfile context
	sh "mkdir -p ${dockerFolder}/nginx/generated && rm -Rf ${dockerFolder}/nginx/generated/*"
	sh "cp -Rf -t ${dockerFolder}/nginx/generated/ ${SourceFolder.GENERATED_MEDIA} ${SourceFolder.GENERATED_STATIC}"

	nginxImageName = "${DockerConfiguration.REGISTRY_HOST}/${DockerConfiguration.REGISTRY_PATH}/${ImageNameProvider.PREFIX}${ImageNameProvider.NGINX_NAME}:${ImageNameProvider.NGINX_TAG}"

	buildAndPublishReport(
		"docker build -f ${dockerFolder}/nginx/Dockerfile -t ${nginxImageName} ${dockerFolder}/nginx",
		"Nginx Image Build Log",
		SourceFolder
	)

	ImageNameProvider.add(nginxImageName, 'nginx')

	sh "rm -Rf ${dockerFolder}/nginx/generated"
}

def buildVarnishImage(String dockerFolder, ImageNameProvider, DockerConfiguration, SourceFolder) {
	varnishImageName = "${DockerConfiguration.REGISTRY_HOST}/${DockerConfiguration.REGISTRY_PATH}/${ImageNameProvider.PREFIX}${ImageNameProvider.VARNISH_NAME}:${ImageNameProvider.VARNISH_TAG}"

	buildAndPublishReport(
		"docker build --build-arg https_proxy=${DockerConfiguration.HTTPS_INTERNET_PROXY} --build-arg http_proxy=${DockerConfiguration.HTTP_INTERNET_PROXY} -f ${dockerFolder}/varnish/Dockerfile -t ${varnishImageName} ${dockerFolder}/varnish",
		"Varnish Image Build Log",
		SourceFolder
	)

	ImageNameProvider.add(varnishImageName, 'varnish')
}

return this;
