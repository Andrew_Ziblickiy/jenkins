def gitCredentials = "nlsvc-jenkins-docker"
def configRepositoryUrl = "https://bitbucket.nl.corp.tele2.com/scm/mtel/tel2-m2-config.git"
def SourceFolder
def ImageNameProvider
def RepositoryConfiguration
def DockerConfiguration
def Git
def deploymentEnvironment = "tst"
def gitApplicationBranch = "release/1.0.12_18S05"
def gitMagentoBranch = "develop"
def gitFoundationBranch = "v.1.3"
def gitDockerBranch = "selenium_research_local"
def uiTestsPhpunitDirectretailFilter = "DirectRetailUltraLightSmokeTest"
def uiTestsPhpunitOnlineretailFilter = "UltraLightSmokeOnlineRetailTest"
def uiTestsPhpunitOnlineretailSOFilter = "UltraLightSmokeSimOnlyTest"
def uiTestsPhpunitIndirectretailFilter = "IndirectRetailUltraLightSmokeTest"
def uiTestsUseLocalEnv = true
def uiTestsRecordVideo = true

def uiTestsBaseAdminUrl = 'http://admin.${DEP_ENV}.dde.nl.corp.tele2.com'
def uiTestsDirectretailUrl = 'http://directretailshop.${DEP_ENV}.dde.nl.corp.tele2.com'
def uiTestsDirectretailAdminUrl = 'http://admin.${DEP_ENV}.dde.nl.corp.tele2.com/admin/'
def uiTestsOnlineretailUrl = 'http://espresso-${DEP_ENV}.tele2.nl'
def uiTestsOnlineretailAdminUrl = 'http://admin.${DEP_ENV}.dde.nl.corp.tele2.com/admin/'
def uiTestsIndirectretailUrl = 'http://resellershop.${DEP_ENV}.dde.nl.corp.tele2.com'
def uiTestsIndirectretailAdminUrl = 'http://admin.${DEP_ENV}.dde.nl.corp.tele2.com/admin/'

def uiTestsStubs = [
        ['default', 0, 'authorization_and_authentication/test_mode/login_logout_test_mode', 1],
        ['default', 0, 'apigee_stabber/retailStores/stabbed', 1],
        ['default', 0, 'apigee_stabber/iccid/stabbed', 1],
        ['default', 0, 'apigee_stabber/creditCheck/stabbed', 1],
        ['default', 0, 'apigee_stabber/authToken/stabbed', 1],
        ['default', 0, 'apigee_stabber/msisdn/stabbed', 1],
        ['stores', 1, 'apigee_stabber/retailStores/stabbed', 1],
        ['stores', 1, 'apigee_stabber/msisdn/stabbed', 1],
        ['stores', 1, 'apigee_stabber/iccid/stabbed', 1],
]



def parseStubs(stubs) {
    def result = []

    if (stubs in Collection) {
        stubs.each {
            result.add(it.join(':'))
        }
        result = result.join(",\n")
    } else {
        stubs = stubs.split(',')
        stubs.each {
            result.add(it.trim())
        }
        result = result.join(',')
    }

    return result
}

def saveScreenshots(path) {
    def screenshots = findFiles(glob: "**/dev/tests/functional/var/log/**/screenshots/*")
    screenshots.each {
        screenshotName = it.path.split('/var/log/')[1].replaceAll("[^a-zA-Z0-9\\.\\-]", "_").replace('screenshots','')
        sh "cp \"${it}\" ${path}/${screenshotName}"
    }
}

def saveScreenrecords(path, phpunitFile, phpunitFilter) {
    def namePatterns = []
    if (phpunitFile.split(',').size() > 1) {
        def phpunitFiles = phpunitFile.split(',')
        def phpunitFilters = phpunitFilter.split(',')
        phpunitFiles.eachWithIndex{
            file, i -> namePatterns.add("${file}_${phpunitFilters[i]}")
        }
    } else {
        namePatterns.add("${phpunitFile}_${phpunitFilter}")
    }
    namePatterns.each {
        def videos = findFiles(glob: "**/dev/tests/functional/var/log/**/*${it}*.ogv")
        videos.each {
            video -> sh "cp \"${video.path}\" ${path}/${video.name}"
        }
    }
}

def runUITest(SourceFolder, imageName, testName, phpunitFile, phpunitFilter, stubs='', localEnv=false, recordVideo=true) {

    def magentoSrc = "${workspace}/${SourceFolder.MAGENTO}"
    def foundationSrc = "${workspace}/${SourceFolder.FOUNDATION}"
    def appSrc = "${workspace}/${SourceFolder.APPLICATION}"
    def staticDst = "${workspace}/${SourceFolder.GENERATED_STATIC}_${testName}"
    def mediaDst = "${workspace}/${SourceFolder.GENERATED_MEDIA}_${testName}"
    def varDst = "${workspace}/${SourceFolder.GENERATED_VAR}_${testName}"
    def nginxDst = "${workspace}/${SourceFolder.DOCKER}/docker/local/nginx"
    def dbdumpSrc = "${workspace}/${SourceFolder.DOCKER}/docker/content-builder/dump.sql"

    sh "mkdir -p ${staticDst} ${mediaDst} ${varDst}"
    sh "rm -rf ${staticDst}/* ${mediaDst}/* ${varDst}/*"

    def screenshotsDir = "${SourceFolder.LOGS}/screenshots"
    sh "mkdir -p ${screenshotsDir}"

    def screenrecordsDir = "${SourceFolder.LOGS}/videos"

    if (recordVideo) {
        sh "mkdir -p ${screenrecordsDir}"
    }

    def recordVideoArg = recordVideo ? '--mtf-record-video' : ''

    def logname = "ui-tests-${testName}.log"
    def logfile = "${SourceFolder.LOGS}/${logname}"
    sh "touch ${logfile} && truncate ${logfile} -s 0"

    def stubsArg = stubs ? "--mtf-stubs " + stubs : ''


    def runArgs = ""

    if (localEnv) {
        runArgs = "--create-configs --configure --compile-di --create-di-xml --start-nginx --production --static-deploy --mtf-populate-db --selenium --mtf-phpunit-file=${phpunitFile} --mtf-phpunit-filter=${phpunitFilter} ${recordVideoArg} ${stubsArg}"
    } else {
        runArgs = "--selenium --mtf-remote-host-test --mtf-phpunit-file=${phpunitFile} --mtf-phpunit-filter=${phpunitFilter} ${recordVideoArg} ${stubsArg}"
    }

    def status = sh returnStatus: true, script: "2>&1 1>>${logfile} sh ${SourceFolder.DOCKER}/docker/content-builder/exec.sh -bo \"--memory-swappiness 0 --memory 1g\" --args \"${runArgs}\" --nginx-src ${nginxDst} --dbdump-src ${dbdumpSrc} --magento-src ${magentoSrc} --foundation-src ${foundationSrc} --app-src ${appSrc} --pub-static ${staticDst} --media ${mediaDst} --var ${varDst} -i ${imageName} 2>&1 1>>${logfile}"

    publishHTML(
            [
                    allowMissing: true,
                    alwaysLinkToLastBuild: false,
                    keepAll: true,
                    reportDir: "${SourceFolder.LOGS}",
                    reportFiles: "${logname}",
                    reportName: "UI Tests ${testName}",
                    reportTitles: ''
            ]
    )

    if (recordVideo) {
        saveScreenrecords(screenrecordsDir, phpunitFile, phpunitFilter)
        archiveArtifacts allowEmptyArchive: true, artifacts: "${screenrecordsDir}/*"
    }

    if (status != 0) {
        saveScreenshots(screenshotsDir)
        archiveArtifacts allowEmptyArchive: true, artifacts: "${screenshotsDir}/*"

        error "Testing failed"
    }
}


properties ([
        buildDiscarder(
                logRotator(
                        artifactDaysToKeepStr: '',
                        artifactNumToKeepStr: '',
                        daysToKeepStr: '3',
                        numToKeepStr: '5'
                )
        ),
        disableConcurrentBuilds(),
        parameters ([
                choice(
                        choices: "local\ntst\ndev\nuat\nint\nprf\npde\nprd",
                        description: 'Select environment',
                        name: 'DEP_ENV'
                ),
//                string(
//                        defaultValue: deploymentEnvironment,
//                        description: 'Deployment Environment',
//                        name: 'DEP_ENV'
//                ),
//                booleanParam(
//                        defaultValue: uiTestsUseLocalEnv,
//                        description: 'Run UI tests on local environment',
//                        name: 'UI_TESTS_USE_LOCAL_ENV'
//                ),
                booleanParam(
                        defaultValue: uiTestsRecordVideo,
                        description: 'Record video of UI tests',
                        name: 'UI_TESTS_RECORD_VIDEO'
                ),
                string(
                        defaultValue: uiTestsPhpunitDirectretailFilter,
                        description: 'Direct Retail tests filter',
                        name: 'UI_TESTS_DIRECTRETAIL_FILTER'
                ),
                string(
                        defaultValue: uiTestsPhpunitOnlineretailFilter,
                        description: 'Online Retail tests filter',
                        name: 'UI_TESTS_ONLINERETAIL_FILTER'
                ),
                string(
                        defaultValue: uiTestsPhpunitOnlineretailSOFilter,
                        description: 'Online Retail SimOnly tests filter',
                        name: 'UI_TESTS_ONLINERETAILSO_FILTER'
                ),
                string(
                        defaultValue: uiTestsPhpunitIndirectretailFilter,
                        description: 'Indirect Retail tests filter',
                        name: 'UI_TESTS_INDIRECTRETAIL_FILTER'
                ),
                string(
                        defaultValue: gitApplicationBranch,
                        description: 'Application Branch',
                        name: 'GIT_BRANCH_APP'
                ),
                string(
                        defaultValue: gitMagentoBranch,
                        description: 'Magento Branch',
                        name: 'GIT_BRANCH_MAGENTO'
                ),
                string(
                        defaultValue: gitFoundationBranch,
                        description: 'Foundation Branch',
                        name: 'GIT_BRANCH_FOUNDATION'
                ),
                string(
                        defaultValue: gitDockerBranch,
                        description: 'Docker Branch',
                        name: 'GIT_BRANCH_DOCKER'
                ),
//                string(
//                        defaultValue: uiTestsBaseAdminUrl,
//                        description: 'Base admin url',
//                        name: 'UI_TESTS_BASE_ADMIN_URL'
//                ),
//                string(
//                        defaultValue: uiTestsDirectretailUrl,
//                        description: 'Direct Retail url',
//                        name: 'UI_TESTS_DIRECTRETAIL_URL'
//                ),
//                string(
//                        defaultValue: uiTestsDirectretailAdminUrl,
//                        description: 'Direct Retail admin url',
//                        name: 'UI_TESTS_DIRECTRETAIL_ADMIN_URL'
//                ),
//                string(
//                        defaultValue: uiTestsOnlineretailUrl,
//                        description: 'Online Retail url',
//                        name: 'UI_TESTS_ONLINERETAIL_URL'
//                ),
//                string(
//                        defaultValue: uiTestsOnlineretailAdminUrl,
//                        description: 'Online Retail admin url',
//                        name: 'UI_TESTS_ONLINERETAIL_ADMIN_URL'
//                ),
//                string(
//                        defaultValue: uiTestsIndirectretailUrl,
//                        description: 'Indirect Retail url',
//                        name: 'UI_TESTS_INDIRECTRETAIL_URL'
//                ),
//                string(
//                        defaultValue: uiTestsIndirectretailAdminUrl,
//                        description: 'Indirect Retail admin url',
//                        name: 'UI_TESTS_INDIRECTRETAIL_ADMIN_URL'
//                ),
                booleanParam(
                        defaultValue: false,
                        description: 'Direct Retail use HTTPS',
                        name: 'UI_TESTS_DIRECTRETAIL_USE_HTTPS'
                ),
                booleanParam(
                        defaultValue: false,
                        description: 'Online Retail use HTTPS',
                        name: 'UI_TESTS_ONLINERETAIL_USE_HTTPS'
                ),
                booleanParam(
                        defaultValue: false,
                        description: 'Indirect Retail use HTTPS',
                        name: 'UI_TESTS_INDIRECTRETAIL_USE_HTTPS'
                ),
                booleanParam(
                        defaultValue: false,
                        description: 'Admin use HTTPS',
                        name: 'UI_TESTS_ADMIN_USE_HTTPS'
                ),
                text(
                        defaultValue: parseStubs(uiTestsStubs),
                        description: 'Service Stubs',
                        name: 'UI_TESTS_STUBS'
                ),
                booleanParam(
                        defaultValue: true,
                        description: 'Enable Direct Retail UI tests',
                        name: 'UI_TESTS_DIRECTRETAIL_ENABLED'
                ),
                booleanParam(
                        defaultValue: true,
                        description: 'Enable Online Retail UI tests',
                        name: 'UI_TESTS_ONLINERETAIL_ENABLED'
                ),
                booleanParam(
                        defaultValue: true,
                        description: 'Enable Online Retail SimOnly UI tests',
                        name: 'UI_TESTS_ONLINERETAILSO_ENABLED'
                ),
                booleanParam(
                        defaultValue: false,
                        description: 'Enable Indirect Retail UI tests',
                        name: 'UI_TESTS_INDIRECTRETAIL_ENABLED'
                )
        ])
])


node {

    checkout changelog: false, poll: false, scm: [$class: 'GitSCM', branches: [[name: "master"]], browser: [$class: 'Stash', repoUrl: ''], doGenerateSubmoduleConfigurations: false, extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: "config"], [$class: 'CleanBeforeCheckout']], submoduleCfg: [], userRemoteConfigs: [[credentialsId: gitCredentials, url: configRepositoryUrl]]]

    stage('Init') {

        Git = load "config/jenkins/Git.groovy"
        RepositoryConfiguration = load "config/jenkins/RepositoryConfiguration.groovy"
        SourceFolder = load "config/jenkins/SourceFolderProvider.groovy"

        sh "rm -rf ${workspace}/${SourceFolder.LOGS}/*"
        sh "rm -rf ${workspace}/${SourceFolder.MAGENTO}/*"
        sh "rm -rf ${workspace}/${SourceFolder.FOUNDATION}/*"
        sh "rm -rf ${workspace}/${SourceFolder.APPLICATION}/*"
        sh "rm -rf ${workspace}/${SourceFolder.DOCKER}/*"

        //	Configure ImageNameProvider
        ImageNameProvider = load "config/jenkins/ImageNameProvider.groovy"
        ImageNameProvider.APP_TAG = params.DEP_ENV

        //	Configure Docker
        DockerConfiguration = load "config/jenkins/DockerConfiguration.groovy"
        DockerConfiguration.addDeploymentEnvironment(params.DEP_ENV)
        DockerConfiguration.CONFIG_FOLDER = "tele2_generic"

        sh "mkdir -p ${SourceFolder.LOGS} ${SourceFolder.SRC}"

        Git.pull(RepositoryConfiguration.MAGENTO, params.GIT_BRANCH_MAGENTO, SourceFolder.MAGENTO, RepositoryConfiguration.CREDENTIALS)
        Git.pullWithClean(RepositoryConfiguration.APPLICATION, params.GIT_BRANCH_APP, SourceFolder.APPLICATION, RepositoryConfiguration.CREDENTIALS)
        Git.pullWithClean(RepositoryConfiguration.FOUNDATION, params.GIT_BRANCH_FOUNDATION, SourceFolder.FOUNDATION, RepositoryConfiguration.CREDENTIALS)
        Git.pullWithClean(RepositoryConfiguration.DOCKER, params.GIT_BRANCH_DOCKER, SourceFolder.DOCKER, RepositoryConfiguration.CREDENTIALS)
    }

    stage('Build Image') {

        logfile = "${SourceFolder.LOGS}/static-content-dev-image-build.log"
        imageName = "${ImageNameProvider.CONTENT_BUILDER_NAME}:develop"
        sh "touch ${logfile} && truncate ${logfile} -s 0"
        status = sh returnStatus: true, script: "2>&1 1>>${logfile} sh ${SourceFolder.DOCKER}/docker/content-builder/build.sh -bo \"--build-arg https_proxy=${DockerConfiguration.HTTPS_INTERNET_PROXY} --build-arg http_proxy=${DockerConfiguration.HTTP_INTERNET_PROXY}\" -i ${imageName} 2>&1 1>>${logfile}"

        publishHTML(
                [
                        allowMissing: true,
                        alwaysLinkToLastBuild: false,
                        keepAll: true,
                        reportDir: "${SourceFolder.LOGS}",
                        reportFiles: 'static-content-dev-image-build.log',
                        reportName: 'UI Tests Image Build Log',
                        reportTitles: ''
                ]
        )
    }

    stage('UI Tests') {

        uiTestsUseLocalEnv = (params.DEP_ENV == 'local') ? true : false

        if (!uiTestsUseLocalEnv) {

            def config = DockerConfiguration.getConfig(params.DEP_ENV)

            if (null == config) {
                return false
            }
            def swarmHost = config.SWARM_HOST

            def getUrlProtocol = {secure -> secure ? 'https' : 'http'}


//            def baseAdminUrl = params.UI_TESTS_BASE_ADMIN_URL.replace('${DEP_ENV}',params.DEP_ENV)
            def baseAdminUrl = "${getUrlProtocol(params.UI_TESTS_ADMIN_USE_HTTPS)}://admin.${params.DEP_ENV}.${swarmHost}"

//            def directRetailUrl = params.UI_TESTS_DIRECTRETAIL_URL.replace('${DEP_ENV}',params.DEP_ENV)
//            def directRetailAdminUrl = params.UI_TESTS_DIRECTRETAIL_ADMIN_URL.replace('${DEP_ENV}',params.DEP_ENV)
            def directRetailUrl = "${getUrlProtocol(params.UI_TESTS_DIRECTRETAIL_USE_HTTPS)}://directretailshop.${params.DEP_ENV}.${swarmHost}"
            def directRetailAdminUrl = "${getUrlProtocol(params.UI_TESTS_ADMIN_USE_HTTPS)}://admin.${params.DEP_ENV}.${swarmHost}/admin/"

//            def onlineRetailUrl = params.UI_TESTS_ONLINERETAIL_URL.replace('${DEP_ENV}',params.DEP_ENV)
//            def onlineRetailAdminUrl = params.UI_TESTS_ONLINERETAIL_ADMIN_URL.replace('${DEP_ENV}',params.DEP_ENV)
            def onlineRetailUrl = "${getUrlProtocol(params.UI_TESTS_ONLINERETAIL_USE_HTTPS)}://espresso-${params.DEP_ENV}.tele2.nl"
            def onlineRetailAdminUrl = "${getUrlProtocol(params.UI_TESTS_ADMIN_USE_HTTPS)}://admin.${params.DEP_ENV}.${swarmHost}/admin/"

//            def indirectRetailUrl = params.UI_TESTS_INDIRECTRETAIL_URL.replace('${DEP_ENV}',params.DEP_ENV)
//            def indirectRetailAdminUrl = params.UI_TESTS_INDIRECTRETAIL_ADMIN_URL.replace('${DEP_ENV}',params.DEP_ENV)
            def indirectRetailUrl = "${getUrlProtocol(params.UI_TESTS_INDIRECTRETAIL_USE_HTTPS)}://resellershop.${params.DEP_ENV}.${swarmHost}"
            def indirectRetailAdminUrl = "${getUrlProtocol(params.UI_TESTS_ADMIN_USE_HTTPS)}://admin.${params.DEP_ENV}.${swarmHost}/admin/"


            //	Configure backend
            sh "sed \"s#magento_host#${baseAdminUrl}#\" ${SourceFolder.APPLICATION}/config/functional_tests/config.xml.dist > ${SourceFolder.APPLICATION}/dev/tests/functional/config.xml"

            // //Configure phpunit configuration for directretail
            sh "sed \"s#magento_host#${directRetailUrl}#\" ${SourceFolder.APPLICATION}/config/functional_tests/phpunit.xml.dist > ${SourceFolder.APPLICATION}/dev/tests/functional/phpunit.directretail.xml"
            sh "sed -i \"s#magento_admin_host#${directRetailAdminUrl}#\" ${SourceFolder.APPLICATION}/dev/tests/functional/phpunit.directretail.xml"

            //	Configure phpunit configuration for onlineretail
            sh "sed \"s#magento_host#${onlineRetailUrl}#\" ${SourceFolder.APPLICATION}/config/functional_tests/phpunitOnlineRetail.xml > ${SourceFolder.APPLICATION}/dev/tests/functional/phpunit.online.xml"
            sh "sed -i \"s#magento_admin_host#${onlineRetailAdminUrl}#\" ${SourceFolder.APPLICATION}/dev/tests/functional/phpunit.online.xml"

            //	Configure phpunit configuration for indirectretail
            sh "sed \"s#magento_host#${indirectRetailUrl}#\" ${SourceFolder.APPLICATION}/config/functional_tests/phpunit.xml.dist > ${SourceFolder.APPLICATION}/dev/tests/functional/phpunit.indirectretail.xml"
            sh "sed -i \"s#magento_admin_host#${indirectRetailAdminUrl}#\" ${SourceFolder.APPLICATION}/dev/tests/functional/phpunit.indirectretail.xml"
            sh "sed -i \"s#Tele2DirectRetailNl#Tele2IndirectRetailNl#\" ${SourceFolder.APPLICATION}/dev/tests/functional/phpunit.indirectretail.xml"

        }


        // Increase selenium timeout
        def phpunitFilePath = "${SourceFolder.APPLICATION}/dev/tests/functional/vendor/phpunit/phpunit-selenium/PHPUnit/Extensions/Selenium2TestCase/Driver.php"
        def replacedPhpUnitFile = readFile phpunitFilePath
        replacedPhpUnitFile = replacedPhpUnitFile.replaceAll('\\$timeout;', "300;")
        writeFile file: phpunitFilePath , text: replacedPhpUnitFile

        def composerExists = fileExists "composer.phar"
        if (!composerExists) {
            sh "https_proxy=${DockerConfiguration.HTTPS_INTERNET_PROXY} http_proxy=${DockerConfiguration.HTTP_INTERNET_PROXY} curl -o composer.phar https://getcomposer.org/composer.phar"
        }
        sh "rm -rf ${SourceFolder.APPLICATION}/dev/tests/functional/vendor/* && cd ${SourceFolder.APPLICATION}/dev/tests/functional && https_proxy=http://proxy.dcn.versatel.net:3128 http_proxy=http://proxy.dcn.versatel.net:3128 php ${workspace}/composer.phar install"

        sh "rm -Rf ${workspace}/${SourceFolder.MAGENTO}/dev/tests/functional/var/log/*"


        parallelNodes = [:]

        if (params.UI_TESTS_DIRECTRETAIL_ENABLED) {
            parallelNodes['directretail'] = {
                runUITest(SourceFolder, imageName, 'directretail', 'phpunit.directretail.xml', params.UI_TESTS_DIRECTRETAIL_FILTER, parseStubs(params.UI_TESTS_STUBS), uiTestsUseLocalEnv, params.UI_TESTS_RECORD_VIDEO)
            }
        }

        if (params.UI_TESTS_ONLINERETAIL_ENABLED) {
            parallelNodes['onlineretail'] = {
                runUITest(SourceFolder, imageName, 'onlineretail', 'phpunit.online.xml', params.UI_TESTS_ONLINERETAIL_FILTER, parseStubs(params.UI_TESTS_STUBS), uiTestsUseLocalEnv, params.UI_TESTS_RECORD_VIDEO)
            }
        }

        if (params.UI_TESTS_ONLINERETAILSO_ENABLED) {
            parallelNodes['onlineretailSO'] = {
                runUITest(SourceFolder, imageName, 'onlineretailSO', 'phpunit.online.xml', params.UI_TESTS_ONLINERETAILSO_FILTER, parseStubs(params.UI_TESTS_STUBS), uiTestsUseLocalEnv, params.UI_TESTS_RECORD_VIDEO)
            }
        }

        if (params.UI_TESTS_INDIRECTRETAIL_ENABLED) {
            parallelNodes['indirectretail'] = {
                runUITest(SourceFolder, imageName, 'indirectretail', 'phpunit.indirectretail.xml', params.UI_TESTS_INDIRECTRETAIL_FILTER, parseStubs(params.UI_TESTS_STUBS), uiTestsUseLocalEnv, params.UI_TESTS_RECORD_VIDEO)
            }
        }

        parallel parallelNodes

    }
}
