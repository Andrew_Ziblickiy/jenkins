def gitCredentials = "nlsvc-jenkins-docker"
def configRepositoryUrl = "https://bitbucket.nl.corp.tele2.com/scm/mtel/tel2-m2-config.git"
def SourceFolder
def ImageNameProvider
def RepositoryConfiguration
def DockerConfiguration
def Git
def gitApplicationBranch = "18S09"
def gitMagentoBranch = "develop"
def gitFoundationBranch = "v.1.3"
def gitDockerBranch = "selenium_research_local"
def environment = "dev"
def uiTestsPhpunitDirectretailFilter = "Tele2DirectRetailNl"
def uiTestsPhpunitOnlineretailFilter = "Tele2OnlineRetailNl"

def uiTestsStubs = [
        ['default', 0, 'authorization_and_authentication/test_mode/login_logout_test_mode', 1],
        ['default', 0, 'apigee_stabber/retailStores/stabbed', 1],
        ['default', 0, 'apigee_stabber/iccid/stabbed', 1],
        ['default', 0, 'apigee_stabber/creditCheck/stabbed', 1],
        ['default', 0, 'apigee_stabber/authToken/stabbed', 1],
        ['default', 0, 'apigee_stabber/msisdn/stabbed', 1],
        ['stores', 1, 'apigee_stabber/retailStores/stabbed', 1],
        ['stores', 1, 'apigee_stabber/msisdn/stabbed', 1],
        ['stores', 1, 'apigee_stabber/iccid/stabbed', 1],
]


def parseStubs(stubs) {
    def result = []
    stubs.each {
        result.add(it.join(':'))
    }
    return result.join(",")
}

def saveScreenshots(path) {
    def screenshots = findFiles(glob: "**/dev/tests/functional/var/log/**/screenshots/*")
    screenshots.each {
        screenshotName = it.path.split('/var/log/')[1].replaceAll("[^a-zA-Z0-9\\.\\-]", "_").replace('screenshots','')
        sh "cp \"${it}\" ${path}/${screenshotName}"
    }
}

def saveScreenrecords(path, phpunitFile, phpunitFilter) {
    def namePatterns = []
    if (phpunitFile.split(',').size() > 1) {
        def phpunitFiles = phpunitFile.split(',')
        def phpunitFilters = phpunitFilter.split(',')
        phpunitFiles.eachWithIndex{
            file, i -> namePatterns.add("${file}_${phpunitFilters[i]}")
        }
    } else {
        namePatterns.add("${phpunitFile}_${phpunitFilter}")
    }
    namePatterns.each {
        def videos = findFiles(glob: "**/dev/tests/functional/var/log/**/*${it}*.ogv")
        videos.each {
            video -> sh "cp \"${video.path}\" ${path}/${video.name}"
        }
    }
}

def runUITest(SourceFolder, imageName, testName, phpunitFile, phpunitFilter, stubs='', recordVideo=false) {

    def magentoSrc = "${workspace}/${SourceFolder.MAGENTO}"
    def foundationSrc = "${workspace}/${SourceFolder.FOUNDATION}"
    def appSrc = "${workspace}/${SourceFolder.APPLICATION}"
    def staticDst = "${workspace}/${SourceFolder.GENERATED_STATIC}_${testName}"
    def mediaDst = "${workspace}/${SourceFolder.GENERATED_MEDIA}_${testName}"
    def varDst = "${workspace}/${SourceFolder.GENERATED_VAR}_${testName}"

    sh "mkdir -p ${staticDst} ${mediaDst} ${varDst}"
    sh "rm -rf ${staticDst}/* ${mediaDst}/* ${varDst}/*"

    def screenshotsDir = "${SourceFolder.LOGS}/screenshots"
    sh "mkdir -p ${screenshotsDir}"

    def screenrecordsDir = "${SourceFolder.LOGS}/videos"

    if (recordVideo) {
        sh "mkdir -p ${screenrecordsDir}"
    }

    def recordVideoArg = recordVideo ? '--mtf-record-video' : ''

    def logname = "ui-tests-${testName}.log"
    def logfile = "${SourceFolder.LOGS}/${logname}"
    sh "touch ${logfile} && truncate ${logfile} -s 0"

    def stubsArg = stubs ? "--mtf-stubs " + stubs : ''

    def runArgs = "--selenium --mtf-remote-host-test --mtf-phpunit-file=${phpunitFile} --mtf-phpunit-filter=${phpunitFilter} ${recordVideoArg} ${stubsArg}"


    def status = sh returnStatus: true, script: "2>&1 1>>${logfile} sh docker_develop/docker/content-builder/exec.sh -bo \"--memory-swappiness 0 --memory 3g\" --args \"${runArgs}\" --magento-src ${magentoSrc} --foundation-src ${foundationSrc} --app-src ${appSrc} --pub-static ${staticDst} --media ${mediaDst} --var ${varDst} -i ${imageName} 2>&1 1>>${logfile}"

    def shortLogName = "uiTests_results.log"
    def shortLogFile = "${workspace}/${SourceFolder.LOGS}/${shortLogName}"
    sh "touch ${shortLogFile} && truncate ${shortLogFile} -s 0"
    sh "cat ${logfile} | sed -n '/MTF tests arguments/,/Builder/p' | sed '/^127.0.0.1/ d' | sed -E -e 's/(.+?Builder: )/-------------\\n/g' 1>${shortLogFile} 2>/dev/null"

    publishHTML(
            [
                    allowMissing: true,
                    alwaysLinkToLastBuild: false,
                    keepAll: true,
                    reportDir: "${SourceFolder.LOGS}",
                    reportFiles: "${logname}",
                    reportName: "UI Tests ${testName}",
                    reportTitles: ''
            ]
    )

    publishHTML(
            [
                    allowMissing: true,
                    alwaysLinkToLastBuild: false,
                    keepAll: true,
                    reportDir: "${SourceFolder.LOGS}",
                    reportFiles: "${shortLogName}",
                    reportName: "UI Tests ${testName} results",
                    reportTitles: ''
            ]
    )

    if (recordVideo) {
        saveScreenrecords(screenrecordsDir, phpunitFile, phpunitFilter)
        archiveArtifacts allowEmptyArchive: true, artifacts: "${screenrecordsDir}/*"
    }

    if (status != 0) {
        saveScreenshots(screenshotsDir)
        archiveArtifacts allowEmptyArchive: true, artifacts: "${screenshotsDir}/*"

        error "Testing failed"
    }
}


properties ([
        buildDiscarder(
                logRotator(
                        artifactDaysToKeepStr: '',
                        artifactNumToKeepStr: '',
                        daysToKeepStr: '1',
                        numToKeepStr: '3'
                )
        ),
        disableConcurrentBuilds(),
        parameters ([
                string(
                        defaultValue: gitApplicationBranch,
                        description: 'Application Branch',
                        name: 'GIT_BRANCH_APP'
                ),
                string(
                        defaultValue: environment,
                        description: 'Environment',
                        name: 'ENV'
                )
        ])
])


node {

    checkout changelog: false, poll: false, scm: [$class: 'GitSCM', branches: [[name: "master"]], browser: [$class: 'Stash', repoUrl: ''], doGenerateSubmoduleConfigurations: false, extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: "config"], [$class: 'CleanBeforeCheckout']], submoduleCfg: [], userRemoteConfigs: [[credentialsId: gitCredentials, url: configRepositoryUrl]]]

    stage('Init') {

        Git = load "config/jenkins/Git.groovy"
        RepositoryConfiguration = load "config/jenkins/RepositoryConfiguration.groovy"
        SourceFolder = load "config/jenkins/SourceFolderProvider.groovy"

        sh "rm -rf ${workspace}/${SourceFolder.LOGS}/*"
        sh "rm -rf ${workspace}/${SourceFolder.MAGENTO}/*"
        sh "rm -rf ${workspace}/${SourceFolder.FOUNDATION}/*"
        sh "rm -rf ${workspace}/${SourceFolder.APPLICATION}/*"

        //	Configure ImageNameProvider
        ImageNameProvider = load "config/jenkins/ImageNameProvider.groovy"
        ImageNameProvider.APP_TAG = params.ENV

        //	Configure Docker
        DockerConfiguration = load "config/jenkins/DockerConfiguration.groovy"
        DockerConfiguration.addDeploymentEnvironment(params.ENV)
        DockerConfiguration.CONFIG_FOLDER = "tele2_generic"

        sh "mkdir -p ${SourceFolder.LOGS} ${SourceFolder.SRC}"

        Git.pull(RepositoryConfiguration.MAGENTO, gitMagentoBranch, SourceFolder.MAGENTO, RepositoryConfiguration.CREDENTIALS)
        Git.pullWithClean(RepositoryConfiguration.APPLICATION, params.GIT_BRANCH_APP, SourceFolder.APPLICATION, RepositoryConfiguration.CREDENTIALS)
        Git.pullWithClean(RepositoryConfiguration.FOUNDATION, gitFoundationBranch, SourceFolder.FOUNDATION, RepositoryConfiguration.CREDENTIALS)
        Git.pullWithClean(RepositoryConfiguration.DOCKER, gitDockerBranch, 'docker_develop', RepositoryConfiguration.CREDENTIALS)
    }

    stage('Build Image') {

        sh "docker pull dtrdd.nl.corp.tele2.com:9443/aliapana/mag-mtf"
        imageName = "dtrdd.nl.corp.tele2.com:9443/aliapana/mag-mtf"

        // logfile = "${SourceFolder.LOGS}/static-content-dev-image-build.log"
        // imageName = "${ImageNameProvider.CONTENT_BUILDER_NAME}:develop"
        // sh "touch ${logfile} && truncate ${logfile} -s 0"
        // status = sh returnStatus: true, script: "2>&1 1>>${logfile} sh docker_develop/docker/content-builder/build.sh -bo \"--build-arg https_proxy=${DockerConfiguration.HTTPS_INTERNET_PROXY} --build-arg http_proxy=${DockerConfiguration.HTTP_INTERNET_PROXY}\" -i ${imageName} 2>&1 1>>${logfile}"
        //
        // publishHTML(
        //         [
        //                 allowMissing: true,
        //                 alwaysLinkToLastBuild: false,
        //                 keepAll: true,
        //                 reportDir: "${SourceFolder.LOGS}",
        //                 reportFiles: 'static-content-dev-image-build.log',
        //                 reportName: 'UI Tests Image Build Log',
        //                 reportTitles: ''
        //         ]
        // )
    }

    stage('UI Tests') {

        def config = DockerConfiguration.getConfig(params.ENV)

        if (null == config) {
            return false
        }
        def swarmHost = config.SWARM_HOST

        // Increase selenium timeout
        def phpunitFilePath = "${SourceFolder.APPLICATION}/dev/tests/functional/vendor/phpunit/phpunit-selenium/PHPUnit/Extensions/Selenium2TestCase/Driver.php"
        def replacedPhpUnitFile = readFile phpunitFilePath
        replacedPhpUnitFile = replacedPhpUnitFile.replaceAll('\\$timeout;', "300;")
        writeFile file: phpunitFilePath , text: replacedPhpUnitFile


        //	Configure backend
        sh "sed \"s#magento_host#https://admin.${params.ENV}.${swarmHost}#\" ${SourceFolder.APPLICATION}/config/functional_tests/config.xml.dist > ${SourceFolder.APPLICATION}/dev/tests/functional/config.xml"

        // //Configure phpunit configuration for directretail
        sh "sed \"s#magento_host#https://directretailshop.${params.ENV}.${swarmHost}#\" ${SourceFolder.APPLICATION}/config/functional_tests/phpunit.xml.dist > ${SourceFolder.APPLICATION}/dev/tests/functional/phpunit.directretail.xml"
        sh "sed -i \"s#magento_admin_host#https://admin.${params.ENV}.${swarmHost}/admin/#\" ${SourceFolder.APPLICATION}/dev/tests/functional/phpunit.directretail.xml"

        //	Configure phpunit configuration for onlineretail
        sh "sed \"s#magento_host#https://espresso-${params.ENV}.tele2.nl#\" ${SourceFolder.APPLICATION}/config/functional_tests/phpunitOnlineRetail.xml > ${SourceFolder.APPLICATION}/dev/tests/functional/phpunit.online.xml"
        sh "sed -i \"s#magento_admin_host#https://admin.${params.ENV}.${swarmHost}/admin/#\" ${SourceFolder.APPLICATION}/dev/tests/functional/phpunit.online.xml"


        def composerExists = fileExists "composer.phar"
        if (!composerExists) {
            sh "https_proxy=${DockerConfiguration.HTTPS_INTERNET_PROXY} http_proxy=${DockerConfiguration.HTTP_INTERNET_PROXY} curl -o composer.phar https://getcomposer.org/composer.phar"
        }
        sh "rm -rf ${SourceFolder.APPLICATION}/dev/tests/functional/vendor/* && cd ${SourceFolder.APPLICATION}/dev/tests/functional && https_proxy=http://proxy.dcn.versatel.net:3128 http_proxy=http://proxy.dcn.versatel.net:3128 php ${workspace}/composer.phar install"

        sh "rm -Rf ${workspace}/${SourceFolder.MAGENTO}/dev/tests/functional/var/log/*"


        parallel(

                "directretail" : {

                    runUITest(SourceFolder, imageName, 'directretail', 'phpunit.directretail.xml', uiTestsPhpunitDirectretailFilter, parseStubs(uiTestsStubs))
                },

                "onlineretail" : {

                    runUITest(SourceFolder, imageName, 'onlineretail', 'phpunit.online.xml', uiTestsPhpunitOnlineretailFilter, parseStubs(uiTestsStubs))
                }
        )
    }
}
