configRepositoryUrl = "https://Andrew_Ziblickiy@bitbucket.org/Andrew_Ziblickiy/jenkins.git"
gitCredentials = "Andrew_Ziblickiy"
def Configuration
def Container
def StashWrapper
def PullRequestProgressSymbol
def StashApi

properties([
	buildDiscarder(logRotator(artifactDaysToKeepStr: '', artifactNumToKeepStr: '', daysToKeepStr: '', numToKeepStr: '5')),
	disableConcurrentBuilds(),
	parameters([
		string(defaultValue: '', description: '', name: 'PULL_REQUEST_ID'),
		string(defaultValue: '', description: '', name: 'PULL_REQUEST_USER_DISPLAY_NAME'),
		string(defaultValue: '', description: '', name: 'PULL_REQUEST_FROM_HASH'),
		string(defaultValue: '', description: '', name: 'PULL_REQUEST_TO_HASH'),
		string(defaultValue: '', description: '', name: 'PULL_REQUEST_FROM_BRANCH'),
		string(defaultValue: '', description: '', name: 'PULL_REQUEST_TO_BRANCH')
	])
])

def ensureBuilsIsActualForBranches(Container)
{
    if (Container.get('StashWrapper').isDestinationBranchWasModified()) {
        // Container.get('PrQueue').set(Container.get('Configuration').get('PULL_REQUEST_ID'))
		// StashWrapper.setError("Sorry, we need stop build process according to destination branch changes\n Your build will be built immediately")
		error "Sorry, we need stop build process according to destination branch changes"
	}
}

node('master') {
    try {
        stage("Init") {
			checkout changelog: false, poll: false, scm: [$class: 'GitSCM', branches: [[name: "master"]], browser: [$class: 'Stash', repoUrl: ''], doGenerateSubmoduleConfigurations: false, extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: "config"], [$class: 'CleanBeforeCheckout']], submoduleCfg: [], userRemoteConfigs: [[credentialsId: gitCredentials, url: configRepositoryUrl]]]
		    Configuration = load "config/jenkins/lib/Configuration.groovy"
		    Container = load "config/jenkins/lib/Container.groovy"
		    Container.set('Configuration', Configuration)

            Configuration.init()
            Configuration.loadGroovy([
                "config/env/common.groovy",
                'config/env/build/folders.groovy',
                "config/env/swarm/dde.groovy",
                "config/env/build/repository.groovy",
                "config/env/build/pull-request.groovy"
            ])
            sh "mkdir -p ${Configuration.get('LOGS')}"
            //  Load libraries
            StashWrapper = Container.get('StashWrapper')
            // def PrQueue = Container.get('PrQueue')
			//
            // Container.get('ImageNameFactory').createAll(Configuration)
            // PrQueue.init(Configuration)
            StashWrapper.init(Container)
			//
            // def prIdInQueue = PrQueue.get()
			//
            // if (PULL_REQUEST_ID) { //  from job parameter
            //     if (prIdInQueue) {
            //         //  if we have job in queue and have pr id for build
            //         //  we should build pr in queue and after pr from job parameter
            //         StashWrapper.postComment(
            //             PULL_REQUEST_ID,
            //             "Hey Hey, your pull request build queue number was stolen by pull request with id ${prIdInQueue}, do not warry Jenkins Ninja will start your build after :)"
            //         )
            //         PrQueue.set(PULL_REQUEST_ID)
            //         PULL_REQUEST_ID = prIdInQueue
            //     }
            // } else if (prIdInQueue) {
            //     PULL_REQUEST_ID = prIdInQueue
            //     PrQueue.set('')
            // }

			Configuration.set('PULL_REQUEST_ID', PULL_REQUEST_ID)
			Configuration.set('PULL_REQUEST_USER_DISPLAY_NAME', PULL_REQUEST_USER_DISPLAY_NAME)
			Configuration.set('PULL_REQUEST_FROM_HASH', PULL_REQUEST_FROM_HASH)
			Configuration.set('PULL_REQUEST_TO_HASH', PULL_REQUEST_TO_HASH)
			Configuration.set('PULL_REQUEST_FROM_BRANCH', PULL_REQUEST_FROM_BRANCH)
			Configuration.set('PULL_REQUEST_TO_BRANCH', PULL_REQUEST_TO_BRANCH)

			StashApi = Container.get('StashApi')
			StashApi.init(Configuration)
			PullRequestProgressSymbol = Container.get('PullRequestProgressSymbol')

			Configuration.done()

			PullRequestProgressSymbol.set(Configuration, 'PULL_REQUEST_EXECUTION_PROGRESS_PULL_SOURCE', 'executing')
			StashApi.postBuildComment()

            currentBuild.setDescription("PR# ${PULL_REQUEST_ID}, ${PULL_REQUEST_FROM_BRANCH} -> ${PULL_REQUEST_TO_BRANCH}")

            def Git = Container.get('Git')
            try {
                // StashWrapper.start()
				StashApi.setBuildStatus('INPROGRESS')
                Git.pullWithMerge(
                    Configuration.get('REPOSITORY_APPLICATION'),
                    Configuration.get('PULL_REQUEST_FROM_BRANCH'),
                    Configuration.get('PULL_REQUEST_TO_BRANCH'),
                    Configuration.get('SOURCE_APPLICATION'),
                    Configuration.get('REPOSITORY_CREDENTIALS')
                )
            } catch (e) {
                writeFile file: "${Configuration.get('LOGS')}/merge-error.log" , text: "${e}"
                // StashWrapper
                    // .unApprove()
                    // .setError("Could not merge, resolve conflicts first", "${workspace}/${Configuration.get('LOGS')}/merge-error.log")
                // ;
                error "Can not merge: ${e}"
            }

            Git.pullMagento(Configuration)
            Git.pullDocker(Configuration)
            Git.pullFoundation(Configuration)
			PullRequestProgressSymbol.set(Configuration, 'PULL_REQUEST_EXECUTION_PROGRESS_PULL_SOURCE', 'success')
        }

        stage('PHP Checks') {
			PullRequestProgressSymbol.set(Configuration, 'PULL_REQUEST_EXECUTION_PROGRESS_INIT_PHING', 'executing')
			StashApi.postBuildComment()

            def Phing = Container.get('Phing')
			Phing
                .init(Container)
                .execute("merge")
                .execute("prepare-output-folders")
            ;
			//  Copy foundation to the application dir
	        sh "cp -Rf ${Configuration.get('SOURCE_FOUNDATION')}/app/code/* ${Configuration.get('BUILD_SOURCE')}/app/code"

			def pullRequestChangeSet = StashWrapper.getChangeSet("--ext php")

			PullRequestProgressSymbol.set(Configuration, 'PULL_REQUEST_EXECUTION_PROGRESS_INIT_PHING', 'success')

			PullRequestProgressSymbol.set(Configuration, 'PULL_REQUEST_EXECUTION_PROGRESS_PHPCS', 'executing')
			PullRequestProgressSymbol.set(Configuration, 'PULL_REQUEST_EXECUTION_PROGRESS_PHPUnit', 'executing')
			PullRequestProgressSymbol.set(Configuration, 'PULL_REQUEST_EXECUTION_PROGRESS_PHPMD', 'executing')
			PullRequestProgressSymbol.set(Configuration, 'PULL_REQUEST_EXECUTION_PROGRESS_PHPCPD', 'executing')
			StashApi.postBuildComment()

            def isPhingOk = Phing.test(
                pullRequestChangeSet,
                { success, errorMessage, logFile, name ->
					if (success) {
						PullRequestProgressSymbol.set(Configuration, "PULL_REQUEST_EXECUTION_PROGRESS_${name}", 'success')
					} else {
						PullRequestProgressSymbol.set(Configuration, "PULL_REQUEST_EXECUTION_PROGRESS_${name}", 'fail')
						echo "Failed with testing ${name}"
					}
					StashApi.postBuildComment()
                },
				true
            )

			if (!isPhingOk) {
				error "PHP Checks failed"
			}

			PullRequestProgressSymbol.set(Configuration, 'PULL_REQUEST_EXECUTION_PROGRESS_PHPUnitCoverage', 'executing')
			StashApi.postBuildComment()

			Phing.execute(
				"validate-code-coverage -Dconfig.phpunit.accepted_coverage=${Configuration.get('PR_CODE_COVERAGE_LEVEL')}",
				"Code_Coverage",
				"Code coverage is below then accepted ${Configuration.get('PR_CODE_COVERAGE_LEVEL')}%",
				{ success, errorMessage, logFile, name ->
					if (success) {
						PullRequestProgressSymbol.set(Configuration, "PULL_REQUEST_EXECUTION_PROGRESS_PHPUnitCoverage", 'success')
					} else {
						PullRequestProgressSymbol.set(Configuration, "PULL_REQUEST_EXECUTION_PROGRESS_PHPUnitCoverage", 'fail')
						echo "Failed with testing ${name}"
					}
					StashApi.postBuildComment()
                }
			)

            ensureBuilsIsActualForBranches(Container)
		}

		stage('Build Helper Image') {
			PullRequestProgressSymbol.set(Configuration, 'PULL_REQUEST_EXECUTION_PROGRESS_BUILDING_STATIC_GENERATION_CONTAINER', 'executing')
			StashApi.postBuildComment()
			Container.get('ImageBuilder').buildContentBuilderImage(Configuration, Container)
			PullRequestProgressSymbol.set(Configuration, 'PULL_REQUEST_EXECUTION_PROGRESS_BUILDING_STATIC_GENERATION_CONTAINER', 'success')
			StashApi.postBuildComment()
		}

        def jsChangeset = StashWrapper.getChangeSet("--ext js")

        stage('JS CS') {

			if (jsChangeset == '') {
				PullRequestProgressSymbol.set(Configuration, 'PULL_REQUEST_EXECUTION_PROGRESS_JSCS', 'skip')
				return
			}
			PullRequestProgressSymbol.set(Configuration, 'PULL_REQUEST_EXECUTION_PROGRESS_JSCS', 'executing')
			StashApi.postBuildComment()

            Container.get('Js').jscs(
                Configuration,
                jsChangeset,
                { success, errorMessage, logFile ->
                    if (success) {
						PullRequestProgressSymbol.set(Configuration, 'PULL_REQUEST_EXECUTION_PROGRESS_JSCS', 'success')
					} else {
						PullRequestProgressSymbol.set(Configuration, 'PULL_REQUEST_EXECUTION_PROGRESS_JSCS', 'fail')
					}
					StashApi.postBuildComment()
                }
            )

			ensureBuilsIsActualForBranches(Container)
		}

		stage('JS Lint') {

			if (jsChangeset == '') {
				PullRequestProgressSymbol.set(Configuration, 'PULL_REQUEST_EXECUTION_PROGRESS_JSLINT', 'skip')
				StashApi.postBuildComment()
				return
			}
			PullRequestProgressSymbol.set(Configuration, 'PULL_REQUEST_EXECUTION_PROGRESS_JSLINT', 'executing')
			StashApi.postBuildComment()

            Container.get('Js').eslint(
                Configuration,
                jsChangeset,
                { success, errorMessage, logFile ->
					if (success) {
						PullRequestProgressSymbol.set(Configuration, 'PULL_REQUEST_EXECUTION_PROGRESS_JSLINT', 'success')
					} else {
						PullRequestProgressSymbol.set(Configuration, 'PULL_REQUEST_EXECUTION_PROGRESS_JSLINT', 'fail')
					}
					StashApi.postBuildComment()
                }
            )

			ensureBuilsIsActualForBranches(Container)
		}

		stage('Generating Assets') {
			PullRequestProgressSymbol.set(Configuration, 'PULL_REQUEST_EXECUTION_PROGRESS_MAGENTO_INSTALLATION_STATIC_GENERATION', 'executing')
			StashApi.postBuildComment()
			Configuration.set('STATIC_GENERATION_OPTIONS', '"--configure --install-magento --create-configs --static-deploy  --production"')

			try {
				Container.get('ContentBuilder').generateStaticContent(Configuration, Container)
			} catch (e) {
				PullRequestProgressSymbol.set(Configuration, 'PULL_REQUEST_EXECUTION_PROGRESS_MAGENTO_INSTALLATION_STATIC_GENERATION', 'fail')
				throw e
			}

			PullRequestProgressSymbol.set(Configuration, 'PULL_REQUEST_EXECUTION_PROGRESS_MAGENTO_INSTALLATION_STATIC_GENERATION', 'success')
			StashApi.postBuildComment()
		}

		stage('JS Unit') {
			return
			PullRequestProgressSymbol.set(Configuration, 'PULL_REQUEST_EXECUTION_PROGRESS_JSUNIT', 'executing')
			StashApi.postBuildComment()

            Container.get('Js').jsunit(
                Configuration,
                { success, errorMessage, logFile ->
					if (success) {
						PullRequestProgressSymbol.set(Configuration, 'PULL_REQUEST_EXECUTION_PROGRESS_JSUNIT', 'success')
					} else {
						PullRequestProgressSymbol.set(Configuration, 'PULL_REQUEST_EXECUTION_PROGRESS_JSUNIT', 'fail')
					}
					StashApi.postBuildComment()
                }
            )

			ensureBuilsIsActualForBranches(Container)
		}

		stage('Notify') {
			StashApi.setBuildStatus('SUCCESSFUL')
			Configuration.set('PULL_REQUEST_EXECUTION_STATE', 'success')
			StashApi.postBuildComment()

			ensureBuilsIsActualForBranches(Container)

			StashWrapper.approve()
			StashWrapper.merge()
		}

    } catch (e) {
		//Configuration.set('PULL_REQUEST_EXECUTION_STATE', 'FAILED :fire:')
		//StashApi.setBuildStatus('FAILED')
		//StashApi.postBuildComment()
		//StashWrapper.unApprove()
		error "Build failed with error: ${e}"
		e.printStackTrace()
    } finally {
		stage('Clean') {
			try {
				sh "rm -Rf ${Configuration.get('GENERATED_STATIC')} ${Configuration.get('GENERATED_VAR')} ${Configuration.get('GENERATED_MEDIA')}"
			} catch (e) {
				// do nothing
			}
		}
    }
}
