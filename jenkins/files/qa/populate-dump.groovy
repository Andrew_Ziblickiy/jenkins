configRepositoryUrl = "https://bitbucket.nl.corp.tele2.com/scm/mtel/tel2-m2-config.git"
gitCredentials = "nlsvc-jenkins-docker"
def Container
def Configuration
def MysqlContainer
def dumpFile

properties ([
		buildDiscarder(
				logRotator(
						artifactDaysToKeepStr: '',
						artifactNumToKeepStr: '',
						daysToKeepStr: '3',
						numToKeepStr: '5'
				)
		),
		disableConcurrentBuilds(),
        parameters ([
            choice(
                choices: "dev\nint\nuat\nprd",
                description: 'Environment from',
                name: 'fromEnv'
            ),
            choice(
                choices: "dev\nint\nprf\nuat\ntst\nneon",
                description: 'Environment to',
                name: 'toEnv'
            )
        ])
])

node('unix_01') {

    checkout changelog: false, poll: false, scm: [$class: 'GitSCM', branches: [[name: "master"]], browser: [$class: 'Stash', repoUrl: ''], doGenerateSubmoduleConfigurations: false, extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: "config"], [$class: 'CleanBeforeCheckout']], submoduleCfg: [], userRemoteConfigs: [[credentialsId: gitCredentials, url: configRepositoryUrl]]]
    Configuration = load "config/jenkins/lib/Configuration.groovy"
    Container = load "config/jenkins/lib/Container.groovy"
    Container.set('Configuration', Configuration)

    stage('Init') {
        Configuration.init()
        Configuration.loadGroovy([
            'config/env/common.groovy',
            'config/env/build/folders.groovy',
            'config/env/build/services.groovy'
        ])
        Configuration.done()
        MysqlContainer = Container.get('MysqlContainer')
    }

    try {
        stage('Start Mysql Container') {
            MysqlContainer.start()
        }

        stage('Making Dump') {
            dumpFile = Container.get('MysqlDumpFactory').create(fromEnv, MysqlContainer)
            echo "Dumpe file is: ${dumpFile}"
        }

        stage('Configure Dump') {
            dumpFile = Container.get('MysqlDumpConfigurator').configure(Configuration, MysqlContainer, dumpFile, fromEnv, toEnv)
        }

        stage('Install') {
            Container.get('MysqlDumpInstaller').install(Container, dumpFile, toEnv)
        }

		stage('Setup upgrade') {
			echo Container.get('RemoteApi').setupUpgrade(Configuration)
		}

		stage('Clean Cache') {
			echo Container.get('RemoteApi').cleanCache(Configuration)
		}

    } finally {
        stage ('Stop Container') {
            MysqlContainer.stop()
        }
    }
}
