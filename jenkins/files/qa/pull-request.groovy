configRepositoryUrl = "https://bitbucket.nl.corp.tele2.com/scm/mtel/tel2-m2-config.git"
gitCredentials = "nlsvc-jenkins-docker"
def Configuration
def Container

properties(
    [
        pipelineTriggers([
            bitbucketpr(
                projectPath : 'https://bitbucket.nl.corp.tele2.com/',
                approveIfSuccess: true,
                branchesFilter: '',
                branchesFilterBySCMIncludes: false,
                cancelOutdatedJobs: true,
                checkDestinationCommit: true,
                ciKey: 'm2tele2-pr-jenkins',
                ciName: 'Jenkins',
                ciSkipPhrases: 'WIP,[WIP]',
                commentTrigger: 'test this please',
                credentialsId: "nlsvc-jenkins-docker",
                cron: '* * * * *',
                password: '',
                repositoryName: 'tel2-m2-app',
                repositoryOwner: 'mtel',
                username: ''
            )
        ]),
        pollSCM('* * * * *')
    ]
)

node {

    checkout([$class: 'GitSCM', branches: [[name: "*/${sourceBranch}"]], doGenerateSubmoduleConfigurations: false, extensions: [[$class: 'PreBuildMerge', options: [mergeRemote: 'origin', mergeTarget: "${targetBranch}"]]], submoduleCfg: [], userRemoteConfigs: [[credentialsId: 'nlsvc-jenkins-docker', url: 'https://bitbucket.nl.corp.tele2.com/scm/mtel/tel2-m2-app.git']]])

    stage('Init') {
        // echo "${pullRequestId}"
        sh "env"
    }
}
