//  Jenkins
gitCredentials = "nlsvc-jenkins-docker"
configRepositoryUrl = "http://t2nl-devtooling.itservices.lan:7990/scm/mtel/tel2-m2-config.git"

//  Application
appFolder = "app"
appRepostoryUrl = "http://t2nl-devtooling.itservices.lan:7990/scm/mtel/tel2-m2-app.git"
dockerFolder = "docker"
dockerRepositoryUrl = "http://t2nl-devtooling.itservices.lan:7990/scm/mtel/tel2-docker-env.git"
env = "int"

//  Docker
dockerConfig = [
    "int" : [
        "registryHost" : "dtrdd.nl.corp.tele2.com:9443",
        "registryPath" : "/development/mag-",
        "clientBundle" : "DOCKER_HOST=tcp://dockerdde.nl.corp.tele2.com:8443 DOCKER_TLS_VERIFY=1 DOCKER_CERT_PATH=/var/jenkins_home/client-bundles/dta-dmz-client",
        "ucpOwner" : "nlsvc-jenkins-docker",
        "ucpGroup" : "/Shared/Development",
        "dtrCredentials" : "nlsvc-jenkins-docker",
        "imagePullTag" : "latest",
        "imageTag" : "int"
    ],
    "prf" : [
        "registryHost" : "dtrdd.nl.corp.tele2.com:9443",
        "registryPath" : "/development/mag-",
        "clientBundle" : "DOCKER_HOST=tcp://dockerdde.nl.corp.tele2.com:8443 DOCKER_TLS_VERIFY=1 DOCKER_CERT_PATH=/var/jenkins_home/client-bundles/dta-dmz-client",
        "ucpOwner" : "nlsvc-jenkins-docker",
        "ucpGroup" : "/Shared/Development",
        "dtrCredentials" : "nlsvc-jenkins-docker",
        "imagePullTag" : "int",
        "imageTag" : "prf"
    ],
    "uat" : [
        "registryHost" : "dtrdd.nl.corp.tele2.com:9443",
        "registryPath" : "/development/mag-",
        "clientBundle" : "DOCKER_HOST=tcp://dockerdde.nl.corp.tele2.com:8443 DOCKER_TLS_VERIFY=1 DOCKER_CERT_PATH=/var/jenkins_home/client-bundles/dta-dmz-client",
        "ucpOwner" : "nlsvc-jenkins-docker",
        "ucpGroup" : "/Shared/Development",
        "dtrCredentials" : "nlsvc-jenkins-docker",
        "imagePullTag" : "prf",
        "imageTag" : "release",
    ]
]

//  Appdynamics
appdynamicsConfig = [
    "int" : [
        "CONTROLLER_HOST" : "appdcontrol.tele2.com",
        "CONTROLLER_PORT" : 443,
        "APPLICATION_NAME" : "nl-magento-int",
        "TIER_NAME" : "magento-php",
        "ACCOUNT_NAME" : "test",
        "ACCOUNT_ACCESS_KEY" : "91f57f69-8566-4e65-839a-7202a734470c"
    ],
    "prf" : [
        "CONTROLLER_HOST" : "appdcontrol.tele2.com",
        "CONTROLLER_PORT" : 443,
        "APPLICATION_NAME" : "nl-magento-prf",
        "TIER_NAME" : "magento-php",
        "ACCOUNT_NAME" : "test",
        "ACCOUNT_ACCESS_KEY" : "91f57f69-8566-4e65-839a-7202a734470c"
    ],
    "uat" : [
        "CONTROLLER_HOST" : "appdcontrol.tele2.com",
        "CONTROLLER_PORT" : 443,
        "APPLICATION_NAME" : "nl-magento-uat",
        "TIER_NAME" : "magento-php",
        "ACCOUNT_NAME" : "test",
        "ACCOUNT_ACCESS_KEY" : "91f57f69-8566-4e65-839a-7202a734470c"
    ]
]

//  Docker swarm
stackName = "m2tele2"
swarmAppEndpoint = "dde.nl.corp.tele2.com"
swarmAppHost = ""

shopImage = ""
shopTunerImage = ""
varnishImage = ""
nginxImage = ""

def Docker
def DockerConfiguration

properties([buildDiscarder(logRotator(artifactDaysToKeepStr: '', artifactNumToKeepStr: '', daysToKeepStr: '3', numToKeepStr: '3')), disableConcurrentBuilds(), pipelineTriggers([])])

node{
    try {
    	stage('Request Release'){
    	    timeout(time: 15, unit: 'MINUTES') {
    	        input message: "Want to promote to ${ENVIRONMENT}?", ok: 'Promote'//, submitter: 'app_jenkins_unix_release'
    	        node {

                    stage('Init') {
                        
                        swarmAppHost = "${ENVIRONMENT}.${swarmAppEndpoint}"

                        shopImage = "${dockerRegistryHostInt}${dockerRegistryPathInt}shop:int"
                        shopTunerImage = "${dockerRegistryHostInt}${dockerRegistryPathInt}shop-tuner:int"
                        varnishImage = "${dockerRegistryHostInt}${dockerRegistryPathInt}varnish:int"
                        nginxImage = "${dockerRegistryHostInt}${dockerRegistryPathInt}nginx:int"
                    }

                    stage('Pull image') {

                        withCredentials([usernamePassword(credentialsId: "${dtrIntCredentials}", usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
                	        sh "docker login -u ${USERNAME} -p ${PASSWORD} ${dockerRegistryHostInt}"
                	        sh "docker pull ${dockerRegistryHostInt}${dockerRegistryPathInt}shop:${dockerImagePullTag}"
                            sh "docker pull ${dockerRegistryHostInt}${dockerRegistryPathInt}shop-tuner:${dockerImagePullTag}"
                	        sh "docker pull ${dockerRegistryHostInt}${dockerRegistryPathInt}nginx:${dockerImagePullTag}"
                	        sh "docker pull ${dockerRegistryHostInt}${dockerRegistryPathInt}varnish:${dockerImagePullTag}"
                	        sh "docker logout ${dockerRegistryHostInt}"
                	    }
                    }

                    stage('Tag images for prf') {
                        sh "docker image tag ${dockerRegistryHostInt}${dockerRegistryPathInt}shop:${dockerImagePullTag} ${shopImage}"
                        sh "docker image tag ${dockerRegistryHostInt}${dockerRegistryPathInt}shop-tuner:${dockerImagePullTag} ${shopTunerImage}"
                        sh "docker image tag ${dockerRegistryHostInt}${dockerRegistryPathInt}varnish:${dockerImagePullTag} ${varnishImage}"
                        sh "docker image tag ${dockerRegistryHostInt}${dockerRegistryPathInt}nginx:${dockerImagePullTag} ${nginxImage}"
                    }

                    stage('Push tagged images') {
                        sleep 5

                        withCredentials([usernamePassword(credentialsId: "${dtrIntCredentials}", usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
                	        sh "docker login -u ${USERNAME} -p ${PASSWORD} ${dockerRegistryHostInt}"
                            sh "docker image push ${shopImage}"
                            sh "docker image push ${shopTunerImage}"
                            sh "docker image push ${varnishImage}"
                            sh "docker image push ${nginxImage}"
                	        sh "docker logout ${dockerRegistryHostInt}"
                	    }
                    }

                    stage('Deploy to Int') {
                        checkout changelog: false, poll: false, scm: [$class: 'GitSCM', branches: [[name: "master"]], browser: [$class: 'Stash', repoUrl: ''], doGenerateSubmoduleConfigurations: false, extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: dockerFolder], [$class: 'CleanBeforeCheckout']], submoduleCfg: [], userRemoteConfigs: [[credentialsId: gitCredentials, url: dockerRepositoryUrl]]]
                        checkout changelog: false, poll: false, scm: [$class: 'GitSCM', branches: [[name: "master"]], browser: [$class: 'Stash', repoUrl: ''], doGenerateSubmoduleConfigurations: false, extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: "config"], [$class: 'CleanBeforeCheckout']], submoduleCfg: [], userRemoteConfigs: [[credentialsId: gitCredentials, url: configRepositoryUrl]]]

                        def ymlFolder = "${dockerFolder}/docker/tele2_dev"
                        def parsedYaml = "${ymlFolder}/${stackName}-${env}-docker-compose.yml"

                        def yaml = readFile "${ymlFolder}/docker-compose.yml"
                        yaml = yaml.replaceAll("#NGINX_IMAGE_NAME#", nginxImage)
                        yaml = yaml.replaceAll("#PHP_IMAGE_NAME#", shopImage)
                        yaml = yaml.replaceAll("#VARNISH_IMAGE_NAME#", varnishImage)
                        yaml = yaml.replaceAll("#TUNER_IMAGE_NAME#", shopTunerImage)
                        yaml = yaml.replaceAll("#ENV#", env)
                		//	Appdynamics
                		yaml = yaml.replaceAll("#APPDYNAMICS_TIER_NAME#", appdynamicsConfig.TIER_NAME)
                		yaml = yaml.replaceAll("#APPDYNAMICS_NODE_NAME#", swarmAppHost)
                		yaml = yaml.replaceAll("#APPDYNAMICS_ACCOUNT_NAME#", appdynamicsConfig.ACCOUNT_NAME)
                		yaml = yaml.replaceAll("#APPDYNAMICS_ACCOUNT_ACCESS_KEY#", appdynamicsConfig.ACCOUNT_ACCESS_KEY)
						yaml = yaml.replaceAll("#APPDYNAMICS_APPLICATION_NAME#", appdynamicsConfig.APPLICATION_NAME)

                		//	Appdynamics END
                        yaml = yaml.replaceAll("#DB_HOST#", "magentodb.${env}.nl.corp.tele2.com")
                        yaml = yaml.replaceAll("#UCPOWNER#", dockerUCPOwner)
                        yaml = yaml.replaceAll("#UCPGROUP#", dockerUCPGroup)
                		yaml = yaml.replaceAll("#PHP_DEPLOY_REPLICAS#", "2")
                		yaml = yaml.replaceAll("#MAGE_MODE#", "production")
                        //yaml = yaml.replaceAll("#HOST#", "http://${swarmAppHost}")

                        yaml = yaml.replaceAll("#STACK_NAME#", "${stackName}_${env}")

                        //  Parse Hosts
                        def hostNameParser = load "config/jenkins/HostNameParser.groovy"
                        yaml = hostNameParser.parse(yaml, "int", swarmAppEndpoint)

                		yaml = yaml.replaceAll("#APPLICATION_DOMAIN#", "shop.${swarmAppHost}")
                        yaml = yaml.replaceAll("#DBPREFIX#", "magento")
                        writeFile file: parsedYaml , text: yaml

                        echo "$yaml"

                        def cmd = "${dockerClientBundle} docker stack deploy -c ${parsedYaml} ${stackName}_${env}"
                        sh "${cmd}"
                    }

                    stage("Wait for an Application") {

                        Docker = load "config/jenkins/Docker.groovy"
                        DockerConfiguration = load "config/jenkins/DockerConfiguration.groovy"

                		if (!Docker.isApplicationUp("${env}", DockerConfiguration)) {
                			error "Application is not ready or we exceed our timeout limits"
                		}

                		echo "Application is up"
                	}

                    stage("Update varnish") {
                        Docker.updateService("${env}", 'varnish', DockerConfiguration)
                        sleep 3
                    }

                     stage ("Wurmup cache") {
                         sh "php config/crawler/warmup.php --url http://espresso-sit.tele2.nl/mobiel/smartphones/ --docker-bundle \"${dockerClientBundle}\" --track-service m2tele2_int_tuner"
                     }
    	        }
    	    }
    	}
    } catch (error) {
    	throw error
    }
}
