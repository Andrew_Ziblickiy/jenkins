configRepositoryUrl = "https://bitbucket.nl.corp.tele2.com/scm/mtel/tel2-m2-config.git"
gitCredentials = "nlsvc-jenkins-docker"
def Configuration
def Container

properties([buildDiscarder(logRotator(artifactDaysToKeepStr: '', artifactNumToKeepStr: '', daysToKeepStr: '3', numToKeepStr: '5')), disableConcurrentBuilds(), pipelineTriggers([])])

node('unix_docker') {

    stage("Init") {

        checkout changelog: false, poll: false, scm: [$class: 'GitSCM', branches: [[name: "master"]], browser: [$class: 'Stash', repoUrl: ''], doGenerateSubmoduleConfigurations: false, extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: "config"], [$class: 'CleanBeforeCheckout']], submoduleCfg: [], userRemoteConfigs: [[credentialsId: gitCredentials, url: configRepositoryUrl]]]
        Configuration = load "config/jenkins/lib/Configuration.groovy"
        Container = load "config/jenkins/lib/Container.groovy"
        Container.set('Configuration', Configuration)

        Configuration.init()
        Configuration.loadGroovy([
            "config/env/common.groovy",
            'config/env/build/folders.groovy',
            "config/env/swarm/dde.groovy",
			"config/env/build/repository.groovy",
            "config/env/neon.groovy"
        ])

        Container.get('ImageNameFactory').createAll(Configuration)
        Container.get('Git').pullAll(Configuration)

        Configuration.done()
		echo Configuration.toString()
        sh "mkdir -p ${Configuration.get('LOGS')}"
    }

    Container
        .get('Stage')
        .stageStaticGeneration(Container)
        .stageApplication(Container)
        .stageNginxVarnish(Container)
        .stagePushImages(Container)
        .stageDeploy(Container)
		.stageWaitForTuner(Container)
}
