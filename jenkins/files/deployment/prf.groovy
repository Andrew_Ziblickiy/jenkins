configRepositoryUrl = "https://bitbucket.nl.corp.tele2.com/scm/mtel/tel2-m2-config.git"
gitCredentials = "nlsvc-jenkins-docker"
def Configuration
def Container
def Docker

properties([buildDiscarder(logRotator(artifactDaysToKeepStr: '', artifactNumToKeepStr: '', daysToKeepStr: '3', numToKeepStr: '3')), disableConcurrentBuilds(), pipelineTriggers([])])

node{
    try {
    	stage('Request Release Prf'){
    	    timeout(time: 15, unit: 'MINUTES') {
    	        input message: 'Want to promote to Prf?', ok: 'Promote'//, submitter: 'app_jenkins_unix_release'
    	        node {

                    stage('Init') {
                        checkout changelog: false, poll: false, scm: [$class: 'GitSCM', branches: [[name: "master"]], browser: [$class: 'Stash', repoUrl: ''], doGenerateSubmoduleConfigurations: false, extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: "config"], [$class: 'CleanBeforeCheckout']], submoduleCfg: [], userRemoteConfigs: [[credentialsId: gitCredentials, url: configRepositoryUrl]]]
                        Configuration = load "config/jenkins/lib/Configuration.groovy"
                        Container = load "config/jenkins/lib/Container.groovy"
                        Container.set('Configuration', Configuration)

                        Configuration.init()
                        Configuration.loadGroovy([
                            "config/env/common.groovy",
                            'config/env/build/folders.groovy',
                            "config/env/swarm/dde.groovy",
                			"config/env/build/repository.groovy",
                            "config/env/int.groovy" //  load dev env for pulling images
                        ])

                        Container.get('ImageNameFactory').createAll(Configuration)
                        Docker = Container.get('Docker')

                        Configuration.done()
                        sh "mkdir -p ${Configuration.get('LOGS')}"
                    }

                    stage('Pull image') {
                        Docker.pull(Configuration)
                    }

                    stage('Tag images') {

                        //  loading int configuration
                        Configuration.loadGroovy([
                            "config/env/prf.groovy"
                        ])

                        Docker.tag(Container)
                        Configuration.set('DOCKER_IMAGE_TUNER', Configuration.get('DOCKER_IMAGE_APPLICATION'))
                        Configuration.set('DOCKER_IMAGE_ADMIN', Configuration.get('DOCKER_IMAGE_APPLICATION'))
                    }

                    stage('Push tagged images') {
                        Docker.push(Configuration)
                    }

                    stage('Deploy to Prf') {
                        Container.get('Git').pullDocker(Configuration)
                        Docker.deploy(Container)
                    }

                    Container.get('Stage').stageWaitForTuner(Container)
    	        }
    	    }
    	}
    } catch (error) {
    	throw error
    }
}
