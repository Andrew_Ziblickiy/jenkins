class Repository {
	final static MAGENTO = "http://epbyminw0858.minsk.epam.com:8083/scm/mtel/tel2-m2-ee.git"
	final static APPLICATION = "http://epbyminw0858.minsk.epam.com:8083/scm/mtel/tel2-m2-app.git"
	final static DOCKER = "http://epbyminw0858.minsk.epam.com:8083/scm/mtel/tel2-docker-env.git"
	final static API = "http://epbyminw0858.minsk.epam.com:8083/scm/mtel/tel2-test-runner.git"
	final static FOUNDATION = "http://epbyminw0858.minsk.epam.com:8083/scm/mtel/tel2-m2-foundation.git"
	final static CONFIG = "http://epbyminw0858.minsk.epam.com:8083/scm/mtel/tel2-m2-config.git"

	final static CREDENTIALS = "tele2-stash"

	static void pull(rep, branch, folder) {
		Context.script.checkout changelog: false, poll: false, scm: [$class: 'GitSCM', branches: [[name: branch]], browser: [$class: 'Stash', repoUrl: ''], doGenerateSubmoduleConfigurations: false, extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: folder]], submoduleCfg: [], userRemoteConfigs: [[credentialsId: CREDENTIALS, url: rep]]]
	}
}

class SourceFolder {
	final static MAGENTO = "magento"
    final static APPLICATION = "application"
    final static DOCKER = "docker"
    final static API = "api"
    final static CONFIG = "config"
    final static FOUNDATION = "foundation"
    final static GENERATED = "generated"
    final static GENERATED_STATIC = "generated/generated"
    final static GENERATED_VAR = "generated/var"
    final static GENERATED_MEDIA = "generated/media"
    final static CONTENT_BUILDER = "docker/docker/content-builder"
}

class Context {
	static script;
}


//  Repositories
rep_magento = "http://epbyminw0858.minsk.epam.com:8083/scm/mtel/tel2-m2-ee.git"
rep_application = "http://epbyminw0858.minsk.epam.com:8083/scm/mtel/tel2-m2-app.git"
rep_docker = "http://epbyminw0858.minsk.epam.com:8083/scm/mtel/tel2-docker-env.git"
rep_api = "http://epbyminw0858.minsk.epam.com:8083/scm/mtel/tel2-test-runner.git"
rep_foundation = "http://epbyminw0858.minsk.epam.com:8083/scm/mtel/tel2-m2-foundation.git"
rep_ = "http://epbyminw0858.minsk.epam.com:8083/scm/mtel/tel2-m2-foundation.git"
git_credentials = "tele2-stash"

//  Folders
dir_magento = "magento"
dir_application = "application"
dir_docker = "docker"
dir_api = "api"
dir_foundation = "foundation"

//  Docker
registry_host = ""
php_source_image = "php:7.0-fpm"

//  Image
application_image_tag = "latest"
application_image_name = "m2tele2-php-fpm"
application_image = "${application_image_name}:${application_image_tag}"
magento_image_tag = "latest"
magento_image_name = "magento"
magento_image = "${magento_image_name}:${magento_image_tag}"
selenium_image = "m2tele2-selenium:latest"
nginx_image = "m2tele2-nginx:latest"
varnish_image = "m2tele2-varnish:latest"

//  Api
api_app_port = "8080"

phing = "phing-2.15.2.phar"

def buildDockerMagentoImage() {

	def commitFile = "magento_latest_commit_id"
	def commit = ''
	def exists = fileExists "${commitFile}"
	def magentoDockerFile = "${dir_docker}/docker/${STACK_ENV}/php-fpm/MagentoDockerfile"
	def magentoDockerFileExists = fileExists "${magentoDockerFile}"

	if (magentoDockerFileExists) {
		//  check if commit file exists
        if (exists) {
            commit = readFile "${commitFile}"
        }

        isImageExists = sh returnStatus: true, script: "docker images | grep ${magento_image_name} | grep ${magento_image_tag}"
        currentCommit = sh returnStdout: true, script: "cd $dir_magento && git rev-parse --short HEAD"

        if (currentCommit != commit || isImageExists != 0) {

			echo "Build magento image"

            //  Copy Docker file to magento folder
            sh "cp -f ${magentoDockerFile} ${dir_magento}"

            //  we need to build magento image
            sh returnStatus: true, script: "docker build -f ${dir_magento}/MagentoDockerfile -t ${magento_image} ${dir_magento}"

            //  save latest commit id
            writeFile file: commitFile, text: currentCommit
        } else {
            echo "It seems like magento container have the latest code"
        }
	}
}

def buildApplicationImage() {

	sh "mkdir -p ${dir_application}/docker ${dir_application}/container-api"
	sh "cp -Rf ${dir_docker}/docker/${STACK_ENV}/* ${dir_application}/docker"
	sh "cp -Rf ${dir_api}/src/* ${dir_application}/container-api"

	def applicationDockerFile = "${dir_application}/docker/php-fpm/Dockerfile"
	def dockerfile = readFile applicationDockerFile
    dockerfile = dockerfile.replaceAll("#MagentoImage#", "${magento_image}")
    writeFile file: applicationDockerFile , text: dockerfile

    //  Create deploy info
    info = "Application=${GIT_BRANCH_APP};Magento=${GIT_BRANCH_MAGENTO_EE}"
    writeFile file: "${dir_application}/DeployInfo.conf", text: info

    //  Copy foundation to the application dir
    sh "cp -Rf ${dir_foundation}/app/code/* ${dir_application}/app/code"

	sh "docker build -f ${dir_application}/docker/php-fpm/Dockerfile -t ${application_image} ${dir_application}"
}

def deployApplicationStack(stackName) {

	def ymlFolder = "${dir_docker}/docker/${STACK_ENV}"

	def yaml = readFile "${ymlFolder}/docker-compose.yml"
    yaml = yaml.replaceAll("#SELENIUM_IMAGE_NAME#", selenium_image)
    yaml = yaml.replaceAll("#NGINX_IMAGE_NAME#", nginx_image)
    yaml = yaml.replaceAll("#PHP_IMAGE_NAME#", application_image)
    yaml = yaml.replaceAll("#VARNISH_IMAGE_NAME#", varnish_image)
    writeFile file: "${ymlFolder}/${stackName}-docker-compose.yml" , text: yaml

	if (params.REMOVE_STACK) {

		status = sh returnStatus: true, script: "docker stack ls | grep ${stackName}"

		if (status == 0) {
			sh "docker stack rm ${stackName}"
            sleep 30
		}
	}

    sh "docker stack deploy -c ${ymlFolder}/${stackName}-docker-compose.yml ${stackName}"
}

def owasp(hostname){
	stage('Security Test, OWASPzap') {
		mountPoint = "/tmp/owasp"
		sh "mkdir -p ${mountPoint}"

	    // Run zap scanner
	    def filenumber = new Random().nextInt()
	    def reportname = "report${filenumber}.html"
	    def containerid = sh returnStdout: true, script: "docker run --rm -v ${mountPoint}:/zap/wrk/:rw -t -d --dns 10.6.0.16 -u root owasp/zap2docker-stable zap-baseline.py -t ${hostname} -i -m5 -a -r ${reportname}"
		isContainerWorking = 1
		retry = 0
		timeout = 20
		retryLimit = 25
	    while(isContainerWorking == 1) {
			if (retry > retryLimit) {
				error('Took to long for the checking application via OWASPzap')
			}

			returnCode = sh returnStatus: true, script: "1>/dev/null 2>&1 docker inspect ${containerid}"
			if (returnCode != 0) {
				sh "cp ${mountPoint}/${reportname} ${reportname}"
				isContainerWorking = 0
			}
			sleep "${timeout}".toInteger()
	        retry++
	    }

	    // obtain the report
	    publishHTML (target: [
	      allowMissing: false,
	      alwaysLinkToLastBuild: false,
	      keepAll: true,
	      reportDir: '',
	      reportFiles: "${reportname}",
	      reportName: "OWASPzap Report"
	    ])
    }
}

properties ([
	buildDiscarder(
		logRotator(
			artifactDaysToKeepStr: '',
			artifactNumToKeepStr: '',
			daysToKeepStr: '',
			numToKeepStr: '10'
		)
	),
    parameters ([
        string (
            defaultValue: 'epam_bison',
            description: 'Stack Environment',
            name: 'STACK_ENV'
        ),
        string (
            defaultValue: '17S10',
            description: 'Application Branch',
            name: 'GIT_BRANCH_APP'
        ),
        string (
            defaultValue: 'develop',
            description: 'Magento EE Branch',
            name: 'GIT_BRANCH_MAGENTO_EE'
        ),
        string (
            defaultValue: 'master',
            description: 'Docker environment Branch',
            name: 'GIT_BRANCH_DOCKER_ENV'
        ),
        string (
            defaultValue: 'master',
            description: 'Container Api Branch',
            name: 'GIT_BRANCH_CONTAINER_API'
        ),
        string (
            defaultValue: 'develop',
            description: 'Foundation Branch',
            name: 'GIT_BRANCH_FOUNDATION'
        ),
        string(
            defaultValue: 'build',
            description: 'Build directory',
            name: 'BUILD_DIR'
        ),
        string(
            defaultValue: '${BUILD_DIR}/src',
            description: 'Source code folder',
            name: 'SRC'
        ),
        string(
            defaultValue: 'epam.tele2.dev',
            description: 'Domain',
            name: 'APPLICATION_DOMAIN'
        ),
        string(
            defaultValue: 'api.${APPLICATION_DOMAIN}',
            description: 'Domain',
            name: 'API_APPLICATION_DOMAIN'
        ),
        string(
            defaultValue: 'http://epbymogs0063t5.minsk.epam.com',
            description: 'Public domain which allow us to reach application from any device without modifying hosts',
            name: 'PUBLIC_DOMAIN'
        ),
        string(
            defaultValue: '7',
            description: 'Code coverage level',
            name: 'CODE_COVERAGE_LEVEL'
        ),
        booleanParam(
            defaultValue: true,
            description: 'Remove stack instead of update',
            name: 'REMOVE_STACK'
        )
    ])
])

node {
    stage('Checkout') {
        Context.script = this
        Repository.pull(Repository.MAGENTO, params.GIT_BRANCH_MAGENTO_EE, SourceFolder.MAGENTO)
        Repository.pull(Repository.APPLICATION, params.GIT_BRANCH_APP, SourceFolder.APPLICATION)
        Repository.pull(Repository.DOCKER, params.GIT_BRANCH_DOCKER_ENV, SourceFolder.DOCKER)
        Repository.pull(Repository.API, params.GIT_BRANCH_CONTAINER_API, SourceFolder.API)
        Repository.pull(Repository.FOUNDATION, params.GIT_BRANCH_FOUNDATION, SourceFolder.FOUNDATION)
        Repository.pull(Repository.CONFIG, "master", SourceFolder.CONFIG)
    }

    stage('Configure Phing') {

        sh "cp -Rf -t ./ ${SourceFolder.CONFIG}/phing/*"

        sh "mkdir -p ${SRC}"
        sh "php ${phing} init-env -Denv=jenkins -Dsrc=\$(realpath ${SRC}) -Ddir.jenkins.m2app=${SourceFolder.APPLICATION} -Ddir.jenkins.m2ee=${SourceFolder.MAGENTO} -Ddir.jenkins.m2docker=${SourceFolder.DOCKER} -Ddir.jenkins.m2tr=${SourceFolder.API} -Ddir.jenkins.m2foundation=${SourceFolder.FOUNDATION}"
        sh "php ${phing} make-configs -Dhost=${APPLICATION_DOMAIN}"
    }

    stage('Merge') {
        sh "php ${phing} merge"
        //  Copy foundation to the application dir
        sh "cp -Rf ${SourceFolder.FOUNDATION}/app/code/* ${SRC}/app/code"
    }

    stage('Local Tests') {
        sh "php ${phing} prepare-output-folders"

        parallel(
            //  Run phpcs
            "phpcs": { sh "php ${phing} phpcs" },

            //  Run phpmd
            //"phpmd": { sh "php ${phing} phpmd" },

            //  Run phpcpd
            "phpcpd": { sh "php ${phing} phpcpd" },

            //  Run phpunit
            "phpunit": { sh "php ${phing} phpunit" },
        )

        publishHTML(
            [
                allowMissing: true,
                alwaysLinkToLastBuild: false,
                keepAll: true,
                reportDir: 'output/phpunit/html',
                reportFiles: 'index.html',
                reportName: 'PHPUnit Code Coverage',
                reportTitles: ''
            ]
        )

        sh "php ${phing} validate-code-coverage -Dconfig.phpunit.accepted_coverage=${CODE_COVERAGE_LEVEL}"
    }

    stage('Build Images') {
		buildDockerMagentoImage()
		buildApplicationImage()

		dockerFolder = "${dir_docker}/docker/${STACK_ENV}"

		//  We expect that we have nginx and varnish always and we need build images
		parallel(
			//  Build nginx
			"nginx" : {
				sh "docker build -f ${dockerFolder}/nginx/Dockerfile -t ${nginx_image} ${dockerFolder}/nginx"
			},

			//  Build varnish
			"varnish" : {
				sh "docker build -f ${dockerFolder}/varnish/Dockerfile -t ${varnish_image} ${dockerFolder}/varnish"
			}
		)

		exists = fileExists "${dockerFolder}/selenium/Dockerfile"
        if (exists) {
            sh "docker build -f ${dockerFolder}/selenium/Dockerfile -t ${selenium_image} ${dockerFolder}/selenium"
        }
    }

    stage('Deploy') {
        deployApplicationStack("m2tele2_${STACK_ENV}")
    }

    stage('Wait For An Application') {
        sh "cd ${dir_api} && php phing-2.15.2.phar wait:application:up -Dcfg.app.url=${APPLICATION_DOMAIN} -Dcfg.app.port=80 -Dcfg.app.wait.attempts=40"
    }

    stage('Post Deploy Tests') {
        parallel(
            //  Run js tests
            "javascript" : {
                sh "cd ${dir_api} && php phing-2.15.2.phar run-js-tests -Dcfg.api.host=${API_APPLICATION_DOMAIN} -Dcfg.api.port=${api_app_port}"
            },

            //  Run selenium tests
            "mtf" : {
                sh "cd ${dir_api} && php phing-2.15.2.phar is:selenium:up -Dcfg.api.host=${API_APPLICATION_DOMAIN} -Dcfg.api.port=${api_app_port}"
                sh "cd ${dir_api} && php phing-2.15.2.phar test:run:mtf -Dcfg.api.host=${API_APPLICATION_DOMAIN} -Dcfg.api.port=${api_app_port}"
            }
        )
    }

    owasp(params.PUBLIC_DOMAIN)
}