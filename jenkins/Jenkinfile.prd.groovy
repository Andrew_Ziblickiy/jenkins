//  Jenkins
dtrUatCredentials = "nlsvc-jenkins-docker"
dtrPrdCredentials = "nlsvc-docker-rlsmgr"
gitCredentials = "nlsvc-jenkins-docker"

//  Application
appFolder = "app"
appRepostoryUrl = "https://bitbucket.nl.corp.tele2.com/scm/mtel/tel2-m2-app.git"
dockerFolder = "docker"
dockerRepositoryUrl = "https://bitbucket.nl.corp.tele2.com/scm/mtel/tel2-docker-env.git"
env = "prd"
configRepositoryUrl = "https://bitbucket.nl.corp.tele2.com/scm/mtel/tel2-m2-config.git"

//  Docker Registry
dockerRegistryHostUat = "dtrdd.nl.corp.tele2.com:9443"
dockerRegistryHostPrd = "dtrpd.nl.corp.tele2.com:9443"
dockerRegistryPath = "/development/mag-"

//  Docker Client Bundle
dockerClientBundle = "DOCKER_HOST=tcp://dockerpde.nl.corp.tele2.com:8443 DOCKER_TLS_VERIFY=1 DOCKER_CERT_PATH=/var/jenkins_home/client-bundles/prd-dmz-client"

//  Docker UCP
dockerUCPGroup = "/Shared/Development"

//  Docker swarm
stackName = "m2tele2"
swarmAppEndpoint = "pde.nl.corp.tele2.com"
swarmAppHost = "${swarmAppEndpoint}"

def Docker
def DockerConfiguration

properties([buildDiscarder(logRotator(artifactDaysToKeepStr: '', artifactNumToKeepStr: '', daysToKeepStr: '3', numToKeepStr: '5')), disableConcurrentBuilds(), pipelineTriggers([])])

def IsUpstream(){
    def upsteam = false
    for (cause in currentBuild.rawBuild.getCauses())
    {
        if(cause.class.toString().contains("UpstreamCause")){
            upsteam = true
        }
    }
    return upsteam
}

node {
    try {
        stage('Request Release PRD'){
            timeout(time: 15, unit: 'MINUTES') {
                if(!IsUpstream()) {
                    input message: 'Want to promote to PRD?', ok: 'Promote', submitter: 'Docker_Member_DMZ_DTA_Magento'
                }
                node {
                    def version, shopImage, shopTunerImage, varnishImage, nginxImage = ""
                    stage('Pull release image') {

                        checkout changelog: false, poll: false, scm: [$class: 'GitSCM', branches: [[name: 'uat']], browser: [$class: 'Stash', repoUrl: ''], doGenerateSubmoduleConfigurations: false, extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: appFolder], [$class: 'CleanBeforeCheckout']], submoduleCfg: [], userRemoteConfigs: [[credentialsId: gitCredentials, url: appRepostoryUrl]]]
                        version = readFile "${appFolder}/VERSION"
                        version = version.trim()

                        echo "Trying tag images with version: ${version}"

                        withCredentials([usernamePassword(credentialsId: "${dtrUatCredentials}", usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
                            sh "docker login -u ${USERNAME} -p ${PASSWORD} ${dockerRegistryHostUat}"
                            sh "docker pull ${dockerRegistryHostUat}${dockerRegistryPath}shop:${version}"
                            sh "docker pull ${dockerRegistryHostUat}${dockerRegistryPath}shop-tuner:${version}"
                            sh "docker pull ${dockerRegistryHostUat}${dockerRegistryPath}nginx:${version}"
                            sh "docker pull ${dockerRegistryHostUat}${dockerRegistryPath}varnish:${version}"
                            sh "docker logout ${dockerRegistryHostUat}"
                        }
                    }

                    stage('Tag images with release') {

                        shopImage = "${dockerRegistryHostPrd}${dockerRegistryPath}shop:${version}"
                        shopTunerImage = "${dockerRegistryHostPrd}${dockerRegistryPath}shop-tuner:${version}"
                        varnishImage = "${dockerRegistryHostPrd}${dockerRegistryPath}varnish:${version}"
                        nginxImage = "${dockerRegistryHostPrd}${dockerRegistryPath}nginx:${version}"

                        sh "docker image tag ${dockerRegistryHostUat}${dockerRegistryPath}shop:${version} ${shopImage}"
                        sh "docker image tag ${dockerRegistryHostUat}${dockerRegistryPath}shop-tuner:${version} ${shopTunerImage}"
                        sh "docker image tag ${dockerRegistryHostUat}${dockerRegistryPath}varnish:${version} ${varnishImage}"
                        sh "docker image tag ${dockerRegistryHostUat}${dockerRegistryPath}nginx:${version} ${nginxImage}"
                    }

                    stage('Push tagged images') {
                        withCredentials([usernamePassword(credentialsId: "${dtrPrdCredentials}", usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
                            sh "docker login -u ${USERNAME} -p ${PASSWORD} ${dockerRegistryHostPrd}"
                            withCredentials([string(credentialsId: 'REPOSITORY_PASSPHRASE', variable: 'repo_phrase'), string(credentialsId: 'ROOT_PASSPHRASE', variable: 'root_phrase')]) {
                                sh "DOCKER_CONTENT_TRUST_ROOT_PASSPHRASE=${root_phrase} DOCKER_CONTENT_TRUST_REPOSITORY_PASSPHRASE=${repo_phrase} DOCKER_CONTENT_TRUST=1 docker image push ${shopImage}"
                                sh "DOCKER_CONTENT_TRUST_ROOT_PASSPHRASE=${root_phrase} DOCKER_CONTENT_TRUST_REPOSITORY_PASSPHRASE=${repo_phrase} DOCKER_CONTENT_TRUST=1 docker image push ${shopTunerImage}"
                                sh "DOCKER_CONTENT_TRUST_ROOT_PASSPHRASE=${root_phrase} DOCKER_CONTENT_TRUST_REPOSITORY_PASSPHRASE=${repo_phrase} DOCKER_CONTENT_TRUST=1 docker image push ${nginxImage}"
                                sh "DOCKER_CONTENT_TRUST_ROOT_PASSPHRASE=${root_phrase} DOCKER_CONTENT_TRUST_REPOSITORY_PASSPHRASE=${repo_phrase} DOCKER_CONTENT_TRUST=1 docker image push ${varnishImage}"
                            }
                            sh "docker logout ${dockerRegistryHostPrd}"
                        }
                    }

                    stage('Deploy to prd') {

                        checkout changelog: false, poll: false, scm: [$class: 'GitSCM', branches: [[name: "${env}"]], browser: [$class: 'Stash', repoUrl: ''], doGenerateSubmoduleConfigurations: false, extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: dockerFolder], [$class: 'CleanBeforeCheckout']], submoduleCfg: [], userRemoteConfigs: [[credentialsId: gitCredentials, url: dockerRepositoryUrl]]]
                        checkout changelog: false, poll: false, scm: [$class: 'GitSCM', branches: [[name: "master"]], browser: [$class: 'Stash', repoUrl: ''], doGenerateSubmoduleConfigurations: false, extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: "config"], [$class: 'CleanBeforeCheckout']], submoduleCfg: [], userRemoteConfigs: [[credentialsId: gitCredentials, url: configRepositoryUrl]]]

                        def HostNameParser = load "config/jenkins/HostNameParser.groovy"
                        def StackResource = load "config/jenkins/StackResource.groovy"
                        Docker = load "config/jenkins/Docker.groovy"
                        def ImageNameProvider = load "config/jenkins/ImageNameProvider.groovy"
                        def SourceFolder = load "config/jenkins/SourceFolderProvider.groovy"
                        def AppdynamicsConfigProvider = load "config/jenkins/AppdynamicsConfigProvider.groovy"

                        DockerConfiguration = load "config/jenkins/DockerConfiguration.groovy"
                        DockerConfiguration.addDeploymentEnvironment(env)

                        ImageNameProvider.add(shopImage, 'app')
                        ImageNameProvider.add(shopTunerImage, 'tuner')
                        ImageNameProvider.add(nginxImage, 'nginx')
                        ImageNameProvider.add(varnishImage, 'varnish')

                        Docker.deploy(env, ImageNameProvider, DockerConfiguration, SourceFolder, HostNameParser, AppdynamicsConfigProvider, StackResource)
                    }

                    stage("Warm up cache") {
                        // checkout changelog: false, poll: false, scm: [$class: 'GitSCM', branches: [[name: "master"]], browser: [$class: 'Stash', repoUrl: ''], doGenerateSubmoduleConfigurations: false, extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: "config"], [$class: 'CleanBeforeCheckout']], submoduleCfg: [], userRemoteConfigs: [[credentialsId: gitCredentials, url: configRepositoryUrl]]]
                        sh "php config/crawler/warmup.php --url https://www.tele2.nl/mobiel/smartphones/ --docker-bundle \"${dockerClientBundle}\" --track-service m2tele2_prd_tuner"
                    }
                }
            }
        }
    } catch (error) {
        throw error
    }
}
