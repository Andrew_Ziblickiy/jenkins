class Repository {
	final static MAGENTO = "http://t2nl-devtooling.itservices.lan:7990/scm/mtel/tel2-m2-ee.git"
	final static FOUNDATION = "http://t2nl-devtooling.itservices.lan:7990/scm/mtel/tel2-m2-foundation.git"
	final static CONFIG = "http://t2nl-devtooling.itservices.lan:7990/scm/mtel/tel2-m2-config.git"
	final static STASH_API = "http://t2nl-devtooling.itservices.lan:7990/scm/mtel/jenkins-stash-integration.git"
	final static DOCKER = "http://t2nl-devtooling.itservices.lan:7990/scm/mtel/tel2-docker-env.git"

	static BRANCH_MAGENTO = 'develop'
	static BRANCH_CONFIG = 'master'
	static BRANCH_FOUNDATION = 'develop'
	static DOCKER_BRANCH = "master"

	final static CREDENTIALS = "f5c2a131-daba-44cb-811e-3b60e1e01419"

	static void pull(script, rep, branch, folder) {
		script.checkout changelog: false, poll: false, scm: [$class: 'GitSCM', branches: [[name: branch]], browser: [$class: 'Stash', repoUrl: ''], doGenerateSubmoduleConfigurations: false, extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: folder]], submoduleCfg: [], userRemoteConfigs: [[credentialsId: CREDENTIALS, url: rep]]]
	}

	static void pullWithMerge(script, rep, branch, mergeBranch, folder) {
		script.checkout changelog: false, poll: false, scm: [$class: 'GitSCM', branches: [[name: branch]], browser: [$class: 'Stash', repoUrl: ''], doGenerateSubmoduleConfigurations: false, extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: folder], [$class: 'PreBuildMerge', options: [fastForwardMode: 'FF', mergeRemote: 'origin', mergeStrategy: 'default', mergeTarget: mergeBranch]]], submoduleCfg: [], userRemoteConfigs: [[credentialsId: CREDENTIALS, url: rep]]]
	}
}

class SourceFolder {
	final static MAGENTO = "magento"
    final static CONFIG = "config"
	final static CONFIG_FOUNDATION = "phing/foundation"
	final static STASH_API = "stash_api"
	final static DOCKER = "docker"
    final static FOUNDATION = "foundation"
    final static GENERATED = "generated"
    final static GENERATED_STATIC = "generated/static"
    final static GENERATED_VAR = "generated/var"
    final static GENERATED_MEDIA = "generated/media"
    final static CONTENT_BUILDER = "docker/docker/content-builder"
    final static SRC = "build/src"
    final static LOGS = "logs"
}

class Stash {

    static BIN = ""
    static PULL_REQUEST_ID = ""
    static SOURCE_BRANCH_COMMIT = ""
	static SOURCE_BRANCH = ""
    static DESTINATION_BRANCH_COMMIT = ""
	static DESTINATION_BRANCH = ""
	static STASH_NO_PR = false

	class Error {
		static MESSAGE = ""
		static FILE = ""
		static REMOVE_FILE = true
		static FULL_LOG = false

		static void set(String message, String file = "", boolean remove = true, boolean fullLog = false) {
			MESSAGE = message
			FILE = file
			REMOVE_FILE = remove
			FULL_LOG = fullLog
		}
	}

	static execute(script, String command, boolean returnStatus = false, boolean returnStdout = false) {
		if (returnStatus) {
			def status = script.sh returnStatus: true, script: "php ${BIN} ${command}"
			return status
		} else if (returnStdout) {
			def out = script.sh returnStdout: true, script: "php ${BIN} ${command}"
			return out
		} else {
			script.sh "php ${BIN} ${command}"
		}
	}

	static start(script, pullRequestId) {
		return Stash.execute(script, "pr:jenkins:job --buildId ${Jenkins.ENV.BUILD_ID} ${pullRequestId} start", true)
	}

	static stopWithError(script, pullRequestId) {
		def msg = ""
		def msgFile = ""
		def status = 0
		def keys = ""
		if (Stash.Error.MESSAGE) {
			msg = "--message '${Stash.Error.MESSAGE}'"
		}

		if (Stash.Error.FILE) {
			msgFile = "--messageFile ${Stash.Error.FILE}"
			if (Stash.Error.FULL_LOG) {
				keys = "${keys} --messageFileFullLength 1"
				Stash.Error.FULL_LOG = false
			}
		}

		status = Stash.execute(script, "pr:jenkins:job --error ${keys} ${msg} ${msgFile} ${pullRequestId} stop", true)

		if (Stash.Error.FILE && Stash.Error.REMOVE_FILE) {
			script.sh "rm -f ${Stash.Error.FILE}"
		}

		return status
	}

	static stopWithSuccess(script, pullRequestId) {
		return Stash.execute(script, "pr:jenkins:job --success ${pullRequestId} stop", true)
	}

	static approve(script, pullRequestId) {
		return Stash.execute(script, "pr:approve ${pullRequestId}", true)
	}

	static unApprove(script, pullRequestId) {
		return Stash.execute(script, "pr:unapprove ${pullRequestId}", true)
	}
}

class Docker {
	static LOCAL_ENVS = ""

	class Image {
		static CONTENT_BUILDER_NAME = "m2tele2-static-generator"
        static CONTENT_BUILDER_TAG = "1.0"

		static buildAndPublishReport(script, String command, String reportTitle) {
		    def num = new Random().nextInt()
		    def file = "${SourceFolder.LOGS}/docker-build-${num}.log"

		    script.sh "touch ${file}"
		    def status = script.sh returnStatus: true, script: "2>&1 1>>${file} ${Docker.LOCAL_ENVS} ${command} 2>&1 1>>${file}"

		    script.publishHTML(
                [
                    allowMissing: true,
                    alwaysLinkToLastBuild: false,
                    keepAll: true,
                    reportDir: "${SourceFolder.LOGS}",
                    reportFiles: "docker-build-${num}.log",
                    reportName: "${reportTitle}",
                    reportTitles: ''
                ]
            )

            if (status != 0) {
				def msg = "Cannot build image, see [${reportTitle}] report for details"
				Stash.Error.set(msg, file)
                error "Error when building image\n${command}"
            }
		}
	}
}

class Jenkins {
	static SERVICE_ACCOUNT = "nlsvc-jenkins-docker"
	static ENV = ""
}

class Phing {

    static VERSION = "2.15.2"
	static CODE_COVERAGE_LEVEL = "15"

    public static init(script)
    {
        script.sh "cp -Rf -t ./ ${SourceFolder.CONFIG}/phing/phing-2.15.2.phar ${SourceFolder.CONFIG}/${SourceFolder.CONFIG_FOUNDATION}/build.xml ${SourceFolder.CONFIG}/${SourceFolder.CONFIG_FOUNDATION}/build.properties"

        Phing.execute(script, "make-local-properties -Denv=jenkins -Ddir.config=${SourceFolder.CONFIG}/${SourceFolder.CONFIG_FOUNDATION} -Dconfig=${SourceFolder.CONFIG}/${SourceFolder.CONFIG_FOUNDATION} -Dsrc=\$(realpath ${SourceFolder.SRC}) -Ddir.jenkins.m2ee=${SourceFolder.MAGENTO} -Ddir.jenkins.m2foundation=${SourceFolder.FOUNDATION}")
        Phing.execute(script, "make-phpunit-config")
    }

    public static execute(script, String command, String errorMsg = "Phing execution failed", String reportName = null)
    {
		def num = new Random().nextInt()
		def file = "${SourceFolder.LOGS}/phing-exec-${num}.log"
		script.sh "touch ${file}"

		try {
			script.sh "2>&1 1>>${file} php phing-${Phing.VERSION}.phar ${command} 2>&1 1>>${file}"
			script.sh "rm -f ${file}"
		} catch (e) {
			file = "${script.workspace}/${file}";

			if (reportName) {
				script.publishHTML(
	                [
	                    allowMissing: true,
	                    alwaysLinkToLastBuild: false,
	                    keepAll: true,
	                    reportDir: "${SourceFolder.LOGS}",
	                    reportFiles: "phing-exec-${num}.log",
	                    reportName: "${reportName}",
	                    reportTitles: ''
	                ]
	            )
			}

			Stash.Error.set(errorMsg, file, true, true)
			error "${errorMsg}, ${e}"
		}
    }
}

def buildContentBuilderImage(imageName) {
	logfile = "${SourceFolder.LOGS}/static-content-image-build.log"
	sh "touch ${logfile} && truncate ${logfile} -s 0"
	status = sh returnStatus: true, script: "2>&1 1>>${logfile} ${Docker.LOCAL_ENVS} sh ${SourceFolder.CONTENT_BUILDER}/build.sh -bo \"--build-arg https_proxy=http://proxy.dcn.versatel.net:3128 --build-arg http_proxy=http://proxy.dcn.versatel.net:3128\" -i ${imageName} 2>&1 1>>${logfile}"

	if (status != 0) {
		Stash.Error.set("Could not build HelperImage for building static content and js unit tests", "${logfile}", false)
	}

	publishHTML(
        [
            allowMissing: true,
            alwaysLinkToLastBuild: false,
            keepAll: true,
            reportDir: "${SourceFolder.LOGS}",
            reportFiles: 'static-content-image-build.log',
            reportName: 'ST Image Build',
            reportTitles: ''
        ]
    )

	return status
}

def generateStaticContent(magentoSrc, foundationSrc, appSrc, staticDst, mediaDst, varDst, imageName) {
	sh "mkdir -p ${mediaDst} ${staticDst} ${varDst}"
    sh "rm -Rf ${mediaDst}/* ${staticDst}/* ${varDst}/*"

    logfile = "${SourceFolder.LOGS}/static-content.log"

    sh "touch ${logfile} && truncate ${logfile} -s 0"

	//sh "cp -f ${SourceFolder.CONFIG}/${SourceFolder.CONFIG_FOUNDATION}/conf/config.php.dist ${SourceFolder.MAGENTO}/app/etc/config.php"
	sh "cp -f ${SourceFolder.CONFIG}/${SourceFolder.CONFIG_FOUNDATION}/conf/env.php.dist ${SourceFolder.MAGENTO}/app/etc/env.php"
	sh "cp -Rf ${SourceFolder.CONFIG}/${SourceFolder.CONFIG_FOUNDATION}/conf/enterprise ${SourceFolder.MAGENTO}/app/etc"

	status = sh returnStatus: true, script: "2>&1 1>>${logfile} ${Docker.LOCAL_ENVS} sh ${SourceFolder.CONTENT_BUILDER}/exec.sh -bo \"--rm -e https_proxy=http://proxy.dcn.versatel.net:3128 -e http_proxy=http://proxy.dcn.versatel.net:3128\" --magento-src ${magentoSrc} --foundation-src ${foundationSrc} --var ${varDst} --args \"--configure --install-magento --create-di-xml --compile-di\" -i ${imageName} 2>&1 1>>${logfile}"

	if (status != 0) {
		def file = "${workspace}/${SourceFolder.LOGS}/static-content.log";
		Stash.Error.set("Could not install magento and build static content", "${file}", false)
	}

	publishHTML(
        [
            allowMissing: true,
            alwaysLinkToLastBuild: false,
            keepAll: true,
            reportDir: "${SourceFolder.LOGS}",
            reportFiles: 'static-content.log',
            reportName: 'ST Gen Log',
            reportTitles: ''
        ]
    )

	return status
}

Stash.BIN = "${SourceFolder.STASH_API}/src/Stash/bin/console.php"

properties ([
	buildDiscarder(
		logRotator(
			artifactDaysToKeepStr: '',
			artifactNumToKeepStr: '',
			daysToKeepStr: '3',
			numToKeepStr: '5'
		)
	),
	disableConcurrentBuilds(),
	pipelineTriggers([cron('H/3 * * * *')])
])

node {

	try {
	    stage('Init') {
			Jenkins.ENV = env;
			sh "mkdir -p ${SourceFolder.LOGS} ${SourceFolder.SRC}"
			Repository.pull(this, Repository.CONFIG, "master", SourceFolder.CONFIG)
	        Repository.pull(this, Repository.STASH_API, "master", SourceFolder.STASH_API)

	        isVendorInstalled = sh returnStatus: true, script: "ls -l ${SourceFolder.STASH_API}/vendor > /dev/null"
	        if (isVendorInstalled != 0) {
	            sh "https_proxy=http://proxy.dcn.versatel.net:3128 http_proxy=http://proxy.dcn.versatel.net:3128 wget http://getcomposer.org/composer.phar && mv composer.phar ${SourceFolder.STASH_API}"
	            sh "cd ${SourceFolder.STASH_API} && https_proxy=http://proxy.dcn.versatel.net:3128 http_proxy=http://proxy.dcn.versatel.net:3128 php composer.phar install --no-dev --prefer-dist -o"
	        }

			def parametersFile = "${workspace}/${SourceFolder.CONFIG}/${SourceFolder.CONFIG_FOUNDATION}/stash/parameters-parsed.yml"
			sh "cp -f ${workspace}/${SourceFolder.CONFIG}/${SourceFolder.CONFIG_FOUNDATION}/stash/parameters.yml ${parametersFile}"

			//	configure username and password
			withCredentials([usernamePassword(credentialsId: "${Jenkins.SERVICE_ACCOUNT}", usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
				def parameters = readFile parametersFile
				parameters = parameters.replaceAll("#username#", "${USERNAME}")
				parameters = parameters.replaceAll("#password#", "${PASSWORD}")
				writeFile file: parametersFile , text: parameters
		    }

			Stash.execute(this, "conf:import ${parametersFile} 2>&1 1>/dev/null")

			isConfigValid = Stash.execute(this, "conf:validate 2>&1 1>/dev/null", true)

	        if (isConfigValid != 0) {
	            error "Stash Api not properly configured"
	        }

			response = Stash.execute(this, "pr:jenkins:get-id --build-not-found-phrase BUILD_NOT_FOUND -f pid:sci:dci:sb:db", false, true)
			if (response == 'BUILD_NOT_FOUND') {
				currentBuild.result = 'SUCCESS'
				Stash.STASH_NO_PR = true
   				error('NO_PR')
			}

	        res = response.split(':')
	        Stash.PULL_REQUEST_ID = res[0]
	        Stash.SOURCE_BRANCH_COMMIT = res[1]
	        Stash.DESTINATION_BRANCH_COMMIT = res[2]
			Stash.SOURCE_BRANCH = res[3]
			Stash.DESTINATION_BRANCH = res[4]

			try {
				Stash.start(this, Stash.PULL_REQUEST_ID)
				Repository.pullWithMerge(this, Repository.FOUNDATION, Stash.SOURCE_BRANCH, Stash.DESTINATION_BRANCH, SourceFolder.FOUNDATION)
				Repository.pull(this, Repository.MAGENTO, Repository.BRANCH_MAGENTO, SourceFolder.MAGENTO)
				Repository.pull(this, Repository.DOCKER, Repository.DOCKER_BRANCH, SourceFolder.DOCKER)
			} catch (e) {
				writeFile file: "${SourceFolder.LOGS}/merge-error.log" , text: "${e}"
				Stash.unApprove(this, Stash.PULL_REQUEST_ID)
				Stash.Error.set("Could not merge, resolve conflicts first", "${workspace}/${SourceFolder.LOGS}/merge-error.log")
				// Stash.execute(this, "pr:comment:post ${Stash.PULL_REQUEST_ID} \"####BUILD FAILED\n**Reason: Could not merge, resolve conflicts first**\n\" -f ${workspace}/${SourceFolder.LOGS}/merge-error.log")
				// sh "rm -f ${SourceFolder.LOGS}/merge-error.log"
				error "Can not merge: ${e}"
			}
	    }

	    stage('Build') {
			Phing.init(this)
	        Phing.execute(this, "merge")

	        //  Copy foundation to the application dir
	        sh "cp -Rf ${SourceFolder.FOUNDATION}/app/code/* ${SourceFolder.SRC}/app/code"

			Phing.execute(this, "prepare-output-folders")

	        parallel(
	            //  Run phpcs
	            "phpcs": {
					try {
						Phing.execute(this, "phpcs", "PHP Code Styles checks failed", "PHPCS")
					} catch (e) {
						error "Code style checks: ${e}"
					}
				},

	            //  Run phpmd
	            //"phpmd": { Phing.execute("phpmd") },

	            //  Run phpcpd
	            "phpcpd": { Phing.execute(this, "phpcpd", "PHP copy past detector errors", "PHPCPD") },

	            //  Run phpunit
	            "phpunit": {
					try {
						Phing.execute(this, "phpunit", "PHPUnit test failed", "PHPUNIT")
					} catch (e) {
						Stash.execute(this, "pr:comment:post ${Stash.PULL_REQUEST_ID} 'Phpunit tests failed'")
						error "Phpunit tests failed: ${e}"
					}
				},

				"build-helper-image": {
					status = buildContentBuilderImage("${Docker.Image.CONTENT_BUILDER_NAME}:${Docker.Image.CONTENT_BUILDER_TAG}")

					if (status != 0) {
						error "Cant build static content helper image"
					}
				}
	        )

	        publishHTML(
	            [
	                allowMissing: true,
	                alwaysLinkToLastBuild: false,
	                keepAll: true,
	                reportDir: 'output/phpunit/html',
	                reportFiles: 'index.html',
	                reportName: 'PHPUnit Code Coverage',
	                reportTitles: ''
	            ]
	        )

			// skip this because not tests yet
	        //Phing.execute(this, "validate-code-coverage -Dconfig.phpunit.accepted_coverage=${Phing.CODE_COVERAGE_LEVEL}")
	    }

		stage('Installation checks') {
			status = generateStaticContent(
	            "${workspace}/${SourceFolder.MAGENTO}",
				"${workspace}/${SourceFolder.FOUNDATION}",
	            "",
	            "${workspace}/${SourceFolder.GENERATED_STATIC}",
	            "${workspace}/${SourceFolder.GENERATED_MEDIA}",
	            "${workspace}/${SourceFolder.GENERATED_VAR}",
	            "${Docker.Image.CONTENT_BUILDER_NAME}:${Docker.Image.CONTENT_BUILDER_TAG}"
	        )

	        if (status != 0) {
	            error "Pipeline error during building static content"
	        }
		}

		stage('Notify') {
			Stash.approve(this, Stash.PULL_REQUEST_ID)
			Stash.stopWithSuccess(this, Stash.PULL_REQUEST_ID)
		}

		stage('Clean') {
			sh "rm -Rf ${SourceFolder.SRC}/*"
		}

	} catch (e) {
		if (Stash.PULL_REQUEST_ID) {
			Stash.stopWithError(this, Stash.PULL_REQUEST_ID)
		} else if (Stash.STASH_NO_PR) {
			currentBuild.result = 'SUCCESS'
			return
		}
		error "Build failed with error: ${e}"
	}
}
