VERSION = "2.15.2"
CODE_COVERAGE_LEVEL = 20

def init(SourceFolder)
{
    sh "cp -Rf -t ./ ${SourceFolder.CONFIG}/phing/*"

    execute("init-env -Denv=jenkins -Dsrc=\$(realpath ${SourceFolder.SRC}) -Ddir.jenkins.m2app=${SourceFolder.APPLICATION} -Ddir.jenkins.m2ee=${SourceFolder.MAGENTO} -Ddir.jenkins.m2docker=${SourceFolder.DOCKER} -Ddir.jenkins.m2tr=${SourceFolder.API} -Ddir.jenkins.m2foundation=${SourceFolder.FOUNDATION}")
    execute("make-phpunit-config")
}

def execute(String command)
{
    sh "php phing-${VERSION}.phar ${command}"
}

def test(SourceFolder)
{
    parallel(
        //  Run phpcs
        "phpcs": { execute("phpcs") },

        //  Run phpmd
        //"phpmd": { Phing.execute("phpmd") },

        //  Run phpcpd
        //"phpcpd": { execute("phpcpd") },

        //  Run phpunit
        "phpunit": {
            execute("phpunit")
            publishHTML(
                [
                    allowMissing: true,
                    alwaysLinkToLastBuild: false,
                    keepAll: true,
                    reportDir: "${SourceFolder.SRC}/output/phpunit/html",
                    reportFiles: 'index.html',
                    reportName: 'PHPUnit',
                    reportTitles: ''
                ]
            )
        },
    )
}

def build(SourceFolder) {

    init(SourceFolder)
    execute("merge")

    //  Copy foundation to the application dir
    sh "cp -Rf ${SourceFolder.FOUNDATION}/app/code/* ${SourceFolder.SRC}/app/code"

    //  Clean up output folders
    execute("prepare-output-folders")

    //  Run php unit, php code sniffer, php copy paste detector in parallel
    test(SourceFolder)

    //  Publichs phpunit html report to Jenkins
    publishHTML(
        [
            allowMissing: true,
            alwaysLinkToLastBuild: false,
            keepAll: true,
            reportDir: 'output/phpunit/html',
            reportFiles: 'index.html',
            reportName: 'PHPUnit Code Coverage',
            reportTitles: ''
        ]
    )

    //  Validate code coverage
    execute("validate-code-coverage -Dconfig.phpunit.accepted_coverage=${CODE_COVERAGE_LEVEL}")
}

return this
