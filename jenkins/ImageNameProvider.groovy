class ImageNameProvider
{
    private static images = [:]

    public static PREFIX = "mag-"

    public static MAGENTO_NAME = "m2tele2-base"
    public static MAGENTO_TAG = "latest"

    public static APP_NAME = "shop"
    public static APP_TAG = "latest"

    public static NGINX_NAME = "nginx"
    public static NGINX_TAG = "latest"

    public static VARNISH_NAME = "varnish"
    public static VARNISH_TAG = "latest"

	public static REDIS_NAME = "redis"

    public static CONTENT_BUILDER_NAME = "m2tele2-static-generator"
    public static CONTENT_BUILDER_TAG = "1.0"

    public static add(String name, String alias) {
	    images[alias] = name;
	}

    public static get(String alias, String env = null) {
	    def img = images[alias]

		if (env) {
			def name = "${alias}-${env}"
			if (images[name]) {
				img = images[name]
			}
		}

		return img
	}

    public static getAll() {
        return images
    }

    public static has(String alias) {
		return images.containsKey(alias);
	}
}

return ImageNameProvider;
