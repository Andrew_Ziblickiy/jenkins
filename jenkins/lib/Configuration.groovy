configuration = [:]
loadedFiles = [:]

def loadGroovy(List list)
{
    def isConfigChanged = false
    list.each {
        if (loadedFiles[it]) {
            return
        }
        def exists = fileExists it
        echo "Check if file ${it} exists"

        if (exists) {
            echo "Loading ${it}"
            def c = load it
            configuration << c
        }

        loadedFiles[it] = true
        isConfigChanged = true
    }

    if (isConfigChanged) {
        dump(configuration['DUMP_FILE'])
    }
}

def String toString()
{
    def c = ""
    for (e in configuration) {
        c += "export ${e.key}=${e.value}\n"
    }

    return c
}

def init()
{
    configuration['BUILD_ID'] = BUILD_ID ? BUILD_ID : getEnvVariable('BUILD_ID')
    configuration['BUILD_URL'] = BUILD_URL ? BUILD_URL : getEnvVariable('BUILD_URL')
    configuration['BUILD_DISPLAY_NAME'] = BUILD_DISPLAY_NAME
    configuration['JOB_BASE_NAME'] = JOB_BASE_NAME
    configuration['JOB_URL'] = JOB_URL
}

def getEnvVariable(String name)
{
    def value = sh returnStdout: true, script: "echo \$${name}"
    return value.trim()
}

def printConfiguration()
{
    echo toString()
}

def done()
{
    dump(configuration['DUMP_FILE'])
}

def dump(String filename)
{
    writeFile file: filename, text: toString()
}

def get(String name)
{
    echo name
    return configuration[name]
}

def has(String name)
{
    return configuration.containsKey(name)
}

def all()
{
    return configuration
}

def set(String name, value = "")
{
    configuration[name] = value
}

return this
