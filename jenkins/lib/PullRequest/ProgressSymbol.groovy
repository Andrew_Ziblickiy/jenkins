def set(Configuration, String key, String state)
{
    echo "Resolving state ${state} for ${key}"
    def symbolKey = 'PULL_REQUEST_EXECUTION_PROGRESS_SYMBOL_UNDEF'
    switch(state) {
        case 'wait':
            symbolKey = 'PULL_REQUEST_EXECUTION_PROGRESS_SYMBOL_WAIT'
        break
        case 'executing':
            symbolKey = 'PULL_REQUEST_EXECUTION_PROGRESS_SYMBOL_EXECUTING'
        break
        case 'success':
            symbolKey = 'PULL_REQUEST_EXECUTION_PROGRESS_SYMBOL_SUCCESS'
        break
        case 'fail':
            symbolKey = 'PULL_REQUEST_EXECUTION_PROGRESS_SYMBOL_FAIL'
        break
        case 'skip':
            symbolKey = 'PULL_REQUEST_EXECUTION_PROGRESS_SYMBOL_SKIP'
        break
    }

    echo "Setting ${key} as ${Configuration.get(symbolKey)}"
    def symbol = Configuration.get(symbolKey)

    if (state == 'executing') {
        symbol = "**${symbol}**"
    }

    Configuration.set(key, symbol)
}

return this
