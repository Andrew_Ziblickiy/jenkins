dumpDirectory = "dump"
MYSQL_FACTORY_DATABASE_NAME = "magento"

def resolveHost(String env)
{
    echo "Resolving mysql host for ${env} env"
    if (env == "prd") {
        dbhost = "magentodb.nl.corp.tele2.com"
    } else {
        dbhost = "magentodb.${env}.nl.corp.tele2.com"
    }

    echo "Mysql Host: ${dbhost}"
    return dbhost
}

def makeFileName(String env, String date)
{
    def dumpDir = getDumpDirectory()
    return "${dumpDir}/${date}.${env}.dump.sql"
}

def getMysqlPasswordCredentialsId(String env)
{
    return "magento2-mysql-${env}-pass"
}

def optimizeDatabase(MysqlContainer)
{
    echo "Optimizing database"
    def queries = "TRUNCATE queue_message_status; DELETE FROM queue_message; DELETE FROM tele2_order_request; TRUNCATE customer_visitor; TRUNCATE report_event; TRUNCATE report_viewed_product_index; delete from quote_item_option; delete from quote_item; delete from quote_address; delete from quote;"
    MysqlContainer.execute("mysql -uroot -proot -D magento -e \"${queries}\"")
}

def makeDump(MysqlContainer, String host, String username, String password, String file, String env)
{
    //  Create database
    MysqlContainer.execute("mysql -uroot -proot -e \"CREATE DATABASE ${MYSQL_FACTORY_DATABASE_NAME} CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;\"")

    //  Make dump
    if (env == 'prd') {
        MysqlContainer.execute("mysqldump --skip-lock-tables -h ${host} --no-data -u ${username} -p${password} magento > ${file}")
        MysqlContainer.execute("mysqldump --skip-lock-tables -h ${host} -t --ignore-table=magento.tele2_order_request --ignore-table=magento.queue_message --ignore-table=magento.queue_message_status -u ${username} -p${PASSWORD} magento >> ${file}")
    } else {
        MysqlContainer.execute("mysqldump -h ${host} --skip-lock-tables -u ${username} -p${password} magento > ${file}")
    }

    //  Install dump
    MysqlContainer.execute("mysql -uroot -proot -D ${MYSQL_FACTORY_DATABASE_NAME} < ${file}")

    //  Configure our installed dump
    optimizeDatabase(MysqlContainer)

    //  Make optimized dump
    MysqlContainer.execute("mysqldump -uroot -proot ${MYSQL_FACTORY_DATABASE_NAME} > ${file}")
}

def removePreviousDump(String env)
{
    def prevDumpFileName = makeFileName(env, getPreviousDayDate())

    sh "rm -f ${prevDumpFileName}"
}

def getDumpDirectory()
{
    return dumpDirectory
}

def getCurrentDate()
{
    def date = sh returnStdout: true, script: "date +%Y-%m-%d"
    return date.trim()
}

def getPreviousDayDate()
{
    def prevDate = sh returnStdout: true, script: "date -d \"yesterday 13:00\" '+%Y-%m-%d'"
    return prevDate.trim()
}

def validateEnvironment(String env)
{
    if (env == 'prd') {
        // error "MysqlDumpFactory is not supporting prd database instance right now, sorry, contact AtheD, aliaksei.panasik@tele2.com for support"
    }
}

def isDumpFileExists(String env)
{
    def date = getCurrentDate()
    def fileName = makeFileName(env, date)
    def exists = fileExists fileName

    return exists
}

def getDumpFileName(String env)
{
    return makeFileName(env, getCurrentDate())
}

def create(String env, MysqlContainer)
{
    validateEnvironment(env)

    def host = resolveHost(env)
    dumpDirectory = MysqlContainer.getDumpDirectory()
    def fileName = getDumpFileName(env)

    if (!isDumpFileExists(env)) {
        removePreviousDump(env)

        try {
            withCredentials([usernamePassword(credentialsId: getMysqlPasswordCredentialsId(env), usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
                makeDump(MysqlContainer, host, "${USERNAME}", "${PASSWORD}", fileName, env)
            }
        } catch(e) {
            sh "rm -f ${fileName}" //  Remove generated file in case file was created but on some stage we got error
            error "${e}"
        }
    }

    return fileName
}

return this
