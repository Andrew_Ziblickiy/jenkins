def Config
commitFileName = "bitbucket-build-status.json"
buildCommentFileName = "bitbucket-build-comment.txt"
buildCommentMessageId = null
buildCommentVersion = 0

def init(Configuration)
{
    Configuration.loadGroovy([
        "${Configuration.get('SOURCE_CONFIG')}/env/build/bitbucket.groovy"
    ])

    Config = Configuration
}

def createBuildStatusJson(String status)
{
    def filename = "${Config.get('LOGS')}/${commitFileName}"
    def msg = """
{
	"state": "${status}",
	"key": "m2tele2-pull-request-build-${Config.get('PULL_REQUEST_FROM_HASH')}",
	"name": "Jenkins build #${Config.get('BUILD_ID')}",
	"url": "${Config.get('BUILD_URL')}",
	"description": "Build for ${PULL_REQUEST_FROM_HASH}"
}
"""
    writeFile file: filename, text: msg

    return filename
}

def setBuildStatus(String status)
{
    def filename = createBuildStatusJson(status)
    withCredentials([usernamePassword(credentialsId: Config.get('REPOSITORY_CREDENTIALS'), usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
        sh "curl -u ${USERNAME}:${PASSWORD} -H \"Content-Type: application/json\" -X POST ${Config.get('BITBUCKET_URL')}/rest/build-status/1.0/commits/${Config.get('PULL_REQUEST_FROM_HASH')} -d @${filename}"
    }
}

def postBuildComment()
{
    // /rest/api/1.0/projects/{projectKey}/repos/{repositorySlug}/pull-requests/{pullRequestId}/comments
    def buildCommentFilename = "${Config.get('LOGS')}/${buildCommentFileName}"
    def msg = createBuildCommentMessage()
    def url = "${Config.get('BITBUCKET_URL')}/rest/api/${Config.get('BITBUCKET_REST_VERSION')}/projects/${Config.get('BITBUCKET_PROJECT_KEY')}/repos/${Config.get('BITBUCKET_REPOSITORY_NAME')}/pull-requests/${Config.get('PULL_REQUEST_ID')}/comments"

    if (buildCommentMessageId) {
        echo "Updating comment for ${buildCommentMessageId}"
        // update comment
        writeFile file: buildCommentFilename, text: """{
    "version" : ${buildCommentVersion},
    "text" : "${msg}"
}"""

        buildCommentVersion++
        withCredentials([usernamePassword(credentialsId: Config.get('REPOSITORY_CREDENTIALS'), usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
            sh "curl -u ${USERNAME}:${PASSWORD} -H \"Content-Type: application/json\" -X PUT ${url}/${buildCommentMessageId} -d @${buildCommentFilename}"
        }
    } else {
        echo "Saving comment body"
        //  add comment
        writeFile file: buildCommentFilename, text: """{
    "text" : "${msg}"
}"""
        echo "Posting comment"
        withCredentials([usernamePassword(credentialsId: Config.get('REPOSITORY_CREDENTIALS'), usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
            buildCommentMessageId = sh returnStdout: true, script: "curl -sS -u ${USERNAME}:${PASSWORD} -H \"Content-Type: application/json\" -X POST ${url} -d @${buildCommentFilename} | php -r 'echo json_decode(file_get_contents(\"php://stdin\"), true)[\"id\"];'"
            echo "Comment id ${buildCommentMessageId}"
        }
    }
}

def getBuildCommentMessageIdFileName()
{
    return "${Config.get('LOGS')}/${buildCommentMessageIdFileName}"
}

def createBuildCommentMessage()
{
    def msg = """
[**Build [${Config.get('BUILD_ID')}](${Config.get('BUILD_URL')}) ${Config.get('PULL_REQUEST_EXECUTION_STATE')}**] ${Config.get('PULL_REQUEST_FROM_BRANCH')} [${Config.get('PULL_REQUEST_FROM_HASH')}] -> ${Config.get('PULL_REQUEST_TO_BRANCH')} [${Config.get('PULL_REQUEST_TO_HASH')}]

**Jenkins:**
 - [Build Page](${Config.get('BUILD_URL')})
 - [Console Output](${Config.get('BUILD_URL')}console)

**Progress:**

 ${Config.get('PULL_REQUEST_EXECUTION_PROGRESS_SYMBOL_SUCCESS')} Initialization
 ${Config.get('PULL_REQUEST_EXECUTION_PROGRESS_PULL_SOURCE')} Pull Required Sources
 ${Config.get('PULL_REQUEST_EXECUTION_PROGRESS_INIT_PHING')} Init Phing
 ${Config.get('PULL_REQUEST_EXECUTION_PROGRESS_PHPCS')} PHP Code Style Checks [Log](${Config.get('BUILD_URL')}PHPCS)
 ${Config.get('PULL_REQUEST_EXECUTION_PROGRESS_PHPUnit')} PHP Unit Tests [Log](${Config.get('BUILD_URL')}PHPUnit), [Dashboard](${Config.get('BUILD_URL')}PHPUnit_20Code_20Coverage)
 ${Config.get('PULL_REQUEST_EXECUTION_PROGRESS_PHPCPD')} PHP Copy Paste Detector [Log](${Config.get('BUILD_URL')}PHPCPD)
 ${Config.get('PULL_REQUEST_EXECUTION_PROGRESS_PHPMD')} PHP Mess Detector [Log](${Config.get('BUILD_URL')}PHPMD)
 ${Config.get('PULL_REQUEST_EXECUTION_PROGRESS_PHPUnitCoverage')} PHP Unit Coverage Check (n>=${Config.get('PR_CODE_COVERAGE_LEVEL')}%) [Code Coverage Level](${Config.get('BUILD_URL')}Code_5fCoverage)
 ${Config.get('PULL_REQUEST_EXECUTION_PROGRESS_BUILDING_STATIC_GENERATION_CONTAINER')} Build Helper Image [Log](${Config.get('BUILD_URL')}ST_20Image_20Build)
 ${Config.get('PULL_REQUEST_EXECUTION_PROGRESS_JSCS')} Javascript Code Style Checks [Log](${Config.get('BUILD_URL')}JS_20CS)
 ${Config.get('PULL_REQUEST_EXECUTION_PROGRESS_JSLINT')} Javascript Linter [Log](${Config.get('BUILD_URL')}JS_20Lint)
 ${Config.get('PULL_REQUEST_EXECUTION_PROGRESS_MAGENTO_INSTALLATION_STATIC_GENERATION')} Magento Installation & Assets Generation [Log](${Config.get('BUILD_URL')}ST_20Gen_20Log)
 ${Config.get('PULL_REQUEST_EXECUTION_PROGRESS_JSUNIT')} Javascript Unit Tests [Log](${Config.get('BUILD_URL')}JS_20Unit_20Tests)
"""
    msg = msg.replaceAll("\n", '\\\\n')
    return msg
}

return this
