def install(Container, String dumpFile, String env)
{
    def MysqlHelper = Container.get('MysqlHelper')
    def Configuration = Container.get('Configuration')
    Configuration.loadGroovy([
        "config/env/${env}.groovy",
        "config/env/swarm/dde.groovy"
    ])

    echo "Installing dump ${dumpFile} to ${env}"
    def clientBundle = "DOCKER_HOST=${Configuration.get('DOCKER_CLIENT_BUNDLE_UCP_HOST')} DOCKER_TLS_VERIFY=1 DOCKER_CERT_PATH=${Configuration.get('DOCKER_CLIENT_BUNDLE_CERT_PATH')}"
    def dropFile = getDropTablesSqlFile()
    sh "cat ${dropFile}"

    if (env == 'tst' || env == 'neon') {
        def containerId = sh returnStdout:true, script: "${clientBundle} docker ps | grep m2tele2_mysql_mysql | awk '{print \$1}'"
        containerId = containerId.trim()
        echo "Container id is: ${containerId}"

        sh "${clientBundle} docker exec -i ${containerId} mysql -uroot -proot -D ${Configuration.get('MAGE_DB_DATABASE')} < ${dropFile}"
        sh "${clientBundle} docker exec -i ${containerId} mysql -uroot -proot -D ${Configuration.get('MAGE_DB_DATABASE')} < ${dumpFile}"
    } else {
        def dbHost = MysqlHelper.resolveHost(env)
        echo "Mysql host: ${dbHost}"

        def MysqlContainer = Container.get('MysqlContainer')

        try {
            MysqlContainer.start()
            withCredentials([usernamePassword(credentialsId: MysqlHelper.getMysqlPasswordCredentialsId(env), usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
                MysqlContainer.execute("mysql -h ${dbHost} -u ${username} -p${password} -D magento < ${dropFile}")
                MysqlContainer.execute("mysql -h ${dbHost} -u ${username} -p${password} -D magento < ${dumpFile}")
            }
        } finally {
            MysqlContainer.stop()
        }
    }
}

def getDropTablesSqlFile()
{
    def file = "remove-tables.sql"
    def removeQuery ="""
SET FOREIGN_KEY_CHECKS = 0;
SET GROUP_CONCAT_MAX_LEN=32768;
SET @tables = NULL;
SELECT GROUP_CONCAT('`', table_name, '`') INTO @tables FROM information_schema.tables WHERE table_schema = (SELECT DATABASE());
SELECT IFNULL(@tables,'dummy') INTO @tables;
SET @tables = CONCAT('DROP TABLE IF EXISTS ', @tables);
PREPARE stmt FROM @tables;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;
SET FOREIGN_KEY_CHECKS = 1;
    """
    writeFile file: file, text: removeQuery
    return file
}

return this
