def buildMagentoImage(Configuration, Git, Container)
{
	Configuration.loadGroovy(["${Configuration.get('SOURCE_CONFIG')}/env/service/php.groovy"])

	def magentoDockerFile = "${Configuration.get('SOURCE_DOCKER')}${Configuration.get('SOURCE_FOLDER_TELE2_ENV')}/magento/Dockerfile"
    def commitFile = "${Configuration.get('GENERATED')}/magento-latest-commit.cache"
    def commit = ''
    def exists = fileExists "${commitFile}"
	def magentoImageName = Configuration.get('DOCKER_IMAGE_MAGENTO')

	//  check if commit file exists
    if (exists) {
        commit = readFile "${commitFile}"
    }

	//	Checks
    isImageExists = sh returnStatus: true, script: "1>/dev/null 2>&1 docker inspect ${magentoImageName}"
    currentCommit = sh returnStdout: true, script: "cd ${Configuration.get('SOURCE_MAGENTO')} && git rev-parse --short HEAD"

    if (currentCommit != commit || isImageExists != 0) {

		//	######################
		//	##
		//	##	TEMPORARY FIX
		//	##	some part of images are storing in git, in that case we should add them, when we upload them to db we MUST REMOVE THAT CODE
		Git.pullWithClean(Configuration.get('REPOSITORY_MEDIA'), "dev", Configuration.get('SOURCE_MEDIA'), Configuration.get('REPOSITORY_CREDENTIALS'))

		try {
			//	copy media to generated media
			sh "cp -Rf ${Configuration.get('SOURCE_MEDIA')}/media ${Configuration.get('SOURCE_MAGENTO')}"
		} catch (e) {
			echo "Media copying error: ${e}"
		}
		//	##
		//	######################

		def buildArgs = "--build-arg https_proxy=${Configuration.get('HTTPS_INTERNET_PROXY')} --build-arg http_proxy=${Configuration.get('HTTP_INTERNET_PROXY')}"
		buildArgs += " --build-arg MAGE_PHP_FPM_MAX_CHILDREN=\$MAGE_PHP_FPM_MAX_CHILDREN --build-arg MAGE_PHP_FPM_START_SERVERS=\$MAGE_PHP_FPM_START_SERVERS"
		buildArgs += " --build-arg MAGE_PHP_FPM_MIN_SPARE_SERVERS=\$MAGE_PHP_FPM_MIN_SPARE_SERVERS --build-arg MAGE_PHP_FPM_MAX_SPARE_SERVERS=\$MAGE_PHP_FPM_MAX_SPARE_SERVERS"
		buildArgs += " --build-arg MAGE_PHP_INI_MEMORY_LIMIT=\$MAGE_PHP_INI_MEMORY_LIMIT --build-arg MAGE_PHP_INI_MAX_EXECUTION_TIME=\$MAGE_PHP_INI_MAX_EXECUTION_TIME"

		//  Build image
		echo "Building magento image"
		buildAndPublishReport(
			"source ${Configuration.get('DUMP_FILE')} && docker build ${buildArgs} -f ${magentoDockerFile} -t ${magentoImageName} \$SOURCE_MAGENTO",
			"Magento Image Build Log",
			Configuration
		)

        //  save latest commit id
        writeFile file: commitFile, text: currentCommit
    } else {
        echo "It seems like magento image has the latest code"
    }
	return this
}

def buildApplicationImage(Configuration, Container)
{
	def applicationDirectory = Configuration.get('SOURCE_APPLICATION')

	Container.get('PHP').disableModules(Configuration)

	//  Copy necessary staff
	sh "mkdir -p ${applicationDirectory}/docker/php-fpm"
	sh "cp -Rf ${Configuration.get('SOURCE_DOCKER')}${Configuration.get('SOURCE_FOLDER_TELE2_ENV')}/php-fpm/* ${applicationDirectory}/docker/php-fpm"

	//  Copy foundation to the application dir
	sh "rm -Rf ${applicationDirectory}/app/code/Alpha"
    sh "cp -Rf ${Configuration.get('SOURCE_FOUNDATION')}/app/code/* ${applicationDirectory}/app/code"

	//  Create magento configuration files
    sh "cp -f ${applicationDirectory}/app/etc/env.php.configurable-dist ${applicationDirectory}/app/etc/env.php"

    //  Copy generated content
    sh "mkdir -p ${applicationDirectory}/generated && rm -Rf ${applicationDirectory}/generated/*"
    sh "cp -Rf -t ${applicationDirectory}/generated/ ${Configuration.get('GENERATED_STATIC')}/deployed_version.txt ${Configuration.get('GENERATED_MEDIA')}"

	//  Create deploy info
	def buildDateTime = sh returnStdout: true, script: "date"
    info = "Application=${Configuration.get('REPOSITORY_BRANCH_APPLICATION')}:${Configuration.get('GIT_APPLICATION_COMMIT_ID')};"
	info += "Magento=${Configuration.get('REPOSITORY_BRANCH_MAGENTO')}:${Configuration.get('GIT_MAGENTO_COMMIT_ID')};"
	info += "Foundation=${Configuration.get('REPOSITORY_BRANCH_FOUNDATION')}:${Configuration.get('GIT_FOUNDATION_COMMIT_ID')};"
	info += "BUILD_DATE=${buildDateTime};"
	info += "JOB_URL=${Configuration.get('BUILD_URL')}"
    writeFile file: "${applicationDirectory}/DeployInfo.conf", text: info

    echo "Build Image with info: ${info}"

	//	Resolve and set build info for Appdynamics
	def buildArguments = "--build-arg CONTROLLER_HOST=${Configuration.get('MAGE_APPDYNAMICS_CONTROLLER_HOST')}"
	buildArguments += " --build-arg CONTROLLER_PORT=${Configuration.get('MAGE_APPDYNAMICS_CONTROLLER_PORT')}"
	buildArguments += " --build-arg DOCKER_IMAGE_MAGENTO=${Configuration.get('DOCKER_IMAGE_MAGENTO')}"

	echo "Build arguments: ${buildArguments}"

	def applicationDockerFile = "${Configuration.get('SOURCE_DOCKER')}${Configuration.get('SOURCE_FOLDER_TELE2_ENV')}/php-fpm/Dockerfile"

    //  Build image
    buildAndPublishReport(
        "docker build ${buildArguments} -f ${applicationDockerFile} -t ${Configuration.get('DOCKER_IMAGE_APPLICATION')} ${applicationDirectory}",
        "Application Image Build Log",
		Configuration
    )

	return this
}

def buildContentBuilderImage(Configuration, Container)
{
	buildAndPublishReport(
		"source ${Configuration.get('DUMP_FILE')} && sh \$SOURCE_DOCKER/env/tele2/content-builder/build.sh -bo \"--build-arg https_proxy=\$HTTPS_INTERNET_PROXY --build-arg http_proxy=\$HTTP_INTERNET_PROXY\" -i \$DOCKER_IMAGE_CONTENT_BUILDER",
		'Helper Image Build Log',
		Configuration
	)

	return this
}

def buildImageContainerApplication(Configuration)
{
	buildAndPublishReport(
		"source ${Configuration.get('DUMP_FILE')} && docker build --build-arg https_proxy=\$HTTPS_INTERNET_PROXY --build-arg http_proxy=\$HTTP_INTERNET_PROXY -f \$SOURCE_DOCKER/env/tele2/qa/container-app/Dockerfile -t \$DOCKER_IMAGE_CONTAINER_APPLICATION \$SOURCE_DOCKER/env/tele2/qa/container-app/",
		'App Container Image Build Log',
		Configuration
	)

	return this
}

def buildAutotestsImage(Configuration)
{
	buildAndPublishReport(
		"source ${Configuration.get('DUMP_FILE')} && docker build --build-arg https_proxy=\$HTTPS_INTERNET_PROXY --build-arg http_proxy=\$HTTP_INTERNET_PROXY -f \$SOURCE_DOCKER/env/tele2/qa/node/Dockerfile -t \$DOCKER_IMAGE_AUTOTESTS \$SOURCE_DOCKER/env/tele2/qa/node/",
		'Autotests Container Image Build Log',
		Configuration
	)

	return this
}

def buildAndPublishReport(String command, String reportTitle, Configuration) {
    def num = new Random().nextInt()
    def file = "${Configuration.get('LOGS')}/docker-build-${num}.log"
    sh "touch ${file}"
    def status = sh returnStatus: true, script: "2>&1 1>>${file} ${command} 2>&1 1>>${file}"

    publishHTML(
        [
            allowMissing: true,
            alwaysLinkToLastBuild: false,
            keepAll: true,
            reportDir: "${Configuration.get('LOGS')}",
            reportFiles: "docker-build-${num}.log",
            reportName: "${reportTitle}",
            reportTitles: ''
        ]
    )

    //  Remove log file
    sh "rm -f ${file}"

    if (status != 0) {
        error "Error when building image\n${command}"
    }
}

def buildTunerImage(Configuration, Container) {
	Configuration.set('DOCKER_IMAGE_TUNER', Configuration.get('DOCKER_IMAGE_APPLICATION'))
	return this

	def tunerDockerFile = "${Configuration.get('SOURCE_DOCKER')}${Configuration.get('SOURCE_FOLDER_TELE2_ENV')}/tuner/Dockerfile"
	buildAndPublishReport(
		"source ${Configuration.get('DUMP_FILE')} && docker build --build-arg DOCKER_IMAGE_APPLICATION=\$DOCKER_IMAGE_APPLICATION -f ${tunerDockerFile} -t \$DOCKER_IMAGE_TUNER \${SOURCE_DOCKER}\${SOURCE_FOLDER_TELE2_ENV}/tuner",
		"Tuner Image Build Log",
		Configuration
	)
	return this
}

def buildNginxImage(Configuration, Container) {
	def nginxFolder = "${Configuration.get('SOURCE_DOCKER')}${Configuration.get('SOURCE_FOLDER_TELE2_ENV')}/nginx"

	//  we need copy static to nginx dockerfile context
	sh "mkdir -p ${nginxFolder}/generated && rm -Rf ${nginxFolder}/generated/*"
	sh "cp -Rf -t ${nginxFolder}/generated/ ${Configuration.get('GENERATED_MEDIA')} ${Configuration.get('GENERATED_STATIC')}"

	buildAndPublishReport(
		"source ${Configuration.get('DUMP_FILE')} && docker build --build-arg DOCKER_IMAGE_NGINX_BASE=\$DOCKER_IMAGE_NGINX_BASE -f ${nginxFolder}/Dockerfile -t \$DOCKER_IMAGE_NGINX ${nginxFolder}",
		"Nginx Image Build Log",
		Configuration
	)

	sh "rm -Rf ${nginxFolder}/generated/*"
	return this
}

def buildVarnishImage(Configuration, Container) {
	def varnishFolder = "${Configuration.get('SOURCE_DOCKER')}${Configuration.get('SOURCE_FOLDER_TELE2_ENV')}/varnish"
	buildAndPublishReport(
		"source ${Configuration.get('DUMP_FILE')} && docker build  --build-arg DOCKER_IMAGE_VARNISH_BASE=\$DOCKER_IMAGE_VARNISH_BASE --build-arg https_proxy=\$HTTPS_INTERNET_PROXY --build-arg http_proxy=\$HTTP_INTERNET_PROXY -f ${varnishFolder}/Dockerfile -t \$DOCKER_IMAGE_VARNISH ${varnishFolder}",
		"Varnish Image Build Log",
		Configuration
	)
	return this
}

return this;
