def jsunit(Configuration, fn = null)
{
    logfile = "${Configuration.get('LOGS')}/js-unit.log"
    sh "touch ${logfile} && truncate ${logfile} -s 0"
    def varDst = "${workspace}/${Configuration.get('GENERATED_VAR')}"
    def nodeModules = "${workspace}/node_modules"
    sh "mkdir -p ${nodeModules}"
    sh "cp -f ${Configuration.get('SOURCE_APPLICATION')}/package.json ${workspace}/package.json"

    def cmd = "2>&1 1>>${logfile} docker run --rm -u root -e http_proxy=http://proxy.dcn.versatel.net:3128 -e https_proxy=http://proxy.dcn.versatel.net:3128 -v ${workspace}/package.json:/var/www/tele2/package.json:rw -i -v ${nodeModules}:/var/www/tele2/node_modules ${Configuration.get('DOCKER_IMAGE_CONTENT_BUILDER')}"
    sh "${cmd} npm install"
    // sh "${cmd} cd /var/www/tele2/node_modules/grunt-contrib-jasmine; npm install"

    status = sh returnStatus: true, script: "2>&1 1>>${logfile} docker run -i -v ${nodeModules}:/var/www/tele2/node_modules -v ${varDst}:/var/www/tele2/pub/static -v ${workspace}/${Configuration.get('BUILD_SRC')}:/var/www/tele2/${Configuration.get('BUILD_SRC')} ${Configuration.get('DOCKER_IMAGE_CONTENT_BUILDER')} grunt spec:DirectRetailNl 2>&1 1>>${logfile}"

    publishHTML(
        [
            allowMissing: true,
            alwaysLinkToLastBuild: false,
            keepAll: true,
            reportDir: "${Configuration.get('LOGS')}",
            reportFiles: 'js-unit.log',
            reportName: 'JS Unit Tests',
            reportTitles: ''
        ]
    )

    if (status != 0) {
        if (fn) {
            def file = "${workspace}/${logfile}"
            fn.call(0, "JS Unit Failed", file)
        }
        error "JS Unit exited with non zero status code"
    }

    if (fn) {
        def file = "${workspace}/${logfile}"
        fn.call(1, "JS Unit Success", file)
    }
}

def eslint(Configuration, String jsChangeset = "", fn = null)
{
    logfile = "${Configuration.get('LOGS')}/js-lint.log"
    sh "touch ${logfile} && truncate ${logfile} -s 0"
    status = sh returnStatus: true, script: "2>&1 1>>${logfile} docker run -i -v ${workspace}/${Configuration.get('BUILD_SOURCE')}:/var/www/tele2/${Configuration.get('BUILD_SOURCE')} ${Configuration.get('DOCKER_IMAGE_CONTENT_BUILDER')} eslint --debug -c ${Configuration.get('BUILD_SOURCE')}/dev/tests/static/testsuite/Magento/Test/Js/_files/eslint/.eslintrc ${jsChangeset} 2>&1 1>>${logfile}"

    publishHTML(
        [
            allowMissing: true,
            alwaysLinkToLastBuild: false,
            keepAll: true,
            reportDir: "${Configuration.get('LOGS')}",
            reportFiles: 'js-lint.log',
            reportName: 'JS Lint',
            reportTitles: ''
        ]
    )

    if (status != 0) {
        if (fn) {
            def file = "${workspace}/${logfile}"
            fn.call(0, "JS Lint Failed", file)
        }
        error "JS Lint exited with non zero status code"
    }

    if (fn) {
        def file = "${workspace}/${logfile}"
        fn.call(1, "JS Lint Success", file)
    }
}

def jscs(Configuration, String jsChangeset = "", fn = null)
{
    logfile = "${Configuration.get('LOGS')}/js-cs.log"
    sh "touch ${logfile} && truncate ${logfile} -s 0"
    status = sh returnStatus: true, script: "2>&1 1>>${logfile} docker run -i -v ${workspace}/${Configuration.get('BUILD_SOURCE')}:/var/www/tele2/${Configuration.get('BUILD_SOURCE')} ${Configuration.get('DOCKER_IMAGE_CONTENT_BUILDER')} jscs -c ${Configuration.get('BUILD_SOURCE')}/dev/tests/static/testsuite/Magento/Test/Js/_files/jscs/.jscsrc ${jsChangeset} 2>&1 1>>${logfile}"

    publishHTML(
        [
            allowMissing: true,
            alwaysLinkToLastBuild: false,
            keepAll: true,
            reportDir: "${Configuration.get('LOGS')}",
            reportFiles: 'js-cs.log',
            reportName: 'JS CS',
            reportTitles: ''
        ]
    )

    if (status != 0) {
        if (fn) {
            def file = "${workspace}/${logfile}"
            fn.call(0, "JSCS Failed", file)
        }
        error "JS CS exited with none zero status code"
    }
    if (fn) {
        def file = "${workspace}/${logfile}"
        fn.call(1, "JSCS Success", file)
    }
}

return this
