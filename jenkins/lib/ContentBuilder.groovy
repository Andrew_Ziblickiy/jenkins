def generateStaticContent(Configuration, Container)
{
    def lockFile = "${Configuration.get('GENERATED')}/static-commits.lock"
    def configFileBackup = "${workspace}/config.php"
    def deployedVersionBackup = "${workspace}/deployed_version.txt"
    def exists = fileExists lockFile
    def currentLockCommit = "${Configuration.get('GIT_MAGENTO_COMMIT_ID')}|"
    currentLockCommit += "${Configuration.get('GIT_APPLICATION_COMMIT_ID')}|"
    currentLockCommit += "${Configuration.get('GIT_FOUNDATION_COMMIT_ID')}|"
    currentLockCommit += "${Configuration.get('GIT_DOCKER_COMMIT_ID')}"

    if (exists) {
        def previousBuildCommit =  readFile lockFile
        previousBuildCommit = previousBuildCommit.trim()

        if (currentLockCommit == previousBuildCommit) {


            def configFileExists = fileExists configFileBackup
            def deployedVersionBackupExists = fileExists deployedVersionBackup

            if (configFileExists && deployedVersionBackupExists) {
                sh "cp -f ${configFileBackup} ${Configuration.get('SOURCE_APPLICATION')}/app/etc/config.php"
                sh "mkdir -p ${Configuration.get('GENERATED_STATIC')} && cp -f ${deployedVersionBackup} ${Configuration.get('GENERATED_STATIC')}/deployed_version.txt"
                echo "Skipping static generation because of nothing was changed"
                Configuration.set('MAGE_HAS_STATIC_CHANGES', 0)
                return
            }
        }
    }
    Configuration.set('MAGE_HAS_STATIC_CHANGES', 1)
    writeFile file: lockFile , text: currentLockCommit

    def magentoSrc = "${workspace}/${Configuration.get('SOURCE_MAGENTO')}"
    def foundationSrc = "${workspace}/${Configuration.get('SOURCE_FOUNDATION')}"
    def appSrc = "${workspace}/${Configuration.get('SOURCE_APPLICATION')}"
    def staticDst = "${workspace}/${Configuration.get('GENERATED_STATIC')}"
    def mediaDst = "${workspace}/${Configuration.get('GENERATED_MEDIA')}"
    def varDst = "${workspace}/${Configuration.get('GENERATED_VAR')}"

	sh "mkdir -p ${mediaDst} ${staticDst} ${varDst}"
    sh "rm -Rf ${mediaDst}/* ${staticDst}/* ${varDst}/*"

    logfile = "${Configuration.get('LOGS')}/static-content.log"

    sh "touch ${logfile} && truncate ${logfile} -s 0"

    def cmd = "source ${Configuration.get('DUMP_FILE')} && 2>&1 1>>${logfile} sh ${Configuration.get('SOURCE_DOCKER')}/env/tele2/content-builder/exec.sh -bo \"--rm -e https_proxy=\$HTTPS_INTERNET_PROXY -e http_proxy=\$HTTP_INTERNET_PROXY\" --args \"\$STATIC_GENERATION_OPTIONS\" --magento-src ${magentoSrc} --foundation-src ${foundationSrc} --app-src ${appSrc} --pub-static ${staticDst} --media ${mediaDst} --var ${varDst} -i \$DOCKER_IMAGE_CONTENT_BUILDER 2>&1 1>>${logfile}"
	status = sh returnStatus: true, script: cmd

    publishHTML(
        [
            allowMissing: true,
            alwaysLinkToLastBuild: false,
            keepAll: true,
            reportDir: "${Configuration.get('LOGS')}",
            reportFiles: 'static-content.log',
            reportName: 'ST Gen Log',
            reportTitles: ''
        ]
    )

    if (status != 0) {
        sh "rm -f ${lockFile} ${configFileBackup}"
        error "Generating JS/CSS error"
    }

    //  we should to save config.php
    sh "cp -f ${appSrc}/app/etc/config.php ${configFileBackup}"
    sh "cp -f ${Configuration.get('GENERATED_STATIC')}/deployed_version.txt ${deployedVersionBackup}"


	//	fix resource config add attribute
	// sh "cp -f ${SourceFolder.CONFIG}/resource_config.json ${varDst}/resource_config.json"

	return status
}

return this
