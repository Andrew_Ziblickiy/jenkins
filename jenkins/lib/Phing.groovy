VERSION = "2.15.2"
def Config

def init(Container)
{
    Config = Container.get('Configuration')
    Config.loadGroovy([
        "${Config.get('SOURCE_CONFIG')}/env/build/pull-request.groovy"
    ])

    sh "rm -f build.properties build.properties.generated"
    sh "cp -Rf -t ./ ${Config.get('SOURCE_CONFIG')}/phing/*"
    sh "mkdir -p ${Config.get('BUILD_SOURCE')}"

    execute("init-env -Denv=jenkins -Dsrc=\$(realpath ${Config.get('BUILD_SOURCE')}) -Ddir.jenkins.m2app=${Config.get('SOURCE_APPLICATION')} -Ddir.jenkins.m2ee=${Config.get('SOURCE_MAGENTO')} -Ddir.jenkins.m2docker=${Config.get('SOURCE_DOCKER')} -Ddir.jenkins.m2foundation=${Config.get('SOURCE_FOUNDATION')}")
    execute("make-phpunit-config")

    return this
}

def execute(String command)
{
    sh "php phing-${VERSION}.phar ${command}"

    return this
}

def execute(String command, String reportName, String errorMessage, fn = null)
{
    def num = new Random().nextInt()
    def fileName = "phing-exec-${reportName}.log"
    def file = "${Config.get('LOGS')}/${fileName}"
    sh "rm -f ${file} && touch ${file}"

    try {
        sh "2>&1 1>>${file} php phing-${VERSION}.phar ${command} 2>&1 1>>${file}"

        if (fn) {
            echo "Call function ok"
            fn.call(1, errorMessage, file, reportName)
        }
    } catch (e) {
        file = "${workspace}/${file}";
        if (fn) {
            fn.call(0, errorMessage, file, reportName)
        }
        error "${errorMsg}, ${e}"
    } finally {
        publishHTML(
            [
                allowMissing: true,
                alwaysLinkToLastBuild: false,
                keepAll: true,
                reportDir: "${Config.get('LOGS')}",
                reportFiles: "${fileName}",
                reportName: "${reportName}",
                reportTitles: ''
            ]
        )
    }
}

def test(String changeset = "", fn = null, boolean returnStatus = false)
{
    try {
        parallel(
            //  Run phpcs
            "phpcs": {
                def cmd = "phpcs"
                if (changeset) {
                    cmd = "phpcs -Ddir.src.tele2=\"${changeset}\""
                }
                execute(cmd, "PHPCS", "PHPCS Failed", fn)
            },

            //  Run phpmd
            "phpmd": {
                try {
                    execute("phpmd", "PHPMD", "PHPMD Failed", fn)
                } catch(e) {}
            },

            //  Run phpcpd
            "phpcpd": {
                try {
                    execute("phpcpd", "PHPCPD", "PHPCPD Failed", fn)
                } catch(e) {}
            },

            //  Run phpunit
            "phpunit": {
                execute("phpunit", "PHPUnit", "PHPUnit failed", fn)
                publishHTML(
                    [
                        allowMissing: true,
                        alwaysLinkToLastBuild: false,
                        keepAll: true,
                        reportDir: "${Config.get('BUILD_SOURCE')}/output/phpunit/html",
                        reportFiles: 'index.html',
                        reportName: 'PHPUnit Code Coverage',
                        reportTitles: ''
                    ]
                )
            },
        )
        if (returnStatus) {
            return true
        }
    } catch(e) {
        if (returnStatus) {
            return false
        }

        throw e
    }
}

def build()
{
    execute("merge")

    //  Copy foundation to the application dir
    sh "cp -Rf ${Config.get('SOURCE_FOUNDATION')}/app/code/* ${Config.get('BUILD_SOURCE')}/app/code"

    //  Clean up output folders
    execute("prepare-output-folders")

    //  Run php unit, php code sniffer, php copy paste detector in parallel
    test()

    //  Publichs phpunit html report to Jenkins
    publishHTML(
        [
            allowMissing: true,
            alwaysLinkToLastBuild: false,
            keepAll: true,
            reportDir: 'output/phpunit/html',
            reportFiles: 'index.html',
            reportName: 'PHPUnit Code Coverage',
            reportTitles: ''
        ]
    )

    //  Validate code coverage
    execute("validate-code-coverage -Dconfig.phpunit.accepted_coverage=${Config.get('PR_CODE_COVERAGE_LEVEL')}")
}

return this
