services = [:]

def get(String name)
{
    loadService(name)
    return services[name]
}

def set(String name, service)
{
    services[name] = service
}

def loadService(String name, Configuration = null)
{
    if (services[name]) {
        return
    }

    if (!Configuration) {
        Configuration = get('Configuration')
    }

    if (!Configuration) {
        error "Unable to get Configuration"
    }

    Configuration.loadGroovy(["${Configuration.get('SOURCE_CONFIG')}/env/build/services.groovy"])

    if (Configuration.has("SERVICE_${name}")) {
        def path = Configuration.get("SERVICE_${name}")
        def servicePath = "${Configuration.get('SOURCE_CONFIG')}/${Configuration.get('SERVICE_relativeConfigPath')}/${path}"

        def exists = fileExists servicePath
        echo "Check if Service ${name} exists"

        if (exists) {
            echo "Loading ${name} => ${servicePath}"
            def c = load servicePath
            set(name, c)
            return
        }
    }

    error "Unable to load service ${name} by the following path ${servicePath}"
}

return this
