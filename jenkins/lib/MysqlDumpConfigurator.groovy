MYSQL_DATABASE_NAME = 'magento_configration'

def createEnvDomainsMap(Configuration)
{
    return [
        'MAGE_DIRECTRETAIL_DOMAIN' : Configuration.get('MAGE_DIRECTRETAIL_DOMAIN'),
        'MAGE_ONLINERETAIL_DOMAIN' : Configuration.get('MAGE_ONLINERETAIL_DOMAIN'),
        'MAGE_CALLCENTERSHOP_DOMAIN' : Configuration.get('MAGE_CALLCENTERSHOP_DOMAIN'),
        'MAGE_RESELLER_DOMAIN' : Configuration.get('MAGE_RESELLER_DOMAIN'),
        'MAGE_CDN_DOMAIN' : Configuration.get('MAGE_CDN_DOMAIN'),
        'MAGE_ADMIN_DOMAIN' : Configuration.get('MAGE_ADMIN_DOMAIN'),
        'MAGE_INTERNAL_API_DOMAIN' : Configuration.get('MAGE_INTERNAL_API_DOMAIN')
    ]
}

def configureDomainNames(Configuration, MysqlContainer, String fromEnv, String toEnv)
{
    Configuration.loadGroovy([
        "${Configuration.get('SOURCE_CONFIG')}/env/${fromEnv}.groovy"
    ])

    fromEnvDomains = createEnvDomainsMap(Configuration)

    Configuration.loadGroovy([
        "${Configuration.get('SOURCE_CONFIG')}/env/${toEnv}.groovy"
    ])

    toEnvDomains = createEnvDomainsMap(Configuration)

    fromEnvDomains.each { key, domain ->
        def query = "UPDATE core_config_data SET value = REPLACE(value, \"http://${domain}/\", \"http://${toEnvDomains[key]}/\") WHERE path like \"%base_%\" AND value LIKE \"%http://${domain}/%\";"
        execute(MysqlContainer, query)
        query = "UPDATE core_config_data SET value = REPLACE(value, \"https://${domain}/\", \"https://${toEnvDomains[key]}/\") WHERE path like \"%base_%\" AND value LIKE \"%https://${domain}/%\";"
        execute(MysqlContainer, query)
    }
}

def configureCookieDomain(Configuration, MysqlContainer)
{
    def query = "update core_config_data set value = \"${Configuration.get('MAGE_ONLINERETAIL_DOMAIN')}\" where scope = \"stores\" and scope_id = \"2\" and path = \"web/cookie/cookie_domain\";"
    execute(MysqlContainer, query)
}

def configureLogstash(Configuration, MysqlContainer, String fromEnv, String toEnv)
{
    if (toEnv == 'tst' || toEnv == 'neon' || toEnv == 'local') {
        echo "Disabling logstash"
        def query = "update core_config_data set value = 0 where path = \"logging/enable_logstash/logstash_enabled\";"
        execute(MysqlContainer, query)
    }
}

def execute(MysqlContainer, String query)
{
    echo "Executing: ${query}"
    MysqlContainer.execute("mysql -uroot -proot -D ${MYSQL_DATABASE_NAME} -e '${query}'")
}

def createDumpFileName(String dumpFile, String toEnv)
{
    return "${dumpFile}.reconfigure.for.${toEnv}.dump.sql"
}

def configure(Configuration, MysqlContainer, String dumpFile, String fromEnv, String toEnv)
{
    def file = createDumpFileName(dumpFile, toEnv)
    echo "Installing dump"
    //  Create database
    MysqlContainer.execute("mysql -uroot -proot -e \"CREATE DATABASE ${MYSQL_DATABASE_NAME} CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;\"")

    try {
        MysqlContainer.execute("mysql -uroot -proot -D ${MYSQL_DATABASE_NAME} < ${dumpFile}")

        //  Configure magento domain names
        configureDomainNames(Configuration, MysqlContainer, fromEnv, toEnv)
        configureCookieDomain(Configuration, MysqlContainer)
        configureLogstash(Configuration, MysqlContainer, fromEnv, toEnv)

        MysqlContainer.execute("mysqldump -uroot -proot ${MYSQL_DATABASE_NAME} > ${file}")
    } finally {
        MysqlContainer.execute("mysql -uroot -proot -e 'drop database ${MYSQL_DATABASE_NAME};'")
    }

    return file
}

return this
