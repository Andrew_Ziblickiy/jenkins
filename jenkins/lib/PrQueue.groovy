queueFile = ""

def init(Configuration)
{
	queueFile = "${Configuration.get('LOGS')}/${Configuration.get('PR_QUEUE_FILE')}"
	def isFileExists = fileExists queueFile

	if (!isFileExists) {
		sh "touch ${queueFile}"
	}
}

def get()
{
	def prId = sh returnStdout: true, script: "cat ${queueFile}"

	return prId.trim()
}

def set(id)
{
	writeFile file: queueFile , text: "${id}"
}

return this
