apiEndpointIp = "82.175.98.36"

def executeMagentoCommand(Configuration, String name, String command)
{
    echo "Executing ${name}, command ${command}"
    def host = Configuration.get('MAGE_INTERNAL_API_DOMAIN')
    def json ="""
{
    "${name}" : {
        "cmd" : "${command}"
    }
}
"""
    def output = ""
    def tmpFile = "${workspace}/remote-api-json-body.json"
    writeFile file: tmpFile, text: json
    try {
        output = sh returnStdout: true, script: "curl -H \"Host: ${host}\" -H \"Content-type: application/json\" --data \"@${tmpFile}\"  http://${apiEndpointIp}/"
    } catch(e) {
        throw e
    } finally {
        sh "rm -f ${tmpFile}"
    }

    return output
}

def cleanCache(Configuration)
{
    return executeMagentoCommand(Configuration, 'cc', "cache:clean")
}

def setupUpgrade(Configuration)
{
    return executeMagentoCommand(Configuration, 'setup-upgrade', "setup:upgrade --keep-generated")
}

return this
