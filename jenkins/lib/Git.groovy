def pull(rep, branch, folder, creds) {
    checkout changelog: false, poll: false, scm: [$class: 'GitSCM', branches: [[name: branch]], browser: [$class: 'Stash', repoUrl: ''], doGenerateSubmoduleConfigurations: false, extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: folder]], submoduleCfg: [], userRemoteConfigs: [[credentialsId: creds, url: rep]]]
}

def pullWithClean(rep, branch, folder, creds) {
    checkout changelog: false, poll: false, scm: [$class: 'GitSCM', branches: [[name: branch]], browser: [$class: 'Stash', repoUrl: ''], doGenerateSubmoduleConfigurations: false, extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: folder], [$class: 'CleanBeforeCheckout']], submoduleCfg: [], userRemoteConfigs: [[credentialsId: creds, url: rep]]]
}

def pullWithMerge(rep, branch, mergeBranch, folder, creds) {
	checkout changelog: false, poll: false, scm: [$class: 'GitSCM', branches: [[name: branch]], browser: [$class: 'Stash', repoUrl: ''], doGenerateSubmoduleConfigurations: false, extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: folder], [$class: 'PreBuildMerge', options: [fastForwardMode: 'FF', mergeRemote: 'origin', mergeStrategy: 'DEFAULT', mergeTarget: mergeBranch]]], submoduleCfg: [], userRemoteConfigs: [[credentialsId: creds, url: rep]]]
}

def fetchAndFillCommit(Configuration, String path, String commitIdConstant)
{
    def commit = sh returnStdout: true, script: "cd ${path} && git rev-parse --short HEAD"
    Configuration.set(commitIdConstant, commit.trim())
}

def pullAll(Configuration)
{
    Configuration.loadGroovy([
        "${Configuration.get('SOURCE_CONFIG')}/env/build/repository.groovy"
    ])

    pullMagento(Configuration)
    pullDocker(Configuration)
    pullApplication(Configuration)
    pullFoundation(Configuration)
}

def pullStashApi(Configuration)
{
    pullWithClean(
        Configuration.get('REPOSITORY_STASH_API'),
        Configuration.get('REPOSITORY_BRANCH_STASH_API'),
        Configuration.get('SOURCE_STASH_API'),
        Configuration.get('REPOSITORY_CREDENTIALS')
    )

    fetchAndFillCommit(Configuration, Configuration.get('SOURCE_STASH_API'), 'GIT_STASH_API_COMMIT_ID')
}

def pullAutotests(Configuration)
{
    pullWithClean(
        Configuration.get('REPOSITORY_AUTOTESTS'),
        Configuration.get('REPOSITORY_BRANCH_AUTOTESTS'),
        Configuration.get('SOURCE_AUTOTESTS'),
        Configuration.get('REPOSITORY_CREDENTIALS')
    )

    fetchAndFillCommit(Configuration, Configuration.get('SOURCE_AUTOTESTS'), 'GIT_STASH_AUTOTESTS_COMMIT_ID')
}

def pullConfiguration(Configuration)
{
    pullWithClean(
        Configuration.get('REPOSITORY_CONFIG'),
        Configuration.get('REPOSITORY_BRANCH_CONFIG'),
        Configuration.get('SOURCE_CONFIG'),
        Configuration.get('REPOSITORY_CREDENTIALS')
    )

    fetchAndFillCommit(Configuration, Configuration.get('SOURCE_CONFIG'), 'GIT_CONFIG_COMMIT_ID')
}

def pullMagento(Configuration)
{
    pullWithClean(
        Configuration.get('REPOSITORY_MAGENTO'),
        Configuration.get('REPOSITORY_BRANCH_MAGENTO'),
        Configuration.get('SOURCE_MAGENTO'),
        Configuration.get('REPOSITORY_CREDENTIALS')
    )

    fetchAndFillCommit(Configuration, Configuration.get('SOURCE_MAGENTO'), 'GIT_MAGENTO_COMMIT_ID')
}

def pullDocker(Configuration)
{
    pullWithClean(
        Configuration.get('REPOSITORY_DOCKER'),
        Configuration.get('REPOSITORY_BRANCH_DOCKER'),
        Configuration.get('SOURCE_DOCKER'),
        Configuration.get('REPOSITORY_CREDENTIALS')
    )

    fetchAndFillCommit(Configuration, Configuration.get('SOURCE_DOCKER'), 'GIT_DOCKER_COMMIT_ID')
}

def pullApplication(Configuration)
{
    pullWithClean(
        Configuration.get('REPOSITORY_APPLICATION'),
        Configuration.get('REPOSITORY_BRANCH_APPLICATION'),
        Configuration.get('SOURCE_APPLICATION'),
        Configuration.get('REPOSITORY_CREDENTIALS')
    )

    fetchAndFillCommit(Configuration, Configuration.get('SOURCE_APPLICATION'), 'GIT_APPLICATION_COMMIT_ID')
}

def pullFoundation(Configuration)
{
    pullWithClean(
        Configuration.get('REPOSITORY_FOUNDATION'),
        Configuration.get('REPOSITORY_BRANCH_FOUNDATION'),
        Configuration.get('SOURCE_FOUNDATION'),
        Configuration.get('REPOSITORY_CREDENTIALS')
    )

    fetchAndFillCommit(Configuration, Configuration.get('SOURCE_FOUNDATION'), 'GIT_FOUNDATION_COMMIT_ID')
}

return this;
