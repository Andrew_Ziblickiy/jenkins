def parse(String file, Map variables)
{
    def fileSource = readFile file
    variables.each { key, value ->
        fileSource = fileSource.replaceAll('\\$' + '\\{' + key + '}', "${value}")
    }
    writeFile file: file , text: fileSource
}

return this
