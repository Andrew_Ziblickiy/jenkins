def scan(String hostname, String dockerOptions = "", String owaspOptions = "") {
	def mountPoint = "${workspace}/owasp"
	sh "mkdir -p ${mountPoint}"

    // Run zap scanner
    def filenumber = new Random().nextInt()
    def reportname = "report${filenumber}.html"
    def containerid = sh returnStdout: true, script: "docker run --rm -v ${mountPoint}:/zap/wrk/:rw -t -d -u root ${dockerOptions} owasp/zap2docker-stable zap-baseline.py -t ${hostname} ${owaspOptions} -i -m5 -a -r ${reportname}"
	def isContainerWorking = 1
	def retry = 0
	def timeout = 20
	def retryLimit = 35
	def returnCode

    while(isContainerWorking == 1) {
		if (retry > retryLimit) {
			error('Took to long for the checking application via OWASPzap')
		}

		returnCode = sh returnStatus: true, script: "1>/dev/null 2>&1 docker inspect ${containerid}"
		if (returnCode != 0) {
			isContainerWorking = 0
		}
		sleep "${timeout}".toInteger()
        retry++
    }

    // obtain the report
    publishHTML (target: [
      allowMissing: false,
      alwaysLinkToLastBuild: false,
      keepAll: true,
      reportDir: "${mountPoint}",
      reportFiles: "${reportname}",
      reportName: "OWASPzap Report"
    ])
}

return this
