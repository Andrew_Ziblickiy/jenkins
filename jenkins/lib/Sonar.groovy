BIN = "/var/jenkins_home/tools/hudson.plugins.sonar.SonarRunnerInstallation/SonarQube_Scanner/bin/sonar-scanner"
HOST = "https://sonar.nl.corp.tele2.com/"

def scan(String propertiesFile, String projectRoot)
{
	withSonarQubeEnv('Tele2 Sonar') {
        def status = sh returnStatus: true, script: "${BIN} -Dsonar.projectBaseDir=${projectRoot} -Dproject.settings=${propertiesFile} -Dsonar.host.url=${HOST}"
    }
}

return this
