def stageBuildMagento(Container)
{
    stage('Build Magento') {
        def ImageBuilder = Container.get('ImageBuilder')
        def Configuration = Container.get('Configuration')
        ImageBuilder.buildMagentoImage(Configuration, Container.get('Git'), Container)
    }

    return this
}

def stageApplication(Container)
{
    stage('Build Application') {
        def ImageBuilder = Container.get('ImageBuilder')
        def Configuration = Container.get('Configuration')
        ImageBuilder.buildMagentoImage(Configuration, Container.get('Git'), Container)
        ImageBuilder.buildApplicationImage(Configuration, Container)
        ImageBuilder.buildTunerImage(Configuration, Container)
    }

    return this
}

def stageNginxVarnish(Container)
{
    stage('Build Varnish & Nginx') {
        def ImageBuilder = Container.get('ImageBuilder')
        def Configuration = Container.get('Configuration')
        parallel(
            //  Build nginx
            "nginx" : {
                ImageBuilder.buildNginxImage(Configuration, Container)
            },

            //  Build varnish
            "varnish" : {
                ImageBuilder.buildVarnishImage(Configuration, Container)
            }
        )
    }

    return this
}

def stageStaticGeneration(Container)
{
    stage("Generate JS/CSS") {
        def Configuration = Container.get('Configuration')
        Container.get('ImageBuilder').buildContentBuilderImage(Configuration, Container)
        Container.get('ContentBuilder').generateStaticContent(Configuration, Container)
    }

    return this
}

def stagePushImages(Container)
{
    stage('Push Images') {
        Container.get('Docker').push(Container.get('Configuration'))
    }

    return this
}

def stageDeploy(Container)
{
    stage('Deploy') {
        Container.get('Docker').deploy(Container)
    }

    return this
}

def stageWaitForTuner(Container)
{
    stage('DB Upgrade/Reindex') {
        Container.get('Docker').isApplicationUp(Container.get('Configuration'))
    }
    return this
}

return this
