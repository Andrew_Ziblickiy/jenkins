def disableModules(Configuration)
{
    def configurationRoot = "${Configuration.get('SOURCE_APPLICATION')}/app/etc"
    def disableModulesFile = "${configurationRoot}/disabled_modules"
    def configFile = "${configurationRoot}/config.php"

    def isDisableModulesFileExists = fileExists disableModulesFile
    def isConfigFileExists = fileExists disableModulesFile

    if (isDisableModulesFileExists && isConfigFileExists) {
        def disabledModules = sh returnStdout: true, script: "cat ${disableModulesFile}"
        disabledModules = disabledModules.trim().split(',')

        def configSource = readFile configFile
        disabledModules.each {
            echo "Disabling module: ${it}"
            configSource = configSource.replace("'${it}' => 1", "'${it}' => 0")
        }
        echo "Magento module configuration:"
        echo configSource
        writeFile file: configFile , text: configSource
    }
}

return this
