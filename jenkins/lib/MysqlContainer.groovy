image = "percona"
dumpDirectory = "dump"
MYSQL_ROOT_PASSWORD = "root"
MYSQL_ALLOW_EMPTY_PASSWORD = '1'
containerId = null

def ensureThatDumpDirectoryExists()
{
    def dumpDir = getDumpDirectory()
    sh "mkdir -p ${dumpDir}"
}

def getDumpDirectory()
{
    return "${workspace}/${dumpDirectory}"
}

def getContainerId()
{
    return containerId
}

def start(String runArguments = "")
{
    if (containerId) {
        return containerId
    }

    ensureThatDumpDirectoryExists()

    dumpDir = getDumpDirectory()
    containerId = sh returnStdout: true, script: "docker run ${runArguments} --rm --env MYSQL_ROOT_PASSWORD='${MYSQL_ROOT_PASSWORD}' --env MYSQL_ALLOW_EMPTY_PASSWORD='${MYSQL_ALLOW_EMPTY_PASSWORD}' --rm -i -d -v ${dumpDir}:/dump ${image}"
    containerId = containerId.trim()

    echo "Container was started: id #${containerId}"
    echo "Dump directory: ${dumpDir}"

    ensureContainerReadyForWork()

    return containerId
}

def stop()
{
    if (containerId) {
        sh "docker stop ${containerId}"
        containerId = null
    }
}

def execute(String command)
{
    echo "Executing: ${command}"
    sh "docker exec -i ${containerId} ${command}"
}

def ensureMysqlIsUp(int timeout = 5, int iterations = 10)
{
    sleep(10)
    return
    def iteration = 0
    def status = 'is not running'

    echo "Checking is mysql up"

    while ({
        sleep(timeout)
        iteration++

        status = sh returnStatus: true, script: "docker exec -i ${containerId} service mysql status"

        echo "Status: ${status}"
        status = status.toInteger()

        echo "Is up [${iteration}]: ${status}"
        (status != 0 || iteration >= iterations)
    }()) continue

    if (status != 0) {
        error "Timeout is exceeded while waiting for "
    }

    //  sometimes additional time needed for mysql sock
    sleep(5)
}

def ensureContainerIsUp(int timeout = 3, int iterations = 3) {
    def iteration = 0
    def status = 'false'

    echo "Checking is container up"

    while ({
        sleep(timeout)
        iteration++

        status = sh returnStdout: true, script: "docker inspect -f {{.State.Running}} ${containerId}"
        status = status.trim()

        echo "Is up [${iteration}]: ${status}"
        (status == 'false' && iteration <= iterations)
    }()) continue

    if (status != 'true') {
        error "It looks like container does not exists or not found, or incorrect logic"
    }
}

def ensureContainerReadyForWork()
{
    ensureContainerIsUp()
    ensureMysqlIsUp()
}

return this
