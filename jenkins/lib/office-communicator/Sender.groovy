def send(String template, String link, String proxy = null)
{
    def response
    if (proxy) {
        response = httpRequest contentType: 'APPLICATION_JSON', httpMode: 'POST', requestBody: template, responseHandle: 'NONE', url: link, httpProxy: proxy
    } else {
        response = httpRequest contentType: 'APPLICATION_JSON', httpMode: 'POST', requestBody: template, responseHandle: 'NONE', url: link
    }

    return response
}

return this
