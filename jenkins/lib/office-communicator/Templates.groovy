colorSuccess = '0075FF'
colorFailed = 'd61b1b'
colorInfo = 'e0d535'

messageTemplate ="""
{
    "@type": "MessageCard",
    "@context": "http://schema.org/extensions",
    "summary": "__SUBJECT__",
    "themeColor": "__THEME_COLOR__",
    "sections": [
        {
            "title": "**__CARD_TITLE__**",
            __USER_IMAGE__
            "activityTitle": "Started by **__USER_NAME__**",
            "activitySubtitle": "__USER_EMAIL__",
            "facts": [
                { "name": "Date:", "value": "__DATE__" },
                { "name": "Status:", "value": "__STATUS__" },
                { "name": "Build URL:", "value": "__BUILD_URL__" }
            ],
    		"text": "__DESCRIPTION__"
        }
    ]
}
"""

def createMessage(
    String subject,
    String cardTitle,
    String userName,
    String userEmail,
    String date,
    String status,
    String buildUrl,
    String description = "Regular Deployment",
    String userImage = ""
) {
    def msg = [
        "__SUBJECT__" : subject,
        "__CARD_TITLE__" : cardTitle,
        "__USER_IMAGE__" : userImage,
        "__USER_NAME__" : userName,
        "__USER_EMAIL__" : userEmail,
        "__DATE__" : date,
        "__STATUS__" : status,
        "__BUILD_URL__" : buildUrl,
        "__DESCRIPTION__" : description
    ]

    msg["__USER_IMAGE__"] = userImage ? "'activityImage': '${userImage}'," : ""

    return msg
}

def parseMessage(Map message, String template)
{
    def tpl = template

    for (e in message) {
        tpl = tpl.replace(e.key, e.value)
    }

    return tpl
}

def getBuildBeginMessage(Map message)
{
    message['__THEME_COLOR__'] = colorInfo

    return parseMessage(message, messageTemplate)
}

def getBuildSuccessMessage(Map message)
{
    message['__THEME_COLOR__'] = colorSuccess

    return parseMessage(message, messageTemplate)
}

def getBuildFailedMessage(Map message)
{
    message['__THEME_COLOR__'] = colorFailed

    return parseMessage(message, messageTemplate)
}

return this
