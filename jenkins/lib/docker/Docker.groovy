def login(Configuration) {
    echo "Trying login to docker registry"
    def registryHost = Configuration.get('DOCKER_REGISTRY_HOST')

	withCredentials([usernamePassword(credentialsId: Configuration.get('DOCKER_DTR_CREDENTIALS'), usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
        sh "docker login -u ${USERNAME} -p ${PASSWORD} ${registryHost}"
    }
}

def logout(Configuration) {
    echo "Trying logout to docker registry"

    //sh "docker logout ${Configuration.get('DOCKER_DTR_REGISTRY_HOST')}"
    echo "Logout from repository is commented, skiping docker logout ${Configuration.get('DOCKER_REGISTRY_HOST')}"
}

def push(Configuration) {
    def file = "${Configuration.get('LOGS')}/docker-push.log"
    def status = 0
    def imageName = ""
    sh "touch ${file} && truncate ${file} -s 0"

	login(Configuration)

    def images = [
        Configuration.get('DOCKER_IMAGE_APPLICATION'),
        Configuration.get('DOCKER_IMAGE_NGINX'),
        Configuration.get('DOCKER_IMAGE_VARNISH')
    ]

    if (Configuration.get('DOCKER_IMAGE_APPLICATION') != Configuration.get('DOCKER_IMAGE_ADMIN')) {
        images.add(Configuration.get('DOCKER_IMAGE_ADMIN'))
    }

    if (Configuration.get('DOCKER_IMAGE_APPLICATION') != Configuration.get('DOCKER_IMAGE_TUNER')) {
        images.add(Configuration.get('DOCKER_IMAGE_TUNER'))
    }

    def attempts = 2
    def attemp = 1

    images.each {
        echo "Pushing image: ${it}"
        status = sh returnStatus: true, script: "2>&1 1>>${file} docker image push ${it} 2>&1 1>>${file}"

        if (status != 0) {
            echo "Failed to push image: ${it}"

            if (attemp <= attempts) {
                echo "Trying to login again"
                login(Configuration)
                echo "Pushing"
                status = sh returnStatus: true, script: "2>&1 1>>${file} docker image push ${it} 2>&1 1>>${file}"
                attemp += 1
            } else {
                publishHTML(
                    [
                        allowMissing: true,
                        alwaysLinkToLastBuild: false,
                        keepAll: true,
                        reportDir: "${Configuration.get('LOGS')}",
                        reportFiles: "docker-push.log",
                        reportName: "Push Images Log",
                        reportTitles: ''
                    ]
                )

                error "Could not push image ${it}"
            }
        }
    }

    publishHTML(
        [
            allowMissing: true,
            alwaysLinkToLastBuild: false,
            keepAll: true,
            reportDir: "${Configuration.get('LOGS')}",
            reportFiles: "docker-push.log",
            reportName: "Push Images Log",
            reportTitles: ''
        ]
    )

	logout(Configuration)
}

def pushTrusted(Configuration)
{
    login(Configuration)
    withCredentials([string(credentialsId: 'REPOSITORY_PASSPHRASE', variable: 'repo_phrase'), string(credentialsId: 'ROOT_PASSPHRASE', variable: 'root_phrase')]) {
        sh "DOCKER_CONTENT_TRUST_ROOT_PASSPHRASE=${root_phrase} DOCKER_CONTENT_TRUST_REPOSITORY_PASSPHRASE=${repo_phrase} DOCKER_CONTENT_TRUST=1 docker image push ${Configuration.get('DOCKER_IMAGE_APPLICATION')}"
        sh "DOCKER_CONTENT_TRUST_ROOT_PASSPHRASE=${root_phrase} DOCKER_CONTENT_TRUST_REPOSITORY_PASSPHRASE=${repo_phrase} DOCKER_CONTENT_TRUST=1 docker image push ${Configuration.get('DOCKER_IMAGE_NGINX')}"
        sh "DOCKER_CONTENT_TRUST_ROOT_PASSPHRASE=${root_phrase} DOCKER_CONTENT_TRUST_REPOSITORY_PASSPHRASE=${repo_phrase} DOCKER_CONTENT_TRUST=1 docker image push ${Configuration.get('DOCKER_IMAGE_VARNISH')}"
        if (Configuration.get('DOCKER_IMAGE_APPLICATION') != Configuration.get('DOCKER_IMAGE_ADMIN')) {
            sh "DOCKER_CONTENT_TRUST_ROOT_PASSPHRASE=${root_phrase} DOCKER_CONTENT_TRUST_REPOSITORY_PASSPHRASE=${repo_phrase} DOCKER_CONTENT_TRUST=1 docker image push ${Configuration.get('DOCKER_IMAGE_ADMIN')}"
        }

        if (Configuration.get('DOCKER_IMAGE_APPLICATION') != Configuration.get('DOCKER_IMAGE_TUNER')) {
            sh "DOCKER_CONTENT_TRUST_ROOT_PASSPHRASE=${root_phrase} DOCKER_CONTENT_TRUST_REPOSITORY_PASSPHRASE=${repo_phrase} DOCKER_CONTENT_TRUST=1 docker image push ${Configuration.get('DOCKER_IMAGE_TUNER')}"
        }
    }
    logout(Configuration)
}

def pull(Configuration)
{
    def file = "${Configuration.get('LOGS')}/docker-pull.log"
    sh "touch ${file} && truncate ${file} -s 0"

    def images = [
        Configuration.get('DOCKER_IMAGE_APPLICATION'),
        // Configuration.get('DOCKER_IMAGE_TUNER'),
        Configuration.get('DOCKER_IMAGE_NGINX'),
        // Configuration.get('DOCKER_IMAGE_ADMIN'),
        Configuration.get('DOCKER_IMAGE_VARNISH')
    ]

    login(Configuration)
    images.each {
        status = sh returnStatus: true, script: "2>&1 1>>${file} docker image pull ${it} 2>&1 1>>${file}"

        if (status != 0) {
            error "Unable to pull image [${it}]"
        }
    }
    logout(Configuration)
}

def tag(Container)
{
    def Configuration = Container.get('Configuration')
    def imageVariables = [
        'DOCKER_IMAGE_APPLICATION',
        'DOCKER_IMAGE_NGINX',
        'DOCKER_IMAGE_VARNISH',
    ]

    //  save current images
    def oldImages = [
        'DOCKER_IMAGE_APPLICATION' : Configuration.get('DOCKER_IMAGE_APPLICATION'),
        'DOCKER_IMAGE_NGINX' : Configuration.get('DOCKER_IMAGE_NGINX'),
        'DOCKER_IMAGE_VARNISH' : Configuration.get('DOCKER_IMAGE_VARNISH')
    ]

    echo "These images:"
    echo "${oldImages}"

    //  Create new image names from configuration
    Container.get('ImageNameFactory').createAll(Configuration, true)

    def newImages = [
        'DOCKER_IMAGE_APPLICATION' : Configuration.get('DOCKER_IMAGE_APPLICATION'),
        'DOCKER_IMAGE_NGINX' : Configuration.get('DOCKER_IMAGE_NGINX'),
        'DOCKER_IMAGE_VARNISH' : Configuration.get('DOCKER_IMAGE_VARNISH')
    ]

    echo "Will be tagget with:"
    echo "${newImages}"

    oldImages.each { name, index ->
        sh "docker image tag ${oldImages[name]} ${newImages[name]}"
    }
}

def isApplicationUp(Configuration, int iterations = 50, int timeout = 30) {

    def clientBundle = "DOCKER_HOST=${Configuration.get('DOCKER_CLIENT_BUNDLE_UCP_HOST')} DOCKER_TLS_VERIFY=1 DOCKER_CERT_PATH=${Configuration.get('DOCKER_CLIENT_BUNDLE_CERT_PATH')}"
    //  so, at this moment we will check only tuner container if it finished
    //  without errors

    def delimiter = "~~"
    def cmd = "${clientBundle} docker service ps ${Configuration.get('MAGE_STACK_NAME')}_tuner --format \"{{.Name}}${delimiter}{{.DesiredState}}${delimiter}{{.Error}}\""
    def result = ""
    def i = 0
    def chunks
    while (i <= iterations) {
        result = sh returnStdout: true, script: cmd

        if (result) {
            chunks = result.split("\n")[0].split(delimiter)
            if (chunks[1] == 'Shutdown' && chunks.length == 2) {
                return true
            }
        }
        i++
        sleep timeout
    }

    return false
}

def deploy(Container) {

    echo "Starting deployment preparation process"
    def Configuration = Container.get('Configuration')

    Configuration.loadGroovy([
        //  Loaded resources
        "${Configuration.get('SOURCE_CONFIG')}/env/swarm/resources.groovy"
    ])

    def dockerComposeFile = "${Configuration.get('SOURCE_DOCKER')}${Configuration.get('SOURCE_FOLDER_TELE2_ENV')}/docker-compose.yml"

    if (!Configuration.has('MAGE_STACK_NAME')) {
        Configuration.set('MAGE_STACK_NAME', "m2tele2_${Configuration.get('ENV')}")
    }

    Container.get('Envsubst').parse(dockerComposeFile, Configuration.all())
    sh "cat ${dockerComposeFile}"

    def clientBundle = "DOCKER_HOST=${Configuration.get('DOCKER_CLIENT_BUNDLE_UCP_HOST')} DOCKER_TLS_VERIFY=1 DOCKER_CERT_PATH=${Configuration.get('DOCKER_CLIENT_BUNDLE_CERT_PATH')}"
    def cmd = "${clientBundle} docker stack deploy -c ${dockerComposeFile} ${Configuration.get('MAGE_STACK_NAME')}"
    echo "${cmd}"
    sh "${cmd}"
}

def updateService(String env, String service, DockerConfiguration)
{
    def config = DockerConfiguration.getConfig(env)

    if (null == config) {
        return false
    }

    def clientBundle = "DOCKER_HOST=${config.UCP_HOST} DOCKER_TLS_VERIFY=1 DOCKER_CERT_PATH=${config.CERT_PATH}"
    def cmd = "${clientBundle} docker service update m2tele2_${env}_${service} --force"
    sh "${cmd}"
}

return this
