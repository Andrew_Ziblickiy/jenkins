class DockerConfiguration
{
    public static HTTP_INTERNET_PROXY = "http://proxy.dcn.versatel.net:3128"
    public static HTTPS_INTERNET_PROXY = "http://proxy.dcn.versatel.net:3128"
    public static HTTP_PROXY = "http://t2nl-b2bproxy.dmz.lan:8080"
    public static HTTPS_PROXY = "http://t2nl-b2bproxy.dmz.lan:8080"

    public static REGISTRY_HOST = "dtrdd.nl.corp.tele2.com:9443"
	public static REGISTRY_PATH = "development"
    public static STACK_NAME = "m2tele2"
    public static CONFIG_FOLDER = "tele2_dev"

    public static UCP_HOST = "tcp://dockerpde.nl.corp.tele2.com:8443"
    public static UCP_OWNER = "nlsvc-jenkins-docker"
    public static UCP_GROUP = "/Shared/Development"

    private static conf = [:]
    private static deployEnvs = [:]

    public class Ucp {
	    static HOST = "tcp://dockerpde.nl.corp.tele2.com:8443"
	    static OWNER = "nlsvc-jenkins-docker"
	    static GROUP = "/Shared/Development"
	}

    public static addEnvConfig(String env, String registryHost, String swarmHost, String ucpHost, String certPath, phpReplicas = 1, varnishreplicas = 2, nginxReplicas = 2, String dtrJenkinsCredentials = "nlsvc-jenkins-docker") {
		conf[env] = [
            "REGISTRY_HOST": registryHost,
            "DTR_JENKINS_CREDENTIALS" : dtrJenkinsCredentials,
            'SWARM_HOST' : swarmHost,
            'UCP_HOST' : ucpHost,
            'CERT_PATH' : certPath,
            'PHP_DEPLOY_REPLICAS' : phpReplicas,
            'VARNISH_DEPLOY_REPLICAS' : varnishreplicas,
            'NGINX_DEPLOY_REPLICAS' : nginxReplicas
        ]
	}

    public static getConfig(String env) {
		if (conf.containsKey(env)) {
			return conf[env]
		}
		return null
	}

    public static getRegistryHost(String env) {
		if (env) {
			def envConfig = conf[env]
			if (envConfig) {
				return envConfig.REGISTRY_HOST
			}
		}
		return null
	}

    public static getDtrCredentials(String env) {
        if (env) {
			def envConfig = conf[env]
			if (envConfig) {
				return envConfig.DTR_JENKINS_CREDENTIALS
			}
		}
		return null
    }

    public static addDeploymentEnvironment(String env) {
        deployEnvs[env] = true
    }

    public static canDeploy(String env) {
		def exists = deployEnvs.containsKey(env)

		return exists
	}

    public static getDeployemntEnvironments() {
        def keys = deployEnvs.keySet() as List

        return keys
    }
}

DockerConfiguration.addEnvConfig(
	'prd',
	'dtrpd.nl.corp.tele2.com:9443',
	'pde.nl.corp.tele2.com',
	'tcp://dockerpde.nl.corp.tele2.com:8443',
	'/var/jenkins_home/client-bundles/prd-dmz-client',
	6
)

DockerConfiguration.addEnvConfig(
	'dev',
	'dtrdd.nl.corp.tele2.com:9443',
	'dde.nl.corp.tele2.com',
	'tcp://dockerdde.nl.corp.tele2.com:8443',
	'/var/jenkins_home/client-bundles/dta-dmz-client',
    1
)

DockerConfiguration.addEnvConfig(
	'prf',
	'dtrdd.nl.corp.tele2.com:9443',
	'dde.nl.corp.tele2.com',
	'tcp://dockerdde.nl.corp.tele2.com:8443',
	'/var/jenkins_home/client-bundles/dta-dmz-client',
	1,
    1,
    1
)

DockerConfiguration.addEnvConfig(
	'int',
	'dtrdd.nl.corp.tele2.com:9443',
	'dde.nl.corp.tele2.com',
	'tcp://dockerdde.nl.corp.tele2.com:8443',
	'/var/jenkins_home/client-bundles/dta-dmz-client',
	2
)

DockerConfiguration.addEnvConfig(
	'uat',
	'dtrdd.nl.corp.tele2.com:9443',
	'dde.nl.corp.tele2.com',
	'tcp://dockerdde.nl.corp.tele2.com:8443',
	'/var/jenkins_home/client-bundles/dta-dmz-client',
	2
)

DockerConfiguration.addEnvConfig(
	'generic',
	'dtrdd.nl.corp.tele2.com:9443',
	'dde.nl.corp.tele2.com',
	'tcp://dockerdde.nl.corp.tele2.com:8443',
	'/var/jenkins_home/client-bundles/dta-dmz-client',
	1
)

DockerConfiguration.addEnvConfig(
	'puma',
	'dtrdd.nl.corp.tele2.com:9443',
	'dde.nl.corp.tele2.com',
	'tcp://dockerdde.nl.corp.tele2.com:8443',
	'/var/jenkins_home/client-bundles/dta-dmz-client',
	1
)

DockerConfiguration.addEnvConfig(
	'panda',
	'dtrdd.nl.corp.tele2.com:9443',
	'dde.nl.corp.tele2.com',
	'tcp://dockerdde.nl.corp.tele2.com:8443',
	'/var/jenkins_home/client-bundles/dta-dmz-client',
	1
)

DockerConfiguration.addEnvConfig(
	'neon',
	'dtrdd.nl.corp.tele2.com:9443',
	'dde.nl.corp.tele2.com',
	'tcp://dockerdde.nl.corp.tele2.com:8443',
	'/var/jenkins_home/client-bundles/dta-dmz-client',
	1
)

DockerConfiguration.addEnvConfig(
	'tst',
	'dtrdd.nl.corp.tele2.com:9443',
	'dde.nl.corp.tele2.com',
	'tcp://dockerdde.nl.corp.tele2.com:8443',
	'/var/jenkins_home/client-bundles/dta-dmz-client',
	1
)

DockerConfiguration.addEnvConfig(
	'sprint',
	'dtrdd.nl.corp.tele2.com:9443',
	'dde.nl.corp.tele2.com',
	'tcp://dockerdde.nl.corp.tele2.com:8443',
	'/var/jenkins_home/client-bundles/dta-dmz-client',
	1
)

return DockerConfiguration
