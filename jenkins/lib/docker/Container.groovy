containerId = null

def getContainerId()
{
    return containerId
}

def start(String command)
{
    if (containerId) {
        return containerId
    }

    ensureThatDumpDirectoryExists()

    dumpDir = getDumpDirectory()
    containerId = sh returnStdout: true, script: command
    containerId = containerId.trim()

    echo "Container was started: id #${containerId}"
    echo "Dump directory: ${dumpDir}"

    ensureContainerReadyForWork()

    return containerId
}

def stop()
{
    if (containerId) {
        sh "docker stop ${containerId}"
        containerId = null
    }
}

def execute(String command)
{
    echo "Executing: ${command}"
    sh "docker exec -i ${containerId} ${command}"
}

def ensureContainerIsUp(int timeout = 3, int iterations = 3) {
    def iteration = 0
    def status = 'false'

    echo "Checking is container up"

    while ({
        sleep(timeout)
        iteration++

        status = sh returnStdout: true, script: "docker inspect -f {{.State.Running}} ${containerId}"
        status = status.trim()

        echo "Is up [${iteration}]: ${status}"
        (status == 'false' && iteration <= iterations)
    }()) continue

    if (status != 'true') {
        error "It looks like container does not exists or not found, or incorrect logic"
    }
}

def ensureContainerReadyForWork()
{
    ensureContainerIsUp()
}

return this
