def createBase(Configuration)
{
    return "${Configuration.get('DOCKER_REGISTRY_HOST')}/${Configuration.get('DOCKER_REGISTRY_PATH')}/${Configuration.get('DOCKER_IMAGE_PREFIX')}"
}

def create(Configuration, String nameKey, String tagKey, String configurationKey, boolean force = false)
{
    def image = "${createBase(Configuration)}${Configuration.get(nameKey)}:${Configuration.get(tagKey)}"

    if (force || !Configuration.get(configurationKey)) {
        Configuration.set(configurationKey, image)
    }

    return image
}

def createAll(Configuration, boolean force = false)
{
    createApplicationImageName(Configuration, force)
    createAdminImageName(Configuration, force)
    createTunerImageName(Configuration, force)
    createMagentoImageName(Configuration, force)
    createNginxImageName(Configuration, force)
    createVarnishImageName(Configuration, force)
}

def createApplicationImageName(Configuration, boolean force = false)
{
    return create(
        Configuration,
        'DOCKER_IMAGE_APPLICATION_NAME',
        'DOCKER_IMAGE_APPLICATION_TAG',
        'DOCKER_IMAGE_APPLICATION',
        force
    )
}

def createAdminImageName(Configuration, boolean force = false)
{
    return create(
        Configuration,
        'DOCKER_IMAGE_ADMIN_NAME',
        'DOCKER_IMAGE_ADMIN_TAG',
        'DOCKER_IMAGE_ADMIN',
        force
    )
}

def createTunerImageName(Configuration, boolean force = false)
{
    return create(Configuration, 'DOCKER_IMAGE_TUNER_NAME', 'DOCKER_IMAGE_TUNER_TAG', 'DOCKER_IMAGE_TUNER', force)
}

def createMagentoImageName(Configuration, boolean force = false)
{
    return create(Configuration, 'DOCKER_IMAGE_MAGENTO_NAME', 'DOCKER_IMAGE_MAGENTO_TAG', 'DOCKER_IMAGE_MAGENTO', force)
}

def createNginxImageName(Configuration, boolean force = false)
{
    return create(Configuration, 'DOCKER_IMAGE_NGINX_NAME', 'DOCKER_IMAGE_NGINX_TAG', 'DOCKER_IMAGE_NGINX', force)
}

def createVarnishImageName(Configuration, boolean force = false)
{
    return create(Configuration, 'DOCKER_IMAGE_VARNISH_NAME', 'DOCKER_IMAGE_VARNISH_TAG', 'DOCKER_IMAGE_VARNISH', force)
}

return this
