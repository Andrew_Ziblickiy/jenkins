class Container
{
    protected context
    protected id
    protected name

    Container(context, name)
    {
        this.context = context
        this.name = name
    }

    public id()
    {
        this.id
    }

    public start(String command)
    {
        if (this.id) {
            return this.id
        }

        this.context.echo "Starting new container with command: ${command}"
        def containerId = this.context.sh returnStdout: true, script: command
        this.id = containerId.trim()

        this.context.echo "Container was started: id #${this.id}"

        this.ensureContainerIsUp()

        return this.id
    }

    public isUp()
    {

    }

    public stop(boolean rm = false)
    {
        def status = 0
        if (this.id) {
            this.context.echo "Stopping container #${this.id}"
            try {
                status = this.context.sh returnStatus: true, script: "docker stop ${this.id}"

                if (rm) {
                    this.context.sh "docker container rm ${this.id}"
                }

            } catch(e) {
                status = 0
            }

            this.id = null
        }

        return status
    }

    public getIp()
    {
        def ip = this.context.sh returnStdout: true, script: "docker inspect --format='{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' ${this.id}"
        return ip.trim()
    }

    public execute(String command, returnStdout = false, String options = "")
    {
        this.context.echo "Executing: ${command}"
        def stdOut = ""
        if (returnStdout) {
            stdOut = this.context.sh returnStdout: true, script: "docker exec -i ${options} ${this.id} ${command}"
        } else {
            this.context.sh "docker exec -i ${options} ${this.id} ${command}"
        }
        return stdOut
    }

    protected ensureContainerIsUp(int timeout = 3, int iterations = 3) {
        def iteration = 0
        def status = 'false'
        def containerId = this.id

        this.context.echo "Checking is container up"

        while ({
            this.context.sleep(timeout)
            iteration++

            status = this.context.sh returnStdout: true, script: "docker inspect -f {{.State.Running}} ${containerId}"
            status = status.trim()

            this.context.echo "Is up [${iteration}]: ${status}"
            (status == 'false' && iteration <= iterations)
        }()) continue

        if (status != 'true') {
            this.context.error "It looks like container does not exists or not found, or incorrect logic"
        }
    }
}

def create(String name = null)
{
    return new Container(this, name)
}

return this
