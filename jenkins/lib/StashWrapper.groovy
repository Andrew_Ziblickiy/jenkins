BIN = ""
def Config
ERROR_MESSAGE = ""
ERROR_FILE = ""
ERROR_REMOVE_FILE = true
ERROR_FULL_LOG = false

def setError(String message, String file = "", boolean remove = true, boolean fullLog = false)
{
    ERROR_MESSAGE = message
    ERROR_FILE = file
    ERROR_REMOVE_FILE = remove
    ERROR_FULL_LOG = fullLog

    echo "Setting error"
    echo message
    echo file
    echo remove
    echo fullLog

    return this
}

def init(Container)
{
    Config = Container.get('Configuration')
    BIN = "${Config.get('SOURCE_STASH_API')}/src/Stash/bin/console.php"

    Config.loadGroovy([
        "${Config.get('SOURCE_CONFIG')}/env/build/pull-request.groovy"
    ])
    Config.set('PR_DOES_NOT_EXISTS', false)
    echo "We are here INIT"
    Container.get('Git').pullStashApi(Config)

    isVendorInstalled = sh returnStatus: true, script: "ls -l ${Config.get('SOURCE_STASH_API')}/vendor > /dev/null"
    if (isVendorInstalled != 0) {
        def proxy = "https_proxy=${Config.get('HTTPS_INTERNET_PROXY')} http_proxy=${Config.get('HTTP_INTERNET_PROXY')}"
        // sh "docker run --rm -i -e https_proxy=http://proxy.dcn.versatel.net:3128 -e http_proxy=http://proxy.dcn.versatel.net:3128 -v ${workspace}/${Config.get('SOURCE_STASH_API')}:/var/www --workdir /var/www php:7.1-fpm curl -o composer.phar http://getcomposer.org/composer.phar"
        // sh "docker run --rm -i -e https_proxy=http://proxy.dcn.versatel.net:3128 -e http_proxy=http://proxy.dcn.versatel.net:3128 -v ${workspace}/${Config.get('SOURCE_STASH_API')}:/var/www --workdir /var/www php:7.1-fpm php composer.phar install --no-dev --prefer-dist -o"
        sh "wget http://getcomposer.org/composer.phar && mv composer.phar ${Config.get('SOURCE_STASH_API')}"
        sh "cd ${Config.get('SOURCE_STASH_API')} && php composer.phar install --no-dev --prefer-dist -o"
    }

    configure(Container, Config)
}

def configure(Container, Configuration)
{
    def parametersFile = "${workspace}/${Configuration.get('SOURCE_CONFIG')}/jenkins/stash/parameters-parsed.yml"
    sh "cp -f ${workspace}/${Configuration.get('SOURCE_CONFIG')}/jenkins/stash/parameters.yml ${parametersFile}"

    //	configure username and password
    withCredentials([usernamePassword(credentialsId: Configuration.get('REPOSITORY_CREDENTIALS'), usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
        def parameters = readFile parametersFile
        parameters = parameters.replaceAll("#username#", "${USERNAME}")
        parameters = parameters.replaceAll("#password#", "${PASSWORD}")
        writeFile file: parametersFile , text: parameters
    }

    execute("conf:import ${parametersFile} 2>&1 1>/dev/null")

    isConfigValid = execute("2>&1 1>/dev/null conf:validate 2>&1 1>/dev/null", true)

    if (isConfigValid != 0) {
        error "Stash Api not properly configured"
    }
}

def execute(String command, boolean returnStatus = false, boolean returnStdout = false)
{
    //'SOURCE_STASH_API' : 'stash_api',

    //execute("pr:show --format \"{.DCI}\" ${Config.get('PULL_REQUEST_ID')}", false, true)

    //BIN = "https://bitbucket.nl.corp.tele2.com/projects/MTEL/repos/jenkins-stash-integration/browse/src/Stash/bin/console.php"



    //BIN = "${'stash_api'}/src/Stash/bin/console.php"

    if (returnStatus) {
		def status = sh returnStatus: true, script: "php ${BIN} ${command}"
		return status
	} else if (returnStdout) {
		def out = sh returnStdout: true, script: "php ${BIN} ${command}"
		return out
	} else {
		sh "php ${BIN} ${command}"
	}
}

def merge()
{
    execute("pr:merge ${Config.get('PULL_REQUEST_ID')}", true)
}

def start()
{
    execute("pr:jenkins:job --buildId ${Config.get('BUILD_ID')} ${Config.get('PULL_REQUEST_ID')} start", true)
}

def stopWithErrors()
{
    def msg = ""
    def msgFile = ""
    def status = 0
    def keys = ""

    if (ERROR_MESSAGE) {
        msg = "--message '${ERROR_MESSAGE}'"
    }

    if (ERROR_FILE) {
        msgFile = "--messageFile ${ERROR_FILE}"
        if (ERROR_FULL_LOG) {
            keys = "${keys} --messageFileFullLength 1"
            ERROR_FULL_LOG = false
        }
    }

    status = execute("pr:jenkins:job --buildId ${Config.get('BUILD_ID')} --error ${keys} ${msg} ${msgFile} ${Config.get('PULL_REQUEST_ID')} stop", true)

    if (ERROR_FILE && ERROR_REMOVE_FILE) {
        sh "rm -f ${ERROR_FILE}"
    }

    return status
}

def stopWithSuccess()
{
    execute("pr:jenkins:job --success ${Config.get('PULL_REQUEST_ID')} stop", true)

    return this
}

def getChangeSet(String keys = "")
{
    return execute("pr:changes --base-path=${Config.get('BUILD_SOURCE')}/ ${keys} --skip-deleted --only-files -- ${Config.get('PULL_REQUEST_ID')}", false, true)
}

def postComment(String prId, String message = "")
{
    execute("pr:comment:post ${prId} \"${message}\"")
}

def progress(String message = "")
{

}

def approve()
{
    execute("pr:approve ${Config.get('PULL_REQUEST_ID')}", true)
    return this
}

def unApprove()
{
    execute("pr:unapprove ${Config.get('PULL_REQUEST_ID')}", true)
    return this
}

def isDestinationBranchWasModified()
{
    def currentCommit = execute("pr:show --format \"{.DCI}\" ${Config.get('PULL_REQUEST_ID')}", false, true)
    echo "Build comment ${Config.get('PULL_REQUEST_TO_HASH')}, current commit ${currentCommit}"

    return Config.get('PULL_REQUEST_TO_HASH') != currentCommit
}

return this
