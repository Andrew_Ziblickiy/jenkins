def resolveHost(String env)
{
    echo "Resolving mysql host for ${env} env"
    if (env == "prd") {
        dbhost = "magentodb.nl.corp.tele2.com"
    } else if (env == 'tst' || env == 'neon') {
        dbhost = "mysql"
    } else {
        dbhost = "magentodb.${env}.nl.corp.tele2.com"
    }

    echo "Mysql Host: ${dbhost}"
    return dbhost
}

def getMysqlPasswordCredentialsId(String env)
{
    return "magento2-mysql-${env}-pass"
}

return this
