cloneDirectory = "m2application"

def getApplicationVersion(Configuration)
{
    def version
    try {
        clone(Configuration)
        def branches = fetchBranches()
        branches = parse(branches)
        version = getLatest(branches)
    } finally {
        clean(Configuration)
    }
    echo "Latest Application version is: ${version}"

    return version
}

def getLatest(branchMaps)
{
    def keys = branchMaps.keySet()
    def list = []
    echo "Branche keys: ${keys}"
    keys.each {
        list.add(it.toInteger())
    }
    list.sort()
    echo "Sorted: ${list}"
    def currentVersion = list.last()
    echo "Version: ${currentVersion}"
    def latest = branchMaps.get("${currentVersion}")
    return latest
}

def parse(branches)
{
    def parsed = [:]
    branches.each { name, index ->
        def t = name.split('_')
        def version = t[0].split("\\.")
        def sprint = t[1].split('S')
        def major = version[0]
        def minor = formatVersionNumber(version[1])
        def patch = formatVersionNumber(version[2])
        parsed.put("${sprint[0]}${sprint[1]}${major}${minor}${patch}", [
            'name' : name,
            'id' : "${sprint[0]}${sprint[1]}",
            'year' : sprint[0],
            'sprint' : sprint[1],
            'major' : major,
            'minor' : minor,
            'patch' : patch
        ])
    }
    return parsed
}

def formatVersionNumber(String versionNumber)
{
    if (versionNumber.toInteger() < 10) {
        versionNumber = "0${versionNumber}"
    }
    return versionNumber
}

def fetchBranches()
{
    def b = sh returnStdout: true, script: "cd ${cloneDirectory} && git fetch && git branch --all | grep remotes/origin/release/"
    b = b.split("\n")
    def branches = []
    b.each { name, index ->
        branches.add(name.trim().replace('remotes/origin/release/', ''))
    }
    return branches
}

def clone(Configuration)
{
    sh "git clone ${Configuration.get('REPOSITORY_APPLICATION')} ${cloneDirectory}"
}

def clean(Configuration)
{
    sh "rm -Rf ${cloneDirectory}"
}

return this
