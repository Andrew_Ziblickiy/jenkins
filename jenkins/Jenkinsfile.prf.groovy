//  Jenkins
dtrIntCredentials = "nlsvc-jenkins-docker"
dtrPrfCredentials = "nlsvc-jenkins-docker"
gitCredentials = "nlsvc-jenkins-docker"
configRepositoryUrl = "https://bitbucket.nl.corp.tele2.com/scm/mtel/tel2-m2-config.git"

//  Application
appFolder = "app"
appRepostoryUrl = "https://bitbucket.nl.corp.tele2.com/scm/mtel/tel2-m2-app.git"
dockerFolder = "docker"
dockerRepositoryUrl = "https://bitbucket.nl.corp.tele2.com/scm/mtel/tel2-docker-env.git"
env = "prf"

//  Docker Registry
dockerRegistryHostInt = "dtrdd.nl.corp.tele2.com:9443"
dockerRegistryHostPrf = "dtrdd.nl.corp.tele2.com:9443"
dockerRegistryPathInt = "/development/mag-"
dockerRegistryPathPrf = "/development/mag-"

//  Docker Client Bundle
dockerClientBundle = "DOCKER_HOST=tcp://dockerdde.nl.corp.tele2.com:8443 DOCKER_TLS_VERIFY=1 DOCKER_CERT_PATH=/var/jenkins_home/client-bundles/dta-dmz-client"

//  Docker UCP
dockerUCPOwner = "nlsvc-jenkins-docker"
dockerUCPGroup = "/Shared/Development"

//  Docker swarm
stackName = "m2tele2"
swarmAppEndpoint = "dde.nl.corp.tele2.com"
swarmAppHost = "${env}.${swarmAppEndpoint}"

//  Appdynamics
appdynamicsConfig = [
    "CONTROLLER_HOST" : "appdcontrol.tele2.com",
    "CONTROLLER_PORT" : 443,
    "APPLICATION_NAME" : "nl-magento-prf",
    "TIER_NAME" : "magento-php",
    "ACCOUNT_NAME" : "test",
    "ACCOUNT_ACCESS_KEY" : "cfa937b2-1c77-4978-9e7e-9ab07b948a63"
]

dockerImagePullTag = "int"
shopImage = "${dockerRegistryHostPrf}${dockerRegistryPathPrf}shop:release"
shopTunerImage = "${dockerRegistryHostPrf}${dockerRegistryPathPrf}shop-tuner:release"
varnishImage = "${dockerRegistryHostPrf}${dockerRegistryPathPrf}varnish:release"
nginxImage = "${dockerRegistryHostPrf}${dockerRegistryPathPrf}nginx:release"

def Docker
def DockerConfiguration

properties([buildDiscarder(logRotator(artifactDaysToKeepStr: '', artifactNumToKeepStr: '', daysToKeepStr: '3', numToKeepStr: '5')), disableConcurrentBuilds(), pipelineTriggers([])])

node{
    try {
    	stage('Request Release Int'){
    	    timeout(time: 15, unit: 'MINUTES') {
    	        input message: 'Want to promote to Int?', ok: 'Promote'//, submitter: 'app_jenkins_unix_release'
    	        node {

                    stage('Pull image') {

                        withCredentials([usernamePassword(credentialsId: "${dtrIntCredentials}", usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
                	        sh "docker login -u ${USERNAME} -p ${PASSWORD} ${dockerRegistryHostInt}"
                	        sh "docker pull ${dockerRegistryHostInt}${dockerRegistryPathInt}shop:${dockerImagePullTag}"
                            sh "docker pull ${dockerRegistryHostInt}${dockerRegistryPathInt}shop-tuner:${dockerImagePullTag}"
                	        sh "docker pull ${dockerRegistryHostInt}${dockerRegistryPathInt}nginx:${dockerImagePullTag}"
                	        sh "docker pull ${dockerRegistryHostInt}${dockerRegistryPathInt}varnish:${dockerImagePullTag}"
                	        sh "docker logout ${dockerRegistryHostInt}"
                	    }
                    }

                    stage('Tag images with release') {
                        sh "docker image tag ${dockerRegistryHostInt}${dockerRegistryPathInt}shop:${dockerImagePullTag} ${shopImage}"
                        sh "docker image tag ${dockerRegistryHostInt}${dockerRegistryPathInt}shop-tuner:${dockerImagePullTag} ${shopTunerImage}"
                        sh "docker image tag ${dockerRegistryHostInt}${dockerRegistryPathInt}varnish:${dockerImagePullTag} ${varnishImage}"
                        sh "docker image tag ${dockerRegistryHostInt}${dockerRegistryPathInt}nginx:${dockerImagePullTag} ${nginxImage}"
                    }

                    stage('Push tagged images') {
                        withCredentials([usernamePassword(credentialsId: "${dtrPrfCredentials}", usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
                	        sh "docker login -u ${USERNAME} -p ${PASSWORD} ${dockerRegistryHostPrf}"
                            sh "docker image push ${shopImage}"
                            sh "docker image push ${shopTunerImage}"
                            sh "docker image push ${varnishImage}"
                            sh "docker image push ${nginxImage}"
                	        sh "docker logout ${dockerRegistryHostPrf}"
                	    }
                    }


                    stage('Deploy to Prf') {
                        checkout changelog: false, poll: false, scm: [$class: 'GitSCM', branches: [[name: "master"]], browser: [$class: 'Stash', repoUrl: ''], doGenerateSubmoduleConfigurations: false, extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: dockerFolder], [$class: 'CleanBeforeCheckout']], submoduleCfg: [], userRemoteConfigs: [[credentialsId: gitCredentials, url: dockerRepositoryUrl]]]
                        checkout changelog: false, poll: false, scm: [$class: 'GitSCM', branches: [[name: "master"]], browser: [$class: 'Stash', repoUrl: ''], doGenerateSubmoduleConfigurations: false, extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: "config"], [$class: 'CleanBeforeCheckout']], submoduleCfg: [], userRemoteConfigs: [[credentialsId: gitCredentials, url: configRepositoryUrl]]]

                        def HostNameParser = load "config/jenkins/HostNameParser.groovy"
                        def StackResource = load "config/jenkins/StackResource.groovy"
                        Docker = load "config/jenkins/Docker.groovy"
                        def ImageNameProvider = load "config/jenkins/ImageNameProvider.groovy"
                        def SourceFolder = load "config/jenkins/SourceFolderProvider.groovy"
                        def AppdynamicsConfigProvider = load "config/jenkins/AppdynamicsConfigProvider.groovy"

                        DockerConfiguration = load "config/jenkins/DockerConfiguration.groovy"
                        DockerConfiguration.addDeploymentEnvironment(env)

                        ImageNameProvider.add(shopImage, 'app')
                        ImageNameProvider.add(shopTunerImage, 'tuner')
                        ImageNameProvider.add(nginxImage, 'nginx')
                        ImageNameProvider.add(varnishImage, 'varnish')

                        Docker.deploy(env, ImageNameProvider, DockerConfiguration, SourceFolder, HostNameParser, AppdynamicsConfigProvider, StackResource)
                    }

                    stage("Wait for an Application") {

                        if (!Docker.isApplicationUp("${env}", DockerConfiguration)) {
                            error "Application is not ready or we exceed our timeout limits"
                        }

                        echo "Application is up"
                    }

                    stage("Update varnish") {
                        Docker.updateService("${env}", 'varnish', DockerConfiguration)
                        sleep 3
                    }
    	        }
    	    }
    	}
    } catch (error) {
    	throw error
    }
}
