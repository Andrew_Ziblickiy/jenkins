dumpDirectory = "dump"

def resolveHost(String env)
{
    echo "Resolving mysql host for ${env} env"
    if (env == "prd") {
        dbhost = "magentodb.nl.corp.tele2.com"
    } else {
        dbhost = "magentodb.${env}.nl.corp.tele2.com"
    }

    echo "Mysql Host: ${dbhost}"
    return dbhost
}

def makeFileName(String env, String date)
{
    def dumpDir = getDumpDirectory()
    return "${dumpDir}/${date}.${env}.dump.sql"
}

def startContainer(String dumpDir)
{
    def containerId = sh returnStdout: true, script: "docker run --env MYSQL_ROOT_PASSWORD='root' --env MYSQL_ALLOW_EMPTY_PASSWORD='1' --rm -i -d -v ${dumpDir}:/dump percona"
    containerId = containerId.trim()

    echo "Container was started: id #${containerId}"
    return containerId
}

def ensureMysqlIsUp(String containerId, int timeout = 5, int iterations = 10)
{
    def iteration = 0
    def status = 'is not running'

    echo "Checking is mysql up"

    while ({
        sleep(timeout)
        iteration++

        status = sh returnStdout: true, script: "docker exec -i ${containerId} service mysql status"

        echo "Status: ${status}"
        status = status.trim().contains('is running')

        echo "Is up [${iteration}]: ${status}"
        (!status && iteration <= iterations)
    }()) continue

    if (!status) {
        error "Timeout is exceeded while waiting for "
    }

    //  sometimes additional time needed for mysql sock
    sleep(5)
}

def ensureContainerIsUp(String containerId, int timeout = 3, int iterations = 3) {
    def iteration = 0
    def status = 'false'

    echo "Checking is container up"

    while ({
        sleep(timeout)
        iteration++

        status = sh returnStdout: true, script: "docker inspect -f {{.State.Running}} ${containerId}"
        status = status.trim()

        echo "Is up [${iteration}]: ${status}"
        (status == 'false' && iteration <= iterations)
    }()) continue

    if (status != 'true') {
        error "It looks like container does not exists or not found, or incorrect logic"
    }
}

def ensureConainerReadyForWork(String containerId)
{
    ensureContainerIsUp(containerId)
    ensureMysqlIsUp(containerId)
}

def executeCommand(String containerId, String command)
{
    echo "Executing: ${command}"
    sh "docker exec -i ${containerId} ${command}"
}

def getMysqlPasswordCredentialsId(String env)
{
    return "magento2-mysql-${env}-pass"
}

def optimizeDatabase(String containerId)
{
    echo "Optimizing database"
    def queries = "TRUNCATE queue_message_status; DELETE FROM queue_message; DELETE FROM tele2_order_request; TRUNCATE customer_visitor; TRUNCATE report_event; TRUNCATE report_viewed_product_index; delete from quote_item_option; delete from quote_item; delete from quote_address; delete from quote; truncate media_storage_file_storage;"
    executeCommand(containerId, "mysql -uroot -proot -D magento -e \"${queries}\"")
}

def prepareMysqlDockerImage()
{
    //  Pull mysql image
    sh "docker pull percona"
}

def makeDump(String containerId, String host, String username, String password, String file)
{
    //  Create database
    executeCommand(containerId, "mysql -uroot -proot -e \"CREATE DATABASE magento CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;\"")

    //  Make dump
    executeCommand(containerId, "mysqldump -h ${host} --skip-lock-tables -u ${username} -p${password} magento > ${file}")

    //  Install dump
    executeCommand(containerId, "mysql -uroot -proot -D magento < ${file}")

    //  Configure our installed dump
    optimizeDatabase(containerId)

    //  Make optimized dump
    executeCommand(containerId, "mysqldump -uroot -proot magento > ${file}")
}

def removePreviousDump(String env)
{
    def prevDumpFileName = makeFileName(env, getPreviousDayDate())

    sh "rm -f ${prevDumpFileName}"
}

def ensureThatDumpDirectoryExists()
{
    def dumpDir = getDumpDirectory()
    sh "mkdir -p ${dumpDir}"
}

def getDumpDirectory()
{
    return "${workspace}/${dumpDirectory}"
}

def getCurrentDate()
{
    def date = sh returnStdout: true, script: "date +%Y-%m-%d"
    return date.trim()
}

def getPreviousDayDate()
{
    def prevDate = sh returnStdout: true, script: "date -d \"yesterday 13:00\" '+%Y-%m-%d'"
    return prevDate.trim()
}

def validateEnvironment(String env)
{
    if (env == 'prd') {
        error "MysqlDumpFactory is not supporting prd database instance right now, sorry, contact AtheD, aliaksei.panasik@tele2.com for support"
    }
}

def create(String env)
{
    validateEnvironment(env)

    def host = resolveHost(env)
    def date = getCurrentDate()
    def fileName = makeFileName(env, date)

    ensureThatDumpDirectoryExists()
    isDumpFileExists = fileExists fileName

    if (!isDumpFileExists) {
        removePreviousDump(env)
        prepareMysqlDockerImage()

        def containerId = startContainer(getDumpDirectory())

        try {
            ensureConainerReadyForWork(containerId)

            withCredentials([usernamePassword(credentialsId: getMysqlPasswordCredentialsId(env), usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
                makeDump(containerId, host, "${USERNAME}", "${PASSWORD}", fileName)
            }
        } catch(e) {
            sh "rm -f ${fileName}" //  Remove generated file in case file was created but on some stage we got error
            error "${e}"
        } finally {
            sh "docker stop ${containerId}"
        }
    }

    return fileName
}

return this
