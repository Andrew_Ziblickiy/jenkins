class DockerConfiguration
{
    public static HTTP_INTERNET_PROXY = "http://proxy.dcn.versatel.net:3128"
    public static HTTPS_INTERNET_PROXY = "http://proxy.dcn.versatel.net:3128"
    public static HTTP_PROXY = "http://t2nl-b2bproxy.dmz.lan:8080"
    public static HTTPS_PROXY = "http://t2nl-b2bproxy.dmz.lan:8080"

    public static REGISTRY_HOST = "dtrdd.nl.corp.tele2.com:9443"
	public static REGISTRY_PATH = "development"
    public static STACK_NAME = "m2tele2"
    public static CONFIG_FOLDER = "tele2_dev"

    public static UCP_HOST = "tcp://dockerpde.nl.corp.tele2.com:8443"
    public static UCP_OWNER = "nlsvc-jenkins-docker"
    public static UCP_GROUP = "/Shared/Development"

    private static conf = [:]
    private static deployEnvs = [:]

    public class Ucp {
	    static HOST = "tcp://dockerpde.nl.corp.tele2.com:8443"
	    static OWNER = "nlsvc-jenkins-docker"
	    static GROUP = "/Shared/Development"
	}

    public static addEnvConfig(String env, Map config) {
		conf[env] = config
	}

    public static getConfig(String env) {
		if (conf.containsKey(env)) {
			return conf[env]
		}
		return null
	}

    public static getDtrCredentials(String env) {
        if (env) {
            def envConfig = conf[env]
            if (envConfig) {
                return envConfig.DTR_JENKINS_CREDENTIALS
            }
        }
        return null
    }

    public static getRegistryHost(String env) {
		if (env) {
			def envConfig = conf[env]
			if (envConfig) {
				return envConfig.REGISTRY_HOST
			}
		}
		return null
	}

    public static addDeploymentEnvironment(String env) {
        deployEnvs[env] = true
    }

    public static canDeploy(String env) {
		def exists = deployEnvs.containsKey(env)

		return exists
	}

    public static getDeployemntEnvironments() {
        def keys = deployEnvs.keySet() as List

        return keys
    }
}

DockerConfiguration.addEnvConfig(
	'prd',
    [
        "REGISTRY_HOST": 'dtrpd.nl.corp.tele2.com:9443',
        'SWARM_HOST' : 'pde.nl.corp.tele2.com',
        "DTR_JENKINS_CREDENTIALS" : 'nlsvc-jenkins-docker',
        'UCP_HOST' : 'tcp://dockerpde.nl.corp.tele2.com:8443',
        'CERT_PATH' : '/var/jenkins_home/client-bundles/prd-dmz-client',
        'PHP_DEPLOY_REPLICAS' : 6,
        'PHP_UPDATE_CONFIG_PARALELISM' : 2,
        'VARNISH_DEPLOY_REPLICAS' : 2,
        'NGINX_DEPLOY_REPLICAS' : 2
    ]
)

DockerConfiguration.addEnvConfig(
	'dev',
    [
        "REGISTRY_HOST": 'dtrdd.nl.corp.tele2.com:9443',
        'SWARM_HOST' : 'dde.nl.corp.tele2.com',
        "DTR_JENKINS_CREDENTIALS" : 'nlsvc-jenkins-docker',
        'UCP_HOST' : 'tcp://dockerdde.nl.corp.tele2.com:8443',
        'CERT_PATH' : '/var/jenkins_home/client-bundles/dta-dmz-client',
        'PHP_DEPLOY_REPLICAS' : 1,
        'PHP_UPDATE_CONFIG_PARALELISM' : 1,
        'VARNISH_DEPLOY_REPLICAS' : 2,
        'NGINX_DEPLOY_REPLICAS' : 1
    ]
)

DockerConfiguration.addEnvConfig(
	'prf',
    [
        "REGISTRY_HOST": 'dtrdd.nl.corp.tele2.com:9443',
        'SWARM_HOST' : 'dde.nl.corp.tele2.com',
        "DTR_JENKINS_CREDENTIALS" : 'nlsvc-jenkins-docker',
        'UCP_HOST' : 'tcp://dockerdde.nl.corp.tele2.com:8443',
        'CERT_PATH' : '/var/jenkins_home/client-bundles/dta-dmz-client',
        'PHP_DEPLOY_REPLICAS' : 1,
        'PHP_UPDATE_CONFIG_PARALELISM' : 1,
        'VARNISH_DEPLOY_REPLICAS' : 1,
        'NGINX_DEPLOY_REPLICAS' : 1
    ]
)

DockerConfiguration.addEnvConfig(
	'int',
    [
        "REGISTRY_HOST": 'dtrdd.nl.corp.tele2.com:9443',
        'SWARM_HOST' : 'dde.nl.corp.tele2.com',
        "DTR_JENKINS_CREDENTIALS" : 'nlsvc-jenkins-docker',
        'UCP_HOST' : 'tcp://dockerdde.nl.corp.tele2.com:8443',
        'CERT_PATH' : '/var/jenkins_home/client-bundles/dta-dmz-client',
        'PHP_DEPLOY_REPLICAS' : 2,
        'PHP_UPDATE_CONFIG_PARALELISM' : 1,
        'VARNISH_DEPLOY_REPLICAS' : 2,
        'NGINX_DEPLOY_REPLICAS' : 2
    ]
)

DockerConfiguration.addEnvConfig(
	'uat',
    [
        "REGISTRY_HOST": 'dtrdd.nl.corp.tele2.com:9443',
        'SWARM_HOST' : 'dde.nl.corp.tele2.com',
        "DTR_JENKINS_CREDENTIALS" : 'nlsvc-jenkins-docker',
        'UCP_HOST' : 'tcp://dockerdde.nl.corp.tele2.com:8443',
        'CERT_PATH' : '/var/jenkins_home/client-bundles/dta-dmz-client',
        'PHP_DEPLOY_REPLICAS' : 2,
        'PHP_UPDATE_CONFIG_PARALELISM' : 1,
        'VARNISH_DEPLOY_REPLICAS' : 2,
        'NGINX_DEPLOY_REPLICAS' : 2
    ]
)

DockerConfiguration.addEnvConfig(
	'generic',
    [
        "REGISTRY_HOST": 'dtrdd.nl.corp.tele2.com:9443',
        'SWARM_HOST' : 'dde.nl.corp.tele2.com',
        "DTR_JENKINS_CREDENTIALS" : 'nlsvc-jenkins-docker',
        'UCP_HOST' : 'tcp://dockerdde.nl.corp.tele2.com:8443',
        'CERT_PATH' : '/var/jenkins_home/client-bundles/dta-dmz-client',
        'PHP_DEPLOY_REPLICAS' : 1,
        'PHP_UPDATE_CONFIG_PARALELISM' : 1,
        'VARNISH_DEPLOY_REPLICAS' : 1,
        'NGINX_DEPLOY_REPLICAS' : 1
    ]
)

DockerConfiguration.addEnvConfig(
	'puma',
    [
        "REGISTRY_HOST": 'dtrdd.nl.corp.tele2.com:9443',
        'SWARM_HOST' : 'dde.nl.corp.tele2.com',
        "DTR_JENKINS_CREDENTIALS" : 'nlsvc-jenkins-docker',
        'UCP_HOST' : 'tcp://dockerdde.nl.corp.tele2.com:8443',
        'CERT_PATH' : '/var/jenkins_home/client-bundles/dta-dmz-client',
        'PHP_DEPLOY_REPLICAS' : 1,
        'PHP_UPDATE_CONFIG_PARALELISM' : 1,
        'VARNISH_DEPLOY_REPLICAS' : 1,
        'NGINX_DEPLOY_REPLICAS' : 1
    ]
)

DockerConfiguration.addEnvConfig(
	'panda',
    [
        "REGISTRY_HOST": 'dtrdd.nl.corp.tele2.com:9443',
        'SWARM_HOST' : 'dde.nl.corp.tele2.com',
        "DTR_JENKINS_CREDENTIALS" : 'nlsvc-jenkins-docker',
        'UCP_HOST' : 'tcp://dockerdde.nl.corp.tele2.com:8443',
        'CERT_PATH' : '/var/jenkins_home/client-bundles/dta-dmz-client',
        'PHP_DEPLOY_REPLICAS' : 1,
        'PHP_UPDATE_CONFIG_PARALELISM' : 1,
        'VARNISH_DEPLOY_REPLICAS' : 1,
        'NGINX_DEPLOY_REPLICAS' : 1
    ]
)

DockerConfiguration.addEnvConfig(
	'neon',
    [
        "REGISTRY_HOST": 'dtrdd.nl.corp.tele2.com:9443',
        'SWARM_HOST' : 'dde.nl.corp.tele2.com',
        "DTR_JENKINS_CREDENTIALS" : 'nlsvc-jenkins-docker',
        'UCP_HOST' : 'tcp://dockerdde.nl.corp.tele2.com:8443',
        'CERT_PATH' : '/var/jenkins_home/client-bundles/dta-dmz-client',
        'PHP_DEPLOY_REPLICAS' : 1,
        'PHP_UPDATE_CONFIG_PARALELISM' : 1,
        'VARNISH_DEPLOY_REPLICAS' : 1,
        'NGINX_DEPLOY_REPLICAS' : 1
    ]
)

DockerConfiguration.addEnvConfig(
	'tst',
    [
        "REGISTRY_HOST": 'dtrdd.nl.corp.tele2.com:9443',
        'SWARM_HOST' : 'dde.nl.corp.tele2.com',
        "DTR_JENKINS_CREDENTIALS" : 'nlsvc-jenkins-docker',
        'UCP_HOST' : 'tcp://dockerdde.nl.corp.tele2.com:8443',
        'CERT_PATH' : '/var/jenkins_home/client-bundles/dta-dmz-client',
        'PHP_DEPLOY_REPLICAS' : 1,
        'PHP_UPDATE_CONFIG_PARALELISM' : 1,
        'VARNISH_DEPLOY_REPLICAS' : 1,
        'NGINX_DEPLOY_REPLICAS' : 1
    ]
)

DockerConfiguration.addEnvConfig(
	'sprint',
    [
        "REGISTRY_HOST": 'dtrdd.nl.corp.tele2.com:9443',
        'SWARM_HOST' : 'dde.nl.corp.tele2.com',
        "DTR_JENKINS_CREDENTIALS" : 'nlsvc-jenkins-docker',
        'UCP_HOST' : 'tcp://dockerdde.nl.corp.tele2.com:8443',
        'CERT_PATH' : '/var/jenkins_home/client-bundles/dta-dmz-client',
        'PHP_DEPLOY_REPLICAS' : 1,
        'PHP_UPDATE_CONFIG_PARALELISM' : 1,
        'VARNISH_DEPLOY_REPLICAS' : 1,
        'NGINX_DEPLOY_REPLICAS' : 1
    ]
)

return DockerConfiguration
