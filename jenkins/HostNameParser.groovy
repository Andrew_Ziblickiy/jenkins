class HostNameParser {

	public static parseProduction(String yaml, String swarmAppHost) {
		//  Http
        yaml = yaml.replaceAll("#HOST_1#", "http://directretailshop.tele2.nl")
        // yaml = yaml.replaceAll("#HOST_2#", "http://www.tele2.nl")
		yaml = yaml.replaceAll("#HOST_2#", "")
        yaml = yaml.replaceAll("#HOST_3#", "http://tele2.nl")
        yaml = yaml.replaceAll("#HOST_4#", "http://admin.${swarmAppHost}")
        yaml = yaml.replaceAll("#HOST_5#", "http://m2cdn.tele2.nl")
		yaml = yaml.replaceAll("#HOST_6#", "http://resellershop.tele2.nl")
		yaml = yaml.replaceAll("#HOST_7#", "http://callcentershop.tele2.nl")
		yaml = yaml.replaceAll("#HOST_8#", "")

        //	Https
        yaml = yaml.replaceAll("#HOST_HTTPS_1#", "sni://directretailshop.tele2.nl")
        // yaml = yaml.replaceAll("#HOST_HTTPS_2#", "sni://www.tele2.nl")
		yaml = yaml.replaceAll("#HOST_HTTPS_2#", "")
        yaml = yaml.replaceAll("#HOST_HTTPS_3#", "sni://tele2.nl")
        yaml = yaml.replaceAll("#HOST_HTTPS_4#", "sni://admin.${swarmAppHost}")
        yaml = yaml.replaceAll("#HOST_HTTPS_5#", "sni://m2cdn.tele2.nl")
		yaml = yaml.replaceAll("#HOST_HTTPS_6#", "sni://resellershop.tele2.nl")
		yaml = yaml.replaceAll("#HOST_HTTPS_7#", "sni://callcentershop.tele2.nl")

        //  CDN
        yaml = yaml.replaceAll("#CDN_DOMAIN#", "m2cdn.tele2.nl")

        return yaml
	}

	public static parse(String yaml, String env, String swarmHost) {

		if ("prd" == env) {
			return parseProduction(yaml, swarmHost)
		}

		def proxyEnv = env
		if ("int" == env) {
			proxyEnv = "sit"
		}

		//  HTTP
		yaml = yaml.replaceAll("#HOST_1#", "http://directretailshop.${env}.${swarmHost}")
        yaml = yaml.replaceAll("#HOST_2#", "http://shop.${env}.${swarmHost}")

		//	Espresso will be handled by wordpress stack
		if (env == 'tst' || env == 'neon') {
			yaml = yaml.replaceAll("#HOST_3#", "http://espresso-${proxyEnv}.tele2.nl")
		} else {
			yaml = yaml.replaceAll("#HOST_3#", "")
		}

        yaml = yaml.replaceAll("#HOST_4#", "http://admin.${env}.${swarmHost}")
        yaml = yaml.replaceAll("#HOST_5#", "http://m2cdn.${env}.tele2.nl")
        yaml = yaml.replaceAll("#HOST_6#", "http://resellershop.${env}.${swarmHost}")
		yaml = yaml.replaceAll("#HOST_7#", "http://callcentershop.${env}.${swarmHost}")
		yaml = yaml.replaceAll("#HOST_8#", "- com.docker.ucp.mesh.http.8=external_route=http://m2-api.${env}.${swarmHost},internal_port=80")

		//  HTTPS
		yaml = yaml.replaceAll("#HOST_HTTPS_1#", "sni://directretailshop.${env}.${swarmHost}")
        yaml = yaml.replaceAll("#HOST_HTTPS_2#", "sni://shop.${env}.${swarmHost}")
		//	Espresso will be handled by wordpress stack
		if (env == 'tst' || env == 'neon') {
			yaml = yaml.replaceAll("#HOST_HTTPS_3#", "sni://espresso-${proxyEnv}.tele2.nl")
		} else {
			yaml = yaml.replaceAll("#HOST_HTTPS_3#", "")
		}



        yaml = yaml.replaceAll("#HOST_HTTPS_4#", "sni://admin.${env}.${swarmHost}")
        yaml = yaml.replaceAll("#HOST_HTTPS_5#", "sni://m2cdn.${env}.tele2.nl")
        yaml = yaml.replaceAll("#HOST_HTTPS_6#", "sni://resellershop.${env}.${swarmHost}")
		yaml = yaml.replaceAll("#HOST_HTTPS_7#", "sni://callcentershop.${env}.${swarmHost}")

        //  CDN
        yaml = yaml.replaceAll("#CDN_DOMAIN#", "m2cdn.${env}.tele2.nl")

        return yaml
	}
}

return HostNameParser;
