def generateStaticContent(SourceFolder, imageName, flags = "--production --production-static") {

    def magentoSrc = "${workspace}/${SourceFolder.MAGENTO}"
    def foundationSrc = "${workspace}/${SourceFolder.FOUNDATION}"
    def appSrc = "${workspace}/${SourceFolder.APPLICATION}"
    def staticDst = "${workspace}/${SourceFolder.GENERATED_STATIC}"
    def mediaDst = "${workspace}/${SourceFolder.GENERATED_MEDIA}"
    def varDst = "${workspace}/${SourceFolder.GENERATED_VAR}"

	sh "mkdir -p ${mediaDst} ${staticDst} ${varDst}"
    sh "rm -Rf ${mediaDst}/* ${staticDst}/* ${varDst}/*"

    logfile = "${SourceFolder.LOGS}/static-content.log"

    sh "touch ${logfile} && truncate ${logfile} -s 0"

	status = sh returnStatus: true, script: "2>&1 1>>${logfile} sh ${SourceFolder.CONTENT_BUILDER}/exec.sh -bo \"--rm -e https_proxy=http://proxy.dcn.versatel.net:3128 -e http_proxy=http://proxy.dcn.versatel.net:3128\" --args \"--configure --install-magento --create-configs --compile-di --static-deploy ${flags}\" --magento-src ${magentoSrc} --foundation-src ${foundationSrc} --app-src ${appSrc} --pub-static ${staticDst} --media ${mediaDst} --var ${varDst} -i ${imageName} 2>&1 1>>${logfile}"

	publishHTML(
        [
            allowMissing: true,
            alwaysLinkToLastBuild: false,
            keepAll: true,
            reportDir: "${SourceFolder.LOGS}",
            reportFiles: 'static-content.log',
            reportName: 'ST Gen Log',
            reportTitles: ''
        ]
    )

	//	fix resource config add attribute
	sh "cp -f ${SourceFolder.CONFIG}/resource_config.json ${varDst}/resource_config.json"

	return status
}

return this
