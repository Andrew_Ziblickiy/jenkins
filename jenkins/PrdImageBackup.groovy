dockerClientBundle = "DOCKER_HOST=tcp://dockerpde.nl.corp.tele2.com:8443 DOCKER_TLS_VERIFY=1 DOCKER_CERT_PATH=/var/jenkins_home/client-bundles/prd-dmz-client"
dockerRegistryHostPrd = "dtrpd.nl.corp.tele2.com:9443"
dockerRegistryHostUat = "dtrdd.nl.corp.tele2.com:9443"
dtrPrdCredentials = "nlsvc-docker-rlsmgr"
dockerRegistryPath = "/development/mag-"
version="1.0.6_17S17"

node {
    stage('Backup') {
        withCredentials([usernamePassword(credentialsId: "${dtrPrdCredentials}", usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
            sh "docker login -u ${USERNAME} -p ${PASSWORD} ${dockerRegistryHostPrd}"
            sh "docker pull ${dockerRegistryHostPrd}${dockerRegistryPath}shop:${version}"
            sh "docker pull ${dockerRegistryHostPrd}${dockerRegistryPath}shop-tuner:${version}"
            sh "docker pull ${dockerRegistryHostPrd}${dockerRegistryPath}nginx:${version}"
            sh "docker pull ${dockerRegistryHostPrd}${dockerRegistryPath}varnish:${version}"
            sh "docker logout ${dockerRegistryHostPrd}"
        }

        sh "docker image tag ${dockerRegistryHostPrd}${dockerRegistryPath}shop:${version} ${dockerRegistryHostPrd}${dockerRegistryPath}shop:${version}_backup"
        sh "docker image tag ${dockerRegistryHostPrd}${dockerRegistryPath}shop-tuner:${version} ${dockerRegistryHostPrd}${dockerRegistryPath}shop-tuner:${version}_backup"
        sh "docker image tag ${dockerRegistryHostPrd}${dockerRegistryPath}nginx:${version} ${dockerRegistryHostPrd}${dockerRegistryPath}nginx:${version}_backup"
        sh "docker image tag ${dockerRegistryHostPrd}${dockerRegistryPath}varnish:${version} ${dockerRegistryHostPrd}${dockerRegistryPath}varnish:${version}_backup"

        sh "docker image tag ${dockerRegistryHostPrd}${dockerRegistryPath}shop:${version} ${dockerRegistryHostUat}${dockerRegistryPath}shop:${version}"
        sh "docker image tag ${dockerRegistryHostPrd}${dockerRegistryPath}shop-tuner:${version} ${dockerRegistryHostUat}${dockerRegistryPath}shop-tuner:${version}"
        sh "docker image tag ${dockerRegistryHostPrd}${dockerRegistryPath}nginx:${version} ${dockerRegistryHostUat}${dockerRegistryPath}nginx:${version}"
        sh "docker image tag ${dockerRegistryHostPrd}${dockerRegistryPath}varnish:${version} ${dockerRegistryHostUat}${dockerRegistryPath}varnish:${version}"
    }
}