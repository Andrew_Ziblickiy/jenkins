clientBundle = "DOCKER_HOST=tcp://dockerdde.nl.corp.tele2.com:8443 DOCKER_TLS_VERIFY=1 DOCKER_CERT_PATH=/var/jenkins_home/client-bundles/dta-dmz-client"
gitCredentials = "nlsvc-jenkins-docker"
dockerRepositoryUrl = "https://bitbucket.nl.corp.tele2.com/scm/mtel/tel2-docker-env.git"
dtrCredentials = "nlsvc-jenkins-docker"
dockerRegistryHost = "dtrdd.nl.corp.tele2.com:9443"
dockerRegistryPath = "/development/mag-"
image = "${dockerRegistryHost}${dockerRegistryPath}mysql:generic"

node {
    stage ('Deploy') {
        checkout changelog: false, poll: false, scm: [$class: 'GitSCM', branches: [[name: "master"]], browser: [$class: 'Stash', repoUrl: ''], doGenerateSubmoduleConfigurations: false, extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: "docker"], [$class: 'CleanBeforeCheckout']], submoduleCfg: [], userRemoteConfigs: [[credentialsId: gitCredentials, url: dockerRepositoryUrl]]]
        sh "cd docker/docker/tele2_mysql && docker build -f Dockerfile -t ${image} ."

        withCredentials([usernamePassword(credentialsId: "${dtrCredentials}", usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
            sh "docker login -u ${USERNAME} -p ${PASSWORD} ${dockerRegistryHost}"
            sh "docker image push ${image}"
        }

        sh "cd docker/docker/tele2_mysql && ${clientBundle} docker stack deploy -c docker-compose.yml m2tele2_mysql"
    }
}
