class SourceFolder {
	final static MAGENTO = "magento"
    final static APPLICATION = "application"
    final static DOCKER = "docker"
    final static API = "api"
    final static CONFIG = "config"
    final static FOUNDATION = "foundation"
    final static GENERATED = "generated"
    final static GENERATED_STATIC = "generated/static"
	final static STASH_API = "stash_api"
    final static GENERATED_VAR = "generated/var"
    final static GENERATED_MEDIA = "generated/media"
    final static CONTENT_BUILDER = "docker/docker/content-builder"
	final static GENERATED_NODE_MODULES = "generated/node_modules"
    final static SRC = "build/src"
    final static LOGS = "logs"
}

return SourceFolder;
