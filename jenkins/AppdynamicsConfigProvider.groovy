class AppdynamicsConfigProvider
{
	private static config = [:]

	public static configureEnv(String env, Map cfg)
    {
		config[env] = cfg
	}

	public static getConfig(String env)
    {
		def cfg = config[env]
		return cfg
	}
}

AppdynamicsConfigProvider.configureEnv(
	"dev",
	[
		"CONTROLLER_HOST" : "appdcontrol.tele2.com",
		"CONTROLLER_PORT" : 443,
		"APPLICATION_NAME" : "nl-magento-dev",
		"TIER_NAME" : "magento-php",
		"ACCOUNT_NAME" : "test",
		"ACCOUNT_ACCESS_KEY" : "cfa937b2-1c77-4978-9e7e-9ab07b948a63"
	]
)

AppdynamicsConfigProvider.configureEnv(
	"prf",
	[
		"CONTROLLER_HOST" : "appdcontrol.tele2.com",
		"CONTROLLER_PORT" : 443,
		"APPLICATION_NAME" : "nl-magento-prf",
		"TIER_NAME" : "magento-php",
		"ACCOUNT_NAME" : "test",
		"ACCOUNT_ACCESS_KEY" : "cfa937b2-1c77-4978-9e7e-9ab07b948a63"
	]
)

AppdynamicsConfigProvider.configureEnv(
	"int",
	[
		"CONTROLLER_HOST" : "appdcontrol.tele2.com",
		"CONTROLLER_PORT" : 443,
		"APPLICATION_NAME" : "nl-magento-int",
		"TIER_NAME" : "magento-php",
		"ACCOUNT_NAME" : "test",
		"ACCOUNT_ACCESS_KEY" : "cfa937b2-1c77-4978-9e7e-9ab07b948a63"
	]
)

AppdynamicsConfigProvider.configureEnv(
	"uat",
	[
		"CONTROLLER_HOST" : "appdcontrol.tele2.com",
		"CONTROLLER_PORT" : 443,
		"APPLICATION_NAME" : "nl-magento-uat",
		"TIER_NAME" : "magento-php",
		"ACCOUNT_NAME" : "test",
		"ACCOUNT_ACCESS_KEY" : "cfa937b2-1c77-4978-9e7e-9ab07b948a63"
	]
)

AppdynamicsConfigProvider.configureEnv(
	"prd",
	[
		"CONTROLLER_HOST" : "appdcontrol.tele2.com",
		"CONTROLLER_PORT" : 443,
		"APPLICATION_NAME" : "nl-magento-prd",
		"TIER_NAME" : "magento-php",
		"ACCOUNT_NAME" : "customer1",
		"ACCOUNT_ACCESS_KEY" : "c459528b-3308-44a1-91e9-b8843b6fe8ef"
	]
)

AppdynamicsConfigProvider.configureEnv(
	"generic",
	null
)

AppdynamicsConfigProvider.configureEnv(
	"tst",
	null
)

AppdynamicsConfigProvider.configureEnv(
	"puma",
	null
)

AppdynamicsConfigProvider.configureEnv(
	"panda",
	null
)

AppdynamicsConfigProvider.configureEnv(
	"neon",
	null
)

AppdynamicsConfigProvider.configureEnv(
	"sprint",
	null
)

return AppdynamicsConfigProvider;
