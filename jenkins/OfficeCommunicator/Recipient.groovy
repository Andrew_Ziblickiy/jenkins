recipients = [
    'Bisons' : 'https://outlook.office.com/webhook/93855d8d-615f-4386-898d-069f805e009a@76431109-ff89-42c2-8781-a07ca07a2d57/JenkinsCI/d41bb6f191dc4ad78087dbc55d30ef29/84aea5bc-ac2e-48d8-bcee-24825033e550',
    'Pandas' : 'https://outlook.office.com/webhook/ad62f7f1-c958-4602-b01d-86f1677bc2d2@76431109-ff89-42c2-8781-a07ca07a2d57/JenkinsCI/2f65204a7ffe4bc5a8550b9af4e1dae1/84aea5bc-ac2e-48d8-bcee-24825033e550',
    'Pumas' : 'https://outlook.office.com/webhook/ab0eb1a9-65ea-4ec6-9f19-3cdf269632e6@76431109-ff89-42c2-8781-a07ca07a2d57/JenkinsCI/d73b2e5534504f1b862479c1607f618f/84aea5bc-ac2e-48d8-bcee-24825033e550',
    'Neons' : 'https://outlook.office.com/webhook/8e8a2112-bdbc-470a-9691-23afa9f6431d@76431109-ff89-42c2-8781-a07ca07a2d57/JenkinsCI/82bcaac534fa4a49b78d7ee1ea25c52d/84aea5bc-ac2e-48d8-bcee-24825033e550',
    'Aliaksei Panasik' : 'https://outlook.office.com/webhook/84aea5bc-ac2e-48d8-bcee-24825033e550@76431109-ff89-42c2-8781-a07ca07a2d57/JenkinsCI/c8502bf86afe48ec82a68d6923621328/84aea5bc-ac2e-48d8-bcee-24825033e550'
]

def add(String name, String link)
{
    recipients[name] = link
}

def get(String name)
{
    if (recipients.containsKey(name)) {
        return recipients[name]
    }
    return ''
}

def getAll()
{
    return recipients
}

return this
