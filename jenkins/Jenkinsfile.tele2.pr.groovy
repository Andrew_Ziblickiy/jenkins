def configRepositoryUrl = "https://bitbucket.nl.corp.tele2.com/scm/mtel/tel2-m2-config.git"
def SourceFolder
def ImageNameProvider
def RepositoryConfiguration
def DockerConfiguration
def Git
def deploymentEnvironment = "tst"
def uiTestsPhpunitDirectretailFilter = "DirectRetailUltraLightSmokeTest"
def uiTestsPhpunitOnlineretailFilter = "UltraLightSmokeOnlineRetailTest"
def uiTestsPhpunitOnlineretailSOFilter = "UltraLightSmokeSimOnlyTest"
def uiTestsPhpunitIndirectretailFilter = "IndirectRetailUltraLightSmokeTest"
def uiTestsUseLocalEnv = true
def pullRequestChangeSet = ""


class Repository {
	static MAGENTO = "https://bitbucket.nl.corp.tele2.com/scm/mtel/tel2-m2-ee.git"
	final static APPLICATION = "https://bitbucket.nl.corp.tele2.com/scm/mtel/tel2-m2-app.git"
	final static DOCKER = "https://bitbucket.nl.corp.tele2.com/scm/mtel/tel2-docker-env.git"
	final static API = "https://bitbucket.nl.corp.tele2.com/scm/mtel/tel2-test-runner.git"
	static FOUNDATION = "https://bitbucket.nl.corp.tele2.com/scm/mtel/tel2-m2-foundation.git"
	final static CONFIG = "https://bitbucket.nl.corp.tele2.com/scm/mtel/tel2-m2-config.git"
    final static STASH_API = "https://bitbucket.nl.corp.tele2.com/scm/mtel/jenkins-stash-integration.git"

	static MAGENTO_BRANCH = "develop"
	static API_BRANCH = "master"
	static DOCKER_BRANCH = "master"
	static FOUNDATION_BRANCH = "v.1.3"

	final static CREDENTIALS = "f5c2a131-daba-44cb-811e-3b60e1e01419"

	static void pull(rep, branch, folder) {
		Context.script.checkout changelog: false, poll: false, scm: [$class: 'GitSCM', branches: [[name: branch]], browser: [$class: 'Stash', repoUrl: ''], doGenerateSubmoduleConfigurations: false, extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: folder]], submoduleCfg: [], userRemoteConfigs: [[credentialsId: CREDENTIALS, url: rep]]]
	}

	static void pullWithMerge(script, rep, branch, mergeBranch, folder) {
		script.checkout changelog: false, poll: false, scm: [$class: 'GitSCM', branches: [[name: branch]], browser: [$class: 'Stash', repoUrl: ''], doGenerateSubmoduleConfigurations: false, extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: folder], [$class: 'PreBuildMerge', options: [fastForwardMode: 'FF', mergeRemote: 'origin', mergeStrategy: 'DEFAULT', mergeTarget: mergeBranch]]], submoduleCfg: [], userRemoteConfigs: [[credentialsId: CREDENTIALS, url: rep]]]
	}

	static void pullWithClean(script, rep, branch, folder) {
		script.checkout changelog: false, poll: false, scm: [$class: 'GitSCM', branches: [[name: branch]], browser: [$class: 'Stash', repoUrl: ''], doGenerateSubmoduleConfigurations: false, extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: folder], [$class: 'CleanBeforeCheckout']], submoduleCfg: [], userRemoteConfigs: [[credentialsId: CREDENTIALS, url: rep]]]
	}
}

class Jenkins {
	static SERVICE_ACCOUNT = "nlsvc-jenkins-docker"
	static ENV = ""
}

class Stash {

    static BIN = ""
    static PULL_REQUEST_ID = ""
    static SOURCE_BRANCH_COMMIT = ""
	static SOURCE_BRANCH = ""
    static DESTINATION_BRANCH_COMMIT = ""
	static DESTINATION_BRANCH = ""
	static STASH_NO_PR = false

	class Error {
		static MESSAGE = ""
		static FILE = ""
		static REMOVE_FILE = true
		static FULL_LOG = false

		static void set(String message, String file = "", boolean remove = true, boolean fullLog = false) {
			MESSAGE = message
			FILE = file
			REMOVE_FILE = remove
			FULL_LOG = fullLog
		}
	}

	static execute(script, String command, boolean returnStatus = false, boolean returnStdout = false) {
		if (returnStatus) {
			def status = script.sh returnStatus: true, script: "php ${BIN} ${command}"
			return status
		} else if (returnStdout) {
			def out = script.sh returnStdout: true, script: "php ${BIN} ${command}"
			return out
		} else {
			script.sh "php ${BIN} ${command}"
		}
	}

	static merge(script) {
		Stash.execute(script, "pr:merge ${Stash.PULL_REQUEST_ID}", true)
	}

	static isDesctinationBranchWasModified(script)
	{
		def currentCommit = Stash.execute(script, "pr:show --format \"{.DCI}\" ${Stash.PULL_REQUEST_ID}", false, true)

		script.echo "Current: ${currentCommit}, In Build: ${Stash.DESTINATION_BRANCH_COMMIT}"
		return Stash.DESTINATION_BRANCH_COMMIT != currentCommit
	}

	static start(script, pullRequestId) {
		return Stash.execute(script, "pr:jenkins:job --buildId ${Jenkins.ENV.BUILD_ID} ${pullRequestId} start", true)
	}

	static stopWithError(script, pullRequestId) {
		def msg = ""
		def msgFile = ""
		def status = 0
		def keys = ""

		if (Stash.Error.MESSAGE) {
			msg = "--message '${Stash.Error.MESSAGE}'"
		}

		if (Stash.Error.FILE) {
			msgFile = "--messageFile ${Stash.Error.FILE}"
			if (Stash.Error.FULL_LOG) {
				keys = "${keys} --messageFileFullLength 1"
				Stash.Error.FULL_LOG = false
			}
		}

		status = Stash.execute(script, "pr:jenkins:job --buildId ${Jenkins.ENV.BUILD_ID} --error ${keys} ${msg} ${msgFile} ${pullRequestId} stop", true)

		if (Stash.Error.FILE && Stash.Error.REMOVE_FILE) {
			script.sh "rm -f ${Stash.Error.FILE}"
		}

		return status
	}

	static stopWithSuccess(script, pullRequestId) {
		return Stash.execute(script, "pr:jenkins:job --success ${pullRequestId} stop", true)
	}

	static approve(script, pullRequestId) {
		return Stash.execute(script, "pr:approve ${pullRequestId}", true)
	}

	static unApprove(script, pullRequestId) {
		return Stash.execute(script, "pr:unapprove ${pullRequestId}", true)
	}
}

class Context {
	static script;
}

class Docker {

	static REGISTRY_HOST = "dtrdd.nl.corp.tele2.com:9443/frontend/magento"
	static SWARM_HOST = "pocswarm.nl.corp.tele2.com"
	static STACK_NAME = "m2tele2"
	static CONFIG_FOLDER = "tele2_pr"
	static CLIENT_BUNDLE_ENV_VARS = ""
	static DOCKER_CERT_PATH = "/usr/local/bin/poc-client"
	static LOCAL_ENVS = ""

	class Ucp {
	    static HOST = "ucppoc.nl.corp.tele2.com"
	    static OWNER = "svc-noe-jenkins"
	    static GROUP = "magento"
	}

	class Image {

	    private static images = [:]

	    static BASE_NAME = "m2tele2-base-pr"
	    static BASE_TAG = "latest"

	    static APP_NAME = "m2tele2-app"
	    static APP_TAG = "pr"

	    static NGINX_NAME = "m2tele2-nginx"
	    static NGINX_TAG = "pr"

	    static VARNISH_NAME = "m2tele2-varnish"
        static VARNISH_TAG = "pr"

        static CONTENT_BUILDER_NAME = "m2tele2-static-generator"
        static CONTENT_BUILDER_TAG = "1.0"

		static boolean exists(name) {
			def status = Context.script.sh returnStatus: true, script: "docker inspect ${name}"
			return status == 0
		}

		static add(String name, String alias) {
		    images[alias] = name;
		}

		static get(String alias) {
		    return images[alias]
		}

		static rm(script, String alias) {
			def img = get(alias)
			if (img) {
				script.sh "2>&1 1>/dev/null docker rmi -f ${img} 2>&1 1>/dev/null"
			}
		}
	}
}

class Phing {

    static VERSION = "2.15.2"
	static CODE_COVERAGE_LEVEL = "76"

    public static init(SourceFolder)
    {
        Context.script.sh "cp -Rf -t ./ ${SourceFolder.CONFIG}/phing/*"

        Phing.execute(SourceFolder, "init-env -Denv=jenkins -Dsrc=\$(realpath ${SourceFolder.SRC}) -Ddir.jenkins.m2app=${SourceFolder.APPLICATION} -Ddir.jenkins.m2ee=${SourceFolder.MAGENTO} -Ddir.jenkins.m2docker=${SourceFolder.DOCKER} -Ddir.jenkins.m2tr=${SourceFolder.API} -Ddir.jenkins.m2foundation=${SourceFolder.FOUNDATION}")
        Phing.execute(SourceFolder, "make-phpunit-config")
    }

    public static execute(SourceFolder, String command, String errorMsg = "Phing execution failed", String reportName = null, publish = false)
    {
		def num = new Random().nextInt()
		def file = "${SourceFolder.LOGS}/phing-exec-${num}.log"
		Context.script.sh "touch ${file}"

		try {
			Context.script.sh "2>&1 1>>${file} php phing-${Phing.VERSION}.phar ${command} 2>&1 1>>${file}"

			if (publish) {
				if (command == 'phpunit') {
					Context.script.publishHTML(
						[
							allowMissing: true,
							alwaysLinkToLastBuild: false,
							keepAll: true,
							reportDir: "${SourceFolder.SRC}/output/phpunit/html",
							reportFiles: 'index.html',
							reportName: 'PHPUnit Code Coverage',
							reportTitles: ''
						]
					)
				}
				Context.script.publishHTML(
					[
						allowMissing: true,
						alwaysLinkToLastBuild: false,
						keepAll: true,
						reportFiles: "phing-exec-${num}.log",
						reportDir: "${SourceFolder.LOGS}",
						reportName: "${reportName}",
						reportTitles: ''
					]
				)
			}

			Context.script.sh "rm -f ${file}"
		} catch (e) {
			file = "${Context.script.workspace}/${file}";

			if (reportName) {
				Context.script.publishHTML(
	                [
	                    allowMissing: true,
	                    alwaysLinkToLastBuild: false,
	                    keepAll: true,
	                    reportDir: "${SourceFolder.LOGS}",
	                    reportFiles: "phing-exec-${num}.log",
	                    reportName: "${reportName}",
	                    reportTitles: ''
	                ]
	            )
			}

			Stash.Error.set(errorMsg, file, true, true)
			error "${errorMsg}, ${e}"
		}
    }
}

def initQueue(SourceFolder)
{
	def queueFile = "${SourceFolder.LOGS}/pr-in-queue"
	def isFileExists = fileExists queueFile

	if (!isFileExists) {
		sh "touch ${queueFile}"
	}
}

def getPrInQueue(SourceFolder)
{
	def queueFile = "${SourceFolder.LOGS}/pr-in-queue"
	def prId = sh returnStdout: true, script: "cat ${queueFile}"

	return prId
}

def setPrToQueue(SourceFolder, id)
{
	def queueFile = "${SourceFolder.LOGS}/pr-in-queue"
	writeFile file: queueFile , text: "${id}"
}

Docker.CLIENT_BUNDLE_ENV_VARS = ""

def buildContentBuilderImage(SourceFolder, imageName) {
	logfile = "${SourceFolder.LOGS}/static-content-image-build.log"
	sh "touch ${logfile} && truncate ${logfile} -s 0"
	status = sh returnStatus: true, script: "2>&1 1>>${logfile} ${Docker.LOCAL_ENVS} sh ${SourceFolder.CONTENT_BUILDER}/build.sh -bo \"--build-arg https_proxy=http://proxy.dcn.versatel.net:3128 --build-arg http_proxy=http://proxy.dcn.versatel.net:3128\" -i ${imageName} 2>&1 1>>${logfile}"

	if (status != 0) {
		Stash.Error.set("Could not build HelperImage for building static content and js unit tests", "${logfile}", false)
	}

	publishHTML(
        [
            allowMissing: true,
            alwaysLinkToLastBuild: false,
            keepAll: true,
            reportDir: "${SourceFolder.LOGS}",
            reportFiles: 'static-content-image-build.log',
            reportName: 'ST Image Build',
            reportTitles: ''
        ]
    )

	return status
}

def generateStaticContent(SourceFolder, magentoSrc, foundationSrc, appSrc, staticDst, mediaDst, varDst, imageName) {
	sh "mkdir -p ${mediaDst} ${staticDst} ${varDst}"
    sh "rm -Rf ${mediaDst}/* ${staticDst}/* ${varDst}/*"

    logfile = "${SourceFolder.LOGS}/static-content.log"

    sh "touch ${logfile} && truncate ${logfile} -s 0"

//	status = sh returnStatus: true, script: "2>&1 1>>${logfile} ${Docker.LOCAL_ENVS} sh ${SourceFolder.CONTENT_BUILDER}/exec.sh -bo \"--rm -e https_proxy=http://proxy.dcn.versatel.net:3128 -e http_proxy=http://proxy.dcn.versatel.net:3128\" --args \"--configure --install-magento --create-configs --static-deploy --production --production-static --compile-di\" --magento-src ${magentoSrc} --foundation-src ${foundationSrc} --app-src ${appSrc} --pub-static ${staticDst} --media ${mediaDst} --var ${varDst} -i ${imageName} 2>&1 1>>${logfile}"
	//	production-static is disabled here
	status = sh returnStatus: true, script: "2>&1 1>>${logfile} ${Docker.LOCAL_ENVS} sh ${SourceFolder.CONTENT_BUILDER}/exec.sh -bo \"--rm --memory-swappiness 0 --memory 2g -e https_proxy=http://proxy.dcn.versatel.net:3128 -e http_proxy=http://proxy.dcn.versatel.net:3128\" --args \"--configure --install-magento --create-configs --static-deploy --production --compile-di\" --magento-src ${magentoSrc} --foundation-src ${foundationSrc} --app-src ${appSrc} --pub-static ${staticDst} --media ${mediaDst} --var ${varDst} -i ${imageName} 2>&1 1>>${logfile}"

	if (status != 0) {
		def file = "${workspace}/${SourceFolder.LOGS}/static-content.log";
		Stash.Error.set("Could not build static content", "${file}", false)
	}

	publishHTML(
        [
            allowMissing: true,
            alwaysLinkToLastBuild: false,
            keepAll: true,
            reportDir: "${SourceFolder.LOGS}",
            reportFiles: 'static-content.log',
            reportName: 'ST Gen Log',
            reportTitles: ''
        ]
    )

	return status
}

def executeJavascriptTests(SourceFolder, magentoSrc, appSrc, staticDst, mediaDst, varDst, imageName) {

	nodeModulesFolder = "${workspace}/${SourceFolder.GENERATED_NODE_MODULES}"
	logfile = "${SourceFolder.LOGS}/jsunit-tests.log"

	sh "mkdir -p ${nodeModulesFolder}"
	sh "touch ${logfile} && truncate ${logfile} -s 0"

	status = sh returnStatus: true, script: "2>&1 1>>${logfile} ${Docker.LOCAL_ENVS} sh ${SourceFolder.CONTENT_BUILDER}/exec.sh -bo \"--rm --memory-swappiness 0 --memory 1g -e https_proxy=http://proxy.dcn.versatel.net:3128 -e http_proxy=http://proxy.dcn.versatel.net:3128\" --args \"--js-unit\" --magento-src ${magentoSrc} --app-src ${appSrc} --pub-static ${staticDst} --media ${mediaDst} --var ${varDst} --node-modules ${nodeModulesFolder} -i ${imageName} 2>&1 1>>${logfile}"
	if (status != 0) {
		Stash.Error.set("JS Tests not passed", "${logfile}", false, true)
	}

	publishHTML(
        [
            allowMissing: true,
            alwaysLinkToLastBuild: false,
            keepAll: true,
            reportDir: "${SourceFolder.LOGS}",
            reportFiles: 'jsunit-tests.log',
            reportName: 'JS Unit Tests',
            reportTitles: ''
        ]
    )

	return status
}

def saveScreenshots(path) {
    def screenshots = findFiles(glob: "**/dev/tests/functional/var/log/**/screenshots/*")
    screenshots.each {
        screenshotName = it.path.split('/var/log/')[1].replaceAll("[^a-zA-Z0-9\\.\\-]", "_").replace('screenshots','')
        sh "cp \"${it}\" ${path}/${screenshotName}"
    }
}

def saveScreenrecords(path, phpunitFile, phpunitFilter) {
    def namePatterns = []
    if (phpunitFile.split(',').size() > 1) {
        def phpunitFiles = phpunitFile.split(',')
        def phpunitFilters = phpunitFilter.split(',')
        phpunitFiles.eachWithIndex{
            file, i -> namePatterns.add("${file}_${phpunitFilters[i]}")
        }
    } else {
        namePatterns.add("${phpunitFile}_${phpunitFilter}")
    }
    namePatterns.each {
        def videos = findFiles(glob: "**/dev/tests/functional/var/log/**/*${it}*.ogv")
        videos.each {
            video -> sh "cp \"${video.path}\" ${path}/${video.name}"
        }
    }
}

def ensureBuilsIsActualForBranches() {
	if (Stash.isDesctinationBranchWasModified(this)) {
		setPrToQueue(SourceFolder, Stash.PULL_REQUEST_ID)
		Stash.Error.set("Sorry, we need stop build process according to destination branch changes\n Your build will be built immediately")
		error "Sorry, we need stop build process according to destination branch changes"
	}
}

properties([
	buildDiscarder(logRotator(artifactDaysToKeepStr: '', artifactNumToKeepStr: '', daysToKeepStr: '', numToKeepStr: '5')),
	disableConcurrentBuilds(),
	parameters([string(defaultValue: '', description: '', name: 'prId')]),
	pipelineTriggers([cron('H/12 * * * *')])
])

node('unix_01') {

	try {

		checkout changelog: false, poll: false, scm: [$class: 'GitSCM', branches: [[name: "master"]], browser: [$class: 'Stash', repoUrl: ''], doGenerateSubmoduleConfigurations: false, extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: "config"], [$class: 'CleanBeforeCheckout']], submoduleCfg: [], userRemoteConfigs: [[credentialsId: Repository.CREDENTIALS, url: configRepositoryUrl]]]

	    stage('Init') {
			sh "env"
			Jenkins.ENV = env;
			Context.script = this
			SourceFolder = load "config/jenkins/SourceFolderProvider.groovy"
			Stash.BIN = "${SourceFolder.STASH_API}/src/Stash/bin/console.php"
			sh "mkdir -p ${SourceFolder.LOGS} ${SourceFolder.SRC}"
			sh "rm -rf ${workspace}/${SourceFolder.LOGS}/*"
			initQueue(SourceFolder)

			Git = load "config/jenkins/Git.groovy"
	        RepositoryConfiguration = load "config/jenkins/RepositoryConfiguration.groovy"


			//	Configure ImageNameProvider
	        ImageNameProvider = load "config/jenkins/ImageNameProvider.groovy"
	        ImageNameProvider.APP_TAG = deploymentEnvironment

	        //	Configure Docker
	        DockerConfiguration = load "config/jenkins/DockerConfiguration.groovy"
	        DockerConfiguration.addDeploymentEnvironment(deploymentEnvironment)
	        DockerConfiguration.CONFIG_FOLDER = "tele2_generic"

			//Repository.pullWithClean(this, Repository.CONFIG, "master", SourceFolder.CONFIG)
	        Repository.pull(Repository.STASH_API, "master", SourceFolder.STASH_API)

			// install stash pull request cli tool
	        isVendorInstalled = sh returnStatus: true, script: "ls -l ${SourceFolder.STASH_API}/vendor > /dev/null"
	        if (isVendorInstalled != 0) {
	            sh "https_proxy=http://proxy.dcn.versatel.net:3128 http_proxy=http://proxy.dcn.versatel.net:3128 wget http://getcomposer.org/composer.phar && mv composer.phar ${SourceFolder.STASH_API}"
	            sh "cd ${SourceFolder.STASH_API} && https_proxy=http://proxy.dcn.versatel.net:3128 http_proxy=http://proxy.dcn.versatel.net:3128 php composer.phar install --no-dev --prefer-dist -o"
	        }

			def parametersFile = "${workspace}/${SourceFolder.CONFIG}/jenkins/stash/parameters-parsed.yml"
			sh "cp -f ${workspace}/${SourceFolder.CONFIG}/jenkins/stash/parameters.yml ${parametersFile}"

			//	configure username and password
			withCredentials([usernamePassword(credentialsId: "${Jenkins.SERVICE_ACCOUNT}", usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
				def parameters = readFile parametersFile
				parameters = parameters.replaceAll("#username#", "${USERNAME}")
				parameters = parameters.replaceAll("#password#", "${PASSWORD}")
				writeFile file: parametersFile , text: parameters
		    }

			Stash.execute(this, "conf:import ${parametersFile} 2>&1 1>/dev/null")

			isConfigValid = Stash.execute(this, "conf:validate 2>&1 1>/dev/null", true)

	        if (isConfigValid != 0) {
	            error "Stash Api not properly configured"
	        }

			def arguments = ""
			def prIdInQueue = getPrInQueue(SourceFolder)

			if (prId) {
				if (prIdInQueue) {
					Stash.execut(this, "pr:comment:post ${prId} \"Hey Hey, your pull request build queue number was stolen by pull request with id ${prIdInQueue}, do not warry Jenkins Ninja will start your build after :)\"")
					setPrToQueue(SourceFolder, prId)
					prId = prIdInQueue
				}
				arguments = "--pull-request ${prId}"
			} else if (prIdInQueue) {
				prId = prIdInQueue
				setPrToQueue(SourceFolder, "")
				arguments = "--pull-request ${prId}"
			}

			response = Stash.execute(this, "pr:jenkins:get-id ${arguments} --build-not-found-phrase BUILD_NOT_FOUND -f \"{.PID};{.SCI};{.DCI};{.SB};{.DB}\"", false, true)
			if (response == 'BUILD_NOT_FOUND') {
				currentBuild.result = 'SUCCESS'
				Stash.STASH_NO_PR = true
   				error('NO_PR')
			}

	        res = response.split(';')
	        Stash.PULL_REQUEST_ID = res[0]
	        Stash.SOURCE_BRANCH_COMMIT = res[1]
	        Stash.DESTINATION_BRANCH_COMMIT = res[2]
			Stash.SOURCE_BRANCH = res[3]
			Stash.DESTINATION_BRANCH = res[4]

			try {
				Stash.start(this, Stash.PULL_REQUEST_ID)
				Repository.pullWithMerge(this, Repository.APPLICATION, Stash.SOURCE_BRANCH, Stash.DESTINATION_BRANCH, SourceFolder.APPLICATION)
			} catch (e) {
				writeFile file: "${SourceFolder.LOGS}/merge-error.log" , text: "${e}"
				Stash.unApprove(this, Stash.PULL_REQUEST_ID)
				Stash.Error.set("Could not merge, resolve conflicts first", "${workspace}/${SourceFolder.LOGS}/merge-error.log")

				error "Can not merge: ${e}"
			}

			if (Stash.DESTINATION_BRANCH == 'release/1.0.7_18S01') {
				Repository.MAGENTO_BRANCH = '2.1.9'
				Repository.FOUNDATION_BRANCH = 'v1.1'
			}

			echo "Application branch: ${Stash.SOURCE_BRANCH}"
			echo "Magento branch: ${Repository.MAGENTO_BRANCH}"
			echo "Foundation branch: ${Repository.FOUNDATION_BRANCH}"
			echo "Pull request id: ${Stash.PULL_REQUEST_ID}"
			echo "Destination Branch Commit: ${Stash.DESTINATION_BRANCH_COMMIT}"

			//currentBuild.setDescription("<div>PR# ${Stash.PULL_REQUEST_ID}, <a href='http://t2nl-devtooling:7990/projects/MTEL/repos/tel2-m2-app/pull-requests/${Stash.PULL_REQUEST_ID}/overview' target='_blank'>Open in Stash</a></div>")
			currentBuild.setDescription("PR# ${Stash.PULL_REQUEST_ID}, ${Stash.SOURCE_BRANCH} -> ${Stash.DESTINATION_BRANCH}")

			Repository.pullWithClean(this, Repository.MAGENTO, Repository.MAGENTO_BRANCH, SourceFolder.MAGENTO)
			//Repository.pullWithClean(this, Repository.MAGENTO, '2.1.9', SourceFolder.MAGENTO)
		    Repository.pull(Repository.DOCKER, Repository.DOCKER_BRANCH, SourceFolder.DOCKER)
		    Repository.pull(Repository.API, Repository.API_BRANCH, SourceFolder.API)
		    Repository.pullWithClean(this, Repository.FOUNDATION, Repository.FOUNDATION_BRANCH, SourceFolder.FOUNDATION)
			Git.pullWithClean("https://bitbucket.nl.corp.tele2.com/scm/mtel/tel2-docker-env.git", 'selenium_research_local', 'docker_develop', RepositoryConfiguration.CREDENTIALS)
	    }

		stage('PHP Checks') {
			Context.script = this

			Phing.init(SourceFolder)
	        Phing.execute(SourceFolder, "merge")
			Phing.execute(SourceFolder, "prepare-output-folders")
			//  Copy foundation to the application dir
	        sh "cp -Rf ${SourceFolder.FOUNDATION}/app/code/* ${SourceFolder.SRC}/app/code"

			pullRequestChangeSet = Stash.execute(this, "pr:changes --base-path=${SourceFolder.SRC}/ --ext php --skip-deleted --only-files -- ${Stash.PULL_REQUEST_ID}", false, true)

			parallel(
	            //  Run phpcs
	            "phpcs": {
					try {
						if (pullRequestChangeSet) {
							Phing.execute(SourceFolder, "phpcs -Ddir.src.tele2=\"${pullRequestChangeSet}\"", "PHP Code Styles checks failed", "PHPCS", true)
						} else {
							Phing.execute(SourceFolder, "phpcs", "PHP Code Styles checks failed", "PHPCS", true)
						}
					} catch (e) {
						error "Code style checks: ${e}"
					}
				},

				//  Run phpmd
                "phpmd": {
                    try {
                        Phing.execute(SourceFolder, "phpmd", "PHP Mess Detector checks failed", "PHPMD", true)
                    } catch (e) {
                        echo "Messdetector Errors"
                    }
                },

	            //  Run phpcpd
	            "phpcpd": { Phing.execute(SourceFolder, "phpcpd", "PHPCPD failed", "PHPCPD", true) },

	            //  Run phpunit
	            "phpunit": {
					try {
						Phing.execute(SourceFolder, "phpunit", "PHPUnit Tests Failed", "PHPUNIT", true)
					} catch (e) {
						// timeout(time: 1, unit: 'MINUTES') {
							// input message: 'Want to continue build with failed PHPUNIT tests?', ok: 'Yes'
						// }
						error "Phpunit tests failed: ${e}"
					}
				}
	        )

	        publishHTML(
	            [
	                allowMissing: true,
	                alwaysLinkToLastBuild: false,
	                keepAll: true,
	                reportDir: 'output/phpunit/html',
	                reportFiles: 'index.html',
	                reportName: 'PHPUnit Code Coverage',
	                reportTitles: ''
	            ]
	        )

	        Phing.execute(SourceFolder, "validate-code-coverage -Dconfig.phpunit.accepted_coverage=${Phing.CODE_COVERAGE_LEVEL}", "Code coverage is below then accepted ${Phing.CODE_COVERAGE_LEVEL}%", "Code Coverage", true)

			ensureBuilsIsActualForBranches()
		}

		stage('Build Helper') {
			Context.script = this
			status = buildContentBuilderImage(SourceFolder, "${Docker.Image.CONTENT_BUILDER_NAME}:${Docker.Image.CONTENT_BUILDER_TAG}")

			if (status != 0) {
				error "Cant build static content helper image"
			}

			ensureBuilsIsActualForBranches()
		}

		def jsChangeset = Stash.execute(this, "pr:changes --base-path=${SourceFolder.SRC}/ --skip-deleted --ext js --only-files -- ${Stash.PULL_REQUEST_ID}", false, true)

		stage('JS CS') {

			if (jsChangeset == '') {
				return
				changeset = "${SourceFolder.SRC}/app/design/frontend/**"
			}

			imageName = "${Docker.Image.CONTENT_BUILDER_NAME}:${Docker.Image.CONTENT_BUILDER_TAG}"
			logfile = "${SourceFolder.LOGS}/js-cs.log"
		    sh "touch ${logfile} && truncate ${logfile} -s 0"
			echo "2>&1 1>>${logfile} ${Docker.LOCAL_ENVS} docker run -i -v ${workspace}/${SourceFolder.SRC}:/var/www/tele2/${SourceFolder.SRC} ${imageName} jscs -c ${SourceFolder.SRC}/dev/tests/static/testsuite/Magento/Test/Js/_files/jscs/.jscsrc ${jsChangeset} 2>&1 1>>${logfile}"
			status = sh returnStatus: true, script: "2>&1 1>>${logfile} ${Docker.LOCAL_ENVS} docker run -i -v ${workspace}/${SourceFolder.SRC}:/var/www/tele2/${SourceFolder.SRC} ${imageName} jscs -c ${SourceFolder.SRC}/dev/tests/static/testsuite/Magento/Test/Js/_files/jscs/.jscsrc ${jsChangeset} 2>&1 1>>${logfile}"

			publishHTML(
				[
					allowMissing: true,
					alwaysLinkToLastBuild: false,
					keepAll: true,
					reportDir: "${SourceFolder.LOGS}",
					reportFiles: 'js-cs.log',
					reportName: 'JS CS',
					reportTitles: ''
				]
			)

			if (status != 0) {
				def file = "${workspace}/${SourceFolder.LOGS}/js-cs.log";
				Stash.Error.set("JS CS exited with none zero status code", "${file}", false, true)
				error "JS CS exited with none zero status code"
			}

			ensureBuilsIsActualForBranches()
		}

		stage('JS Lint') {

			if (jsChangeset == '') {
				return
				changeset = "${SourceFolder.SRC}/app/design/frontend/**"
			}

			imageName = "${Docker.Image.CONTENT_BUILDER_NAME}:${Docker.Image.CONTENT_BUILDER_TAG}"
			logfile = "${SourceFolder.LOGS}/js-lint.log"
		    sh "touch ${logfile} && truncate ${logfile} -s 0"
			echo "2>&1 1>>${logfile} ${Docker.LOCAL_ENVS} docker run -i -v ${workspace}/${SourceFolder.SRC}:/var/www/tele2/${SourceFolder.SRC} ${imageName} eslint --debug -c ${SourceFolder.SRC}/dev/tests/static/testsuite/Magento/Test/Js/_files/eslint/.eslintrc ${jsChangeset} 2>&1 1>>${logfile}"
			status = sh returnStatus: true, script: "2>&1 1>>${logfile} ${Docker.LOCAL_ENVS} docker run -i -v ${workspace}/${SourceFolder.SRC}:/var/www/tele2/${SourceFolder.SRC} ${imageName} eslint --debug -c ${SourceFolder.SRC}/dev/tests/static/testsuite/Magento/Test/Js/_files/eslint/.eslintrc ${jsChangeset} 2>&1 1>>${logfile}"

			publishHTML(
		        [
		            allowMissing: true,
		            alwaysLinkToLastBuild: false,
		            keepAll: true,
		            reportDir: "${SourceFolder.LOGS}",
		            reportFiles: 'js-lint.log',
		            reportName: 'JS Lint',
		            reportTitles: ''
		        ]
		    )

			if (status != 0) {
				def file = "${workspace}/${SourceFolder.LOGS}/js-lint.log";
				Stash.Error.set("JS Lint errors", "${file}", false, true)
				error "JS Lint exited with non zero status code"
			}

			ensureBuilsIsActualForBranches()
		}

		stage('Generate Statics') {
			Context.script = this

			sh "rm -f ${SourceFolder.APPLICATION}/app/etc/config.php"

			status = generateStaticContent(
				SourceFolder,
	            "${workspace}/${SourceFolder.MAGENTO}",
				"${workspace}/${SourceFolder.FOUNDATION}",
	            "${workspace}/${SourceFolder.APPLICATION}",
	            "${workspace}/${SourceFolder.GENERATED_STATIC}",
	            "${workspace}/${SourceFolder.GENERATED_MEDIA}",
	            "${workspace}/${SourceFolder.GENERATED_VAR}",
	            "${Docker.Image.CONTENT_BUILDER_NAME}:${Docker.Image.CONTENT_BUILDER_TAG}"
	        )

	        if (status != 0) {
	            error "Pipeline error during building static content"
	        }

			ensureBuilsIsActualForBranches()
		}

		stage('JS Unit') {
			status = executeJavascriptTests(
				SourceFolder,
				"${workspace}/${SourceFolder.MAGENTO}",
	            "${workspace}/${SourceFolder.APPLICATION}",
	            "${workspace}/${SourceFolder.GENERATED_STATIC}",
	            "${workspace}/${SourceFolder.GENERATED_MEDIA}",
	            "${workspace}/${SourceFolder.GENERATED_VAR}",
	            "${Docker.Image.CONTENT_BUILDER_NAME}:${Docker.Image.CONTENT_BUILDER_TAG}"
			)

			if (status != 0) {
	            error "Pipeline error during javascript tests"
//				echo "Javascript tests failed"
	        }

			ensureBuilsIsActualForBranches()
		}

		stage('Notify') {

			ensureBuilsIsActualForBranches()

			Stash.approve(this, Stash.PULL_REQUEST_ID)
			Stash.stopWithSuccess(this, Stash.PULL_REQUEST_ID)
			Stash.merge(this)
		}

		stage('Clean') {
			try {
				sh "rm -Rf ${SourceFolder.GENERATED_STATIC} ${SourceFolder.GENERATED_VAR} ${SourceFolder.GENERATED_MEDIA}"
			} catch (e) {
				// do nothing
			}
		}

	} catch (e) {
		if (Stash.PULL_REQUEST_ID) {
			Stash.stopWithError(this, Stash.PULL_REQUEST_ID)
		} else if (Stash.STASH_NO_PR) {
			currentBuild.result = 'SUCCESS'
			return
		}
		error "Build failed with error: ${e}"
	}
}
