def cliendBundle = "DOCKER_HOST=tcp://dockerdde.nl.corp.tele2.com:8443 DOCKER_TLS_VERIFY=1 DOCKER_CERT_PATH=/var/jenkins_home/client-bundles/dta-dmz-client"

properties ([
	buildDiscarder(
		logRotator(
			artifactDaysToKeepStr: '',
			artifactNumToKeepStr: '',
			daysToKeepStr: '',
			numToKeepStr: '10'
		)
	),
	disableConcurrentBuilds(),
    parameters ([
        choice(
            choices: "php\nvarnish\nnginx\nadmin",
            description: 'Select service',
            name: 'SERVICE'
        ),
        choice(
            choices: "dev\nint\nprf\nuat",
            description: 'Select environment',
            name: 'SCALE_ENVIRONMENT'
        ),
        choice(
            choices: "0\n1\n2\n3\n4\n5\n6\n7\n8",
            description: 'Select number of containers',
            name: 'REPLICAS_NUMBER'
        )
    ])
])

node {
    stage("Scale") {
        sh "${cliendBundle} docker service scale m2tele2_${SCALE_ENVIRONMENT}_${SERVICE}=${REPLICAS_NUMBER}"
    }
}