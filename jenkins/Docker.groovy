def login(String env, DockerConfiguration) {
    echo "Trying login to docker registry"

    def registryHost = DockerConfiguration.getRegistryHost(env)

	withCredentials([usernamePassword(credentialsId: DockerConfiguration.getDtrCredentials(env), usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
        sh "docker login -u ${USERNAME} -p ${PASSWORD} ${registryHost}"
    }
}

def logout(String env, DockerConfiguration) {
    echo "Trying logout to docker registry"

    def registryHost = DockerConfiguration.getRegistryHost(env)
    //sh "docker logout ${registryHost}"
    echo "Logout from repository is commented, skip docker logout ${registryHost}"
}

def push(String env, ImageNameProvider, DockerConfiguration, JenkinsConfiguration, SourceFolder) {
    def file = "${SourceFolder.LOGS}/docker-push-${env}.log"
    def status = 0
    def imageName = ""
    sh "touch ${file} && truncate ${file} -s 0"

	login(env, DockerConfiguration)

    for (image in ImageNameProvider.getAll().values()) {
        echo "Pushing image: ${image}"
        sh "echo \"Pushing image: ${image}\n\" >> ${file}"
        status = sh returnStatus: true, script: "2>&1 1>>${file} docker image push ${image} 2>&1 1>>${file}"

        if (status != 0) {
            sh "echo \"Exited with code: ${status}\n\" >> ${file}"
            publishHTML(
                [
                    allowMissing: true,
                    alwaysLinkToLastBuild: false,
                    keepAll: true,
                    reportDir: "${SourceFolder.LOGS}",
                    reportFiles: "docker-push.log",
                    reportName: "Push Images Log",
                    reportTitles: ''
                ]
            )
			logout(env, DockerConfiguration)
            error "Could not push image ${image}"
        }
    }

	logout(env, DockerConfiguration)

    publishHTML(
        [
            allowMissing: true,
            alwaysLinkToLastBuild: false,
            keepAll: true,
            reportDir: "${SourceFolder.LOGS}",
            reportFiles: "docker-push-${env}.log",
            reportName: "Push Images ${env} Log",
            reportTitles: ''
        ]
    )
}

def isApplicationUp(String env, DockerConfiguration, int iterations = 40, int timeout = 20) {
    def config = DockerConfiguration.getConfig(env)

    if (null == config) {
        return false
    }

    //  so, at this moment we will check only tuner container if it finished
    //  without errors

    def clientBundle = "DOCKER_HOST=${config.UCP_HOST} DOCKER_TLS_VERIFY=1 DOCKER_CERT_PATH=${config.CERT_PATH}"
    def delimiter = "~~"
    def cmd = "${clientBundle} docker service ps ${DockerConfiguration.STACK_NAME}_${env}_tuner --format \"{{.Name}}${delimiter}{{.DesiredState}}${delimiter}{{.Error}}\""
    def result = ""
    def i = 0
    def chunks
    while (i <= iterations) {
        result = sh returnStdout: true, script: cmd

        if (result) {
            chunks = result.split("\n")[0].split(delimiter)
            if (chunks[1] == 'Shutdown' && chunks.length == 2) {
                return true
            }
        }
        i++
        sleep timeout
    }

    return false
}

def deploy(String env, ImageNameProvider, DockerConfiguration, SourceFolder, HostNameParser, AppdynamicsConfigProvider, StackResource = null) {

    echo "Starting deployment preparation process"

    def config = DockerConfiguration.getConfig(env)

    if (null == config) {
        return false
    }

    def ymlFolder = "${SourceFolder.DOCKER}/docker/${DockerConfiguration.CONFIG_FOLDER}"
    def parsedYaml = "${ymlFolder}/${DockerConfiguration.STACK_NAME}-${env}-docker-compose.yml"
    def clientBundle = "DOCKER_HOST=${config.UCP_HOST} DOCKER_TLS_VERIFY=1 DOCKER_CERT_PATH=${config.CERT_PATH}"
	def swarmHost = config.SWARM_HOST
	def phpReplicas = config.PHP_DEPLOY_REPLICAS
    def varnishReplicas = config.VARNISH_DEPLOY_REPLICAS
    def nginxReplicas = config.NGINX_DEPLOY_REPLICAS
    def appdynamics = AppdynamicsConfigProvider.getConfig(env)
    def serverName = "magento.${env}.${swarmHost}"

    //  Parse docker-compose.yml with environment configuration settings
    def yaml = readFile "${ymlFolder}/docker-compose.yml"
    echo "${yaml}"
    echo "${ymlFolder}/docker-compose.yml"

    echo "Before image configuration"
    yaml = yaml.replaceAll("#NGINX_IMAGE_NAME#", ImageNameProvider.get('nginx', env))
    yaml = yaml.replaceAll("#PHP_IMAGE_NAME#", ImageNameProvider.get('app', env))
    yaml = yaml.replaceAll("#VARNISH_IMAGE_NAME#", ImageNameProvider.get('varnish'))
    yaml = yaml.replaceAll("#TUNER_IMAGE_NAME#", ImageNameProvider.get('tuner'))

    echo "Try to configure redis image"
    def redisImage = ImageNameProvider.get('redis')
    if (redisImage) {
        echo "Apply redis image configuration"
        yaml = yaml.replaceAll("#REDIS_IMAGE_NAME#", redisImage)
    } else {
        echo "No needs to configure redis"
    }

    echo "Images were configured"
    yaml = yaml.replaceAll("#ENV#", env)

	//	Appdynamics
    if (appdynamics) {
        echo "Configure Appdynamics"
        yaml = yaml.replaceAll("#APPDYNAMICS_TIER_NAME#", appdynamics.TIER_NAME)
    	yaml = yaml.replaceAll("#APPDYNAMICS_NODE_NAME#", serverName)
    	yaml = yaml.replaceAll("#APPDYNAMICS_ACCOUNT_NAME#", appdynamics.ACCOUNT_NAME)
    	yaml = yaml.replaceAll("#APPDYNAMICS_ACCOUNT_ACCESS_KEY#", appdynamics.ACCOUNT_ACCESS_KEY)
    	yaml = yaml.replaceAll("#APPDYNAMICS_APPLICATION_NAME#", appdynamics.APPLICATION_NAME)
    } else {
        echo "Has no Appdynamics configuration"
    }

    echo "Prepare Database host"
    def dedicatedDatabaseHosts = ["dev", "int", "prf", "uat"]
    def dbHost = "magentodb.${env}.nl.corp.tele2.com"
    if (!dedicatedDatabaseHosts.contains(env)) {
        if ("prd" == env) {
            dbHost = "magentodb.nl.corp.tele2.com"
        } else {
            dbHost = "mysql"
        }
    }
    echo "Done"

    echo "Apply docker UCP configuration"
	//	Appdynamics END
	yaml = yaml.replaceAll("#DB_HOST#", "${dbHost}")
    yaml = yaml.replaceAll("#UCPOWNER#", DockerConfiguration.UCP_OWNER)
    yaml = yaml.replaceAll("#UCPGROUP#", DockerConfiguration.UCP_GROUP)

    echo "Apply docker replicas configuration"

    for (e in config) {
        echo "Search and replace, [#${e.key}#] => [${e.value}]"
        yaml = yaml.replaceAll("#${e.key}#", "${e.value}")
    }

    //  Limitation configuration
    if (StackResource) {
        echo "Trying to apply StackResource configuration"
        def stackResourceConfig = StackResource.getConfig(env)

        if (stackResourceConfig) {
            echo "Applying..."
            //  For understanding please look StackResource.groovy
            for (e in stackResourceConfig) {
                yaml = yaml.replaceAll("#${e.key}#", e.value)
            }
            echo "Done"
        }
    } else {
        echo "Has no StackResource config"
    }

    echo "Set Magento mode and Stack Name"
	yaml = yaml.replaceAll("#MAGE_MODE#", "production")
    yaml = yaml.replaceAll("#STACK_NAME#", "${DockerConfiguration.STACK_NAME}_${env}")

    echo "Parse Application hosts"
	//  Parse Hosts
	yaml = HostNameParser.parse(yaml, env, swarmHost)
    echo "Done"

	yaml = yaml.replaceAll("#APPLICATION_DOMAIN#", "shop.${env}.${swarmHost}")
    yaml = yaml.replaceAll("#DBPREFIX#", "magento")
    writeFile file: parsedYaml , text: yaml

    echo "Done with preparations"
    echo "${yaml}"

    echo "Deploying"
    def cmd = "${clientBundle} docker stack deploy -c ${parsedYaml} ${DockerConfiguration.STACK_NAME}_${env}"
    sh "${cmd}"
    return true
}

def updateService(String env, String service, DockerConfiguration)
{
    def config = DockerConfiguration.getConfig(env)

    if (null == config) {
        return false
    }

    def clientBundle = "DOCKER_HOST=${config.UCP_HOST} DOCKER_TLS_VERIFY=1 DOCKER_CERT_PATH=${config.CERT_PATH}"
    def cmd = "${clientBundle} docker service update m2tele2_${env}_${service} --force"
    sh "${cmd}"
}

return this
