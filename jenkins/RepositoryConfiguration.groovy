class RepositoryConfiguration
{
    public static MAGENTO = "https://bitbucket.nl.corp.tele2.com/scm/mtel/tel2-m2-ee.git"
	public static APPLICATION = "https://bitbucket.nl.corp.tele2.com/scm/mtel/tel2-m2-app.git"
	public static DOCKER = "https://bitbucket.nl.corp.tele2.com/scm/mtel/tel2-docker-env.git"
	public static API = "https://bitbucket.nl.corp.tele2.com/scm/mtel/tel2-test-runner.git"
	public static FOUNDATION = "https://bitbucket.nl.corp.tele2.com/scm/mtel/tel2-m2-foundation.git"
	public static CONFIG = "https://bitbucket.nl.corp.tele2.com/scm/mtel/tel2-m2-config.git"
	public static MEDIA = "https://bitbucket.nl.corp.tele2.com/scm/mtel/tel2-m2-media.git"
	public static MEDIA_PROTOCOL = "https://"
	public static MEDIA_URL = "bitbucket.nl.corp.tele2.com/scm/mtel/tel2-m2-media.git"

    public static BRANCH_MAGENTO = 'develop'
	public static BRANCH_DOCKER = 'master'
	public static BRANCH_API = 'master'
	public static BRANCH_CONFIG = 'master'
	//static BRANCH_FOUNDATION = '602caa8ac33'
	public static BRANCH_FOUNDATION = 'v1.1'

	//final static CREDENTIALS = "f5c2a131-daba-44cb-811e-3b60e1e01419"
	final public static CREDENTIALS = "nlsvc-jenkins-docker"
}

return RepositoryConfiguration
