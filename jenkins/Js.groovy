def executeJavascriptTests(SourceFolder, imageName) {

    def magentoSrc = "${workspace}/${SourceFolder.MAGENTO}"
    def foundationSrc = "${workspace}/${SourceFolder.FOUNDATION}"
    def appSrc = "${workspace}/${SourceFolder.APPLICATION}"
    def staticDst = "${workspace}/${SourceFolder.GENERATED_STATIC}"
    def mediaDst = "${workspace}/${SourceFolder.GENERATED_MEDIA}"
    def varDst = "${workspace}/${SourceFolder.GENERATED_VAR}"

	nodeModulesFolder = "${workspace}/${SourceFolder.GENERATED_NODE_MODULES}"
	logfile = "${SourceFolder.LOGS}/jsunit-tests.log"

	sh "mkdir -p ${nodeModulesFolder}"
	sh "touch ${logfile} && truncate ${logfile} -s 0"

	status = sh returnStatus: true, script: "2>&1 1>>${logfile} sh ${SourceFolder.CONTENT_BUILDER}/exec.sh -bo \"--rm -e https_proxy=http://proxy.dcn.versatel.net:3128 -e http_proxy=http://proxy.dcn.versatel.net:3128\" --args \"--js-unit\" --magento-src ${magentoSrc} --app-src ${appSrc} --pub-static ${staticDst} --media ${mediaDst} --var ${varDst} --node-modules ${nodeModulesFolder} -i ${imageName} 2>&1 1>>${logfile}"

	publishHTML(
        [
            allowMissing: true,
            alwaysLinkToLastBuild: false,
            keepAll: true,
            reportDir: "${SourceFolder.LOGS}",
            reportFiles: 'jsunit-tests.log',
            reportName: 'JS Unit Tests',
            reportTitles: ''
        ]
    )

	return status
}

return this
