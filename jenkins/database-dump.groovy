properties ([
	buildDiscarder(
		logRotator(
			artifactDaysToKeepStr: '',
			artifactNumToKeepStr: '',
			daysToKeepStr: '3',
			numToKeepStr: '5'
		)
	),
	disableConcurrentBuilds(),
    parameters ([
        choice(
            choices: "dev\nint\nprf\nuat\nprd",
            description: 'Select environment',
            name: 'ENVIRONMENT'
        ),
        choice(
            choices: "yes\nno",
            description: 'Remove dump file?',
            name: 'REMOVE_FILE'
        ),
        string(
            defaultValue: '',
            description: 'Password',
            name: 'PASSWORD'
        )
    ])
])

node {
    stage('Dump') {
        sh "mkdir -p dump"
        def dbhost = ""

        if (ENVIRONMENT == "prd") {
            dbhost = "magentodb.nl.corp.tele2.com"
        } else {
            dbhost = "magentodb.${ENVIRONMENT}.nl.corp.tele2.com"
        }
        def dumpDate = sh returnStdout: true, script: "date +%Y-%m-%d"
        dumpDate = dumpDate.trim()
        def dumpName = "${ENVIRONMENT}.${dumpDate}.dump.sql"

		if (ENVIRONMENT == "prd") {
			sh "docker run -i m2tele2-base:latest mysqldump --skip-lock-tables -h ${dbhost} -t --ignore-table=magento.tele2_order_request --ignore-table=magento.queue_message --ignore-table=magento.queue_message_status -u magento -p${PASSWORD} magento > dump/data.${dumpName}"
			sh "docker run -i m2tele2-base:latest mysqldump --skip-lock-tables -h ${dbhost} --no-data -u magento -p${PASSWORD} magento > dump/schema.${dumpName}"
	        sh "cd dump && tar -czf ${dumpName}.tar.gz schema.${dumpName} data.${dumpName}"
	        sh "rm -f dump/*.${dumpName}"
		} else {
			sh "docker run -i m2tele2-base:latest mysqldump --skip-lock-tables -h ${dbhost} -u magento -p${PASSWORD} magento > dump/${dumpName}"
	        sh "cd dump && tar -czf ${dumpName}.tar.gz ${dumpName}"
	        sh "rm -f dump/${dumpName}"
		}

        publishHTML(
            [
                allowMissing: true,
                alwaysLinkToLastBuild: false,
                keepAll: true,
                reportDir: "dump",
                reportFiles: "${dumpName}.tar.gz",
                reportName: "Dump from ${ENVIRONMENT} env at ${dumpDate}",
                reportTitles: ''
            ]
        )

        if (REMOVE_FILE == 'yes') {
		    sh "rm -f dump/${dumpName}.tar.gz"
        }
    }
}
