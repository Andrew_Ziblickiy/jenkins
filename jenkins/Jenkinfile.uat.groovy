//  Jenkins
dtrUatCredentials = "nlsvc-jenkins-docker"
dtrDevCredentials = "nlsvc-jenkins-docker"
gitCredentials = "nlsvc-jenkins-docker"

//  Application
appFolder = "app"
appRepostoryUrl = "https://bitbucket.nl.corp.tele2.com/scm/mtel/tel2-m2-app.git"
dockerFolder = "docker"
dockerRepositoryUrl = "https://bitbucket.nl.corp.tele2.com/scm/mtel/tel2-docker-env.git"
env = "uat"
configRepositoryUrl = "https://bitbucket.nl.corp.tele2.com/scm/mtel/tel2-m2-config.git"

//  Docker Registry
dockerRegistryHostUat = "dtrdd.nl.corp.tele2.com:9443"
dockerRegistryHostDev = "dtrdd.nl.corp.tele2.com:9443"
dockerRegistryPathUat = "/development/mag-"
dockerRegistryPathDev = "/development/mag-"

//  Docker Client Bundle
dockerClientBundle = "DOCKER_HOST=tcp://dockerdde.nl.corp.tele2.com:8443 DOCKER_TLS_VERIFY=1 DOCKER_CERT_PATH=/var/jenkins_home/client-bundles/dta-dmz-client"

//  Docker UCP
dockerUCPOwner = "nlsvc-jenkins-docker"
dockerUCPGroup = "/Shared/Development"

//  Docker Images
dockerPrdShopImage = ""
dockerPrdNginxImage = ""
dockerPrdVarnishImage = ""

dockerImagePullTag = "release"

//  Docker swarm
stackName = "m2tele2"
swarmAppEndpoint = "dde.nl.corp.tele2.com"
swarmAppHost = "${env}.${swarmAppEndpoint}"

shopImage = "${dockerRegistryHostUat}${dockerRegistryPathUat}shop"
shopTunerImage = "${shopImage}-tuner"
varnishImage = "${dockerRegistryHostUat}${dockerRegistryPathUat}varnish"
nginxImage = "${dockerRegistryHostUat}${dockerRegistryPathUat}nginx"

def Docker
def DockerConfiguration

properties([buildDiscarder(logRotator(artifactDaysToKeepStr: '', artifactNumToKeepStr: '', daysToKeepStr: '3', numToKeepStr: '5')), disableConcurrentBuilds(), pipelineTriggers([])])

node{
    try {
    	stage('Request Release UAT'){
    	    timeout(time: 15, unit: 'MINUTES') {
    	        input message: 'Want to promote to UAT?', ok: 'Promote'//, submitter: 'app_jenkins_unix_release'
    	        node {
                    stage('Pull release image') {

                        withCredentials([usernamePassword(credentialsId: "${dtrDevCredentials}", usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
                	        sh "docker login -u ${USERNAME} -p ${PASSWORD} ${dockerRegistryHostDev}"
                	        sh "docker pull ${dockerRegistryHostDev}${dockerRegistryPathDev}shop:${dockerImagePullTag}"
                            sh "docker pull ${dockerRegistryHostDev}${dockerRegistryPathDev}shop-tuner:${dockerImagePullTag}"
                	        sh "docker pull ${dockerRegistryHostDev}${dockerRegistryPathDev}nginx:${dockerImagePullTag}"
                	        sh "docker pull ${dockerRegistryHostDev}${dockerRegistryPathDev}varnish:${dockerImagePullTag}"
                	        sh "docker logout ${dockerRegistryHostDev}"
                	    }
                    }

                    stage('Tag images with release') {
                        checkout changelog: false, poll: false, scm: [$class: 'GitSCM', branches: [[name: 'uat']], browser: [$class: 'Stash', repoUrl: ''], doGenerateSubmoduleConfigurations: false, extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: appFolder], [$class: 'CleanBeforeCheckout']], submoduleCfg: [], userRemoteConfigs: [[credentialsId: gitCredentials, url: appRepostoryUrl]]]
                        def versionFile = "${appFolder}/VERSION"
                        def version = readFile versionFile
                        version = version.trim()

                        echo "Trying tag images with version: ${version}"

                        if (version == '1.0.9_18S03') {
                            input message: 'It looks like incorect release version [1.0.9_18S03], if you need deploy newer version please change VERSION file in uat branch, next release version is 1.0.10_18S03, othewise please contact AtheDocker!', ok: 'Close'
                            currentBuild.result = 'ABORTED'
                            error('1.0.9_18S03 can not be built')
                        }

                        shopImage = "${shopImage}:${version}"
                        shopTunerImage = "${shopTunerImage}:${version}"
                        varnishImage = "${varnishImage}:${version}"
                        nginxImage = "${nginxImage}:${version}"

                        sh "docker image tag ${dockerRegistryHostDev}${dockerRegistryPathDev}shop:${dockerImagePullTag} ${shopImage}"
                        sh "docker image tag ${dockerRegistryHostDev}${dockerRegistryPathDev}shop-tuner:${dockerImagePullTag} ${shopTunerImage}"
                        sh "docker image tag ${dockerRegistryHostDev}${dockerRegistryPathDev}varnish:${dockerImagePullTag} ${varnishImage}"
                        sh "docker image tag ${dockerRegistryHostDev}${dockerRegistryPathDev}nginx:${dockerImagePullTag} ${nginxImage}"
                    }

                    stage('Push tagged images') {
                        sleep 5

                        withCredentials([usernamePassword(credentialsId: "${dtrUatCredentials}", usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
                	        sh "docker login -u ${USERNAME} -p ${PASSWORD} ${dockerRegistryHostUat}"
                            sh "docker image push ${shopImage}"
                            sh "docker image push ${shopTunerImage}"
                            sh "docker image push ${varnishImage}"
                            sh "docker image push ${nginxImage}"
                	        sh "docker logout ${dockerRegistryHostUat}"
                	    }
                    }

                    stage('Deploy to uat') {
                        checkout changelog: false, poll: false, scm: [$class: 'GitSCM', branches: [[name: "${env}"]], browser: [$class: 'Stash', repoUrl: ''], doGenerateSubmoduleConfigurations: false, extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: dockerFolder], [$class: 'CleanBeforeCheckout']], submoduleCfg: [], userRemoteConfigs: [[credentialsId: gitCredentials, url: dockerRepositoryUrl]]]
                        checkout changelog: false, poll: false, scm: [$class: 'GitSCM', branches: [[name: "master"]], browser: [$class: 'Stash', repoUrl: ''], doGenerateSubmoduleConfigurations: false, extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: "config"], [$class: 'CleanBeforeCheckout']], submoduleCfg: [], userRemoteConfigs: [[credentialsId: gitCredentials, url: configRepositoryUrl]]]

                        def HostNameParser = load "config/jenkins/HostNameParser.groovy"
                        def StackResource = load "config/jenkins/StackResource.groovy"
                        Docker = load "config/jenkins/Docker.groovy"
                        def ImageNameProvider = load "config/jenkins/ImageNameProvider.groovy"
                        def SourceFolder = load "config/jenkins/SourceFolderProvider.groovy"
                        def AppdynamicsConfigProvider = load "config/jenkins/AppdynamicsConfigProvider.groovy"

                        DockerConfiguration = load "config/jenkins/DockerConfiguration.groovy"
                        DockerConfiguration.addDeploymentEnvironment(env)

                        ImageNameProvider.add(shopImage, 'app')
                        ImageNameProvider.add(shopTunerImage, 'tuner')
                        ImageNameProvider.add(nginxImage, 'nginx')
                        ImageNameProvider.add(varnishImage, 'varnish')

                        Docker.deploy(env, ImageNameProvider, DockerConfiguration, SourceFolder, HostNameParser, AppdynamicsConfigProvider, StackResource)
                    }

                    stage("Wait for an Application") {

                		if (!Docker.isApplicationUp("${env}", DockerConfiguration)) {
                			error "Application is not ready or we exceed our timeout limits"
                		}

                		echo "Application is up"
                	}

                    stage("Update varnish") {
                        Docker.updateService("${env}", 'varnish', DockerConfiguration)
                        sleep 3
                    }

                    stage("Cache Warm Up") {
                        checkout changelog: false, poll: false, scm: [$class: 'GitSCM', branches: [[name: "master"]], browser: [$class: 'Stash', repoUrl: ''], doGenerateSubmoduleConfigurations: false, extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: "config"], [$class: 'CleanBeforeCheckout']], submoduleCfg: [], userRemoteConfigs: [[credentialsId: gitCredentials, url: configRepositoryUrl]]]
                        sh "php config/crawler/warmup.php --url http://espresso-uat.tele2.nl/mobiel/smartphones/ --docker-bundle \"${dockerClientBundle}\" --track-service m2tele2_uat_tuner"
                    }
    	        }
    	    }
    	}
    } catch (error) {
    	throw error
    }
}
