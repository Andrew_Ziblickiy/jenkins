def gitCredentials = "nlsvc-jenkins-docker"
def configRepositoryUrl = "https://bitbucket.nl.corp.tele2.com/scm/mtel/tel2-m2-config.git"
def SourceFolder
def ImageNameProvider
def RepositoryConfiguration
def ImageBuilder
def DockerConfiguration
def Git
def deploymentEnvironment = "tst"
def Docker
def branch = "18S16"
def phpunitDirectretailFilter = "Tele2DirectRetailNl"
def phpunitOnlineretailFilter = "Tele2OnlineRetailNl"

def saveScreenshots(path) {
	def screenshots = findFiles(glob: '**/dev/tests/functional/var/log/**/screenshots/*')
	screenshots.each {
		screenshotName = it.path.split('/var/log/')[1].replaceAll("[^a-zA-Z0-9\\.\\-]", "_").replace('screenshots','')
		sh "cp \"${it}\" ${path}/${screenshotName}"
	}
	archiveArtifacts allowEmptyArchive: true, artifacts: "${path}/*"
}

properties ([
		buildDiscarder(
				logRotator(
						artifactDaysToKeepStr: '',
						artifactNumToKeepStr: '',
						daysToKeepStr: '3',
						numToKeepStr: '5'
				)
		),
		disableConcurrentBuilds()
])

node {

	checkout changelog: false, poll: false, scm: [$class: 'GitSCM', branches: [[name: "master"]], browser: [$class: 'Stash', repoUrl: ''], doGenerateSubmoduleConfigurations: false, extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: "config"], [$class: 'CleanBeforeCheckout']], submoduleCfg: [], userRemoteConfigs: [[credentialsId: gitCredentials, url: configRepositoryUrl]]]

	stage('Init') {

		Git = load "config/jenkins/Git.groovy"
		RepositoryConfiguration = load "config/jenkins/RepositoryConfiguration.groovy"
		SourceFolder = load "config/jenkins/SourceFolderProvider.groovy"

		//	Configure ImageNameProvider
		ImageNameProvider = load "config/jenkins/ImageNameProvider.groovy"
		ImageNameProvider.APP_TAG = "${deploymentEnvironment}-2.2"
		ImageNameProvider.NGINX_TAG = "${deploymentEnvironment}-2.2"
		ImageNameProvider.VARNISH_TAG = "${deploymentEnvironment}-2.2"
		ImageNameProvider.MAGENTO_TAG = "2.2"

		//	Configure Docker
		DockerConfiguration = load "config/jenkins/DockerConfiguration.groovy"
		DockerConfiguration.addDeploymentEnvironment(deploymentEnvironment)
		DockerConfiguration.CONFIG_FOLDER = "tele2_generic"

		//	Configure Repository
		RepositoryConfiguration.BRANCH_FOUNDATION = "v.1.3"
		//RepositoryConfiguration.BRANCH_DOCKER = "not-minified-static"

		sh "mkdir -p ${SourceFolder.LOGS} ${SourceFolder.SRC}"

		Git.pull(RepositoryConfiguration.MAGENTO, RepositoryConfiguration.BRANCH_MAGENTO, SourceFolder.MAGENTO, RepositoryConfiguration.CREDENTIALS);
		Git.pullWithClean(RepositoryConfiguration.APPLICATION, branch, SourceFolder.APPLICATION, RepositoryConfiguration.CREDENTIALS)
		Git.pullWithClean(RepositoryConfiguration.DOCKER, RepositoryConfiguration.BRANCH_DOCKER, SourceFolder.DOCKER, RepositoryConfiguration.CREDENTIALS);
		Git.pull(RepositoryConfiguration.API, RepositoryConfiguration.BRANCH_API, SourceFolder.API, RepositoryConfiguration.CREDENTIALS);
		Git.pullWithClean(RepositoryConfiguration.FOUNDATION, RepositoryConfiguration.BRANCH_FOUNDATION, SourceFolder.FOUNDATION, RepositoryConfiguration.CREDENTIALS);
	}

	stage('Build') {
		def Phing = load "config/jenkins/Phing.groovy"
		return
		Phing.build(SourceFolder)
	}

	if (DockerConfiguration.canDeploy('dev')) {
		stage('Sonar') {
			srcFile = "${SourceFolder.CONFIG}/sonar/sonar-project.properties"
			dstFile = "${SourceFolder.CONFIG}/sonar/sonar-project.properties.parsed"
			Phing.execute("create-sonar-properties -Dsonar.srcFile=${srcFile} -Dsonar.dstFile=${dstFile}")
			sh "rm -Rf ${SourceFolder.SRC}/app/code/Alpha"
			def Sonar = load "${SourceFolder.CONFIG}/jenkins/Sonar.groovy"
			Sonar.scan(dstFile, "${SourceFolder.SRC}")
		}
	}

	stage("Generate Static Content") {

		ImageBuilder = load "config/jenkins/ImageBuilder.groovy"
		// return
		status = ImageBuilder.buildContentBuilderImage(
				SourceFolder,
				"${ImageNameProvider.CONTENT_BUILDER_NAME}:${ImageNameProvider.CONTENT_BUILDER_TAG}"
		)

		if (status != 0) {
			error "Cant build static content helper image"
		}

		def ContentBuilder = load "config/jenkins/ContentBuilder.groovy"

		status = ContentBuilder.generateStaticContent(
				SourceFolder,
				"${ImageNameProvider.CONTENT_BUILDER_NAME}:${ImageNameProvider.CONTENT_BUILDER_TAG}"
		)

		if (status != 0) {
			error "Pipeline error during building static content"
		}
	}

	stage("Build Application Image") {
		// return

		AppdynamicsConfigProvider = load "config/jenkins/AppdynamicsConfigProvider.groovy"

		ImageBuilder.buildMagentoImage(
				SourceFolder,
				ImageNameProvider,
				DockerConfiguration
		)
		ImageBuilder.buildApplicationImage(
				SourceFolder,
				ImageNameProvider,
				RepositoryConfiguration,
				DockerConfiguration,
				AppdynamicsConfigProvider,
				branch,
				deploymentEnvironment,
				env.JOB_URL
		)
		ImageBuilder.buildApplicationImageWithMedia(
				SourceFolder,
				ImageNameProvider,
				RepositoryConfiguration,
				DockerConfiguration,
				Git
		)
	}

	stage('Build Varnish & Nginx & Tuner') {
		// return
		dockerFolder = "${SourceFolder.DOCKER}/docker/${DockerConfiguration.CONFIG_FOLDER}"

		//  We expect that we have nginx and varnish always and we need build images
		parallel(
				//  Build tuner
				"tuner" : {
					ImageBuilder.buildTunerImage(dockerFolder, ImageNameProvider, DockerConfiguration, SourceFolder)
				},

				//  Build nginx
				"nginx" : {
					ImageBuilder.buildNginxImage(dockerFolder, ImageNameProvider, DockerConfiguration, SourceFolder)
				},

				//  Build varnish
				"varnish" : {
					ImageBuilder.buildVarnishImage(dockerFolder, ImageNameProvider, DockerConfiguration, SourceFolder)
				}
		)
	}

	stage("Push & Deploy") {
		// return
		Docker = load "config/jenkins/Docker.groovy"
		def JenkinsConfiguration = load "config/jenkins/JenkinsConfiguration.groovy"
		def HostNameParser = load "config/jenkins/HostNameParser.groovy"

		Docker.push(deploymentEnvironment, ImageNameProvider, DockerConfiguration, JenkinsConfiguration, SourceFolder)
		Docker.deploy(deploymentEnvironment, ImageNameProvider, DockerConfiguration, SourceFolder, HostNameParser, AppdynamicsConfigProvider)
	}

	stage("Wait for an Application") {
		return
		if (!Docker.isApplicationUp(deploymentEnvironment, DockerConfiguration)) {
			error "Application is not ready or we exceed our timeout limits"
		}

		echo "Application is up"
	}

	stage("Cache WarmUp") {
		def config = DockerConfiguration.getConfig(deploymentEnvironment)

		if (null == config) {
			return
		}

		def proxyEnv = deploymentEnvironment
		def swarmHost = config.SWARM_HOST
		def url = "http://espresso-${deploymentEnvironment}.tele2.nl/mobiel/smartphones/"

		if (deploymentEnvironment == 'int') {
			url = "http://espresso-sit.tele2.nl/mobiel/smartphones/"
		} else if (deploymentEnvironment == 'prf') {
			url = "http://shop.${deploymentEnvironment}.${swarmHost}/mobiel/smartphones/"
		}
		onlineRetailUrl = url

		def status = sh returnStatus:true, script: "php ${SourceFolder.CONFIG}/crawler/warmup.php --url ${url} --no-track"

		if (status != 0) {
			//error "Could not warm up cache"
		}
	}
}
