def gitCredentials = "tele2-stash"
def configRepositoryUrl = "http://epbyminw0858.minsk.epam.com:8083/scm/mtel/tel2-m2-config.git"
def SourceFolder
def ImageNameProvider
def RepositoryConfiguration
def ImageBuilder
def DockerConfiguration
def Git
def deploymentEnvironment = "tst"


properties ([
		buildDiscarder(
				logRotator(
						artifactDaysToKeepStr: '',
						artifactNumToKeepStr: '',
						daysToKeepStr: '3',
						numToKeepStr: '5'
				)
		),
		disableConcurrentBuilds()
])

node {

	checkout changelog: false, poll: false, scm: [$class: 'GitSCM', branches: [[name: "master"]], browser: [$class: 'Stash', repoUrl: ''], doGenerateSubmoduleConfigurations: false, extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: "config"], [$class: 'CleanBeforeCheckout']], submoduleCfg: [], userRemoteConfigs: [[credentialsId: gitCredentials, url: configRepositoryUrl]]]

	stage('Init') {

		Git = load "config/jenkins/Git.groovy"
		RepositoryConfiguration = load "config/jenkins/RepositoryConfiguration.groovy"

		SourceFolder = load "config/jenkins/SourceFolderProvider.groovy"

		//	Configure ImageNameProvider
		ImageNameProvider = load "config/jenkins/ImageNameProvider.groovy"
		ImageNameProvider.APP_TAG = deploymentEnvironment
		ImageNameProvider.NGINX_TAG = deploymentEnvironment
		ImageNameProvider.VARNISH_TAG = deploymentEnvironment

		def imageBuilderFile = "config/jenkins/ImageBuilder.groovy"
		def parseImageBuilderFile = readFile "config/jenkins/ImageBuilder.groovy"
		parseImageBuilderFile = parseImageBuilderFile.replaceAll("--build-arg https_proxy=http://proxy.dcn.versatel.net:3128 --build-arg http_proxy=http://proxy.dcn.versatel.net:3128", "--build-arg https_proxy= --build-arg http_proxy=")
		parseImageBuilderFile = parseImageBuilderFile.replaceAll("RepositoryConfiguration.MEDIA", "\"http://epbyminw0858.minsk.epam.com:8083/scm/mtel/tel2-m2-media.git\"")
		writeFile file: imageBuilderFile , text: parseImageBuilderFile

		def contentBuilderFile = "config/jenkins/ContentBuilder.groovy"
		def parseContentBuilderFile = readFile "config/jenkins/ContentBuilder.groovy"
		parseContentBuilderFile = parseContentBuilderFile.replaceAll("https_proxy=http://proxy.dcn.versatel.net:3128 -e http_proxy=http://proxy.dcn.versatel.net:3128", "https_proxy= -e http_proxy=")
		writeFile file: contentBuilderFile , text: parseContentBuilderFile

		//	Configure Docker
		DockerConfiguration = load "config/jenkins/DockerConfiguration.groovy"
		DockerConfiguration.addDeploymentEnvironment(deploymentEnvironment)
		DockerConfiguration.HTTP_INTERNET_PROXY = ""
		DockerConfiguration.HTTPS_INTERNET_PROXY = ""
		DockerConfiguration.CONFIG_FOLDER = "tele2_generic"

		sh "mkdir -p ${SourceFolder.LOGS} ${SourceFolder.SRC}"

		Git.pull("http://epbyminw0858.minsk.epam.com:8083/scm/mtel/tel2-m2-ee.git", RepositoryConfiguration.BRANCH_MAGENTO, SourceFolder.MAGENTO, RepositoryConfiguration.CREDENTIALS);
		Git.pullWithClean("http://epbyminw0858.minsk.epam.com:8083/scm/mtel/tel2-m2-app.git", "17S17", SourceFolder.APPLICATION, RepositoryConfiguration.CREDENTIALS)
		Git.pull("http://epbyminw0858.minsk.epam.com:8083/scm/mtel/tel2-docker-env.git", RepositoryConfiguration.BRANCH_DOCKER, SourceFolder.DOCKER, RepositoryConfiguration.CREDENTIALS);
		Git.pull("http://epbyminw0858.minsk.epam.com:8083/scm/mtel/tel2-test-runner.git", RepositoryConfiguration.BRANCH_API, SourceFolder.API, RepositoryConfiguration.CREDENTIALS);
		Git.pullWithClean("http://epbyminw0858.minsk.epam.com:8083/scm/mtel/tel2-m2-foundation.git", RepositoryConfiguration.BRANCH_FOUNDATION, SourceFolder.FOUNDATION, RepositoryConfiguration.CREDENTIALS);
	}

	stage('Build') {

		def Phing = load "config/jenkins/Phing.groovy"
		Phing.build(SourceFolder)
	}

	stage("Generate Static Content") {

		ImageBuilder = load "config/jenkins/ImageBuilder.groovy"

		status = ImageBuilder.buildContentBuilderImage(
				SourceFolder,
				"${ImageNameProvider.CONTENT_BUILDER_NAME}:${ImageNameProvider.CONTENT_BUILDER_TAG}"
		)

		if (status != 0) {
			error "Cant build static content helper image"
		}

		def ContentBuilder = load "config/jenkins/ContentBuilder.groovy"

		status = ContentBuilder.generateStaticContent(
				SourceFolder,
				"${ImageNameProvider.CONTENT_BUILDER_NAME}:${ImageNameProvider.CONTENT_BUILDER_TAG}"
		)

		if (status != 0) {
			error "Pipeline error during building static content"
		}
	}

	stage("Build Application Image") {

		AppdynamicsConfigProvider = load "config/jenkins/AppdynamicsConfigProvider.groovy"

		ImageBuilder.buildMagentoImage(
				SourceFolder,
				ImageNameProvider,
				DockerConfiguration
		)
		ImageBuilder.buildApplicationImage(
				SourceFolder,
				ImageNameProvider,
				RepositoryConfiguration,
				DockerConfiguration,
				AppdynamicsConfigProvider,
				"17S17",
				deploymentEnvironment,
				env.JOB_URL
		)
		ImageBuilder.buildApplicationImageWithMedia(
				SourceFolder,
				ImageNameProvider,
				RepositoryConfiguration,
				DockerConfiguration,
				Git
		)
	}

	stage('Build Varnish & Nginx & Tuner') {

		dockerFolder = "${SourceFolder.DOCKER}/docker/${DockerConfiguration.CONFIG_FOLDER}"

		//  We expect that we have nginx and varnish always and we need build images
		parallel(
				//  Build tuner
				"tuner" : {
					ImageBuilder.buildTunerImage(dockerFolder, ImageNameProvider, DockerConfiguration, SourceFolder)
				},

				//  Build nginx
				"nginx" : {
					ImageBuilder.buildNginxImage(dockerFolder, ImageNameProvider, DockerConfiguration, SourceFolder)
				},

				//  Build varnish
				"varnish" : {
					ImageBuilder.buildVarnishImage(dockerFolder, ImageNameProvider, DockerConfiguration, SourceFolder)
				}
		)
	}

	stage("Push & Deploy") {

		def Docker = load "config/jenkins/Docker.groovy"
		def JenkinsConfiguration = load "config/jenkins/JenkinsConfiguration.groovy"
		def HostNameParser = load "config/jenkins/HostNameParser.groovy"

		//Docker.push(deploymentEnvironment, ImageNameProvider, DockerConfiguration, JenkinsConfiguration, SourceFolder)
		Docker.deploy(deploymentEnvironment, ImageNameProvider, DockerConfiguration, SourceFolder, HostNameParser, AppdynamicsConfigProvider)
	}
}
