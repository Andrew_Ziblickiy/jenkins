<?php

$opts = [
	'url:',
	'docker-bundle:',
	'track-service:',
	'track-iterations::',
	'iterations::',
	'no-track'
];
$arguments = getopt("", $opts);

if (false === $arguments || empty($arguments)) {
	fputs(STDERR, 'Could not parse arguments');
	exit(1);
}

foreach ($opts as $opt) {
	if (strpos($opt, '::') !== false) {
	
	} else if (strpos($opt, ':') !== false) {
		$opt = str_replace(':', '', $opt);
		if (!isset($arguments[$opt])) {

			if (in_array($opt, ['track-service', 'docker-bundle']) && isset($arguments['no-track'])) {
				continue;
			}
			
			fputs(STDERR, "Please, pass value for option --" . $opt . PHP_EOL);
			exit(2);
		}
	}
}

$url = $arguments['url'];
$dockerBundle = $arguments['docker-bundle'] ?? null;
$trackService = $arguments['track-service'] ?? null;
$iterations = $arguments['iterations'] ?? 5;
$trackIterations = $arguments['track-iterations'] ?? 30;
$iterations = (int) $iterations;
if ($iterations > 100) {
	$iterations = 10;
} else if ($iterations <= 0) {
	$iterations = 5;
}

$i = 0;
$timeout = 15;
$n = (int) $trackIterations;
$isApplicationUp = false;

echo "Tele2 Online Retail Crawler @ v1.0.0\n\nConfiguration: \n";

if (!isset($arguments['no-track'])) {
	printf(
		" - Docker service check timeout: %s\n - Docker service check attempts: %s\n - Docker service: %s\n - Track iterations: %s\n",
		$timeout,
		$n,
		$trackService,
		$trackIterations
	);
}


printf(
	" - Crawler iterations: %s\n",
	$iterations
);

if (!isset($arguments['no-track'])) {
	while ($i <= $n) {
		sleep($timeout);
		$isApplicationUp = isTrackServiceFinished($trackService, $dockerBundle);
		
		if ($isApplicationUp) {
			break;
		}
		$i++;
	}
	
	if (!$isApplicationUp) {
		fputs(STDERR, 'Could not check service, cammand returned non zero status code or time out');
		exit(3);
	}
}



function isTrackServiceFinished($trackService, $dockerBundle)
{
	//  Check tuner service
	$result = exec(sprintf(
		"%s docker service ps %s --format \"{{.Name}} {{.DesiredState}} {{.Error}}\"",
		$dockerBundle,
		$trackService
	), $output, $status);
	
	if ($status !== 0) {
		return false;
	}
	
	$line = array_shift($output);
	@list($service, $state, $error) = explode(' ', $line);
	
	if ($state == 'Shutdown' && empty($error)) {
		return true;
	}
	
	return false;
}

$ctx = [
	"ssl" => [
		"verify_peer"=>false,
		"verify_peer_name"=>false,
	]
];

echo "\nFetch page with url: $url\n";
$ctx = stream_context_create($ctx);
$catalogPage = @file_get_contents($url, false, $ctx);

if (false === $catalogPage) {
	echo "Could not fetch page, exiting with code 1\n";
	exit(1);
}

echo "Done\n";

echo "Create cache for catalog page\n";
for ($i = 0; $i <= $iterations; ++$i) {
    @file_get_contents($url, false, $ctx);
}
echo "Done\n";

$pattern = sprintf(
	"#href=\"(%s.[^\"]*)#",
	addcslashes($url, '.')
);


function createCurlHandler(string $url) {
	$ch = curl_init($url);
	
	curl_setopt($ch,CURLOPT_FOLLOWLOCATION, 1);
	curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, 1);
	curl_setopt($ch,CURLOPT_RETURNTRANSFER, 1);
	
	return $ch;
}

function makeRequest(string $url) {
	$ch = createCurlHandler($url);
	
	$result = curl_exec($ch);
	curl_close($ch);
}

$productUrls = [];
$productUrlContents = [];

if (preg_match_all($pattern, $catalogPage, $m)) {
	
	echo "Found product links, " . count($m[1]) . "\n";
	foreach ($m[1] as $url) {
		if (strpos($url, 'text/javascript') !== false) {
			continue;
		}
		$productUrls[] = $url;
	}
	
	if (!empty($productUrls)) {
		for ($i = 0; $i <= $iterations; ++$i) {
			fputs(STDOUT, "Iteration: $i\n");
			
			$mh = curl_multi_init();
			$active = null;
			$handlers = [];
			
			foreach ($productUrls as $url) {
				$info = [
					'ch' => null,
					'url' => $url
				];
				$info['ch'] = $mhc = createCurlHandler($url);
				$handlers[(int) $mhc] = $info;
				curl_multi_add_handle($mh, $mhc);
			}
			
			do {
				$mrc = curl_multi_exec($mh, $active);
			} while ($mrc === CURLM_CALL_MULTI_PERFORM || $active);
			
			foreach ($handlers as $handler) {
				$res = curl_multi_getcontent($handler['ch']);
				if (!isset($productUrlContents[$handler['url']])) {
                    $productUrlContents[$handler['url']] = $res;
                }
				fputs(STDOUT, "Got response from: {$handler['url']}\n");
			}
			
			foreach ($handlers as $handler) {
				curl_multi_remove_handle($mh, $handler['ch']);
				curl_close($handler['ch']);
			}
			curl_multi_close($mh);
			//usleep(500000);
            sleep(2);
		}
	}

	echo "Product pages successfully processed!\n";
}

echo "Wurm up cache for images and static content\n";

$staticPattern = sprintf(
    "#(href|src)=\"(%s.[^\"]*)#",
    addcslashes("http://m2cdn.(int|dev|prf|uat).tele2.nl", '.')
);

$staticContentUrls = [];

echo "Fetch catalog page statics\n";
if (preg_match_all($staticPattern, $catalogPage, $m)) {
    if (isset($m[2])) {
        $staticContentUrls = array_merge($staticContentUrls, $m[2]);
        echo "Found " . count($m[2]) . " urls\n";
    }
}
echo "Done\n";

$staticPattern = sprintf(
    "#(href|src)=\"(%s.[^\"]*)#",
    addcslashes("https://m2cdn(.(dev|int|prf|uat))?.tele2.nl", '.')
);

echo "Fetch catalog page statics\n";
if (preg_match_all($staticPattern, $catalogPage, $m)) {
    if (isset($m[2])) {
        $staticContentUrls = array_merge($staticContentUrls, $m[2]);
        echo "Found " . count($m[2]) . " urls\n";
    }
}
echo "Done\n";

if (!empty($productUrls)) {
    echo "Fetch static for each product page\n";

    foreach ($productUrls as $productUrl) {
        if (isset($productUrlContents[$productUrl])) {
            if (preg_match_all($staticPattern, $productUrlContents[$productUrl], $m)) {
                if (isset($m[2])) {
                    $staticContentUrls = array_merge($staticContentUrls, $m[2]);
                }
            }
        }
    }

    echo "Done\n";
}

echo "Start request process\n";
if (!empty($staticContentUrls)) {
    $staticContentUrls = array_unique($staticContentUrls);
    echo "Found " . count($staticContentUrls) . " urls for static resources\n";
    for ($i = 0; $i <= $iterations; ++$i) {
        fputs(STDOUT, "Iteration: $i\n");

        $mh = curl_multi_init();
        $active = null;
        $handlers = [];

        foreach ($staticContentUrls as $url) {
            $info = [
                'ch' => null,
                'url' => $url
            ];
            $info['ch'] = $mhc = createCurlHandler($url);
            $handlers[(int) $mhc] = $info;
            curl_multi_add_handle($mh, $mhc);
        }

        do {
            $mrc = curl_multi_exec($mh, $active);
        } while ($mrc === CURLM_CALL_MULTI_PERFORM || $active);

        foreach ($handlers as $handler) {
            $res = curl_multi_getcontent($handler['ch']);

            fputs(STDOUT, "Got response from: {$handler['url']}\n");
        }

        foreach ($handlers as $handler) {
            curl_multi_remove_handle($mh, $handler['ch']);
            curl_close($handler['ch']);
        }
        curl_multi_close($mh);
        sleep(2);
    }
}

echo "Done\n";
