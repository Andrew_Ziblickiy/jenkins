FROM jenkins/jenkins:lts

USER root
RUN apt-get update && \
    apt-get -y install vim \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg2 \
	g++ \
	build-essential \
        software-properties-common && \
    curl -fsSL https://download.docker.com/linux/$(. /etc/os-release; echo "$ID")/gpg > /tmp/dkey; apt-key add /tmp/dkey && \
    add-apt-repository \
       "deb [arch=amd64] https://download.docker.com/linux/$(. /etc/os-release; echo "$ID") \
        $(lsb_release -cs) \
        stable" && \
    apt-get update && \
    apt-get -y install docker-ce

RUN apt-get -y install net-tools procps php7.0 php7.0-cgi php7.0-curl php7.0-dom
RUN usermod -a -G docker jenkins
# Comment if you do not need tele2
RUN curl -o /usr/local/share/ca-certificates/Tele2RootCa.crt http://pki.tele2.com/aia/PEM/Tele2-root-CA_Tele2RootCA01.crt
RUN update-ca-certificates
RUN "$JAVA_HOME"/jre/bin/keytool -import -trustcacerts -keystore /etc/ssl/certs/java/cacerts -storepass changeit -noprompt -alias Tele2CaRoot -file /usr/local/share/ca-certificates/Tele2RootCa.crt

#USER jenkins
RUN git config --global http.sslVerify false

